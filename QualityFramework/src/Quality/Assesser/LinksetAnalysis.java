package Quality.Assesser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Utilities.RDFAccess;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;

public class LinksetAnalysis {
	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.LinksetAnalysis.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub




		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration conf = ctx.getConfiguration();
		conf.getLoggerConfig(Benchmarking.CentralityMergeDataset.class.getName()).setLevel(Level.DEBUG);
		ctx.updateLoggers(conf);


		// create Options object
		Options options = new Options();
		options.addOption("subjUriSpace",true, "uriSpace to the subject dataset");
		options.addOption("objUriSpace",true, "uriSpace to the object dataset");
		options.addOption("linksetPath",true, "Path to the linkset");
		// added for impact  linkset
		options.addOption("linkingProperty",true, "Property_URI  indicates the  URI of properties that should be considered working out the linkset impact, if no prop are specified all the properties in the datasets are considered.");
		options.addOption("out",true, "file csv  containing linkset analysis results" );

		CSVWriter writeroutanalysis=null;
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		Map<String,String> ns=null;		
		String[] CSVHeader = new String[8];
		String[] value= new String[8];

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		HashMap<String, Double> weightForCentrality;
		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "LinksetAnalysis", options );
				System.exit(0);
			}


			//			other check on options that we could want 
			if (!cmd.hasOption("subjUriSpace") ||  !cmd.hasOption("objUriSpace")|| 
								!cmd.hasOption("out")	||
					!cmd.hasOption("linksetPath") ||!cmd.hasOption("linkingProperty")
					)  {
				formatter.printHelp( "DataseAnalysis", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}





			String subjUriSpace= cmd.getOptionValue("subjUriSpace");
			String objUriSpace= cmd.getOptionValue("objUriSpace");
			String linksetPath= cmd.getOptionValue("linksetPath");
			String linkingProperty= cmd.getOptionValue("linkingProperty");
			//String testName= cmd.getOptionValue("testname");
			String outanalysis=cmd.getOptionValue("out");


			double link_res=-1.0;




			if (cmd.hasOption("out")){
				outanalysis=cmd.getOptionValue("out");
			
				writeroutanalysis = new CSVWriter(new FileWriter(outanalysis), '\t');



				link_res=LinksetAnalysis.analyseLinkset(subjUriSpace,objUriSpace,linksetPath,
						linkingProperty);




				CSVHeader[0]= "TestName";
				CSVHeader[1]= "linksetName";
				CSVHeader[2]= "TotalNumberofLinks"; 			
				writeroutanalysis.writeNext(CSVHeader);
				
				
				value[0]=outanalysis.substring(outanalysis.lastIndexOf("/")+1, outanalysis.indexOf("_", outanalysis.lastIndexOf("/")+1));
				value[1]=linksetPath.substring(linksetPath.lastIndexOf("/")+1);
				value[2]=myFormatter.format(link_res);
				writeroutanalysis.writeNext(value);

				writeroutanalysis.close();

				System.out.print(" CSV containing the results has been writte into "+ outanalysis );
				
			}
				


			}catch ( Exception e) {
				e.printStackTrace();
			} 
		}










			private static double analyseLinkset(String subjUS, String objUS, String linksetP,
					String linkingP) 
			{

				int offset= 0, limit=1000;

				ResultSetRewindable resP;
				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
				HashSet <String>  conceptsIN=new HashSet<String>();
				Model linksetm = org.apache.jena.riot.RDFDataMgr.loadModel(linksetP);
				String yuri;
				double dslinksetNum=-1.0;

				String queryString=" select distinct ?y where { ?x <"+linkingP+"> ?y. " +
						"FILTER ( regex(str(?x),\"^"+subjUS+"\",\"i\") )\n "+
						"FILTER ( regex(str(?y), \"^"+objUS+"\",\"i\") )}" +
						"order by ?y ";

				ResultSetRewindable resSetOfLinks;
				//list of query result 
				QuerySolution qsol;
				ArrayList<String> ls=new ArrayList<String>();
				String q1;

				do{

					q1= queryString+ 
							"LIMIT   "+limit +"\n"+
							"OFFSET  "+offset;


					SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
					//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
					resSetOfLinks=ResultSetFactory.makeRewindable( RDFAccess.execQuery(linksetm, q1, err, null, true));

					resSetOfLinks.reset();



					listresP.add(resSetOfLinks);
					offset=offset+limit;
				}while (resSetOfLinks.hasNext());




				for(ResultSet resP2:listresP)
				{
					while(resP2.hasNext()) {

						qsol=resP2.next();
						yuri=qsol.getResource("?y").getURI().trim();
						conceptsIN.add(yuri);
					}
				}




				return conceptsIN.size();			



			}









			private static boolean isInSkosPropertySet(String p) {

				return ( p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#narrower") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#related") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#broader"));
			}

			/**
			 * It reads from a CSV file the relation's weights 
			 * @param filePath
			 * @return
			 * @throws IOException 
			 */
			private static HashMap<String,Double> readWeight(String filePath) throws IOException {
				CSVReader reader = new CSVReader(new FileReader(filePath));
				String [] nextLine;
				HashMap <String, Double> res = new  HashMap <String, Double> ();
				while ((nextLine = reader.readNext()) != null) {
					// nextLine[] is an array of values from the line
					res.put(nextLine[0],  Double.valueOf(nextLine[1]));
				}
				return res;
			}


		}
