package Quality.Assesser.LinksetImpactingStructure;

public class RelationAdjacentConcept {
	protected String relation=new String(); 
	protected String object=new String();
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	
	public RelationAdjacentConcept(String r, String o)
	{
		this.relation=r;
		this.object=o;
	}
	
	
	public boolean equals(Object e)
	{
		return (this.relation.equals(((RelationAdjacentConcept)e).relation.trim()) &&		
				this.object.equals(((RelationAdjacentConcept)e).object.trim()));
		
		}
	
	
}
