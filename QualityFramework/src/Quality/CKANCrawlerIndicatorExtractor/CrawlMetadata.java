package Quality.CKANCrawlerIndicatorExtractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.management.remote.SubjectDelegationPermission;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.DataSetLinksetState;
import Quality.SparqlEndPointErrorMessage;
import Quality.sparqlEndPointInfo;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.tdb.TDBFactory;

public class  CrawlMetadata {
	final  static String CKANLODGroup ="http://thedatahub.org/api/rest/group/lodcloud";

	//API  to query the CKAN metadata of a lod dataset, used to figure out if a VOID is associate to each dataset  
	final static String CKANRestDatasetJson="http://thedatahub.org/api/rest/dataset/";

	// the sub group in the LOD we are about to consider
	static String groupName="";

	// first part of the datasets uri to be used in generating the list of  dataset URIs in lod		
	final static String CKANDatasetFirstPart="http://thedatahub.org/dataset/";

	//  group of lod cloud and bibliographic
	//http://thedatahub.org/dataset?groups=lodcloud&q=&groups=bibliographic

	// creating a logger 

	public static Logger LOGGER =
		Logger.getLogger(CrawlMetadata.class.getName());
	static private FileHandler fileTxt;
	static private SimpleFormatter formatterTxt;
	static private com.hp.hpl.jena.query.Dataset dataset;
	static private Model modD;
	//	static private String sparqlEndPointTimeOut;


	final static String lns="http://localspace/";
	final static String rdfns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	final static String rdfsns="http://www.w3.org/2000/01/rdf-schema#";
	final static String voidns="http://rdfs.org/ns/void#";
	final static String dctermsns="http://purl.org/dc/terms/";
	final static String foafns="http://xmlns.com/foaf/0.1/";

	static Resource qcCrawlingC;
	static Resource qcDatasetC;
	static Resource qcCKANDescriptionC;
	static Resource qcVOIDDescriptionC;


	// RDF Property   
	static Property hasCrawledDatasetP;
	static Property rdfTypeP;
	static Property hasCrawlingDateP;
	static Property hasVOIDDescriptionP;
	static Property hasDATAHUBDescriptionP;
	static Property hasGroupP;
	static Property isLinkedToP;
	static Property hasURLP;

	// let have the reference at the VOID properties that we need
	static Property voidsparqlEndpointP;
	static Property foafhomePageP;
	static Property dctermstitleP;
	static Property voiduriSpaceP;

	// VOID class
	static Resource voidLinksetC;
	static Property voidTargeP;
	static Property voidSubjectTargetP;
	static Property voidObjectTargetP;
	static Property voidClassPartitionP;
	static Property voidClassP;
	static Property voidEntitiesP;

	static DataSetLinksetState state=new DataSetLinksetState();

	private static CommandLine cmd;

	private static boolean generateListDatasets =false;

	private static HashSet<String> blackListOfSparqlEndPoint;



	static public String getContents(InputStream in) throws IOException {
		// InputStream in = /* your InputStream */;
		InputStreamReader is = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(is);
		String read="", acc="";
		try {
			read = br.readLine();


			while(read != null) {
				//System.out.println(read);
				acc =acc+read+"\n";
				read = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("Problem reading input "+ in.toString() );
			e.printStackTrace();

		}
		return acc;
	}

	public static HashSet<String> getSparqlEndPointBlackList( String fileName) {
		HashSet<String> bl=new HashSet<String>();
		File name = new File(fileName);
		if (name.isFile()) {
			try {
				BufferedReader input = new BufferedReader(new FileReader(name));
				StringBuffer buffer = new StringBuffer();
				String text;
				while ((text = input.readLine()) != null){
					text=text.trim();
					if (!text.equals("")) bl.add(text.trim());
				}
				input.close();
			} catch (IOException ioException) {
				System.out.println(ioException.getMessage());
			}
		}
		return bl;
	}

	/**
	 * It tries to query the sparqlEndPoint using the uriSpace, 
	 * if no entity are returned it tries to guess an alternative uriSpace
	 * if it is successfully guessing it returns a consolidated uriSpace, otherwise throws an exception   
	 * @param uriSpace
	 * @param sparqlEndpoint
	 * @param timeout 
	 * @return
	 * @throws Exception
	 */
	private static String Consolidate( String uriSpace, String sparqlEndpoint) throws  UriSpaceException{

		String comm= uriSpace;
		String queryTemplate ="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
		"select * where { ?x ?p ?y. Filter(regex(STR(?x), \"^URISPACE\")) } LIMIT 1 ";

		int last=0;
		//let try to query
		boolean validated=false;
		int c= sparqlEndpoint.indexOf(":"); // often in  port is indicated in the uri of the sparql end point, you have it also in the uri space
		do {
			String replacedQuery=queryTemplate.replace("URISPACE", comm);
			com.hp.hpl.jena.query.Query query = QueryFactory.create(replacedQuery);		
			QueryExecution exec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
			//			QueryEngineHTTP exec=(QueryEngineHTTP) QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
			//			if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);

			ResultSet	result = exec.execSelect();
			if (!result.hasNext()){

				if (c>-4) { 
					String firstPart=	sparqlEndpoint.substring(0, sparqlEndpoint.indexOf("/", c));
					comm=firstPart+comm.substring(comm.indexOf("/", 7));
					c=-4;

				} else{
					// we have to figure out which could be the right uriSpace 
					// we start reducing the initial one deleting the last XXX/  in http://.../... /XXX/
					if (comm.endsWith("/")){
						comm=comm.substring(0, comm.length()-1);
					}
					last=comm.lastIndexOf("/");
					// http:// 7 chars
					if (last>7 ) comm=comm.substring(0,last);
					else { 
						// replace the uri space so that we receive some info... 
						String replacedQuery2 ="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
						"select * where { ?x ?p ?y } LIMIT 100 ";
						com.hp.hpl.jena.query.Query query1 = QueryFactory.create(replacedQuery2);		
						exec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query1);
						//if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);

						result = exec.execSelect();	
						if (!result.hasNext()){ 
							//LOGGER.warning("the "+ sparqlEndpoint +" endpoint does not return data");
							throw new  UriSpaceException ("the "+ sparqlEndpoint +" endpoint does not return data");
						}
						else{
							//LOGGER.warning(uriSpace +" is not a valid Uri Space for " + sparqlEndpoint +" and it is not possible to guess an alternative");
							throw new UriSpaceException( uriSpace +" is not a valid Uri Space for " + sparqlEndpoint +" and it is not possible to guess an alternative");
						}
					}
				}
			}
			else validated=true;
			exec.close();

		} while (!validated );
		return comm;

	}

	/** 
	 * It attaches to the entities instances of qc:dataset the info pertaining to the access, namely the void:sparqlEndpoint and void:uriSpace 
	 * 
	 */
	private static void buildDatasetAccessVOID(Dataset dataSet) throws IOException{

		InputStream ip= CrawlMetadata.class.getResourceAsStream("../../queryForGettingAccessToSparqlEndPoint/ConstructSparqlEndPointForDataset.sparql");
		String queryString = getContents(ip);
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;


		QueryExecution qexec = QueryExecutionFactory.create(query, dataSet) ;
		Model   modD = dataSet.getDefaultModel(); 

		modD.add( qexec.execConstruct()) ;

		qexec.close() ;
		modD.close();

	}

	/**
	 * It queries an sparql end point with a Construct and return a model containing the  construct result 
	 * @param endpoint_service
	 * @param queryString
	 * @return 
	 */
	public static Model execConstructQueryingASparqlEndPoint(String endpoint_service, String queryString){
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint_service, query);
		//		QueryEngineHTTP exec = (QueryEngineHTTP) QueryExecutionFactory.sparqlService(endpoint_service, query);
		//		if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);
		//	
		Model answer=null;
		try{
			answer = exec.execConstruct();
			//defaultModel.add(answer) ;
		} catch (Exception e){
			LOGGER.info("problems quering "+  endpoint_service+ " by contruct: " + e.getMessage());

		}finally{
			exec.close();
		}
		return  answer;


	}

	public static ResultSet execQueryingASparqlEndPoint(String endpoint_service, String queryString, SparqlEndPointErrorMessage err){

		LOGGER.fine("quering "+ endpoint_service +" by " +queryString);
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint_service, query);
		//		QueryEngineHTTP exec = (QueryEngineHTTP) QueryExecutionFactory.sparqlService(endpoint_service, query);
		//if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);
		//exec.setTimeout(5000); //not yet implemented in Jena  
		ResultSet res=null;
		//Context c=exec.getContext();
		if (CrawlMetadata.blackListOfSparqlEndPoint==null || !CrawlMetadata.blackListOfSparqlEndPoint.contains(endpoint_service)) {

			try{
				res= exec.execSelect();
				//defaultModel.add(answer) ;
				err.responseCode=200;
			} catch (QueryExceptionHTTP e){
				err.responseCode= e.getResponseCode();
				err.sparqlAccessProblem+= "\n"+e.getMessage();
				LOGGER.info("problems quering"+  endpoint_service+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());

			}
		} else {
			err.responseCode= -1;
			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
		}

		return  res;


	}
	private static void crawlingDataHub(String cDate) throws IOException {
		final String any23Service="http://any23.org/rdfxml/";

		//get the list of datasets from CKAN
		JSONConnectionQuery ckanGroup= new JSONConnectionQuery(CKANLODGroup);
		JSONConnectionQuery ckanDataset=null;


		LOGGER.fine("Starting the CRAWLING of DATAHUB");
		// Creating the cr entity which represent a crawling 
		String crName="cr"+cDate;

		// cr resource lns:crXXXX-XX-XX
		// lns:crXXXX-XX-XX qc:hasCrawlingDate XXXX-XX-XX
		Resource cr=modD.createResource(lns+"cr"+cDate).addProperty(hasCrawlingDateP,
				cDate, "xsd:date");//(QC.HasCrawlingDate,cDate);


		JSONObject json;

		try {
			json = ckanGroup.getJSONResultFor();
			JSONArray a=json.getJSONArray("packages");

			if (a.length()>0){
				for (int i=0; i< a.length();i++){
					ckanDataset=new JSONConnectionQuery(CKANRestDatasetJson+a.getString(i));
					// jsonDataset is the metadata in JSON returned by dataHub
					JSONObject jsonDataset = ckanDataset.getJSONResultFor();


					JSONArray ag = jsonDataset.getJSONArray("groups");
					LOGGER.fine("Starting analyizing the list of LOD dataset ");
					for (int ii=0; ii< ag.length();ii++){

						String groupNameO=ag.getString(ii);
						// lets create a  instance of QC:dataset
						String datasetName=jsonDataset.getString("name") +crName;
						//lns-DSNamecrXXXX-XX-XX
						Resource qcdataset=modD.createResource(lns+datasetName);

						// states its group
						//lns-DSNamecrXXXX-XX-XX group
						qcdataset.addLiteral(hasGroupP, groupNameO);


						// if dataset stays in the right group add to result
						//if (groupNameO.equals(groupName)) 

						// add the type of the resource
						//lns:DSNamecrXXXX-XX-XX rdf:type qc:Dataset
						modD.add(qcdataset, rdfTypeP,  qcDatasetC);
						modD.add(qcdataset, dctermstitleP, jsonDataset.getString("name"));
						modD.add(qcdataset, foafhomePageP, modD.createResource(jsonDataset.getString("url")));

						// add info about Ckan Description
						// lns:namecrXXXX-XX-XXDataHub
						String DatasetCKANDescriptorName=lns+datasetName+"DataHub";
						Resource qcCKANDesc= modD.createResource(DatasetCKANDescriptorName);

						modD.add(qcdataset,hasDATAHUBDescriptionP, qcCKANDesc );
						modD.add(qcCKANDesc, rdfTypeP, QC.ns+QC.CKANDescription );
						// add the uri at the description
						modD.add(qcCKANDesc, hasURLP,CKANRestDatasetJson+a.getString(i));


						// prepare the resource for a VOID description 
						String datasetVOIDDescriptorName=lns+datasetName+"VOID";
						Resource qcVOIDDes=null;


						// cr is crawling dataset
						cr.addProperty(hasCrawledDatasetP,qcdataset);
						LOGGER.fine("creating the  CKAN decription for " + datasetName );
						Model modqcCKANDesc = dataset.getNamedModel(DatasetCKANDescriptorName);
						modqcCKANDesc.add(qcdataset, rdfTypeP, voidns+"Dataset" );
						//			modCKAN.add(datasetResource, dctermstitleP, jsonDataset.getString("name"));
						modqcCKANDesc.add(qcdataset, foafhomePageP, modD.createResource(jsonDataset.getString("url")));
						try{

							modqcCKANDesc.add(qcdataset, voiduriSpaceP, jsonDataset.getJSONObject("extras").getString("namespace"));
						} catch ( Exception e){
							LOGGER.fine("no void uriSpace for  CKAN decription for " + datasetName );
							System.out.println(e.getMessage());
						} 


						// how can we access the dataset
						JSONArray res=jsonDataset.getJSONArray("resources");
						JSONObject oInRes;
						String url="";
						boolean undefinedSparqlEndpoint=true;
						for (int iii=0; iii<res.length(); iii++){
							oInRes= res.getJSONObject(iii);
							if (oInRes.getString("format").equals("api/sparql")) {
								url=oInRes.getString("url");
								// let add info about where is the SparqlEndpoit	
								modqcCKANDesc.add(qcdataset, voidsparqlEndpointP, modD.createResource(url));
								undefinedSparqlEndpoint=false;

							}else if (oInRes.getString("format").equals("meta/void")){
								LOGGER.fine("creating the VOID decription for " + datasetName );
								// we should add a VOID description to the dataset
								url=oInRes.getString("url");
								qcVOIDDes= modD.createResource(datasetVOIDDescriptorName);
								modD.add(qcdataset,  hasVOIDDescriptionP, qcVOIDDes);
								modD.add(qcVOIDDes, rdfTypeP, QC.ns+QC.VOIDDescription );	
								// let's add at the description even the void's url
								modD.add(qcVOIDDes,  hasURLP, url);

								if (cmd.hasOption("void")){
									// lets store the VOID inf in the graph "datasetVOIDDescriptorName"
									Model modqcVOIDDescr= dataset.getNamedModel(datasetVOIDDescriptorName);
									LOGGER.fine("sending the Void url "+url+" to any23service at " + any23Service +" "+ any23Service+url);
									//System.out.println("sending the Void url "+url+" to any23service at " + any23Service +" "+ any23Service+url);
									try{
										modqcVOIDDescr.read(any23Service+url);
									} catch (Exception e) {
										LOGGER.warning("problems downloading the void file "+url+" and processing by any23service " + any23Service +" "+ any23Service+url);
										System.out.println(e.fillInStackTrace());
									}
								}

							}
							// we can also try to figure out possible dump.. 
							if (undefinedSparqlEndpoint) LOGGER.fine(" no sparqlEndPoint is found in datahub for "+ datasetName);
						}


						// which are the interlinks
						try{
							JSONObject ex=jsonDataset.getJSONObject("extras");

							String [] names= JSONObject.getNames(ex);
							for(int iii=0; iii< names.length; iii++){
								if (names[iii].contains("links:")){
									String linkedDatasetName= names[iii].substring(6);
									int num=-1;
									try{
										num=ex.getInt("links:"+linkedDatasetName);
									} catch (JSONException e){

									}
									LOGGER.fine(" we have found that "+datasetName+" is linked to "+linkedDatasetName+crName+ " by "+ num );
									Resource datasetResourceD=modD.createResource(lns+linkedDatasetName+crName);
									modqcCKANDesc.add(qcdataset, isLinkedToP, datasetResourceD);
								}
							}
						}catch (Exception e){
							System.out.println(e.getStackTrace());
						}

						//}

						break;
					}
				}
			}
		} catch (JSONException e) {
			LOGGER.warning( "problems " + e.getMessage());

		}
		LOGGER.fine(" Building the dataset descriptions quering their local metadata representations");
		buildDatasetAccessVOID(dataset);


	}



	private static   void isAnOwlSameAsLinkset(LinksetInfo ls) throws UnretrievableTypesException {
		DatasetInfo subjectdt= state.mapOfDataset.get(ls.subjectLocalName);
		DatasetInfo objectdt= state.mapOfDataset.get(ls.objectLocalName);

		// does the object dataset have a valid uriSpace?
		if ( objectdt.consolidatedUriSpace ==null) 
			if(objectdt.UriSpace !=null) objectdt.consolidatedUriSpace=objectdt.UriSpace;
			else throw new  UnretrievableTypesException("No uriSpace and consolidated uriSpace  so I won't retrieve the object's types for linkset ");

		InputStream ip;
		String queryString ;
		//	int responseCode=0;

		// query to text which kind of linkset property is supported
		queryString=" select distinct *  WHERE"+
		"{"+ 
		"?y <http://www.w3.org/2002/07/owl#sameAs> ?z. Filter( regex(str(?z), \"^OBJECTURISPACE\"))"+
		"}Limit 1";
		queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace);
		SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();

		ResultSet res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString,err );
		ls.subjLastResponseCode=err.responseCode;
		ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

		if( !res.hasNext()) { // probably no sameas

			// query to text which kind of linkset property is supported
			queryString=" select distinct *  WHERE"+
			"{"+ 
			"?y <http://www.w3.org/2002/07/owl#sameAs> ?z."+
			"}Limit 1";

			res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
			ls.subjLastResponseCode=err.responseCode;
			ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
			if( !res.hasNext()) { // its not  a owl:sameAs linkset
				throw new  UnretrievableTypesException("NO owl:sameAs links ");
			} else throw new  UnretrievableTypesException("UNSUPPORTED filter in sparql queries ");
		}	



	}

	private static void getSubjectTypesForLinkset(LinksetInfo ls) throws UnretrievableTypesException, IOException{


		DatasetInfo subjectdt= state.mapOfDataset.get(ls.subjectLocalName);
		DatasetInfo objectdt= state.mapOfDataset.get(ls.objectLocalName);
		//
		//		// does the object dataset have a valid uriSpace?
		//		//		if ( objectdt.consolidatedUriSpace ==null) 
		//		//			if(objectdt.UriSpace !=null) objectdt.consolidatedUriSpace=objectdt.UriSpace;
		//		//			else throw new  UnretrievableTypesException("No uriSpace and consolidated uriSpace  so I won't retrieve the object's types for linkset ");
		//		//		if ( !objectdt.typeIndicatorsRetrieved) 
		//		//			throw new  UnretrievableTypesException("Type for Object dataset are not retrived so I won't retrieve the object's types for linkset ");
		//		//		if ( !subjectdt.typeIndicatorsRetrieved) 
		//		//			throw new  UnretrievableTypesException("Type for Suject dataset are not retrived so I won't retrieve the subject's types for linkset ");
		//
		//
		InputStream ip;
		String queryString ;
		//	int responseCode=0;
		//
		//		// query to text which kind of linkset property is supported
		//		queryString=" select distinct *  WHERE"+
		//		"{"+ 
		//		"?y <http://www.w3.org/2002/07/owl#sameAs> ?z. Filter( regex(str(?z), \"^OBJECTURISPACE\"))"+
		//		"}Limit 1";
		//		queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace);
		//		SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
		//
		//		ResultSet res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString,err );
		//		ls.subjLastResponseCode=err.responseCode;
		//		ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
		//
		//		if( !res.hasNext()) { // probably no sameas
		//
		//			// query to text which kind of linkset property is supported
		//			queryString=" select distinct *  WHERE"+
		//			"{"+ 
		//			"?y <http://www.w3.org/2002/07/owl#sameAs> ?z."+
		//			"}Limit 1";
		//
		//			res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
		//			ls.subjLastResponseCode=err.responseCode;
		//			ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
		//			if( !res.hasNext()) { // its not  a owl:sameAs linkset
		//				throw new  UnretrievableTypesException("NO owl:sameAs links ");
		//			} else throw new  UnretrievableTypesException("UNSUPPORTED filter in sparql queries ");
		//		}	
		isAnOwlSameAsLinkset(ls); 

		SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
		//

		//we don't even try the construct for linkset type 
		// let start from what was the second attempts  in the case of dataset type: Query with  COUNT, GROUP and FILTER

		ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/LSSUB2TypeAndNumOfOccurence.sparql");

		queryString = getContents(ip);

		queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace);

		// second attempt to query  
		LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
		ResultSet res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
		ls.subjLastResponseCode=err.responseCode;
		ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 
			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				ls.subjectTypes.put(type.toString(),noccurence); 
			} 
		}else { // NO GROUP
			// third attempt to query separating the query for types from queries for number of occurences 
			// we ask for Type an Then Query with  COUNT and FILTER
			ip=CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LSSUB3Type.sparql");
			queryString = getContents(ip);
			queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace);

			LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
			res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
			ls.subjLastResponseCode=err.responseCode;
			ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

			if (res!=null && res.hasNext()) {
				workOutNOccurenceSubjectLinkset(res, ls, false);		
			} else { // try with query without filters and then clean the result of all the RDF and OWL  types 
				ip=CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LSSUB4TypeNoFilter.sparql");
				queryString = getContents(ip);
				queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace);


				LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
				res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err );
				ls.subjLastResponseCode=err.responseCode;
				ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
				if (res!=null && res.hasNext()) {
					workOutNOccurenceSubjectLinkset(res, ls, true);	
				} else {// type are not retrievable .. 
					throw new UnretrievableTypesException(" no Linkset types indicators for "+ " no types indicators for "+ subjectdt.crawledDatasetUri );

				}
			}
			//TODO FIX  add the  

		}

		//}
		//TODO add the part assessing the type for object in the linkset

		//		return m;
	}




	private static void workOutNOccurenceSubjectLinkset(ResultSet res, LinksetInfo ls,
			boolean removeOWLRDFTYPE) throws IOException {


		QuerySolution sol;
		RDFNode type; 
		int nOccurence=-1; 
		int responseCode=0;

		DatasetInfo subjectdt= state.mapOfDataset.get(ls.subjectLocalName);
		DatasetInfo objectdt= state.mapOfDataset.get(ls.objectLocalName);

		boolean acceptCount=true;
		SparqlEndPointErrorMessage err= new SparqlEndPointErrorMessage();
		while (res.hasNext()){
			// for each type we ask or work out the number of occurences
			//					Resource bn =m.createResource(); 
			sol=res.next();
			type=sol.get("?type");
			ResultSet occ;
			InputStream ip;
			String queryString;
			String typeStr=type.toString();
			if (removeOWLRDFTYPE && ( 
					(typeStr.startsWith("http://www.w3.org/2000/01/rdf-schema" )) || 
					(typeStr.startsWith("http://www.w3.org/2002/07/owl#")) ||
					(typeStr.startsWith("http://www.w3.org/1999/02/22-rdf-syntax-ns"))
			)){ // discard that type
				continue;
			}


			if (acceptCount){

				// ask for occurence by COUNT 
				ip=CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LSSUB31TypeNumOfOccurence.sparql");
				queryString = (getContents(ip));
				queryString=queryString.replace("TYPE", type.toString()).replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace);


				LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);


				occ= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
				RDFNode numberRDFNode;
				ls.subjLastResponseCode=err.responseCode;
				ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

				if (occ!=null && occ.hasNext()) {
					QuerySolution solAtNumberOfTypeOccurence=occ.next(); 
					numberRDFNode=solAtNumberOfTypeOccurence.get("?nOccurence");
					nOccurence=numberRDFNode.asLiteral().getInt();  
					ls.subjectTypes.put(type.toString(),nOccurence); 
					//ls.subjetTypeQueryWhichHasWorked=3.1f;

				} else acceptCount=false;
			}

			if(!acceptCount) {
				//ask for the instances of a given type and then count them
				ip=CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LSSUB32OccurenceForAType.sparql");
				queryString = (getContents(ip));
				queryString=queryString.replace("TYPE",  type.toString()).replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace);

				LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
				occ= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
				ls.subjLastResponseCode=err.responseCode;
				ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
				// let count the number of instance for the type
				nOccurence=0;
				while (occ.hasNext()){ 
					QuerySolution solAtOccurence=occ.next(); 
					nOccurence++; 
				}
				ls.subjectTypes.put(type.toString(),nOccurence); 
				//ls.subjectTypeQueryWhichHasWorked=3.2f;
			}

		}

		//if (removeOWLRDFTYPE) ls.subjectTypeQueryWhichHasWorked++;

	}

	/**
	 * It tries different queries to figure out dataset's types and the number of occurences for every type contained in a dataset.
	 * In case of success it writes types and occurence into dt.types. 
	 * It does not consider the owl and rdf class as types.
	 * @param dt
	 * @throws UnretrievableTypesException is raised when it is not possible to determine types indicators.. 
	 * @throws Exception is raised when  more general endPoint communication problems arises 
	 */
	private static void getTypesForDataset(DatasetInfo dt) throws UnretrievableTypesException, IOException {

		//	Resource datasetAsResource = modD.getResource(dt.crawledDatasetUri);
		InputStream ip;
		String queryString ;
		//int responseCode=0;
		SparqlEndPointErrorMessage err= new SparqlEndPointErrorMessage();

		ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D2DatasetTypeAndNumOfOccurence.sparql");
		try {
			queryString = getContents(ip);

			LOGGER.fine("quering "+ dt.sparqlEndPoint +" by " +queryString);
			ResultSet res=null;
			// second attempt to query  
			try {

				res= CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, queryString, err);
				dt.lastResponseCode=err.responseCode;
				dt.lastSparqlAccessProblem=err.sparqlAccessProblem;
			} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {
				LOGGER.fine("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
				dt.lastSparqlAccessProblem+= "tries with group and count: "+e.getMessage()+ "\n";
			}
			if (res!=null) {
				QuerySolution sol;
				RDFNode type; 
				int noccurence; 
				while (res.hasNext()){
					sol=res.next();
					type=sol.get("?type");
					noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
					dt.types.put(type.toString(),noccurence); 
				} 
				dt.typeQueryWhichHasWorked=2;
			} else { // third No Group
				ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D3DatasetType.sparql");
				queryString = getContents(ip);
				LOGGER.fine("quering "+ dt.sparqlEndPoint +" by " +queryString);
				try {
					res= CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, queryString, err);
					dt.lastResponseCode=err.responseCode;
					dt.lastSparqlAccessProblem=err.sparqlAccessProblem;
				} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {

					LOGGER.fine("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
					dt.lastSparqlAccessProblem+= "no group: "+e.getMessage()+ "\n";
				}
				if (res!=null && res.hasNext()) {
					workOutNOccurence(res, dt, false);		
				} else { // try with query without filters and then clean the result of all the RDF and OWL  types 
					ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D4DatasetTypeNoFilter.sparql");
					queryString = getContents(ip);
					LOGGER.fine("quering "+ dt.sparqlEndPoint +" by " +queryString);
					try{
						res= CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, queryString, err);
						dt.lastResponseCode=err.responseCode;
						dt.lastSparqlAccessProblem=err.sparqlAccessProblem;

					} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {

						LOGGER.fine("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
						dt.lastSparqlAccessProblem+= "no filters:  "+e.getMessage()+ "\n";
					}
					if (res!=null && res.hasNext()) {
						workOutNOccurence(res, dt, true);	
					} else {// type are not retrievable .. 
						throw new UnretrievableTypesException(" no types indicators for "+ dt.crawledDatasetUri );

					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//dt.lastResponseCode=responseCode; XXXX
		if (cmd.hasOption("stateFileNameOut")){
			LOGGER.info(" writing state into " + cmd.getOptionValue("stateFileNameOut"));
			state.saveStateInJSON(cmd.getOptionValue("stateFileNameOut"));
		}

	}


	private static void workOutNOccurence(ResultSet res, DatasetInfo dt, boolean removeOWLRDFTYPE) throws IOException {

		// for each type ask for number occurence 	
		QuerySolution sol;
		RDFNode type; 
		int nOccurence=-1; 
		boolean acceptCount=true;
		SparqlEndPointErrorMessage err=new SparqlEndPointErrorMessage();
		while (res.hasNext()){
			// for each type we ask or work out the number of occurences
			//					Resource bn =m.createResource(); 
			sol=res.next();
			type=sol.get("?type");
			// only if type is not a blank node
			if (type.isURIResource()) {
				ResultSet occ=null;
				InputStream ip;
				String queryString;
				int responseCode=0;
				String typeStr=type.toString();
				if (removeOWLRDFTYPE && ( 
						(typeStr.startsWith("http://www.w3.org/2000/01/rdf-schema" )) || 
						(typeStr.startsWith("http://www.w3.org/2002/07/owl#")) ||
						(typeStr.startsWith("http://www.w3.org/1999/02/22-rdf-syntax-ns"))
				)){ // discard that type
					continue;
				}


				if (acceptCount){

					// ask for occurence by COUNT 
					ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D31TypeNumOfOccurence.sparql");
					queryString = (getContents(ip));
					queryString=queryString.replace("TYPE", type.toString());

					LOGGER.fine("quering "+ dt.sparqlEndPoint +" by " +queryString);
					try {


						occ= CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, queryString, err);
						dt.lastResponseCode=err.responseCode;
						dt.lastSparqlAccessProblem=err.sparqlAccessProblem;
					} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {

						LOGGER.fine("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
						dt.lastSparqlAccessProblem+= "\"count\" upon each type  : "+e.getMessage()+ "\n";
					}
					RDFNode numberRDFNode;

					if (occ!=null && occ.hasNext()) {
						QuerySolution solAtNumberOfTypeOccurence=occ.next(); 
						numberRDFNode=solAtNumberOfTypeOccurence.get("?nOccurence");
						nOccurence=numberRDFNode.asLiteral().getInt();  
						dt.types.put(type.toString(),nOccurence); 
						dt.typeQueryWhichHasWorked=3.1f;

					} else acceptCount=false;
				}

				if(!acceptCount) {
					//ask for the instances of a given type and then count them

					ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D32OccurenceForAType.sparql");
					queryString = (getContents(ip));
					queryString=queryString.replace("TYPE",  type.toString());

					LOGGER.fine("quering "+ dt.sparqlEndPoint +" by " +queryString);
					try {
						occ= CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, queryString, err);
						dt.lastResponseCode=err.responseCode;
						dt.lastSparqlAccessProblem=err.sparqlAccessProblem;
					} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {

						LOGGER.fine("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
						dt.lastSparqlAccessProblem+= "counting instances for type : "+e.getMessage()+ "\n";
					}
					// let count the number of instance for the type
					nOccurence=0;
					while (occ.hasNext()){ 
						QuerySolution solAtOccurence=occ.next(); 
						nOccurence++; 
					}
					dt.types.put(type.toString(),nOccurence); 
					dt.typeQueryWhichHasWorked=3.2f;
				}

			}
		}
		if (removeOWLRDFTYPE) dt.typeQueryWhichHasWorked++;

	}



	public static void buildindicators(Dataset dataset) {

		// data structure containing info for linkset
		state.linksetInfoArray=new  ArrayList <LinksetInfo> ();

		// data structure containing for each dataset a grph representing the statistic on types, or null if the sparql Endpoint was not asnwering
		HashMap <String, Model> typeInfo=new HashMap <String,Model> ();

		// list of consolidated datasets, couple <datasetLocalname, consolidated urispace> 
		HashSet <String> enconteredLinkset= new HashSet<String> (); 

		// info pertaining the dataset
		state.mapOfDataset= new HashMap<String, DatasetInfo> ();
		QuerySolution querySol;
		RDFNode in;
		Resource subjectDataset;

		String query;

		// querying the  lodal store to get the list of datasets
		if (cmd.hasOption("generateListDatasets") ){
			if (groupName!=null){

				LOGGER.fine(" Analysing the datasets in the group " + groupName);
				// let query for the resources that are in group and have sparql end point
				query= "  PREFIX qc: <http://purl.org/QualCraw/>"+
				"PREFIX void: <http://rdfs.org/ns/void#>"+ 
				"select distinct ?y   ?sparqlEndpoint ?uriSpace  where {"+
				"graph <urn:x-arq:DefaultGraph> {" +
				"?y  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/QualCraw/Dataset>." +
				"?y qc:hasGroup  \""+groupName+"\"^^<http://www.w3.org/2001/XMLSchema#string> ."+
				"Optional {?y void:sparqlEndpoint ?sparqlEndpoint}."+
				"Optional {?y  void:uriSpace ?uriSpace}" +
				"}"+	 
				"}";
			}else{
				LOGGER.fine(" Analysing ALL datasets ");
				query= "  PREFIX qc: <http://purl.org/QualCraw/>"+
				"PREFIX void: <http://rdfs.org/ns/void#>"+ 
				"select distinct ?y   ?sparqlEndpoint ?uriSpace   where {"+
				"graph <urn:x-arq:DefaultGraph> {" +
				"?y  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/QualCraw/Dataset>." +
				"Optional {?y void:sparqlEndpoint ?sparqlEndpoint}."+
				"Optional {?y  void:uriSpace ?uriSpace}" +
				"}"+	 
				"}";	
			}
			com.hp.hpl.jena.query.Query queryDataset = QueryFactory.create(query) ;
			QueryExecution qexec = QueryExecutionFactory.create(queryDataset, dataset) ;
			ResultSet qr=qexec.execSelect();
			// let figure out the set of dataset on which we have to work, which are dataset in the selected group and their linkset


			// list of object
			HashMap <String,DatasetInfo > comObject = new  HashMap <String,DatasetInfo >() ;
			DatasetInfo subjectDatasetInfo;
			while ( qr.hasNext() ){// we find out statistics about class type  for each dataset Linked to the subjectDataset
				querySol = qr.next();

				subjectDatasetInfo = new DatasetInfo();
				subjectDatasetInfo.subject=true;
				subjectDatasetInfo.crawledDatasetUri=querySol.get("?y").toString();

				//			// 
				//			subjectDatasetInfo.UriSpace=querySol.get("?uriSpace").toString();
				//			subjectDatasetInfo.sparqlEndpoint=querySol.get("?sparqlEndpoint").toString();

				subjectDataset=modD.getResource(subjectDatasetInfo.crawledDatasetUri);	 // if I ask for the resource without passing for the model by converting the RDFnode in a resource, we cannot query for properties afterwords

				String subjectUriSpace=null;

				Statement st = subjectDataset.getProperty(voiduriSpaceP);
				if (st!=null){
					RDFNode	node=st.getObject();
					if (node!=null) {// if the subject has a voidUrispace
						subjectDatasetInfo.UriSpace=node.toString();

					}
				}

				Statement st1=subjectDataset.getProperty(voidsparqlEndpointP);
				if (st1!=null){
					RDFNode	node=st1.getObject();
					if (node!=null){
						subjectDatasetInfo.sparqlEndPoint=node.toString();
					}
				}

				String datasetname=subjectDataset.getLocalName();
				if ( !blackListOfSparqlEndPoint.contains(subjectDatasetInfo.sparqlEndPoint) ){
					// if the subject has sparqlEndpoint 
					int localSubjectNameStart = subjectDatasetInfo.crawledDatasetUri.lastIndexOf("/")+1;
					String subjectLocalName=subjectDatasetInfo.crawledDatasetUri.substring(localSubjectNameStart);
					if (!state.mapOfDataset.containsKey(subjectLocalName)){ //if it is the first time we meet the detest
						//int localNameStart = subjectDatasetInfo.crawledDatasetUri.lastIndexOf("/");
						state.mapOfDataset.put(subjectLocalName,subjectDatasetInfo );
						//state.mapOfDataset.put(datasetname,subjectDatasetInfo );
					}else { // if we have already encontered the dataset it was a linked dataset of something
						DatasetInfo dt = state.mapOfDataset.get(subjectLocalName);
						dt.subject=true;
					}

					// let's  includes the subject dataset
					StmtIterator i = subjectDataset.listProperties(isLinkedToP);
					DatasetInfo objectDatasetInfo;

					while (i.hasNext()){
						objectDatasetInfo = new DatasetInfo();
						// never do that: Resource objectDataset= i.next().getObject().asResource();
						RDFNode objectNode =i.next().getObject();
						objectDatasetInfo.crawledDatasetUri=objectNode.toString();
						Resource objectDataset= modD.getResource(objectDatasetInfo.crawledDatasetUri);

						Statement stOb = objectDataset.getProperty(voiduriSpaceP);
						if (stOb!=null){ 
							// object dataset has void:uriSpace
							// find out object sparqlEndpoint
							objectDatasetInfo.UriSpace= stOb.getObject().toString();

						}	
						stOb= objectDataset.getProperty(voidsparqlEndpointP);
						if (stOb!=null){ 	
							objectDatasetInfo.sparqlEndPoint =stOb.getObject().toString();
						}

						if ( !blackListOfSparqlEndPoint.contains(objectDatasetInfo.sparqlEndPoint) ){
							//if (!enconteredDataset.contains(objectDataset.getLocalName())){
							if (!comObject.containsKey(objectDataset.getLocalName())){
								int localNameStart = objectDatasetInfo.crawledDatasetUri.lastIndexOf("/")+1;
								comObject.put(objectDatasetInfo.crawledDatasetUri.toString().substring(localNameStart),objectDatasetInfo );
								//comObject.put(objectDataset.getLocalName(),objectDatasetInfo );
								//enconteredDataset.add(objectDataset.getLocalName());
							}
							if(! enconteredLinkset.contains(subjectDataset.getLocalName()+"_"+objectDataset.getLocalName())){
								LinksetInfo comm= new LinksetInfo();
								comm.name=lns+ subjectDataset.getLocalName()+"_"+objectDataset.getLocalName();
								comm.subjectURI=subjectDataset.getURI();
								comm.objectURI=objectDataset.getURI();	
								comm.subjectLocalName=subjectDataset.getLocalName();
								comm.objectLocalName=objectDataset.getLocalName();	


								// independently if the communication with the subject dataset is successful we complete the linkset decription 
								state.linksetInfoArray.add(comm);

							}
						}
					}
				}
				Iterator <Entry<String, DatasetInfo>> entries = comObject.entrySet().iterator();
				while (entries.hasNext()) {
					Entry <String, DatasetInfo> e =entries.next();
					if (!state.mapOfDataset.containsKey(e.getKey())){
						state.mapOfDataset.put(e.getKey(), e.getValue());
					}
				}
			}
			qexec.close();
			//state.saveState("InitialiList");


			// Since we are considering linkset in datahub which are usually served by the subject sparql end point 
			// we add the linkset sparqlendpoint and  Urispace at linksets
			for (LinksetInfo ls : state.linksetInfoArray){
				DatasetInfo sj= state.mapOfDataset.get(ls.subjectLocalName);
				ls.sparqlEndPoint=sj.sparqlEndPoint;
				ls.UriSpace=	sj.UriSpace;
			}
			state.saveStateInJSON("InitialiList.json");
		}


		if (cmd.hasOption("stateFileNameIn")){
			try {
				state=state.loadStatefromJSON(cmd.getOptionValue("stateFileNameIn"));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		}
		else{
			try {
				state=state.loadStatefromJSON("InitialiList.json");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}

		// consolidating UriSpace
		Set<Entry<String, DatasetInfo>> setOfDataset;
		if (cmd.hasOption("consolidateUriSpace")) {
			//PART   intermediateUriSpaceConsolidated
			setOfDataset=state.mapOfDataset.entrySet();
			for (Entry <String, DatasetInfo> e:setOfDataset ){
				DatasetInfo dt= e.getValue();
				if (dt.UriSpace!=null && dt.sparqlEndPoint!=null){
					try {
						dt.consolidatedUriSpace=Consolidate(dt.UriSpace, dt.sparqlEndPoint);
						LOGGER.info( "SUCCESSFUL consolidation procedure the uriSpace *"+ dt.UriSpace+"* is changed into  *"+dt.consolidatedUriSpace+"* to access " +  dt.sparqlEndPoint);	
					} catch (UriSpaceException e1) {
						LOGGER.warning( "FAILED consolidation procedure for uriSpace "+ dt.UriSpace+" to access " +  dt.sparqlEndPoint +" : " + e1.toString());
						//e1.printStackTrace();
					}catch (com.hp.hpl.jena.sparql.resultset.ResultSetException e1){
						LOGGER.warning( "FAILED no result during consolidation procedure for uriSpace "+ dt.UriSpace+" to access " +  dt.sparqlEndPoint +" : " + e1.toString());
						//e1.printStackTrace();
					}
					catch ( com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP e1) {


						LOGGER.warning( "FAILED ACCESS during  consolidation procedure for uriSpace "+ dt.UriSpace+" to access " +  dt.sparqlEndPoint +" : "  +e1.toString());

						dt.lastSparqlAccessProblem +=e1.toString();
					}
					//					catch (IOException e1){
					//						LOGGER.warning(" problems reading a template query: " + e1.getMessage());
					//					}
					catch(Exception e1 ){
						LOGGER.warning( "Unexpected exception  during  consolidation procedure for uriSpace "+ dt.UriSpace+" to access " +  dt.sparqlEndPoint +" : " +e1.getClass()+"/n" +e1.toString());


					}
				}
			}


			state.state="UriSpaceConsolidated";
			state.saveStateInJSON("intermediateUriSpaceConsolidated")	;	

		} else { // if uri space are not consolidated we are going to use what has been declared in the metadata
			Collection <DatasetInfo> val= state.mapOfDataset.values();
			for (DatasetInfo x : val) {
				x.consolidatedUriSpace=x.UriSpace;
			}
		} 		


		// checking for sparql end point that are alive
		if(cmd.hasOption("checkAvailability")){
			//state.state="TypesExtractions";
			/// let figure out the type 
			setOfDataset= state.mapOfDataset.entrySet();

			for (Entry <String, DatasetInfo> e:setOfDataset ){
				DatasetInfo dt= e.getValue();
				if (dt.sparqlEndPoint!=null){
					query= "SELECT ?s WHERE {?s ?p ?o} limit 10 ";
					try{
						SparqlEndPointErrorMessage err = new SparqlEndPointErrorMessage();
						CrawlMetadata.execQueryingASparqlEndPoint(dt.sparqlEndPoint, query, err);
						dt.availabilityResponseCode=err.responseCode;
						dt.availabilitySparqlAccessProblem=err.sparqlAccessProblem;
					}catch (com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP e1) {
						LOGGER.warning("FAILED   SPARQL END POINT for "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : "  +e1.toString());				
						dt.availabilitySparqlAccessProblem+=e1.toString();
						dt.availabilityResponseCode= e1.getResponseCode();
					}
					catch (com.hp.hpl.jena.sparql.resultset.ResultSetException e1) {

						LOGGER.warning("Unexpected exception when checking Availability for Dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : " +e1.getClass()+ "\n" +e1.toString() +"\n"+ e1.getCause().getMessage() );
						dt.availabilitySparqlAccessProblem+= "\n"+e1.toString() + "\n" + e1.getCause().getMessage();
					}
					catch (Exception e1){
						LOGGER.warning("Unexpected exception when checking Availability for Dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : " +e1.getClass()+"\n" +e1.toString());
						dt.availabilitySparqlAccessProblem+= "\n" +"Unexpected exception when checking Availability for Dataset  "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : " +e1.getClass()+"\n" +e1.toString();
					}
				}
			}

			// let check the availability for the linkset sparql end point considering that in the case we are considereing they coincides with the subject datastet end points

			for (LinksetInfo ls: state.linksetInfoArray) {
				DatasetInfo subjectdt= state.mapOfDataset.get(ls.subjectLocalName);
				ls.availabilityResponseCode= subjectdt.availabilityResponseCode;
				ls.availabilitySparqlAccessProblem=subjectdt.availabilitySparqlAccessProblem;

			}

		}



		String intermediateFileName = null;
		if (cmd.hasOption("stateFileNameOut")){
			intermediateFileName= cmd.getOptionValue("stateFileNameOut").replace(".json", "intermediateDatasetType.json");
			LOGGER.info(" writing intermediate state into " + cmd.getOptionValue("stateFileNameOut").replace(".json", "intermediateDatasetType.json"));
			state.saveStateInJSON(intermediateFileName);
		}


		// check which linkset are owl:sameAs linkset
		if(cmd.hasOption("OwlSameAsOnly")){

			ArrayList <String> sameAsLinksets=  new ArrayList <String>();

			for (LinksetInfo ls: state.linksetInfoArray) {
				try{
					isAnOwlSameAsLinkset(ls);
					sameAsLinksets.add(ls.name);
				}catch (Exception e){
					// we do nothing
				}
			}
			// let's consider only linksets that are not owl:SameAs linksets

			state.saveStateOnlyForMentionedLinkset(sameAsLinksets, null);


		}




		//type extraction for datasets
		if(cmd.hasOption("getDatasetTypes")){
			state.state="TypesExtractions";
			/// let figure out the type 
			setOfDataset= state.mapOfDataset.entrySet();

			for (Entry <String, DatasetInfo> e:setOfDataset ){
				DatasetInfo dt= e.getValue();
				if (dt.availabilitySparqlAccessProblem.isEmpty()&& (dt.sparqlEndPoint!=null)&&// no communication problems in previous step
						!dt.typeIndicatorsRetrieved &&dt.types.isEmpty() // types for this dataset were not retrived in a previous attempt
				){
					try{
						//Model m=
						getTypesForDataset(dt);
						dt.typeIndicatorsRetrieved=true;
						LOGGER.info("SUCCESSFUL TYPES extraction for dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint );
					} catch (UnretrievableTypesException e1){
						LOGGER.warning("FAILED TYPES extraction for dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : "  +e1.toString());
						dt.lastSparqlAccessProblem+= "\n" + e1.getMessage();
					}
					catch ( com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP e1) {
						LOGGER.warning("FAILED  ACCESS during TYPES extraction for dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : "  +e1.toString());				
						dt.lastSparqlAccessProblem+=e1.toString();
						dt.lastResponseCode= e1.getResponseCode();
					}
					catch (IOException e1){
						LOGGER.warning(" problems reading a template query: " + e1.getMessage());
					}
					catch (Exception e1){
						LOGGER.warning("Unexpected exception  during TYPES extraction for dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : " +e1.getClass()+"/n" +e1.toString());
						dt.lastSparqlAccessProblem+= "\n" +"Unexpected exception  during TYPES extraction for dataset "+ dt.crawledDatasetUri +" SparqlEndPoint " +  dt.sparqlEndPoint +" : " +e1.getClass()+"/n" +e1.toString();
					}

					// just after each attempts to query a sparl end point we write the intermediate state so that at least partial result can be obtained 
					if (cmd.hasOption("stateFileNameOut")){
						LOGGER.info(" writing intermediate state into " + intermediateFileName);
						state.saveStateInJSON(intermediateFileName);
					}
				}
			}

		}




		if ( cmd.hasOption("getSubjectLinksetTypes")){
			//			try {
			//				state= state.loadStatefromJSON("DatasetTypeExtracted");
			//			} catch (IOException e) {
			//				e.printStackTrace();
			//			}
			state.state="LinksetSubjectTypeExtracted";
			for (LinksetInfo ls: state.linksetInfoArray ){
				try{
					getSubjectTypesForLinkset(ls);
					ls.typeIndicatorsRetrieved=true;
					LOGGER.info("SUCCESSFUL TYPES extraction for linkset "+ ls.name  );
				} catch (UnretrievableTypesException e1){
					LOGGER.warning("FAILED TYPES extraction for linkset "+ ls.name +" : "  +e1.toString());
					ls.subjSparqlAccessProblem += "\n" + e1.getMessage();

				}
				catch (IOException e1){
					LOGGER.warning(" problems reading a template query: " + e1.getMessage());
				}
				catch (Exception e1){
					LOGGER.warning("Unexpected exception  during TYPES extraction for linkset "+ ls.name +" : " +e1.getClass()+"/n" +e1.toString());
					ls.subjSparqlAccessProblem+= "\n" + e1.getMessage();
				}	
				if (cmd.hasOption("stateFileNameOut")){
					LOGGER.info(" writing intermediate state into " + intermediateFileName);
					state.saveStateInJSON(intermediateFileName);
				}


			}

			state.saveStateInJSON("LinksetSubjectTypeExtracted");

			//PART   LinksetObjectTypeExtracted
			//			try {
			//				state=state.loadStatefromJSON("DatasetObjectTypeExtracted");
			//			} catch (IOException e1) {
			//				// TODO Auto-generated catch block
			//				e1.printStackTrace();
			//			}
			//			state.state="LinksetTypeExtracted";
			//			for (LinksetInfo ls: state.linksetInfoArray ){
			//				try{
			//					getTypesForLinkset(ls);
			//					ls.typeIndicatorsRetrieved=true;
			//					LOGGER.info("SUCCESSFUL TYPES extraction for linkset "+ ls.name  );
			//				} catch (UnretrievableTypesException e1){
			//					LOGGER.warning("FAILED TYPES extraction for linkset "+ ls.name +" : "  +e1.toString());
			//				}
			//			catch (IOException e1){
			//				LOGGER.warning(" problems reading a template query: " + e1.getMessage());
			//			}
			//				catch (Exception e1){
			//					LOGGER.warning("Unexpected exception  during TYPES extraction for linkset "+ ls.name +" : " +e1.getClass()+"/n" +e1.toString());
			//				}		
			//			}
			//
			//			state.saveStateInJSON("LinksetTypeExtracted")	;
		}
		if (cmd.hasOption("stateFileNameOut")){
			LOGGER.info(" writing state into " + cmd.getOptionValue("stateFileNameOut"));
			state.saveStateInJSON(cmd.getOptionValue("stateFileNameOut"));
		}
	}



	/**
	 * @param args
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public static void main(String[] args) throws SecurityException, IOException {

		// create Options object
		Options options = new Options();

		// Options
		options.addOption("c", false, "performs crawling from a DataHUb");
		options.addOption("bl", true, "to indicate a list of sparql end points not to be considered ");
		//options.addOption("sparqlEndPointTimeOut", true, " timeout for queries to   sparqlendpoints");
		options.addOption("group", true, "to indicate a subgroup in the LOD, e.g. bibliographic which is a subgroup in datahub");
		options.addOption("void", false, "retrieves the void indicated in dataHUB and store it in the rdfstore");
		options.addOption("store", true, "directory where to put the jena rdfstore containing what it has been crawled");
		options.addOption("consolidateUriSpace", false, " tries to validate the uriSpace by querying sparqlEndPoint");
		options.addOption("checkAvailability", false, " tries to check if  sparqlEndPoint are responsive");
		options.addOption("OwlSameAsOnly", false, "filter the linkset that aren't owl:sameAs linkset ");
		options.addOption("generateListDatasets", false, "to use if after the crawling you want to generate the intial list of dataset and linkset");
		options.addOption("getDatasetTypes", false, " tries to get the dataset types  by querying sparqlEndPoint");
		options.addOption("getSubjectLinksetTypes", false, " tries to get the subject linkset types  by querying sparqlEndPoint");
		options.addOption("stateFileNameIn", true, "file name where to store info about linkset and datasets, this is usually the file that is given as input to the quality assessment ");
		options.addOption("stateFileNameOut", true, "file name where to store info about linkset and datasets, this is usually the file that is given as output to the quality assessment ");



		CommandLineParser parser = new PosixParser();

		try {
			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "CrawlMetadata", options );
				System.exit(0);
			}
			// Create Logger
			LOGGER = Logger.getLogger("");
			LOGGER.setLevel(Level.ALL);
			Date date= new Date();
			SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			String cDate1= dateFormatter1.format(date);
			fileTxt = new FileHandler("log/CrawlMetadata"+cDate1+".txt");
			formatterTxt = new SimpleFormatter();
			fileTxt.setFormatter(formatterTxt);
			LOGGER.addHandler(fileTxt);

			// let put some  info  about the crawling session in a TDB repository
			String storeDirectory  = cmd.hasOption("store") ?  cmd.getOptionValue("store"): "TDBstore";



			try{
				dataset = TDBFactory.createDataset(storeDirectory) ;
			} catch (Exception e) {
				LOGGER.severe(" Store directory does not exist");
			}
			// in the default model we write all the crawling entities and pertaining metadata

			LOGGER.fine("Created the TDB store");
			// crawling date		
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
			String cDate= dateFormatter.format(date);

			modD = dataset.getDefaultModel() ;

			// let's read the VOID schema
			modD.read(voidns);
			LOGGER.fine("Read the VOID schema and stored in the TDB store");

			// let define some properties we are going to write in the model
			// let's define  the QC vocabulary in modD		 
			qcCrawlingC= modD.createResource((String) QC.ns+QC.Crawling);
			qcDatasetC= modD.createResource((String) QC.ns+QC.Dataset);
			qcCKANDescriptionC= modD.createResource((String) QC.ns+QC.CKANDescription);
			qcVOIDDescriptionC= modD.createResource((String) QC.ns+QC.VOIDDescription);

			// RDF Property   
			hasCrawledDatasetP=modD.createProperty(QC.ns, QC.HasCrawledDataset);
			rdfTypeP=modD.createProperty(rdfns, "type");
			hasCrawlingDateP=modD.createProperty(QC.ns, QC.HasCrawlingDate);
			hasVOIDDescriptionP=modD.createProperty(QC.ns, QC.HasVOIDDescription);
			hasDATAHUBDescriptionP=modD.createProperty(QC.ns, QC.HasDATAHUBDescription);
			hasGroupP=modD.createProperty(QC.ns,QC.HasGroup);
			isLinkedToP=modD.createProperty(QC.ns, QC.IsLinkedTo);
			hasURLP=modD.createProperty(QC.ns, QC.HasURL);

			// let have the reference at the VOID properties that we need
			voidsparqlEndpointP= modD.getProperty(voidns+"sparqlEndpoint");
			foafhomePageP=modD.getProperty(foafns+"homepage");
			dctermstitleP=modD.createProperty(dctermsns+"title");
			voiduriSpaceP=modD.getProperty(voidns+"uriSpace");
			voidLinksetC=modD.getResource(voidns+"Linkset");
			voidTargeP=modD.getProperty(voidns+"target");
			voidSubjectTargetP=modD.getProperty(voidns+"subjectTarget");
			voidObjectTargetP=modD.getProperty(voidns+"objectTarget");
			voidClassPartitionP=modD.getProperty(voidns+"classPartition");
			voidClassP=modD.getProperty(voidns+ "class");
			voidEntitiesP=modD.getProperty(voidns+"entities");

			if(cmd.hasOption("c")) { // let's crawl info from the DataHub
				crawlingDataHub(cDate);
			}

			state=new DataSetLinksetState();


			groupName = cmd.getOptionValue("group");
			//			downloadVoid=cmd.hasOption("void");

			String blFile = cmd.getOptionValue("bl");
			//		// MANAGING SPARQL TIMEOUTS
			//		sparqlEndPointTimeOut=cmd.getOptionValue("sparqlEndPointTimeOut");
			//		if (sparqlEndPointTimeOut==null) sparqlEndPointTimeOut="0";
			blackListOfSparqlEndPoint= new HashSet <String>();
			generateListDatasets=cmd.hasOption("generateListDatasets");
			if(blFile != null) {
				blackListOfSparqlEndPoint= getSparqlEndPointBlackList(blFile)	;
				if (!blackListOfSparqlEndPoint.isEmpty())
					LOGGER.info(" Some sparql endpoints have been inserted in the black list! ");
			}

			buildindicators(dataset);

		} catch (ParseException e) {
			System.err.println( "Parsing failed.  Reason: " + e.getMessage() );
		}
	}



}



