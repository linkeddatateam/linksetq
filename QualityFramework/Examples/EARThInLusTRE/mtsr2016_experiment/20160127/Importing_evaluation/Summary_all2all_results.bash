set -x #echo on

#sum preflabel importing % all the thesauri
cat *2*_importing.csv_core#prefLabel_importingPercentage.csv    | uniq > Total_importing_core#prefLabel_importingPercentage.csv

#sum altlabel importing % all the thesauri
cat *2*_importing.csv_core#altLabel_importingPercentage.csv   | uniq > Total_importing_core#altLabel_importingPercentage.csv


#sum preflabel importing values all the thesauri
cat *2*_importing.csv_core#prefLabel_importingAbsolute.csv  | sort | uniq > Total_importing_core#prefLabel_importingAbsolute.csv

#sum altlabel importing all the thesauri
cat *2*_importing.csv_core#altLabel_importingAbsolute.csv   | sort | uniq > Total_importing_core#altLabel_importingAbsolute.csv

