package Benchmarking;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;



import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;

import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;


import Quality.DataSetLinksetState;
import Quality.Utilities.Debugging;
import Quality.Utilities.RDFAccess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class RandomDeletePropertyValues {
	static Model model;	
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName()); 



	public RandomDeletePropertyValues(Model model){
		//logger = Logger.getLogger(RandomDeletePropertyValues.class);
	//	BasicConfigurator.configure();
		RandomDeletePropertyValues.model=model;
		
	} 

	public static void main(String[] args) {

		// create Options object
		Options options = new Options();
		options.addOption("in", true, "rdf file path from which to delete, this file is not changed unless in=out");
		options.addOption("p", true, "properties whose values should be deleted");
		options.addOption("percentage",true, "percentage expressed as a number between 0 and 100 of values to be deleted" );
		options.addOption("out",true, "output file path" );
		options.addOption("logLevel",true, "the level for the logger" );

		//
		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		

		try {
			cmd = parser.parse( options, args);
			// connecting to the logger
		//	Debugging.loggerConfiguration(logger, cmd.hasOption("logLevel") ? cmd.getOptionValue("logLevel"):null , RandomDeletePropertyValues.class);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "RandomDeletePropertyValues", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("in"))  {
				formatter.printHelp( "RandomDeletePropertyValues", options );
				throw new Exception("in  is missing");
			}
			String in= cmd.getOptionValue("in");
			String properties[]=null;
			if (cmd.hasOption("p")) properties= cmd.getOptionValues("p");
			else { throw new Exception("p the property to cancel is missing");}
			double[] percentages;
			if (cmd.hasOption("percentage")) {
				String[] spercentage= cmd.getOptionValues("percentage");
				int percentageLength=spercentage.length;
				if (properties.length!= spercentage.length) throw new Exception(" For each property you must specify a percentage of values you want delete");
				percentages= new double[percentageLength];
				for ( int i= 0; i<spercentage.length; i++) percentages[i]= Double.parseDouble(spercentage[i]);
			}
			else { throw new Exception("p the property to cancel is missing");}

			model=org.apache.jena.riot.RDFDataMgr.loadModel(in);
			RandomDeletePropertyValues.run(  properties, percentages );

			// finally the model must with deletion must been saved as indicated
			String file=cmd.getOptionValue("out");
			RDFAccess.writeModelInTheFileNameSpecifiedEncoding(file, model);
			model.close();

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}
	private static void run(String[] properties, double[] percentages) throws IOException {
		for( int propertyNumber = 0;  propertyNumber< properties.length; propertyNumber++){
			ArrayList<Statement> statementsToBeDeleted= new ArrayList<Statement>();
			int NumberOfActualProperyValues = 0;
			// read the RDF file
			
			String query = "select (Count(distinct  *) as ?count) where { ?s <"+ properties[propertyNumber]+"> ?o}"; 
			LOGGER.info(query);

			QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
			ResultSet res= qexec.execSelect();
			if(res.hasNext()){
				QuerySolution row= res.next();
				Literal literal = row.get("?count").asLiteral();
				NumberOfActualProperyValues=literal.getInt();
			
			}
			qexec.close();

			int numElementsToBeCancelled= (int) Math.floor(NumberOfActualProperyValues*percentages[propertyNumber]/100);
			int deleted=0;
			LOGGER.info("number of "+properties[propertyNumber]+"'s values to be deleted " + numElementsToBeCancelled+"/"+ NumberOfActualProperyValues );

			Property p= model.createProperty(properties[propertyNumber]);
			do{
				query = "select distinct * where { ?s <"+ properties[propertyNumber]+"> ?o}";
				LOGGER.info(query);
				qexec = QueryExecutionFactory.create(query, model) ;
				res= qexec.execSelect();

				while(res.hasNext()&& deleted<numElementsToBeCancelled){

					String uriS;
					QuerySolution row= res.next();
					// should we keep or delete this ?
					if (Math.random()>0.5) { // random choice
						// we should delete it
						uriS=row.get("?s").asResource().getURI();
						Resource s= model.createResource(uriS);
						RDFNode o= row.get("?o");
						LOGGER.debug("let's delete: "+ uriS + ", "+p+", " +( o.isResource() ? o.asResource().getURI(): o.asLiteral() ) );
						statementsToBeDeleted.add(ResourceFactory.createStatement(s, p, o));
						deleted++;
					}
				}
				model=model.remove(statementsToBeDeleted);
				qexec.close();
				// more than on cicle might happen because a minor number of values  has been deleted as consequence of the random choice 
			}
			while( deleted<numElementsToBeCancelled);



		}
	}
}
