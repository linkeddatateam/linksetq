package Benchmarking;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Assesser.LinksetImpactingStructure.UtilityFunctionsDegreeAssessment;
import Quality.Utilities.RDFAccess;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class RandomDeleteLinksAndTheirConcepts {
	static Model dset;
	static Model lset;	
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName()); 
	private static ExecutionStructures es=new ExecutionStructures(); 

	public static void main(String[] args) {

		// create Options object
		Options options = new Options();
		options.addOption("dataset", true, "rdf dataset path from which to delete concepts");
		options.addOption("datasetnmsp", true, "namespace of dataset");
		options.addOption("linkset", true, "rdf linkset path from which to delete the link related to the concepts deleted");
		options.addOption("predlinkset", true, "skos predicate used in the linkset");
		options.addOption("flag", true, "flag representing if the dataset is the subjec s or the object o of the linkset");
		options.addOption("percentage",true, "percentage expressed as a number between 0 and 100 of concepts to be deleted" );
		options.addOption("outdataset",true, "output dataset path" );
		options.addOption("outlinkset",true, "output linkset path" );



		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			// connecting to the logger
			//	Debugging.loggerConfiguration(logger, cmd.hasOption("logLevel") ? cmd.getOptionValue("logLevel"):null , RandomDeletePropertyValues.class);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "RandomDeleteConcepts", options );
				System.exit(0);
			}
			//other check on options that we could want 
			if (!cmd.hasOption("dataset")||!cmd.hasOption("linkset") 
					||!cmd.hasOption("flag")||!cmd.hasOption("percentage")
					||!cmd.hasOption("datasetnmsp")  
					||!cmd.hasOption("outdataset")
					||!cmd.hasOption("predlinkset")
					||!cmd.hasOption("outlinkset")
					)  {
				formatter.printHelp( "RandomDeleteConcepts", options );
				throw new Exception("some input value  is missing");
			}
			String dataset= cmd.getOptionValue("dataset").trim();
			String linkset= cmd.getOptionValue("linkset").trim();
			String flag= cmd.getOptionValue("flag").trim();
			String outdataset= cmd.getOptionValue("outdataset").trim();
			String datasetnmsp= cmd.getOptionValue("datasetnmsp").trim();
			String outlinkset= cmd.getOptionValue("outlinkset").trim();
			String predlinkset= cmd.getOptionValue("predlinkset").trim();
			int perc= Integer.parseInt(cmd.getOptionValue("percentage"));

			dset=org.apache.jena.riot.RDFDataMgr.loadModel(dataset);
			lset=org.apache.jena.riot.RDFDataMgr.loadModel(linkset);
			es.setModellinks(lset);
			es.extractURILinkofLinkset(flag, datasetnmsp, datasetnmsp, predlinkset);
			RandomDeleteLinksAndTheirConcepts.run2(dataset, linkset,flag,perc,dset,datasetnmsp, lset);

			// finally the model must with deletion must been saved as indicated		
			RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outdataset, dset);
			RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outlinkset, lset);

			dset.close();
			lset.close();

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}
	private static void run2(String datas, String links, String fl, int perc, Model dset,String Mnmsp, Model lset) throws IOException {
		int numoflinks=0;
		String qs, qs1;
		ResultSetRewindable resP=null;
		ArrayList<String> linksetnew=new ArrayList<String>();
		ArrayList<String> alreadydel=new ArrayList<String>();

		int offset=0;
		int limit= 1000;


		int deleted=0;


		File fdelete=new File(datas+"_"+perc+"_URI_ofDeletedLinksetConcepts.csv");
		File fdeletelink=new File(datas+"_"+perc+"_NumberofDeletedLinksetLinks.csv");

		CSVWriter writerdelete = new CSVWriter(new FileWriter(fdelete), '\t');
		CSVWriter writerdeletelink = new CSVWriter(new FileWriter(fdeletelink), '\t');
		fdelete.getParentFile().mkdirs();
		String[] row= new String[1];

		numoflinks=(int)Math.floor((es.getLinkset().size()*perc)/100);
//		if (numoflinks<es.getLinkset().size())
//		{
			while ( deleted<numoflinks)
			{
				for(String l: es.getLinkset())
				{
					if (Math.random()>0.5 && !alreadydel.contains(l)&&deleted<numoflinks) { // random choice
						// we should delete it
						Resource s=null;
						
						//LOGGER.info("let's delete: "+ s.getURI() );
						
						s= lset.createResource(l);
						row[0]=l;
						writerdelete.writeNext(row); 
						dset.remove(dset.listStatements(s,null, (RDFNode)null));
						dset.remove(dset.listStatements(null,null, (RDFNode)s));
						
						if(fl.equalsIgnoreCase("o"))
						{
							lset.remove(lset.listStatements(null,null, (RDFNode)s));
							
						}
						
						if(fl.equalsIgnoreCase("s"))
						{
							//lset.remove(dset.listStatements(s,null, (RDFNode)null));
							lset.remove(lset.listStatements(s,null, (RDFNode)null));
						}
						alreadydel.add(l);
						deleted++;	

					}
				}


			}	
			row[0]=String.valueOf(deleted);
			writerdeletelink.writeNext(row);
			

			writerdelete.flush();
			writerdelete.close();
			writerdeletelink.flush();
			writerdeletelink.close();
		//}

	}



}
