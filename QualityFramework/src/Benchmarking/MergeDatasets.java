package Benchmarking;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.FileManager;

import Quality.DataSetLinksetState;
import Quality.Assesser.LinksetIndex;
import Quality.Utilities.RDFAccess;

public class MergeDatasets {

	/**
	 * @param args
	 * It merges RDF files or complements a datasets with respect to a linkset. It works in two distinct way, 
	 * 
	 * 1) if linkset and NSconvert are not specified it simply merges the  RDF/XML datasets  specified as -in 
	 * For example, 
	 * Benchmarking.MergeDatasets -in x.rdf  -in y.rdf -out zz.rdf  merges x e y creating zz
	 * 
	 * 
	 * 2) if linkset is specified it takes in[0] as subj dataset, in[1] as obj dataset, 
	 *  it works out the strict  complementation  (namely , only interlinked obj entities and their immediate related entities
	 *    are included  in the subj dataset and  results in the   complementation)   
	 *    - in[0] with in[1] wrt linkset and then it changes the namespaces according to NSConvert  
	 *    - linkset a owl:sameAs or skos:exactMatch RDF/XML linkset    
	 *    - (optional) NSConvert is a text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n "
	 *
	 **/
	
	
	public static void main(String[] args) {

		// create Options object
		Options options = new Options();
		options.addOption("in", true, "rdf to be merged");
		options.addOption("linkset", true, "linkset to consider when merging");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("out",true, "merge" );

		//options.addOption("c4owlsameAs",false, "rdfs/owl classes to be  considered linked with owl:sameAs" );
		//options.addOption("c4skosexactMatch",false, "rdfs/owl classes to be considered in the linkset as linked with skos:exactMatch"); 


		DataSetLinksetState.addOptionForFilteringDatasetOrLinkset(options);

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "MergeDatasets", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("in"))  {
				formatter.printHelp( "MergeDatasets", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			String[] in= cmd.getOptionValues("in");
           String nsconvert=null;
           if (cmd.hasOption("NSConvert")) nsconvert= cmd.getOptionValue("NSConvert");
			MergeDatasets.run( in, cmd.getOptionValue("linkset"), nsconvert,cmd.getOptionValue("out"));
		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}

	private static void run(String [] in, String linkset,
			String fileNSConvert, String out) {

		Model model = ModelFactory.createDefaultModel();
       
		try{
			FileOutputStream outp = null;
			if (out!=null)  outp=new FileOutputStream(out);
			if(linkset==null&& fileNSConvert==null){ 	// if it is a simple merge then
				for (int i=0; i< in.length; i++){

					//					InputStream inp= FileManager.get().open( in[i] );
					//
					//					if (inp == null) {
					//						throw new IllegalArgumentException(
					//								"File: " + in[i] + " not found");
					//					}
					Model com=	org.apache.jena.riot.RDFDataMgr.loadModel(in[i]);
					// read the RDF/XML file
					model.add(com);
				}


			}else{
				 LinksetIndex linksetIndex=new LinksetIndex(linkset);
				//merge with change of namespace
				Model linksetM = org.apache.jena.riot.RDFDataMgr.loadModel(linkset);

				// if it isn't a simple merge, but a change of namespace
				Map<String,String> ns= processNSconversion(fileNSConvert);

				//for (int i=0; i< in.length; i++) {
				// let's consider the subj dataset
				Model subjDataset = org.apache.jena.riot.RDFDataMgr.loadModel(in[0]);
				// the obj dataset
				Model objDataset = org.apache.jena.riot.RDFDataMgr.loadModel(in[1]);



				// let's retrieve all the mapped entities
				// let's find what is connected to the obj in the object dataset 
				StmtIterator stm=linksetM.listStatements();
				Model 	importStm= ModelFactory.createDefaultModel();
				while(stm.hasNext()){

					Statement s = stm.next();
					Resource subject = s.getSubject();
					// check if the entity is mapped in the linkset
					String nss=  MergeDatasets.isMappedIn(linksetM,subject.getURI());
					String query = "Construct { <"+ subject.getURI() +"> ?p ?o} where {<"+nss+"> ?p ?o}";
					QueryExecution qexec = QueryExecutionFactory.create(query, objDataset) ;
					importStm.add(qexec.execConstruct());
					// change the namespace to the imported 
				}

				subjDataset.add(importStm);
				stm = subjDataset.listStatements();



				// for each statement in the subj dataset enriched with the 
				//complementation change the namespaces  
				while (stm.hasNext()){
					// // for each statement in the subj dataset 
					//we are changing the namespaces as indicated
					Statement s = stm.next();
					Resource subject = s.getSubject();
					Property predicate = s.getPredicate();
					RDFNode n = s.getObject();
					Resource object=null;
					Resource sbj1 = null, obj1 = null;

					// check if the entity is mapped in the linkset
					String nss=  subject.getURI();
                   
					for (String nsToChange: ns.keySet()){ 
						if (nss.contains(nsToChange)){
							sbj1=model.createResource(nss.replace(nsToChange,ns.get(nsToChange)));
							break;
						}
					}
					if (sbj1==null)  sbj1=model.createResource(subject.getURI());

					

					// find the OBJ
					if (!n.isResource()) { model.add(sbj1, model.createProperty(predicate.getURI()), n ); } 
					else { 
						object= n.asResource();
						// we  normalize the object with respect to subject dataset through  the linkset 
						if (linksetIndex.isInLinksetAsObject(object.getURI())){
							ArrayList <String>lsub = linksetIndex.getEntitiesAssociatedToObject(object.getURI());
						 object=model.createResource(lsub.get(0));
						}	
						
						nss= object.getURI();
						nss= object.getURI();
						
						for (String nsToChange: ns.keySet()){ 
							if (nss.contains(nsToChange)){
								obj1=model.createResource(nss.replace(nsToChange,ns.get(nsToChange)));
								break;
							}
						}
						if (obj1==null)  obj1=model.createResource(object.getURI());
					
						model.add(sbj1, predicate, obj1);
					}

				}
				//for  } 

			}
			if(outp!=null)
				RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out, model);
				//model.write(outp,"N3");
			else model.write(System.out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 * Give an URI it returns the mapped URI according to the linkset or 
	 * its self if there is not mapping for it. 
	 */
	private static String isMappedIn( Model linkset, String uri){
	//	isMapped= false;
		Statement s= linkset.getProperty(linkset.getResource(uri), linkset.getProperty("http://www.w3.org/2002/07/owl#sameAs"));
		String r=null;
		if (s!=null){
			r= s.getObject().asResource().getURI();
//			isMapped=true;
		}else {
			s= linkset.getProperty(linkset.getResource(uri), linkset.getProperty("http://www.w3.org/2004/02/skos/core#exactMatch"));
			if (s!=null){
				r= s.getObject().asResource().getURI();
	//			isMapped=true;
			}
		}

		return (r!=null)? r : uri;
	}

	public static Map<String, String> processNSconversion(String fileNSConvert) {

		HashMap <String,String> result= new HashMap<String, String> ();
        if (fileNSConvert==null) return  result;
		//let's read the file and fill the MAp
		BufferedReader br;
		try {
			if (fileNSConvert!=null){
				br = new BufferedReader(new FileReader(fileNSConvert));

				String line;
				line = br.readLine();

				while (line != null) {
					String[] s= line.split("-->");
					result.put(s[0].trim(), s[1].trim());
					line = br.readLine();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return result;
	}

}
