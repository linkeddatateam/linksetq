set -x #echo on
cd ../..

#####################################
# 	EXAMPLE OF CALL 
# ./scriptSummaryReachabilityResult_k4_mk0.bash ekaw2016 resultsummaries_scripts
# the paramaters are in orders: $1 $2
#####################################



cat "$1"/mk0/*/Test*_reachability_assessment_k4.*_DW_*.csv "$1"/mk0/*/*/Test*_reachability_assessment_k4.*_DW_*.csv | sort | uniq > "$2"/Total_reachability_assessment_k4_mk0.csv
