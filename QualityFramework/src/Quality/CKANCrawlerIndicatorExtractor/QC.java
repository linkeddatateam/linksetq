package Quality.CKANCrawlerIndicatorExtractor;

public final class QC {
public static	String ns ="http://purl.org/QualCraw/";
static	String HasCrawlingDate="hasCrawlingDate";
static	String HasCrawledDataset="hasCrawledDataset";
static String  HasVOIDDescription="hasVOIDDescription";
static String  HasDATAHUBDescription="hasDATAHUBDescription";
static String IsRealizedBy= "isRealizedBy";
static String HasGroup= "hasGroup";
static String IsLinkedTo="isLinkedTo";
static String HasURL= "hasURL";



// rdfs: Classes
static String Crawling= "Crawling";	
static String Dataset= "Dataset";	
public static String CKANDescription= "CKANDescription";
static String VOIDDescription= "VOIDDescription";

}
