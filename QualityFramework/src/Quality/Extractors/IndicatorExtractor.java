package Quality.Extractors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import au.com.bytecode.opencsv.CSVReader;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;

import com.hp.hpl.jena.util.FileManager;


import Quality.DataSetLinksetState;
import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;

/**
 * This tool extracts  types and number of entities as  indicators for subject dataset, object dataset and linkset, starting from a dump or lightweight dump containing only types and   .
 * @author riccardoalbertoni
 *
 */
//* Optionally it can also create a JENA Store in which the indicated dumps loaded 

public class IndicatorExtractor {
	static Model	m;
	static Logger LOGGER;
	static CommandLine cmd;



	public static void main(String[] args)  {

		LOGGER = Logger.getLogger(IndicatorExtractor.class.getName());


		// create Options object
		Options options = new Options();

		// Options
		options.addOption("datasetLinksetState", true , " a JSON file serializing a DataSetLinksetState Object ");	
		options.addOption("subjectDump", true , " a  dump serializing all entities and types plus the RDF links in subject dataset ");
		options.addOption("objectDump", true , " a  dump serializing all entities and  types  in object dataset");
		options.addOption("targetLinkset", true , " the name of the linkset to be consider among those in the JSON file indicated in the datasetLinksetState");
		options.addOption("objectLang", true , "what RDF encoding for object dataset \"RDF/XML\", \"N-TRIPLE\", \"TURTLE\" (or \"TTL\") and \"N3\". null represents the default language, \"RDF/XML\". \"RDF/XML-ABBREV\" is a synonym for \"RDF/XML\"");
		options.addOption("subjectLang", true , "what RDF encoding for subject dataset \"RDF/XML\", \"N-TRIPLE\", \"TURTLE\" (or \"TTL\") and \"N3\". null represents the default language, \"RDF/XML\". \"RDF/XML-ABBREV\" is a synonym for \"RDF/XML\"");
		options.addOption("o", true, "the name of  output JSON file serializing a DataSetLinksetState Object with the new type indicators. It is optional, if not specified results are written in file named as the one specified in  datasetLinksetState adding  _TypesExtracted to the file name");
		options.addOption("noOverwrite", false, " do not overwrite the dataset types indicators if they are already present in the input  DataSetLinksetState JSON file  ");

		// Option to work on rdf dumps
		// create the dataSetLinkset object ??!
		
		// if a datalinksetState is not specified
		org.apache.commons.cli.Option linksetdescription = OptionBuilder.withArgName( "nameProperty=value")
		.hasArgs(10)
		.withValueSeparator()
		.withDescription( "details for a Linkset when a DataSetLinksetState Object is not available, It should include the properties nameOfLinkeset nameOfSubjectDataset nameOfObjectDataset subjectUriSpace objectUriSpace" )
		.create( "DL" );
		options.addOption(linksetdescription);

		org.apache.commons.cli.Option importType = OptionBuilder.withArgName( "KindOFTypes=csVFile")
		.hasArgs(10)
		.withValueSeparator()
		.withDescription( "It imports the types and number of istances from the specified CSV file ( type, #number of Instances for that type). the KindOFTypes can be \"subjectType\" \"objectType\" \"linksetObjectType\" \"linksetSubjectType\"" )
		.create( "ImT" );
		options.addOption(importType);

		//option.addOption("RDFLink", true, the RDF Link Property on which the extractor should focus on " );
		//options.addOption("JENATDBStore", true, " optional directory where to lacate the JENA TDB files (TDBSTORE is the default)");

		CommandLineParser parser = new PosixParser();

		try {

			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "IndicatorExtractor", options );
				System.exit(0);
			}
			// checking if all the parameters needed have been specified 
			if(! cmd.hasOption("datasetLinksetState")&& (! cmd.hasOption("DL"))) {
				System.out.println(" provides a  linksetDescription or specify datasetLinksetState!! "); 
			} else if(! cmd.hasOption("subjectDump")) {
				System.out.println("subjectDump is missing!! "); 
			} else if(! cmd.hasOption("objectDump")) {
				System.out.println("objectDump is missing!! "); 
			} else if(! cmd.hasOption("targetLinkset") &&! (cmd.hasOption("DL"))) {
				System.out.println("targetLinkset is missing!! "); 
			}else if((! cmd.hasOption("o")) && cmd.hasOption("DL")) {
				System.out.println(" since you did't specified a datasetLinksetState you should specify an output datasetLinksetState !!!! "); 
			}else{
				// Create Logger
				LOGGER = Logger.getLogger("");
				LOGGER.setLevel(Level.INFO);
				Date date= new Date();
				SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
				String cDate1= dateFormatter1.format(date);
				FileHandler fileTxt;

				fileTxt = new FileHandler("IndicatorExtractor"+cDate1+".txt");

				SimpleFormatter formatterTxt = new SimpleFormatter();
				fileTxt.setFormatter(formatterTxt);
				LOGGER.addHandler(fileTxt);

				String targetLinkset = null;
				DataSetLinksetState ts=new DataSetLinksetState();
				String filename;
				// load the list of datasets/linkset
				if (cmd.hasOption("datasetLinksetState")){
					filename=cmd.getOptionValue("datasetLinksetState");
					if (filename!=null) ts=ts.loadStatefromJSON(filename);
					else new Exception("dataSetLinksetState incorrect");
				}else { // let's create a new Linkset in the state 
					if (cmd.hasOption("DL")){
						Properties prop=cmd.getOptionProperties("DL");
						LinksetInfo ls= new LinksetInfo();
						DatasetInfo sds= new DatasetInfo();
						DatasetInfo ods= new DatasetInfo();			
						ls.name=prop.getProperty("nameOfLinkeset");
						targetLinkset=prop.getProperty("nameOfLinkeset");
						//nameOfLinkeset nameOfSubjectDataset nameOfObjectDataset subjectUriSpace objectUriSpace
						sds.subject=true;
						sds.UriSpace=prop.getProperty("subjectUriSpace");
						ods.subject=false;
						ods.UriSpace= prop.getProperty("objectUriSpace");
						ls.subjectLocalName= prop.getProperty("nameOfSubjectDataset");
						ls.objectLocalName=prop.getProperty("nameOfObjectDataset");
						ts.linksetInfoArray.add(ls);
						ts.mapOfDataset.put(ls.subjectLocalName, sds);
						ts.mapOfDataset.put(ls.objectLocalName, ods);
					}else new Exception("linksetDescription unspecified"); 
				}
				if (cmd.getOptionValue("targetLinkset")!=null)  targetLinkset = cmd.getOptionValue("targetLinkset");

				// if CSV file containing type and # instance  are specified 
				if (cmd.hasOption("ImT")){
					LinksetInfo ls = null;
					DatasetInfo sds = null;
					DatasetInfo ods = null;	

					for (LinksetInfo l :ts.linksetInfoArray){
						if (l.name.equals(targetLinkset) ){
							ls=l;
							sds= ts.mapOfDataset.get(ls.subjectLocalName);
							ods= ts.mapOfDataset.get(ls.objectLocalName);	
						}
					}
					if (ls==null)  new Exception("linksetDescription not found the CSV type values  cannot be loaded"); 

					Properties prop=cmd.getOptionProperties("ImT");
					if (prop.containsKey("subjectType")){
						sds.types=IndicatorExtractor.readTypeFromCSV(prop.getProperty("subjectType"));
					}
					if (prop.containsKey("objectType")){
						ods.types=IndicatorExtractor.readTypeFromCSV(prop.getProperty("objectType"));
					}
					if (prop.containsKey("linksetSubjectType")){
						ls.subjectTypes=IndicatorExtractor.readTypeFromCSV(prop.getProperty("linksetSubjectType"));
					}
					if (prop.containsKey("linksetObjectType")){
						ls.objectTypes=IndicatorExtractor.readTypeFromCSV(prop.getProperty("linksetObjectType"));
					}
				}

				// create a jena model
				m= ModelFactory.createDefaultModel();
				filename=cmd.getOptionValue("subjectDump");
				InputStream in = FileManager.get().open(filename);
				if (in == null) {
					throw new IllegalArgumentException(
							"File: " + filename + " not found");
				}
				String type=cmd.getOptionValue("subjectLang");
				m.read(in,null, type);

				filename=cmd.getOptionValue("objectDump");
				in = FileManager.get().open(filename);
				if (in == null) {
					throw new IllegalArgumentException(
							"File: " + filename + " not found");
				}
				m.read(in,null,cmd.getOptionValue("objectLang") );

				try {


					if (targetLinkset!=null) workOutTypes(ts, targetLinkset);
					else  new Exception("You should specify targetLinkset if you are starting from a datasetLinksetState or -DL nameOfLinkset otherwise"); 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (cmd.hasOption("o")) {
					ts.saveStateInJSON(cmd.getOptionValue("o"));
					//System.out.println("Type extracted have been stored in "+cmd.getOptionValue("o") );
					LOGGER.info("Type extracted have been stored in "+cmd.getOptionValue("o") );
				}
				else {
					String fileName=cmd.getOptionValue("datasetLinksetState").replace(".json", "_TypesExtracted.json");
					ts.saveStateInJSON(fileName);
					//System.out.println("Type extracted have been stored in "+fileName);
					LOGGER.info("Type extracted have been stored in "+fileName);

				}
			}
		} catch (ParseException e) {
			System.out.println(" Error in the command line ");
			e.printStackTrace();
		}
		catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}

	private static HashMap<String, Integer> readTypeFromCSV(String file) {
		// TODO Auto-generated method stub

		// delete the rows till the sencond value it is  not a integer..
		CSVReader reader;
		HashMap <String, Integer> res = new HashMap <String, Integer>();
		try {
			reader = new CSVReader(new FileReader(file));
			String [] nextLine;
			//int numIstances=0;
		
			while ((nextLine = reader.readNext()) != null) {
				try{
					//numIstances= Integer.parseInt(nextLine[1]) ;
					res.put(nextLine[0], Integer.parseInt(nextLine[1]));
				}catch (java.lang.NumberFormatException e){
					// if the second column is not  a integer it jsut skip the line
				}

			}
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	private static void workOutTypes(DataSetLinksetState ts, String optionValue) throws Exception {

		Iterator <LinksetInfo> iter=ts.linksetInfoArray.iterator();
		boolean notFound=true;
		LinksetInfo ls=new LinksetInfo() ;
		while ( notFound && iter.hasNext() ){
			ls = iter.next();
			if (ls.name.equalsIgnoreCase(optionValue)) notFound=false;	
		}

		if (!notFound){
			DatasetInfo subjectInfo=ts.mapOfDataset.get(ls.subjectLocalName);
			DatasetInfo objectInfo=ts.mapOfDataset.get(ls.objectLocalName);
			if (subjectInfo.consolidatedUriSpace==null) {
				subjectInfo.consolidatedUriSpace= subjectInfo.UriSpace;
				LOGGER.warning("UriSpace "+ subjectInfo.UriSpace + " has been taken as consolidatedUriSpace for "+ ls.subjectLocalName);
			}
			if (objectInfo.consolidatedUriSpace==null) {
				objectInfo.consolidatedUriSpace= objectInfo.UriSpace;
				LOGGER.warning("UriSpace "+ objectInfo.UriSpace + " has been taken as consolidatedUriSpace for "+ ls.objectLocalName);
			}
			if (objectInfo.UriSpace==null) LOGGER.severe(" Cannot assess the quality indicators without a URISpace for " +ls.objectLocalName);
			if (subjectInfo.UriSpace==null) LOGGER.severe(" Cannot assess the quality indicators without a URISpace for " +ls.subjectLocalName);

			if (subjectInfo.consolidatedUriSpace!=null && objectInfo.consolidatedUriSpace!=null){
				// add  subject dataset indicator
				if (!(cmd.hasOption("noOverwrite") && !subjectInfo.types.isEmpty())) 
					workOutDatasetType(subjectInfo,subjectInfo.consolidatedUriSpace );
				// add object dataset indicator
				if (!(cmd.hasOption("noOverwrite") && !objectInfo.types.isEmpty())) 
					workOutDatasetType(objectInfo,objectInfo.consolidatedUriSpace );
				// add subject linkset indicator
				if (!(cmd.hasOption("noOverwrite") && !ls.subjectTypes.isEmpty())) 
					workOutSubjectLinksetType(ls,subjectInfo,objectInfo);
				// add object linkset indicator
				if (!(cmd.hasOption("noOverwrite") && !ls.objectTypes.isEmpty())) 
					workOutObjectLinksetType(ls,subjectInfo,objectInfo);
			}
		} else LOGGER.warning("I haven't found the linkeset " + optionValue);
	}



	/**
	 * It works out the object linkset types for the linkset having subject and object respectively as linkset's subject and object 
	 * @param linksetInfo
	 * @param subjectInfo
	 * @param objectInfo
	 * @param InputModel  if it is null the subject linkset triples are uploaded into the returned model otherwise they are supposed to be already uploaded
	 * @return 
	 * @return 
	 * @throws Exception
	 */
	public static   void workOutObjectLinksetType (LinksetInfo linksetInfo, DatasetInfo subjectInfo, DatasetInfo objectInfo) throws Exception{

		InputStream ip = CrawlMetadata.class.getResourceAsStream("../../QueriesForExtractor/LinksetObjectTypeOnDump.sparql");
		String queryString = CrawlMetadata.getContents(ip);

		queryString= queryString.replace("SUJBECTURISPACE", subjectInfo.consolidatedUriSpace).replace("OBJECTURISPACE", objectInfo.consolidatedUriSpace);

		LOGGER.info("quering "+ objectInfo.sparqlEndPoint +" by " +queryString);

		Query  query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		// let's clean types
		linksetInfo.objectTypes = new HashMap  <String, Integer >();

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence;

			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					linksetInfo.objectTypes.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subjectInfo.crawledDatasetUri +" and " +  objectInfo.crawledDatasetUri );

			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ objectInfo.crawledDatasetUri );		//return m;

	}

	public static   void workOutSubjectLinksetType (LinksetInfo ls, DatasetInfo subject, DatasetInfo object) throws Exception{
		InputStream ip = CrawlMetadata.class.getResourceAsStream("../../QueriesForExtractor/LinksetSubjectTypeOnDump.sparql");
		String queryString = CrawlMetadata.getContents(ip);

		queryString= queryString.replace("SUJBECTURISPACE", subject.consolidatedUriSpace).replace("OBJECTURISPACE", object.consolidatedUriSpace);

		LOGGER.info("quering "+ object.sparqlEndPoint +" by " +queryString);

		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 

			// let's clean types
			ls.objectTypes = new HashMap  <String, Integer >();


			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					ls.subjectTypes.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subject.crawledDatasetUri +" and " +  object.crawledDatasetUri );
			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ object.crawledDatasetUri );

	}


	public static   void workOutDatasetType (DatasetInfo subject, String consolidatedURISpace) throws Exception{
		String queryString;

		InputStream ip = CrawlMetadata.class.getResourceAsStream("../../QueriesForExtractor/DatasetTypeAndNumOfOccurence.sparql");
		queryString = CrawlMetadata.getContents(ip);



		queryString=queryString.replace("CONSOLIDATEDURISPACE", consolidatedURISpace);
		LOGGER.info("quering "+ subject.sparqlEndPoint +" by " +queryString);
		Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		// let's clean types
		subject.types = new HashMap  <String, Integer >();

		if (res!=null) {
			// ResultSetFormatter.out(System.out, res, query);
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 
			while (res.hasNext()){
				sol=res.next();
				type=sol.get("type");
				noccurence=sol.get("numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					subject.types.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subject.crawledDatasetUri +" and " +  subject.crawledDatasetUri );
			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ subject.crawledDatasetUri );
		//return m;


	}




}
