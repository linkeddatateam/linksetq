package Benchmarking;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.util.iterator.FilterDropIterator;

import Quality.DataSetLinksetState;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;
/**
 * 
 * @author paolapodestà
 * It works out the centrality gain in terms of number of rachable entities 
 * 
 *  
 */
public class CentralityGainWRTGoldStandard {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create Options object
		Options options = new Options();
		options.addOption("complemented", true, "complemented dataset on which we want to workout the normalized Gain");
		options.addOption("subject", true, "subject dataset on which we want to workout the normalized Gain");
		options.addOption("goldStandard", true, "dataset take as goldstandard when working out the completeness");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("outgain",true, "file csv  containing gain results" );
		options.addOption("outcompleteness",true, "file csv  containing completeness results" );
		options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");		
		//options.addOption("out",true, "file containing the results" );

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		CSVWriter writeroutcompleteness=null;
		CSVWriter writeroutgain=null;
		CSVWriter writeroutconceptscomplemented=null;
		CSVWriter writeroutconceptsdataset=null;
		
		
		
		
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		Map<String,String> ns=null;		
		String[] CSVHeader = new String[14];

		HelpFormatter formatter = new HelpFormatter();
		try {
			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "GainWRTGoldStandard", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("complemented") ||  !cmd.hasOption("subject") 
					|| !cmd.hasOption("goldStandard") )  {
				formatter.printHelp( "GainWRTGoldStandard", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			
			
			HashMap<String, Double> weightForCentrality;
			
			
			double gainRest=-1.0;
			double gainNoRest=-1.0;
			String subjectPath= cmd.getOptionValue("subject");
			String complementedPath= cmd.getOptionValue("complemented");
			String outgain="";
			String outcompleteness="";
			String testname="";

			
			String goldStandard= cmd.getOptionValue("goldStandard");
			String Wrel= cmd.getOptionValue("WeightFilePath");
			String in= cmd.getOptionValue("in");
			HashMap<String,String> dataset_gs_res= new HashMap<String,String>();
			HashMap<String,String> comple_gs_res= new HashMap<String,String>();
			HashMap<String,String> dataset_gs_Nores= new HashMap<String,String>();
			HashMap<String,String> comple_gs_Nores= new HashMap<String,String>(); 

			
			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#broader>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#related>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#narrower>", 1.0);
				formatter.printHelp("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default", options);
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));
			
			
			
			

			if (cmd.hasOption("outgain")&& cmd.hasOption("outcompleteness")){
				outgain=cmd.getOptionValue("outgain");
				testname=outgain.substring(outgain.lastIndexOf("/")+1, outgain.indexOf("_", outgain.lastIndexOf("/")+1));
				outcompleteness=(cmd.getOptionValue("outcompleteness")); 

				writeroutgain = new CSVWriter(new FileWriter(outgain), '\t');
				writeroutconceptscomplemented = new CSVWriter(new FileWriter("concept_complemented"), '\t');
				writeroutcompleteness=new CSVWriter(new FileWriter(outcompleteness), '\t');
				writeroutconceptsdataset=new CSVWriter(new FileWriter("concept_subj"), '\t');

				if (cmd.hasOption("NSConvert")){// let's change NS the in model 
					ns= MergeDatasets.processNSconversion(cmd.getOptionValue("NSConvert"));
					comple_gs_res= CentralityCompletenessWRTGoldStandard.computeCentralityCompletnessRestrictRel(complementedPath, goldStandard,ns,outcompleteness,writeroutconceptscomplemented, weightForCentrality);
					dataset_gs_res=CentralityCompletenessWRTGoldStandard.computeCentralityCompletnessRestrictRel(subjectPath, goldStandard,ns,outgain,writeroutconceptsdataset, weightForCentrality);
					dataset_gs_Nores=CentralityCompletenessWRTGoldStandard.computeCentralityCompletnessNoRestrictRel(subjectPath, goldStandard,ns,outgain,writeroutconceptsdataset, weightForCentrality);
					comple_gs_Nores= CentralityCompletenessWRTGoldStandard.computeCentralityCompletnessNoRestrictRel(complementedPath, goldStandard,ns,outcompleteness,writeroutconceptscomplemented, weightForCentrality);					
				}
				
				
				CSVHeader[0]= "TestName";
				CSVHeader[1]= "TotalConceptsSubjNoRest";			
				CSVHeader[2]= "TotalConceptsSubjRest";
				CSVHeader[3]= "TotalConceptsGSRest";
				CSVHeader[4]= "TotalConceptsComplementedRest";
				CSVHeader[5]= "TotalConceptsComplementedNoRest";
				
				CSVHeader[6]= "TotalConceptsIntersectionSubjNoRestGSRest";
				CSVHeader[7]= "TotalConceptsIntersectionComplNoRestGSRest";
				CSVHeader[8]= "TotalConceptsIntersectionSubjRestGSRest";
				CSVHeader[9]= "TotalConceptsIntersectionComplRestGSRest";							
				CSVHeader[10]= "Q(TotalConceptsSubjNoRest, GS)";
				CSVHeader[11]= "Q(TotalConceptsComplementedNoRest, GS)";
				CSVHeader[12]= "Q(TotalConceptsSubjRest, GS)";
				CSVHeader[13]= "Q(TotalConceptsComplementedRest, GS)";
				
				writeroutcompleteness.writeNext(CSVHeader);
				
				CSVHeader[0]= testname;
				CSVHeader[1]= myFormatter.format(Double.valueOf(dataset_gs_Nores.get("TotalConceptsDatasetNoR")));
				CSVHeader[2]=  myFormatter.format(Double.valueOf(dataset_gs_res.get("TotalConceptsDataset")));
				
				CSVHeader[3]=myFormatter.format(Double.valueOf(dataset_gs_res.get("TotalConceptsGS")));

				CSVHeader[4]=  myFormatter.format(Double.valueOf(comple_gs_Nores.get("TotalConceptsDatasetNoR")));
				CSVHeader[5]=  myFormatter.format(Double.valueOf(comple_gs_res.get("TotalConceptsDataset")));
				
				CSVHeader[6]= myFormatter.format(Double.valueOf(dataset_gs_Nores.get("TotalConceptsIntersectionNoR"))); 				
				CSVHeader[7]= myFormatter.format(Double.valueOf(comple_gs_Nores.get("TotalConceptsIntersectionNoR")));
				CSVHeader[8]= myFormatter.format(Double.valueOf(dataset_gs_res.get("TotalConceptsIntersection")));
				CSVHeader[9]= myFormatter.format(Double.valueOf(comple_gs_res.get("TotalConceptsIntersection")));

				CSVHeader[10]= myFormatter.format(Double.valueOf(dataset_gs_Nores.get("ValueOfCompletenessNoR")));
				CSVHeader[11]= myFormatter.format(Double.valueOf(comple_gs_Nores.get("ValueOfCompletenessNoR")));
				CSVHeader[12]= myFormatter.format(Double.valueOf(dataset_gs_res.get("ValueOfCompleteness")));
				CSVHeader[13]= myFormatter.format(Double.valueOf(comple_gs_res.get("ValueOfCompleteness")));
			
				
				writeroutcompleteness.writeNext(CSVHeader);
				
				gainRest=Double.valueOf(comple_gs_res.get("ValueOfCompleteness")) 
						-Double.valueOf(dataset_gs_res.get("ValueOfCompleteness"));
				
				gainNoRest=Double.valueOf(comple_gs_Nores.get("ValueOfCompletenessNoR")) 
						-Double.valueOf(dataset_gs_Nores.get("ValueOfCompletenessNoR"));
				//PrintResultsGain(comple_gs_res, writeroutcompleteness);
				//PrintResultsGain(dataset_gs_res, writeroutcompleteness);
				
				/*
				CSVVal[0]= res.get("TestName");
				CSVVal[1]= res.get("DatasetsName"); 
				CSVVal[2]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsDataset")));
				CSVVal[3]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsGS")));			
				CSVVal[4]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsIntersection")));
				CSVVal[5]=  myFormatter.format(Double.valueOf(res.get("Q(comple)")));
				

				writer.writeNext(CSVVal);
				*/
				
				String[] title= new String[4];
				String[] value=new String[4]; 
				
				title[0]="TestName";
				title[1]="DatasetsName";
				title[2]="ValueOfGainRest";
				title[3]="ValueOfGainNoRest";
				writeroutgain.writeNext(title);
							
				
				value[0]=testname;
				value[1]=complementedPath.substring(complementedPath.lastIndexOf("/")+1)+"_"+subjectPath.substring(subjectPath.lastIndexOf("/")+1);
				value[2]=myFormatter.format(gainRest);
				value[3]=myFormatter.format(gainNoRest);
				
				writeroutgain.writeNext(value);
				 
				writeroutgain.close();
				writeroutcompleteness.close();
				writeroutconceptsdataset.close();
				writeroutconceptscomplemented.close();
				
				System.out.print(" CSV containing the results has been writte into "+ outgain );
				System.out.print(" CSV containing the results has been writte into "+ outcompleteness );
				
			}

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}
	
	
	private static void PrintResultsGain(HashMap<String,String> res,CSVWriter writer)
	{

		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);

		String[] CSVVal = new String[7];

		
		
		CSVVal[0]= res.get("TestName");
		CSVVal[1]= res.get("DatasetsName"); 
		CSVVal[2]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsDataset")));
		CSVVal[3]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsGS")));			
		CSVVal[4]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsIntersection")));
		CSVVal[5]=  myFormatter.format(Double.valueOf(res.get("ValueOfCompleteness")));
		

		writer.writeNext(CSVVal);
		

	}

	private static void PrintResultsGain2(HashMap<String,String> res,CSVWriter writer)
	{

		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);

		String[] CSVVal = new String[7];

		
		
		CSVVal[0]= res.get("DatasetsName");
		CSVVal[1]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsDataset")));
		CSVVal[2]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsGS")));			
		CSVVal[3]=  myFormatter.format(Double.valueOf(res.get("TotalConceptsIntersection")));
		CSVVal[4]=  myFormatter.format(Double.valueOf(res.get("ValueOfCompleteness")));

		writer.writeNext(CSVVal);
		

	}
	
	/**
	 * It reads from a CSV file the relation's weights 
	 * @param filePath
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String,Double> readWeight(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		String [] nextLine;
		HashMap <String, Double> res = new  HashMap <String, Double> ();
		while ((nextLine = reader.readNext()) != null) {
			// nextLine[] is an array of values from the line
			res.put(nextLine[0],  Double.valueOf(nextLine[1]));
		}


		return res;


	}
	
	

}


