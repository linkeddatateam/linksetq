package Benchmarking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;



import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import Jama.Matrix;
import Quality.DataSetLinksetState;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetIndex;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Assesser.LinksetImpactingStructure.RDFEdge;
import Quality.Assesser.LinksetImpactingStructure.RelationAdjacentConcept;
import Quality.Utilities.RDFAccess;

public class CentralityMergeDataset {

	//private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName());
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.CentralityMergeDataset.class.getName());


	static ExecutionStructures es=new ExecutionStructures();
	//contains the entry point concepts of path with length k
	//private static  HashMap<Integer, String>  setofIndexURIwithPathOfLenghtKandDeleted =new HashMap<Integer, String>  ();
	//private static  HashMap<Integer, String> setofURIwithPathOfLenghtK =new HashMap<Integer, String> ();

	private static  ArrayList<String>  setofIndexURIwithPathOfLenghtKandDeleted =new ArrayList<String>  ();
	private static  ArrayList<String>  setofURIwithPathOfLenghtK =new ArrayList<String>  ();
	private static   StringBuilder sb = new StringBuilder();
	private static   int cont= -1;


	public static void main(String[] args) {


		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration conf = ctx.getConfiguration();
		conf.getLoggerConfig(Benchmarking.CentralityMergeDataset.class.getName()).setLevel(Level.DEBUG);
		ctx.updateLoggers(conf);

		// create Options object
		Options options = new Options();
		options.addOption("subject", true, "rdf subject");
		options.addOption("object", true, "rdf object");
		options.addOption("k", true, "considered hops");
		options.addOption("linkset", true, "linkset to consider when merging");
		options.addOption("predlinkset", true, "predicate used in the linkset");
		options.addOption("conceptstartingpoints", true, "concepts starting point of created path with length k");
		options.addOption("conceptsdeleted", true, "concepts deleted when creating the path with length k");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("out",true, "merge" );
		options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");		


		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		HashMap<String, Double> weightForCentrality;
		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "CentralityMergeDataset", options );
				System.exit(0);
			}
			//			other check on options that we could want 


			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#broader", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#related", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#narrower", 1.0);
				LOGGER.warn("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default");
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));

			if (!cmd.hasOption("subject")||!cmd.hasOption("object")
					||!cmd.hasOption("k")|| !cmd.hasOption("NSConvert")
					||!cmd.hasOption("linkset")||!cmd.hasOption("predlinkset"))
			{
				formatter.printHelp( "CentralityMergeDataset", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			String  filensconvert= cmd.getOptionValue("NSConvert");
			Map<String,String> ns= processNSconversion(filensconvert);
			String objnmsp="";
			String subjnmsp="";

			if(ns.size()==1){
				for (String nsToChange: ns.keySet()){ 
					//the sbj namespace is the key 
					objnmsp=nsToChange;
					//the obj namespace is the value of the key
					subjnmsp=ns.get(nsToChange);
				}
			}
			else{//TODO se ci sono più conversioni da fare perchè ci sono più nmspc

			}





			//it is the dataset to be "complemented" that is "enriched"
			//that is ALWAYS the subject dataset 
			String subject= cmd.getOptionValue("subject");
			String object= cmd.getOptionValue("object");
			//ATTENZIONE! per allineare k input e numero di hop 
			//kinput=khop+1 
			//khop=kinput-1
			//quindi kinput deve essere sempre >=2
			int k=0;
			int kinput=Integer.parseInt(cmd.getOptionValue("k"));


			if(cmd.hasOption("conceptstartingpoints")&& cmd.hasOption("conceptsdeleted"))
			{//the option is used when we perform all the process from the creation 
				//of the paths to the quality analysis   
				if(kinput>=2)
				{
					k=kinput-1;
					CentralityMergeDataset.run( object,objnmsp, 
							subject, subjnmsp,
							cmd.getOptionValue("conceptstartingpoints"),
							cmd.getOptionValue("conceptsdeleted"),					
							cmd.getOptionValue("out"),k, weightForCentrality);
				}else{
					throw new Exception("Parameter k must be >=2");
				}

			}
			else
			{ 	//this is used when we have only the two datasets and linkset	
				//we do not have created the path of length 4

				//CentralityMergeDataset.createGEMETrestricted( object,objnmsp,
				CentralityMergeDataset.runVisitAndComplementing( object,objnmsp,
						subject, subjnmsp,
						cmd.getOptionValue("linkset"), 
						cmd.getOptionValue("predlinkset"),
						cmd.getOptionValue("out"),kinput, weightForCentrality);
			}
		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}


	private static void run(String obj, String objnmsp,
			String subj,String subjnmsp, 
			String conceptstartingpoints, String conceptsdeleted,
			String out, int k, HashMap<String, Double> weightForCentrality) {

		Model model = ModelFactory.createDefaultModel();

		try{
			//leggere file e creare array con concepts starting points
			//poi inserire array come linkset in modo da considerare solo quei link
			readEntryPointConceptsURIandDeletedConceptsURI(conceptstartingpoints, conceptsdeleted, 
					subjnmsp,objnmsp);
			//readEntryPointConceptsURI(conceptstartingpoints);
			es.setLinkset(setofIndexURIwithPathOfLenghtKandDeleted);
			es.setWeight(weightForCentrality);
			es.setModelsubj(org.apache.jena.riot.RDFDataMgr.loadModel(obj));
			es.setModelobj(org.apache.jena.riot.RDFDataMgr.loadModel(subj));
			es.extractAM("s",subjnmsp, objnmsp,  k);
			es.initializeMatrices(k);
			//per ogni entry point e 
			// per ogni c in insieme di entry point concepts a cui e è legato  
			//       guardo matrice AWM (c,e) e ottengo il predicato p  
			//               creare le tuple (e,p,c) con il namespace 
			//				   corretto ottenuto dal fileNSConvert
			//				  inserisco la tupla (e,p,c) in subj dataset	
			//output subj dataset complementato
			int ik=1;
			int r,c;
			Matrix m;
			String subjURI,objURI,p;
			RDFEdge nrdfe;
			Model 	importStm=null;


			while(ik<=k+1)
			{
				for(String rs: setofIndexURIwithPathOfLenghtKandDeleted)
				{
					r=es.getURIMatrixIndex().indexOf((Object)rs);
					for(String cs: es.getURIMatrixIndex())
					{
						m= es.getPowerM().get(ik);
						if((m.get(r, es.getURIMatrixIndex().indexOf((Object)cs))==1))
						{
							nrdfe= new RDFEdge(rs,cs);
							importStm= ModelFactory.createDefaultModel();
							for ( Map.Entry<String, HashSet<RDFEdge>> entry : es.getAWM().entrySet()) {
								String key = entry.getKey();
								HashSet<RDFEdge> tab = entry.getValue();
								ArrayList <RDFEdge> tabaux=new ArrayList <RDFEdge>();
								tabaux.addAll(tab);
								// do something with key and/or tab
								if(tabaux.contains((Object)nrdfe))
								{
									subjURI=rs.replaceAll("^"+objnmsp+"*", subjnmsp);
									objURI=cs.replaceAll("^"+objnmsp+"*", subjnmsp);
									String query = "Construct { <"+ subjURI +">  <"+key+"> <"+objURI+ ">} where { } ";


									//LOGGER.info("query to select link in linkset query: "+query);
									String query2 = "Construct { <"+ subjURI +"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept> } where{}";
									//LOGGER.info("query to select link in linkset query2: "+query2);
									String query3 = "Construct { <"+ objURI +"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept> } where{}";
									//LOGGER.info("query to select link in linkset query3: "+query3);	
									QueryExecution qexec = QueryExecutionFactory.create(query, es.getModelobj());
									importStm.add(qexec.execConstruct());
									QueryExecution qexec2 = QueryExecutionFactory.create(query2, es.getModelobj());
									QueryExecution qexec3 = QueryExecutionFactory.create(query3, es.getModelobj());
									importStm.add(qexec2.execConstruct());
									importStm.add(qexec3.execConstruct());

								}
							}
							es.getModelobj().add(importStm);
						}
					}
				}
				ik++;
			}
			FileOutputStream outp = null;
			if (out!=null)  outp=new FileOutputStream(out);
			if(outp!=null)
				RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out, es.getModelobj());
			//model.write(outp,"N3");
			else model.write(System.out);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	private static void runVisitAndComplementing(String obj,String objnmsp,
			String subj,String subjnmsp, 
			String linkset, 
			String predlinkset,
			String out, int k, HashMap<String, Double> weightForCentrality) {

		Model model = ModelFactory.createDefaultModel();
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);


		try{
			es.setModellinks(org.apache.jena.riot.RDFDataMgr.loadModel(linkset));
			es.setModelsubj(org.apache.jena.riot.RDFDataMgr.loadModel(subj));
			es.setModelobj(org.apache.jena.riot.RDFDataMgr.loadModel(obj));
			es.extractURILinkofLinkset("o", objnmsp ,subj,predlinkset);



			//File f=new File(out+"_URIConceptsInsertedInComplemented.csv");
			File f=new File("URINOTInsertedInComplemented.csv");
			//f.getParentFile().mkdirs();
			//LOGGER.info("preparing URI Reached file"+f);
			CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
			//HashMap<String, HashSet<RelationAdjacentConcept>> m;
			//m= es.getAWL();

			

			//es.toStringALW();
			for(String rs: es.getLinkset())
			{
				LOGGER.info("guardo link del linkset:    "+ rs);
				//CentralityMergeDataset.visitAndComplement(rs, 1,k, objnmsp, subjnmsp, myFormatter, writer) ;
				CentralityMergeDataset.visitAndComplementAndCount(rs, 1,k, objnmsp, subjnmsp, myFormatter, writer) ;

			}


			LOGGER.info(" Number of TOTAL gained concept "+ cont);
			writer.writeNext( sb.toString().split("TABTAB"));
			writer.flush();
			writer.close();

			FileOutputStream outp = null;
			if (out!=null)  outp=new FileOutputStream(out);
			if(outp!=null)
				//old instruction not working	RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out, es.getModelobj());
				RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out, es.getModelsubj());

			//model.write(outp,"N3");
			else model.write(System.out);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}




	private static void visitAndComplement(String l, int hop, int k, String objnmsp,
			String subjnmsp, DecimalFormat myFormatter, 
			CSVWriter writer) {
		LOGGER.debug("Visit and Complement" + l ); 
		int offset= 0, limit=1000;
		//	int r,c;
		String subjURI,objURI,p;
		//Stack<String> next= new Stack<String>();
		////String[] row1 =new String[2];

		////LOGGER.info("Start complementing link : "+l);
		//r=es.getURIMatrixIndex().indexOf((Object)l);
		ResultSetRewindable resP;
		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		do{ 
			////printALW("<http://yyy/4633>", m);
			String qs="SELECT ?p ?y2 "
					+"\n where {  "
					+" <"+	l +"> ?p ?y2. "
					/* ADDBYRIC, we don't need literals */ 
					+" filter (isURI(?y2))"
					+"}  "
					+"LIMIT "+ limit
					+" OFFSET "+ offset +"\n";

			SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
			//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
			resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(es.getModelobj(), qs, err2, null, false));

			//ResultSetFormatter.out(resP);
			resP.reset();
			//in order to consider all the resP 
			//over the iteration considering the offset 
			//we save a list of results
			listresP.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);



		try{
			if(hop<=k)
			{
				// what we should visit ??

				do{ 
					////printALW("<http://yyy/4633>", m);
					String qs="SELECT ?p ?y2 "
							+"\n where {  "
							+" <"+	l +"> ?p ?y2. "
							/* ADDBYRIC, we don't need literals */ 
							+" filter (isURI(?y2))"
							+"}  "
							+"LIMIT "+ limit
							+" OFFSET "+ offset +"\n";

					SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
					//	LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
					resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(es.getModelobj(), qs, err2, null, false));

					//ResultSetFormatter.out(resP);
					resP.reset();
					
					//in order to consider all the resP 
					//over the iteration considering the offset 
					//we save a list of results
					listresP.add(resP);
					offset=offset+limit;

				}while (resP.size()>0);

				//Let's identify the adjacent nodes


				for(ResultSet resP2:listresP)
				{
					QuerySolution qsol;
					String y2uri;

					while(resP2.hasNext()) {
						qsol=resP2.next();

						//get object and subject
						p=qsol.getResource("?p").getURI();
						//System.out.println("property ?p in qsol: "+ p);

						// check if is it in the skos relation set
						if (isInSkosPropertySet(p) ){
							if (qsol.get("?y2").isURIResource())
							{//control if the URI of the triple matches the URI space 
								//of the considered thesaurus
								y2uri=qsol.getResource("?y2").getURI().trim();

								// Insert in subject and keep visiting the  adjacent nodes	

								subjURI=l.replaceAll("^"+objnmsp+"*", subjnmsp);
								objURI=y2uri.replaceAll("^"+objnmsp+"*", subjnmsp);

								String [] row1= new String [2];



								Model mod= es.getModelsubj();
								Property jenap= mod.createProperty(p), 
										rdfType= mod.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
								if (!es.getModelsubj().contains( ResourceFactory.createResource(objURI), null, (RDFNode) null )	
										) {
									row1[0]=objURI;
									row1[1]=myFormatter.format((int)(hop));
									writer.writeNext(row1);
									LOGGER.info(" gained concept "+ objURI);
								}

								mod.createResource(subjURI)
								.addProperty( jenap,mod.createResource(objURI))
								.addProperty( rdfType, mod.createResource("http://www.w3.org/2004/02/skos/core#Concept")) ;

								mod.createResource(objURI)
								.addProperty( rdfType, mod.createResource("http://www.w3.org/2004/02/skos/core#Concept")) ;

								visitAndComplement(y2uri, hop+1, k, objnmsp,
										subjnmsp, myFormatter, writer );


							}
						}
					}
				}
			}
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//sb.append(l);
			//sb.append("\n");
			LOGGER.info("NULL POINTER EXCEPTION l : "+l);
			LOGGER.info("NULL POINTER EXCEPTION hop : "+hop);
			LOGGER.info("NULL POINTER EXCEPTION e msm : "+ e.getMessage());
		}

	}

	
	
//visitAndComplementAndCount added 20/12/2015 in order to count 
	//the node visited 
	private static void visitAndComplementAndCount(String l, int hop, int k, String objnmsp,
			String subjnmsp, DecimalFormat myFormatter, 
			CSVWriter writer) {
		LOGGER.debug("Visit  Complement and Count " + l ); 
		int offset= 0, limit=1000;
		//	int r,c;
		String subjURI,objURI,p;
		//Stack<String> next= new Stack<String>();
		////String[] row1 =new String[2];

		////LOGGER.info("Start complementing link : "+l);
		//r=es.getURIMatrixIndex().indexOf((Object)l);
		ResultSetRewindable resP;
		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		try{
			if(hop<=k)
			{
				do{ 
					////printALW("<http://yyy/4633>", m);
					String qs="SELECT ?p ?y2 "
							+"\n where {  "
							+" <"+	l +"> ?p ?y2. "
							/* ADDBYRIC, we don't need literals */ 
							+" filter (isURI(?y2))"
							+"}  "
							+"LIMIT "+ limit
							+" OFFSET "+ offset +"\n";

					SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
					//	LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
					resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(es.getModelobj(), qs, err2, null, false));
					//ResultSetFormatter.out(resP);
					resP.reset();
					//in order to consider all the resP 
					//over the iteration considering the offset 
					//we save a list of results
					listresP.add(resP);
					offset=offset+limit;
				}while (resP.size()>0);
				//Let's identify the adjacent nodes
				for(ResultSet resP2:listresP)
				{
					QuerySolution qsol;
					String y2uri;

					while(resP2.hasNext()) {
						qsol=resP2.next();

						//get object and subject
						p=qsol.getResource("?p").getURI();
						//System.out.println("property ?p in qsol: "+ p);

						// check if is it in the skos relation set
						if (isInSkosPropertySet(p) ){
							if (qsol.get("?y2").isURIResource())
							{//control if the URI of the triple matches the URI space 
								//of the considered thesaurus
								y2uri=qsol.getResource("?y2").getURI().trim();

								// Insert in subject and keep visiting the  adjacent nodes	
								subjURI=l.replaceAll("^"+objnmsp+"*", subjnmsp);
								objURI=y2uri.replaceAll("^"+objnmsp+"*", subjnmsp);
								String [] row1= new String [2];
								Model mod= es.getModelsubj();
								Property jenap= mod.createProperty(p), 
										rdfType= mod.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
								if (!es.getModelsubj().contains( ResourceFactory.createResource(objURI), null, (RDFNode) null )	
										) {
									row1[0]=objURI;
									row1[1]=myFormatter.format((int)(hop));
									writer.writeNext(row1);
									LOGGER.info(" gained concept "+ objURI);
								}

								mod.createResource(subjURI)
								.addProperty( jenap,mod.createResource(objURI))
								.addProperty( rdfType, mod.createResource("http://www.w3.org/2004/02/skos/core#Concept")) ;

								mod.createResource(objURI)
								.addProperty( rdfType, mod.createResource("http://www.w3.org/2004/02/skos/core#Concept")) ;

								cont++;
								visitAndComplementAndCount(y2uri, hop+1, k, objnmsp,
										subjnmsp, myFormatter, writer);


							}
						}
					}
				}
			}
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//sb.append(l);
			//sb.append("\n");
			LOGGER.info("NULL POINTER EXCEPTION l : "+l);
			LOGGER.info("NULL POINTER EXCEPTION hop : "+hop);
			LOGGER.info("NULL POINTER EXCEPTION e msm : "+ e.getMessage());
		}

	}


	
	
	
	
	private static boolean isInSkosPropertySet(String p) {

		return ( p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#narrower") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#related") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#broader"));
	}


	public static Map<String, String> processNSconversion(String fileNSConvert) {

		HashMap <String,String> result= new HashMap<String, String> ();
		if (fileNSConvert==null) return  result;
		//let's read the file and fill the MAp
		BufferedReader br;
		try {
			if (fileNSConvert!=null){
				br = new BufferedReader(new FileReader(fileNSConvert));

				String line;
				line = br.readLine();

				while (line != null) {
					String[] s= line.split("-->");
					result.put(s[0].trim(), s[1].trim());
					line = br.readLine();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return result;
	}



	private static void readEntryPointConceptsURIandDeletedConceptsURI(String fileentrypoint, 
			String filedeleted, String datasetNOTcompletenmsp, String datasetcompletenmsp ) {



		String line;
		String line1; 
		String line2;
		//let's read the file and fill the MAp
		BufferedReader br;
		try {
			if (fileentrypoint!=null){
				br = new BufferedReader(new FileReader(fileentrypoint));


				line = br.readLine();

				while (line != null) {

					line1= line.trim().replaceAll("\"", "");
					line2=line1.replaceAll("^"+datasetNOTcompletenmsp+"*", datasetcompletenmsp);
					setofIndexURIwithPathOfLenghtKandDeleted.add( line2);
					setofURIwithPathOfLenghtK.add(line2);
					line = br.readLine();

				}
			}

			if (filedeleted!=null){
				br = new BufferedReader(new FileReader(filedeleted));


				line = br.readLine();

				while (line != null) {
					line1= line.trim().replaceAll("\"", "");
					line2=line1.replaceAll("^"+datasetNOTcompletenmsp+"*", datasetcompletenmsp);
					setofIndexURIwithPathOfLenghtKandDeleted.add(line2);
					line = br.readLine();
				}
			}



		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * It reads from a CSV file the relation's weights 
	 * @param filePath
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String,Double> readWeight(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		String [] nextLine;
		HashMap <String, Double> res = new  HashMap <String, Double> ();
		while ((nextLine = reader.readNext()) != null) {
			// nextLine[] is an array of values from the line
			res.put(nextLine[0],  Double.valueOf(nextLine[1]));
		}
		return res;
	}	


}
