#creating the complementations 
set -x #echo on
cd ../../../

#testset2
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel10_AltLabel10.nt  -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test2/test2.1_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.1_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel10_AltLabel10.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.1_DatasetQuality.csv

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test2/test2.2_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.2_DatasetQuality.csv
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt  -objDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test2/test2.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test2/test2.2_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test2/test2.3_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.3_DatasetQuality.csv
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt  -objDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test2/test2.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test2/test2.3_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test2/test2.4_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.4_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -objDatasetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test2/test2.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test2/test2.4_importedValues


#testset3
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test3/test3.1_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.1_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.1_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test3/test3.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test3/test3.1_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test3/test3.2_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.2_DatasetQuality.csv

#cp  Examples/Benchmarking/GEMET/test3/test3.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test3/test3.2_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test3/test3.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test3/test3.2_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt  -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test3/test3.3_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test3/test3.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test3/test3.3_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test3/test3.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test3/test3.3_importedValues




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt  -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test3/test3.4_complementedDataset.rdf -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test3/test3.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test3/test3.4_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test3/test3.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test3/test3.4_importedValues



#testset4
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch10.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.1_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.1_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.1_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch30.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.2_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard

#cp  Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test4/test4.2_DatasetQuality.csv
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.2_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch30.nt  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.2_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.3_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test4/test4.3_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch50.nt  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.3_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.4_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test4/test4.4_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch90.nt  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.4_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.5_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test4/test4.5_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.nt  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.5_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.9.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test4/test4.6_complementedDataset.rdf   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test4/test4.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test4/test4.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test4/test4.6_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -objDatasetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.9.nt  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test4/test4.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test4/test4.6_importedValues


#testset5
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test5/test5.1_complementedDataset.rdf    -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.1_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.1_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.1_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in  Examples/Benchmarking/GEMET/test5/test5.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.2_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.2_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.3_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.3_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.4_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.4_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5._DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.5_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.6_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.6_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.7_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.7_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test5/test5.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test5/test5.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test5/test5.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test5/test5.8_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test5/test5.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test5/test5.8_importedValues



#testset6
#DIRECTORY test6.1: consiDering GEMET_it_es_en_linkset_exactMatch10.nt
#XXX_prefLabel90_AltLabel50
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.1_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.1_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.2_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.2_DatasetQuality.csv



#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.3_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.3_DatasetQuality.csv




#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.4_importedValues


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.5_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.6_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.6_DatasetQuality.csv




#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.7_importedValues



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.7_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.1/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.1/test6.8_importedValues

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.1/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.1/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6.8_DatasetQuality.csv



#test 6.2: considering GEMET_it_es_en_linkset_exactMatch30.nt

#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.1_complementedDatasetQuality.csv 
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.2_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.3_DatasetQuality.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.6_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.2/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.2/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.2/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.2/test6.8_DatasetQuality.csv






#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.1_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.2_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.3_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.4_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.5_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.6_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.7_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt    -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.2/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.2/test6.8_importedValues




#test3: considering GEMET_it_es_en_linkset_exactMatch50.nt


#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.1_complementedDatasetQuality.csv 
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.2_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.3_DatasetQuality.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.6_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.3/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.3/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.3/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.3/test6.8_DatasetQuality.csv




#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.1_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.2_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.3_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.4_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.5_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.6_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.7_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.3/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.3/test6.8_importedValues


#test6.4: considering GEMET_it_es_en_linkset_exactMatch90.nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.1_complementedDatasetQuality.csv 
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.2_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.3_DatasetQuality.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.6_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.4/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.4/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.4/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.4/test6.8_DatasetQuality.csv





#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.1_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.2_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.3_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.4_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.5_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.6_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.7_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.4/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.4/test6.8_importedValues





#test 6.5:considering GEMET_it_es_en_linkset_exactMatch99.nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.1_complementedDatasetQuality.csv 
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.2_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.3_DatasetQuality.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.6_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.8_DatasetQuality.csv







#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.1_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.2_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.3_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.4_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.5_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.6_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.7_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.5/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.5/test6.8_importedValues




#test 6.5:considering GEMET_it_es_en_linkset_exactMatch99.9nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.1_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.1_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.1_complementedDatasetQuality.csv 
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.1_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test3_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.2_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.2_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.2_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.2_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.2_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.3_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.3_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.3_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.3_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.3_DatasetQuality.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.4_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.4_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.4_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.4_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.1_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.4_DatasetQuality.csv


#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.5_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.5_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.5_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.5_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.1/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.1/test6._DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.6_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.6_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.6_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.6_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.6_DatasetQuality.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.6/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.6/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.6/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.6/test6.8_DatasetQuality.csv


#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.1_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.2.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.2_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.3.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.3_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.4.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.4_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.5.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.5_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.6.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.6_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.7.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.7_importedValues

#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -objDatasetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test6.6/test6.8.csv -importedStatementsPath Examples/Benchmarking/GEMET/test6.6/test6.8_importedValues


cd  Examples/Benchmarking/GEMET/
