package Quality;



import java.util.ArrayList;
import java.util.HashMap;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;


/** 
 * 
 * @author riccardoalbertoni
 *
 *this class extends DataSetLinksetState adding 
 *
 *-  the dumps specified by command line 
 *
 */

public class DataSetLinksetStateWithDumps extends DataSetLinksetState {

	//public HashMap <String, DatasetInfoWithVOID >mapOfDataset;
	//public ArrayList <LinksetInfoWithVOID> linksetInfoArray;

	/**
	 * Given the info about objs, subj, linkset, it initializes the DatasetLinksetStateWithDump
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception 
	 */
	public   DataSetLinksetStateWithDumps(String subjDump, String subjUriSpace, String  objDump, String objUriSpace, String linksetDump, String linkingProperty ) throws Exception {
		// let's considider all the linkset
		mapOfDataset=new HashMap <String, DatasetInfo> ();
		linksetInfoArray = new ArrayList <LinksetInfo>() ;
       
		// let 's read the dumps
		Model objdumpaux, subjdumpaux, linksetdumpaux;


		objdumpaux=org.apache.jena.riot.RDFDataMgr.loadModel(objDump); 
		subjdumpaux=org.apache.jena.riot.RDFDataMgr.loadModel(subjDump) ;
		linksetdumpaux=org.apache.jena.riot.RDFDataMgr.loadModel(linksetDump);
		
		Model model = ModelFactory.createDefaultModel(); //org.apache.jena.riot.RDFDataMgr.loadModel(objDump);
		model.add(objdumpaux);
		model.add(subjdumpaux);
		model.add(linksetdumpaux);
		
		
		
		// subject dataset
		DatasetInfoWithDump subjDataset=null;
		DatasetInfoWithDump objDataset=null;

		subjDataset = new DatasetInfoWithDump();

		// uriPattern
		subjDataset.UriSpace=subjUriSpace;
		subjDataset.consolidatedUriSpace=subjUriSpace;
		subjDataset.dump= model;
		subjDataset.subject=true;
		subjDataset.objdump=subjdumpaux;

		String ssubj=subjDump.substring(subjDump.lastIndexOf("/")+1); 
		mapOfDataset.put(ssubj, subjDataset);

		
		// object dataset
		objDataset=  new DatasetInfoWithDump();
		objDataset.UriSpace=objUriSpace; 
		objDataset.consolidatedUriSpace=objUriSpace;
		String sobj=objDump.substring(objDump.lastIndexOf("/")+1); 
		objDataset.dump=model;
		objDataset.objdump=objdumpaux;

		mapOfDataset.put(sobj, objDataset);
		
		//linkset
		
		String slinkset=linksetDump.substring(linksetDump.lastIndexOf("/")+1); 
		LinksetInfoWithDump linkset=new LinksetInfoWithDump();
		// other intersting thing about the linkeset
		// void:linkPredicate
		linkset.dumpOfLinkset=linksetdumpaux;
		linkset.dumpOfLinksetSbjObj=model;
		linkset.voidlinksetPredicate=linkingProperty;
		linkset.name=ssubj +"_"+sobj+"_"+slinkset+"_"+linkingProperty.substring(linkingProperty.lastIndexOf("/")+1);
		linkset.subjectLocalName=subjDump.substring(subjDump.lastIndexOf("/")+1);
		linkset.objectLocalName=objDump.substring(objDump.lastIndexOf("/")+1);
		linksetInfoArray.add(linkset);
		
	}
	// properties to consider 

	//		public String sparqlEndPoint;
	//		
	//		public String UriSpace;
	//		public String consolidatedUriSpace;
	//		
	//		public String lastSparqlAccessProblem="";
	//		public int lastResponseCode; // the http code returned by the last query done on the dataset sparql end point
	//		
	//		public int availabilityResponseCode=0;
	//		public String availabilitySparqlAccessProblem="";
	//		


}





