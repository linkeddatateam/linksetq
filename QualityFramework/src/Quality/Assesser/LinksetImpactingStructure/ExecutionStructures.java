package Quality.Assesser.LinksetImpactingStructure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;

import Jama.Matrix;
import Quality.SparqlEndPointErrorMessage;





/**
 * @author Paola
 * 
 * Represented information 
 * AWM
 * Represent at global level of a whole linkset 
 * this set of information: <p,<(n1,n2), (n1,n4).....>> where: 
 * p is a particular skos property 
 * (nx,ny) is an Edge labelled with the property p, this means  
 * that in the analyzed graph  there is a triple (nx p ny). 
 *  * p is the same key (the skos property) of the structure W 
 * that represents the weighte associated to each skos property
 * 
 * 
 *FAWM
 *flattened weighted matrix
 *
 *
 *Weight
 *set of weight
 *
 *URIMatrixIndex
 *all the skos property (both literal and URI) existing 
 *in the considered subgraph
 *
 *RNSh
 *set of URI considered for each hop 
 *<h, list of uri encountered at hop h> 
 *private ArrayList<String> RNSh=new ArrayList<String>()
 *
 *
 *CM (Centrality Measure)
 *list that to each link in the considered linkset 
 *associates a number representing the evaluation of 
 *our measure of centrality 
 * 
 * 
 * powerM
 * list of all the powermatrix calculated from 1 to k hop
 * 
 * 
 * 
 * VNMlist
 * list of all the visited node matrix calculated from 1 to k hop
 *
 * PathDeleted
 * number of path of lenght k effectively created
 *
 *ATTENTION: we suppose to consider a number k of hops at least (GREAT or EQUAL) >= 1
 *we do not consider K=0
 *Because we start to calculate the number of hops from the nodes 
 *object of the linkset, belonging to the object dataset. 
 *And the hop K=1 consider the nodes in object dataset reachable from 
 *those belonging to the linkset
 *
 *
 *
 *
 */




public class ExecutionStructures {


	private Model modelsubj=null;
	private Model modelobj=null;
	private Model modellinks=null; 
	private Model all=null;

	private HashMap<Integer, ArrayList<Integer>> linksoflinksetwithpathk=new HashMap<Integer, ArrayList<Integer>>();
	private int numberofpathwithlengthktocreate=0;
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName());
	private int numberconceptsofsubject=0;
	private int numberconceptsofobject=0;



	//set of all visited skos:propertes (prefLabel, broader...) 
	private ArrayList<String> URIMatrixIndex=new ArrayList<String>();
	//set of uri already visited 
	private ArrayList<String> VisitedURI=new ArrayList<String>();
	//flattened weighted matrix
	private Matrix FAWM; 
	//set of weight 
	HashMap<String, Double> Weight= new HashMap<String, Double>();
	//set containing all the skos properties encountered 
	//in k hops
	private HashMap<String, HashSet<RDFEdge>> AWM= new HashMap<String, HashSet<RDFEdge>>(); 
	//set containing all the skos property 
	//encountered in the subgraph considered at each hop
	//<h, list of (properties, uri) encountered at hop h 
	private ArrayList<HashSet<RDFEdge>> RNSh=new ArrayList<HashSet<RDFEdge>>(); 
	
	//list of concepts and their adjacent concepts + rel <URIconcept, <relation,Uriconcept>>
	//consider only URI and only weighted relations
	private HashMap<String, HashSet<RelationAdjacentConcept>> AWL= new HashMap<String, HashSet<RelationAdjacentConcept>>();


	//table containing for each link in the considered linkset 
	//the evaluation of the centrality measure 


	private HashMap<String, Double> CM= new HashMap<String, Double>();

	//contains all the power matrix from 0 to k hops
	private ArrayList<Matrix> powerM=new ArrayList<Matrix>();


	//contains all the visited nodes from 0 to k hops
	private ArrayList<Matrix> VNMlist=new ArrayList<Matrix>();

	//contains the number of all the URI concepts in the dataset considered
	private int totalConcepts=0;


	//contains the linkset in input
	ArrayList<String> linkset=new ArrayList<String>();



	public int getNumberconceptsofsubject() {
		return numberconceptsofsubject;
	}



	public int getNumberconceptsofobject() {
		return numberconceptsofobject;
	}



	public void setNumberconceptsofobject(int numberconceptsofobject) {
		this.numberconceptsofobject = numberconceptsofobject;
	}



	public ExecutionStructures (){

	} 



	public  Model getModelsubj() {
		return modelsubj;
	}



	public  void setModelsubj(Model modelsubj) {
		this.modelsubj = modelsubj;
	}



	public  Model getModelobj() {
		return modelobj;
	}



	public  void setModelobj(Model modelobj) {
		this.modelobj = modelobj;
	}



	public  Model getModellinks() {
		return modellinks;
	}



	public  void setModellinks(Model modellinks) {
		this.modellinks = modellinks;
	}



	public  Model getAll() {
		return all;
	}



	public  void setAll(Model all) {
		this.all = all;
	}



	public  HashMap<Integer, ArrayList<Integer>> getLinksoflinksetwithpathk() {
		return linksoflinksetwithpathk;
	}



	public  void setLinksoflinksetwithpathk(
			HashMap<Integer, ArrayList<Integer>> linksoflinksetwithpathk) {
		this.linksoflinksetwithpathk = linksoflinksetwithpathk;
	}



	public  int getNumberofpathwithlengthktocreate() {
		return numberofpathwithlengthktocreate;
	}



	public  void setNumberofpathwithlengthktocreate(
			int numberofpathwithlengthktocreate) {
		this.numberofpathwithlengthktocreate = numberofpathwithlengthktocreate;
	}



	public  ArrayList<String> getLinkset() {
		return linkset;
	}



	public  void setLinkset(ArrayList<String> linkset) {
		this.linkset = linkset;
	}



	public void addTotalConcepts(int value) {
		this.totalConcepts =this.totalConcepts +value;
	}

	public int getTotalConcepts() {
		return totalConcepts;
	}





	public void setTotalConcepts(int totalConcepts) {
		this.totalConcepts = totalConcepts;
	}





	public ArrayList<Matrix> getVNMlist() {
		return VNMlist;
	}






	public void setVNMlist(ArrayList<Matrix> vNMlist) {
		VNMlist = vNMlist;
	}





	//calcola per ogni passo da 1 a k+1 il totale 
	//intermedio della somma delle powermatrix 
	//e cioè tutti i nodi vistati al passo i con i =1...k+1
	public void initializeVNM(int k, ArrayList<Matrix> pm) 
	{
		Matrix AFlataux=pm.get(1);
		VNMlist.add(0,null);
		VNMlist.add(1,AFlataux);
		for(int i=2; i<=k; i++)
		{
			AFlataux=AFlataux.plus(pm.get(i));
			VNMlist.add(i,AFlataux);

		}
	}

	
	
	
	
	
	
	
	//restricted to relation between object not considering literal value 
	//create a list of (relation, adjacent nodes) ALM instead of the whole AWM matrix

	public void extractALRestrict_isURI_ric(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
						+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		
		boolean trovato=false;

		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{

				
				if(l.equalsIgnoreCase("http://yyy/10058"))
				{
					trovato=true;
				}

				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops
				//structure necessary to collect and analyse the results
				String p=" ";
				String y2uri=" ";

				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();

				//set of next uris to consider 
				HashSet<String> nu;
				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		
				for (int hop=1; hop<=k-1; hop++)
				{
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<=getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/4633"))
							{
								trovato=true;
							}
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/2441"))
							{
								trovato=true;
							}
							if( (!getVisitedURI().contains(e.getObject())&& 
									!linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;


							do{
								qs="SELECT ?p ?y2 "
										+"\n where {  "
										+" <"+	l +"> ?p ?y2. "
										/* ADDBYRIC, we don't need literals */ 
										+" filter (isURI(?y2))"
										+"}  "
										+"LIMIT "+ limit
										+" OFFSET "+ offset +"\n";

								///execute the query
								
								if(l.equalsIgnoreCase("http://yyy/4633"))
								{
									trovato=true;
								}
								
								if(l.equalsIgnoreCase("http://yyy/2441"))
								{
									trovato=true;
								}

								SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
								//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));

								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;			

							}while (resP.size()>0);


							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {


									
									qsol=resP2.next();

									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);

									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();
										if(y2uri.equalsIgnoreCase("http://yyy/4633"))
										{ 
											trovato=true;
										}	
										if(y2uri.equalsIgnoreCase("http://yyy/2441"))
										{
											trovato=true;
										}										
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

											//control if the uri belongs to the correct urispace
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
											//System.out.println("uri EARTH  : "+ y2uri);
											//controls if the uri is already present in the 
											//accessed uri

											if(isURIinRNSh(y2uri,p))
											{
												//System.out.println("property: "+ p);
												//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

												//control the hop number this uri has been discovered
												//is greater than the actual hop
												if(hop<getMinHopOfURIinRNSh(y2uri,p))
												{
													//insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
													insertEdgeinAWL(new RelationAdjacentConcept(p, y2uri),l);
													if(y2uri.trim().matches(nmspc+"\\w*"))
													{
														insertUriinRNSh(y2uri, hop,p);
														//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
														//System.out.println("URI: "+ y2uri);
														insertURIinURIMatrixIndex(y2uri);
													}
												}
											}
											else{
												//the uri has been not visited until now
												//System.out.println("property: "+ p);
												//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
												//insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
												insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													insertURIinURIMatrixIndex(y2uri);
												}
												//System.out.println("URI contains: "+ URIactual.contains(y2uri));
												//System.out.println("URI: "+ y2uri);
											}
										
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											//insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
											insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
										}

								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());

						}
					}//if
				}//hop
			}//linkset
		}
	}	
	
	
	

	//restricted to relation between object not considering literal value 
	//create a list of (relation, adjacent nodes) ALM instead of the whole AWM matrix

	public void extractALRestrict_isURI_simple(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
						+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		
		boolean trovato=false;

		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{

				if(l.equalsIgnoreCase("http://yyy/10058"))
				{
					trovato=true;
				}

				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops
				//structure necessary to collect and analyse the results
				String p=" ";
				String y2uri=" ";

				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();

				//set of next uris to consider 
				HashSet<String> nu;
				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		
				for (int hop=1; hop<=k; hop++)
				{
					LOGGER.info("HOP: "+hop);
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<=getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/9195"))
							{
								trovato=true;
							}
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/12189"))
							{
								trovato=true;
							}
							if( (!getVisitedURI().contains(e.getObject())&& 
									!linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;


							do{
								qs="SELECT ?p ?y2 "
										+"\n where {  "
										+" <"+	l +"> ?p ?y2. "
										/* ADDBYRIC, we don't need literals */ 
										+" filter (isURI(?y2))"
										+"}  "
										+"LIMIT "+ limit
										+" OFFSET "+ offset +"\n";

								///execute the query
								
								if(l.equalsIgnoreCase("http://yyy/4633"))
								{
									trovato=true;
								}
								
								if(l.equalsIgnoreCase("http://yyy/2441"))
								{
									trovato=true;
								}

								SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
								//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));

								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;			

							}while (resP.size()>0);


							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {


									
									qsol=resP2.next();

									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);

									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();
										if(y2uri.equalsIgnoreCase("http://yyy/4633"))
										{ 
											trovato=true;
										}	
										if(y2uri.equalsIgnoreCase("http://yyy/2441"))
										{
											trovato=true;
										}										
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

											//control if the uri belongs to the correct urispace
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
											//System.out.println("uri EARTH  : "+ y2uri);
											//controls if the uri is already present in the 
											//accessed uri

											if(isURIinRNSh(y2uri,p))
											{
												if(y2uri.equalsIgnoreCase("http://yyy/4633"))
												{
													trovato=true;
												}
												
												if(y2uri.equalsIgnoreCase("http://yyy/2441"))
												{
													trovato=true;
												}
												//System.out.println("property: "+ p);
												//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

												//control the hop number this uri has been discovered
												//is greater than the actual hop
												if(hop<getMinHopOfURIinRNSh(y2uri,p))
												{
													//insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
													insertEdgeinAWL(new RelationAdjacentConcept(p, y2uri),l);
													if(y2uri.trim().matches(nmspc+"\\w*"))
													{
														insertUriinRNSh(y2uri, hop,p);
														//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
														//System.out.println("URI: "+ y2uri);
														insertURIinURIMatrixIndex(y2uri);
													}
												}
											}
											else{
												if(y2uri.equalsIgnoreCase("http://yyy/4633"))
												{
													trovato=true;
												}
												
												if(y2uri.equalsIgnoreCase("http://yyy/2441"))
												{
													trovato=true;
												}
												//the uri has been not visited until now
												//System.out.println("property: "+ p);
												//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
												//insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
												insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													insertURIinURIMatrixIndex(y2uri);
												}
												//System.out.println("URI contains: "+ URIactual.contains(y2uri));
												//System.out.println("URI: "+ y2uri);
											}
										
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											//insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
											insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
										}

								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());

						}
					}//if
				}//hop
			}//linkset
		}
	}

	//restricted to relation between object not considering literal value 

	public void extractAMRestrict_isURI(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
						+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		

		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{

				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops
				//structure necessary to collect and analyse the results
				String p=" ";
				String y2uri=" ";

				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();

				//set of next uris to consider 
				HashSet<String> nu;
				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		
				for (int hop=1; hop<=k; hop++)
				{
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<=getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							if( (!getVisitedURI().contains(e.getObject())&& 
									!linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;

							do{
								qs="SELECT ?p ?y2 "
										+"\n where {  "
										+" <"+	l +"> ?p ?y2. "
										/* ADDBYRIC, we don't need literals */ 
										+" filter (isURI(?y2))"
										+"}  "
										+"LIMIT "+ limit
										+" OFFSET "+ offset +"\n";

								///execute the query

								SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
								//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));

								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;			

							}while (resP.size()>0);


							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {


									qsol=resP2.next();

									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);

									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();

											//System.out.println("property: "+ p);
											//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

											//control if the uri belongs to the correct urispace
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
											//System.out.println("uri EARTH  : "+ y2uri);
											//controls if the uri is already present in the 
											//accessed uri
											if(isURIinRNSh(y2uri,p))
											{
												//System.out.println("property: "+ p);
												//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

												//control the hop number this uri has been discovered
												//is greater than the actual hop
												if(hop<getMinHopOfURIinRNSh(y2uri,p))
												{
													insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
													if(y2uri.trim().matches(nmspc+"\\w*"))
													{
														insertUriinRNSh(y2uri, hop,p);
														//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
														//System.out.println("URI: "+ y2uri);
														insertURIinURIMatrixIndex(y2uri);
													}
												}
											}
											else{
												//the uri has been not visited until now
												//System.out.println("property: "+ p);
												//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
												insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													insertURIinURIMatrixIndex(y2uri);
												}
												//System.out.println("URI contains: "+ URIactual.contains(y2uri));
												//System.out.println("URI: "+ y2uri);
											}
										
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
										}

								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());

						}
					}//if
				}//hop
			}//linkset
		}
	}



	//partendo dai nodi del linkset 
	//per ciascuno di essi l,  
	//considering all the property reachable from link l in k hops
	//controlli nodo del risultato: 
	//se non è già visitato e non è nel linkset di partenza
	//strutture riempite: 
	//- URIMatrixIndex array di Uri corrispondente indice della matrice di adiacenza (nel nostro 
	//caso siccome )
	//- RNSh insieme  dei nodi raggiungibili al passo k senza sapere da che l sto partendo
	//- la multirelation matrice AWM

	public void extractAM(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
					+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		

		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{

				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops

				//structure necessary to collect and analyse the results


				String p=" ";
				String y2uri=" ";

				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();


				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();


				//set of next uris to consider 

				HashSet<String> nu;

				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		

				for (int hop=1; hop<=k; hop++)
				{
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<=getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							if( (!getVisitedURI().contains(e.getObject())&& !linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;

							qs="SELECT ?p ?y2"
									+"\n where {  "
									+" <"+	l +"> ?p ?y2. "
									//+"FILTER ( regex(str(?y2), \"^"+nmspc+"\",\"i\") ) "								
									+"} LIMIT "+ limit
									+" OFFSET "+ offset +"\n";

							//System.out.println("query \n"+qs);

							///execute the query

							SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
							//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
							resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));

							//ResultSetFormatter.out(resP);
							resP.reset();
							//in order to consider all the resP 
							//over the iteration considering the offset 
							//we save a list of results
							listresP.add(resP);
							offset=offset+limit;			


							//collecting all the portion of the final result (using offset and limit) 
							//from the dataset : the result is made by pair (?property ?object) 
							//?property is a skos property attached to link l 
							//?object is the value of the skos property: a literal or a uri
							//we consider now all the skos:prop attached to l 
							while (resP.size()>0)
							{
								//repeat the query untill the result it is empty 
								//in order to keep all the result
								//create the query considering offset and limit

								qs="SELECT ?p ?y2"
										+"\n where {  "
										+" <"+	l +"> ?p ?y2 \n"
										//+"FILTER ( regex(str(?y2), \"^"+nmspc+"\",\"i\") )"
										+"} LIMIT "+ limit
										+" OFFSET "+ offset+"\n";


								//System.out.println(" query :\n"+qs);
								///execute the query
								//query = QueryFactory.create(qs);
								err2=new SparqlEndPointErrorMessage();

								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2,null, false));

								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;
							}


							//now we have the entire result 
							//for the number "hop" of step 
							//we analyse the result in order 
							//to fill the structure necessary for the execution

							//procedure to load the result in the adjacence matrix
							//and the new links to follow in the next hop

							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {


									qsol=resP2.next();

									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);

									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();


										//System.out.println("property: "+ p);
										//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

										//control if the uri belongs to the correct urispace
										//System.out.println("property: "+ p);
										//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
										//System.out.println("uri EARTH  : "+ y2uri);
										//controls if the uri is already present in the 
										//accessed uri
										if(isURIinRNSh(y2uri,p))
										{
											//System.out.println("property: "+ p);
											//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

											//control the hop number this uri has been discovered
											//is greater than the actual hop
											if(hop<getMinHopOfURIinRNSh(y2uri,p))
											{
												insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
													//System.out.println("URI: "+ y2uri);
													insertURIinURIMatrixIndex(y2uri);
												}
											}
										}
										else{
											//the uri has been not visited until now
											//System.out.println("property: "+ p);
											//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
											insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
											if(y2uri.trim().matches(nmspc+"\\w*"))
											{
												insertUriinRNSh(y2uri, hop,p);
												insertURIinURIMatrixIndex(y2uri);
											}
											//System.out.println("URI contains: "+ URIactual.contains(y2uri));
											//System.out.println("URI: "+ y2uri);
										}
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
										}

								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());

						}
					}//if
				}//hop
			}//linkset
		}
	}








	public void extractAM2(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza

		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
					+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		

		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{

				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops

				//structure necessary to collect and analyse the results


				String p=" ";
				String y2uri=" ";

				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();


				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();


				//set of next uris to consider 

				HashSet<String> nu;

				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		

				for (int hop=1; hop<=k; hop++)
				{
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							if( (!getVisitedURI().contains(e.getObject())&& !linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;

							qs="SELECT ?p ?y2"
									+"\n where {  "
									+" <"+	l +"> ?p ?y2. "
									//+"FILTER ( regex(str(?y2), \"^"+nmspc+"\",\"i\") ) "								
									+"} LIMIT "+ limit
									+" OFFSET "+ offset +"\n";

							//System.out.println("query \n"+qs);

							///execute the query

							SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
							//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
							resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));

							//ResultSetFormatter.out(resP);
							resP.reset();
							//in order to consider all the resP 
							//over the iteration considering the offset 
							//we save a list of results
							listresP.add(resP);
							offset=offset+limit;			


							//collecting all the portion of the final result (using offset and limit) 
							//from the dataset : the result is made by pair (?property ?object) 
							//?property is a skos property attached to link l 
							//?object is the value of the skos property: a literal or a uri
							//we consider now all the skos:prop attached to l 
							while (resP.size()>0)
							{
								//repeat the query untill the result it is empty 
								//in order to keep all the result
								//create the query considering offset and limit

								qs="SELECT ?p ?y2"
										+"\n where {  "
										+" <"+	l +"> ?p ?y2 \n"
										//+"FILTER ( regex(str(?y2), \"^"+nmspc+"\",\"i\") )"
										+"} LIMIT "+ limit
										+" OFFSET "+ offset+"\n";


								//System.out.println(" query :\n"+qs);
								///execute the query
								//query = QueryFactory.create(qs);
								err2=new SparqlEndPointErrorMessage();

								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2,null, false));

								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;
							}


							//now we have the entire result 
							//for the number "hop" of step 
							//we analyse the result in order 
							//to fill the structure necessary for the execution

							//procedure to load the result in the adjacence matrix
							//and the new links to follow in the next hop

							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {


									qsol=resP2.next();

									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);

									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();


										//System.out.println("property: "+ p);
										//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

										//control if the uri belongs to the correct urispace
										//System.out.println("property: "+ p);
										//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
										//System.out.println("uri EARTH  : "+ y2uri);
										//controls if the uri is already present in the 
										//accessed uri
										if(isURIinRNSh(y2uri,p))
										{
											//System.out.println("property: "+ p);
											//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

											//control the hop number this uri has been discovered
											//is greater than the actual hop
											if(hop<getMinHopOfURIinRNSh(y2uri,p))
											{
												insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
													//System.out.println("URI: "+ y2uri);
													insertURIinURIMatrixIndex(y2uri);
												}
											}
										}
										else{
											//the uri has been not visited until now
											//System.out.println("property: "+ p);
											//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
											insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
											if(y2uri.trim().matches(nmspc+"\\w*"))
											{
												insertUriinRNSh(y2uri, hop,p);
												insertURIinURIMatrixIndex(y2uri);
											}
											//System.out.println("URI contains: "+ URIactual.contains(y2uri));
											//System.out.println("URI: "+ y2uri);
										}
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
										}

								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());

						}
					}//if
				}//hop
			}//linkset
		}
	}








	//extract URI of linkset entry points of browsing

	public void extractURILinkofLinkset(String flag, String objnmsp, String subjnmsp, String predlinkset)
	{

		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	

		String nmspc="";

		//I want to delete the path in the object 
		//to create path in the subject
		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim();
			//i consider all the node of the object 
			//involved in the linkset
			qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?x "
					+"WHERE  { ?y "+predlinkset+" ?x "
					+"FILTER ( regex(str(?x),\"^"+objnmsp+"\",\"i\") ) } "
					+" order by ?x ";
		}
		//I want to delete the path in the subject 
		//to create path in the object		
		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim();
			//i consider all the node of the subject  
			//involved in the linkset			
			qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?x "
					+"WHERE  { ?x "+predlinkset+" ?y "
					+"FILTER ( regex(str(?x), \"^"+subjnmsp+"\",\"i\") ) }"
					+" order by ?x ";

		}

		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
					+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);

		}		

		if(!flag.equalsIgnoreCase("all"))
		{
			//find all the exactMatch in the linkset
			qs1=qs+
					"LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;
			LOGGER.info("query to select link in linkset "+qs1);

			SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(modellinks, qs1, err2, null));
			resP.reset();
			alllinkset.add(resP);
			while (resP.size()>0)
			{
				offset=offset+limit;
				qs1=qs+" limit "+ limit+" offset "+ offset;
				LOGGER.info("query to select link in linkset "+qs);

				err2=new SparqlEndPointErrorMessage();
				resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(modellinks, qs1, err2, null));
				resP.reset();	
				alllinkset.add(resP);
			}



			//start to visit each link of linkset
			//to create the set of node 
			//of the linkset

			if(!alllinkset.isEmpty())
			{

				for(ResultSetRewindable resP2:alllinkset)
				{

					while(resP2.hasNext()) {
						qsol=resP2.next();
						yuri=qsol.getResource("?x").getURI().trim();
						linkset.add(yuri);

					}
				}
			}
		}

	}

	public void calculateNumberofPathWithKhopAndTheirLinkOfLinkset2(double perc, int k, Model m, String Mnmsp)
	{//TODO
		//versione due perchè la uno dava numeri poco 
		//sensati perchè considerando tutti i possibili cammini 
		//eravamo in ordine di migliaia.... 
		//quindi in questo caso il numero di cammini 
		//viene calcolato come numeroconcettidatasetconsiderato/numerohops
		int numofconcepts=-1;
		int numofpossiblepathlingthk=-1;

		numofconcepts=UtilityFunctionsDegreeAssessment.countNumofConceptinModel(m, Mnmsp);
		numofpossiblepathlingthk= (numofconcepts/(k+1)); 

		numberofpathwithlengthktocreate=(int)((numofpossiblepathlingthk*perc)/100);

		Matrix onlyleavesk=getPowerM().get(k+1);
		Matrix allintermediatek=getVNMlist().get(k);


		for(int i=0; i<onlyleavesk.getColumnDimension(); i++)
		{
			for(int j=0; j<onlyleavesk.getColumnDimension(); j++)
			{

				if(onlyleavesk.get(i, j)==1)
				{
					//recupero gli URI che hanno path di lunghezza k
					// aggiungo la coppia indicenodosubj, indicenodoobj
					if(!linksoflinksetwithpathk.containsKey(i))
					{
						ArrayList<Integer> aux=new ArrayList<Integer>();
						linksoflinksetwithpathk.put(i,aux);
					}

				}

			}
		}	
		for(int nn: linksoflinksetwithpathk.keySet())
		{
			for(int i=0;i<allintermediatek.getColumnDimension(); i++)
				if(allintermediatek.get(nn, i)==1)
				{
					ArrayList<Integer> aux=new ArrayList<Integer>();
					aux=linksoflinksetwithpathk.get(nn);
					aux.add(i);
					linksoflinksetwithpathk.put(nn,aux);
				}
		}



	}	


	public void calculateNumberofPathWithKhopAndTheirLinkOfLinkset(double perc, int k)
	{//TODO
		//calcolo il numero di cammini di lunghezza k che devo 
		//considerare in base al numero esistente 
		//dei nodi raggiungibili a lunghezza k+1
		//calcolo anche i link del linkset da cui partono 
		//questi path così considero direttamente 
		//questi e non gli altri


		//solo foglie dei cammini k
		Matrix onlyleavesk=getPowerM().get(k+1);
		Matrix allintermediatek=getVNMlist().get(k);
		int count=0;

		for(int i=0; i<onlyleavesk.getColumnDimension(); i++)
		{
			for(int j=0; j<onlyleavesk.getColumnDimension(); j++)
			{
				count+=onlyleavesk.get(i, j);
				if(onlyleavesk.get(i, j)==1)
				{
					//recupero gli URI che hanno path di lunghezza k
					// aggiungo la coppia indicenodosubj, indicenodoobj
					if(!linksoflinksetwithpathk.containsKey(i))
					{
						ArrayList<Integer> aux=new ArrayList<Integer>();
						linksoflinksetwithpathk.put(i,aux);
					}

				}

			}
		}		
		//il numero dei cammini da creare lo calcolo 
		//in percentuale solo sul numero di cammini di lunghezza k
		numberofpathwithlengthktocreate=(int)((count*perc)/100);

		for(int nn: linksoflinksetwithpathk.keySet())
		{
			for(int i=0;i<allintermediatek.getColumnDimension(); i++)
				if(allintermediatek.get(nn, i)==1)
				{
					ArrayList<Integer> aux=new ArrayList<Integer>();
					aux=linksoflinksetwithpathk.get(nn);
					aux.add(i);
					linksoflinksetwithpathk.put(nn,aux);
				}
		}

	}	

	public  void initializeMatrices(int k)
	{//TODO
		//ATTENZIONE! L'esponente della power matrix è sempre h= K+1 con K hops!!
		//Infatti 
		//per k=0, considero i nodi "possibilmente raggiungibili" dalla posizione di 0 click cioè attaccati direttamente al nodo appartenente 
		//al linkset la matrice di adiacenza normale cioè esponente h= k+1=0+1=1
		//per k=1, considero i nodi "possibilmente raggiungibili" dalla posizione di 1 click cioè attaccati direttamente al 
		//nodo raggiungibile con 1 click quindi esponente h= k+1= 1+1=2 

		initilizeMatrix();
		flatMatrixFunction("MAX");
		initializePowerMatrix(k+1, getFAWM()); 
		initializeVNM(k,getPowerM());
	}	

	public  void initializeMatrices2(int k)
	{//TODO
		//ATTENZIONE! L'esponente della power matrix è sempre h= K+1 con K hops!!
		//Infatti 
		//per k=0, considero i nodi "possibilmente raggiungibili" dalla posizione di 0 click cioè attaccati direttamente al nodo appartenente 
		//al linkset la matrice di adiacenza normale cioè esponente h= k+1=0+1=1
		//per k=1, considero i nodi "possibilmente raggiungibili" dalla posizione di 1 click cioè attaccati direttamente al 
		//nodo raggiungibile con 1 click quindi esponente h= k+1= 1+1=2 

		initilizeMatrix();
		flatMatrixFunction("MAX");
		initializePowerMatrix(k+1, getFAWM()); 

	}		



	public  void calculateTripleToAdd(int k)
	{//TODO
		//calcola le triple da aggiungere nel complementato 
		//quindi devo considerare tutti gli hop da 0 a k+1

		initilizeMatrix();
		flatMatrixFunction("MAX");
		initializePowerMatrix(k+1, getFAWM());
		//così ho una matrice in cui sommo tutti i nodi 
		//raggiungibili da 0 a k hops da aggiungere 
		//come triple nel complementato 
		initializeVNM(k+1,getPowerM());
	}


	public void  VisitedURI(){
		for(int i=0;i<VisitedURI.size(); i++)
		{
			System.out.print("["+i+"]"+" \n"+VisitedURI.get(i).toString());
			System.out.print("\n");		
		}
	}



	public void  toStringURIMatrixIndex(){

		System.out.print("Total URI considered :\n[");
		for(String s: URIMatrixIndex )
		{

			System.out.print(s);
			System.out.print("\n");
		}
		System.out.print("]");
	}



	public void  toStringRNSh(){
		for(int i=0;i<RNSh.size(); i++)
		{
			System.out.print("["+i+"]"+" \n"+RNSh.get(i).toString());
			System.out.print("\n");		
		}
	}



	public void  toStringAWM(){
		for(String s:AWM.keySet() )
		{
			System.out.print("["+s+"] :");
			for(RDFEdge e: AWM.get(s))
				System.out.print(e.toString());
			System.out.print("\n");
		}
	}
	
	

	public void  toStringALW(){
		for(String s:AWL.keySet() )
		{
			System.out.print("["+s+"] :");
			for(RelationAdjacentConcept e: AWL.get(s))
				System.out.print(e.getRelation() + " "+e.getObject());
			System.out.print("\n");
		}
	}

	public void  toStringFAWM(){
		//output per righe 

		System.out.print("                                                  ");
		for(int i=0; i<FAWM.getColumnDimension();i++)

		{

			System.out.print(" "+URIMatrixIndex.get(i));


		}
		System.out.print("\n");
		for(int j=0;j<FAWM.getRowDimension();j++)
		{
			System.out.print("["+URIMatrixIndex.get(j)+"]:");

			for(int i=0; i<FAWM.getColumnDimension();i++)
			{

				System.out.print( "    "+FAWM.get(j, i));

			}
			System.out.print("\n");

		}
	}



	public ArrayList<String> getURIMatrixIndex() {
		return URIMatrixIndex;
	}







	public void setURIMatrixIndex(ArrayList<String> uRIMatrixIndex) {
		URIMatrixIndex = uRIMatrixIndex;
	}







	public Matrix getFAWM() {
		return FAWM;
	}







	public void setFAWM(Matrix fAWM) {
		FAWM = fAWM;
	}







	public HashMap<String, Double> getWeight() {
		return Weight;
	}







	public void setWeight(HashMap<String, Double> weight) {
		Weight = weight;
	}







	public HashMap<String, HashSet<RelationAdjacentConcept>> getAWL() {
		return AWL;
	}



	public void setAWL(HashMap<String, HashSet<RelationAdjacentConcept>> aWL) {
		AWL = aWL;
	}



	public HashMap<String, HashSet<RDFEdge>> getAWM() {
		return AWM;
	}







	public void setAWM(HashMap<String, HashSet<RDFEdge>> aWM) {
		AWM = aWM;
	}







	public ArrayList<HashSet<RDFEdge>> getRNSh() {
		return RNSh;
	}







	public void setRNSh(ArrayList<HashSet<RDFEdge>> rNSh) {
		RNSh = rNSh;
	}







	public HashMap<String, Double> getCM() {
		return CM;
	}







	public void setCM(HashMap<String, Double> cM) {
		CM = cM;
	}








	//Create the flat matrix
	public void  initilizeMatrix(){


		FAWM=new Matrix(URIMatrixIndex.size(),URIMatrixIndex.size());

	}

	//
	//The flattening function  F is defined as follows:F(A)i,j = maxr(Ai,j,r) for i,j= 1, . . ., n and r in L
	//here we consider the max function but in the methods the type of function used is parametric
	public Matrix getMatrixAWM()
	{
		HashSet<RDFEdge> edges;
		int indexr;
		int indexc;
		double weight;
		Matrix AWMaux=new Matrix(URIMatrixIndex.size(),URIMatrixIndex.size());
		//qui dovrei mettere un IF per distinguere i possibili casi 
		//della funzione funct in questo caso considero il MAX

		for(String p:AWM.keySet())
		{//RICSUGGESTSTOFIX perché  due if separati con la stessa condizione ?!? Facciamoli diventare uno 
			//p � la propriet�, mi serve per cercare nel vettore dei pesi
			edges=AWM.get(p);
			for(RDFEdge e: edges)
			{
				if (URIMatrixIndex.contains(e.getSubject().trim()) && 
						URIMatrixIndex.contains(e.getObject().trim()))
				{
					//trasformo gli uri dei subj/obj in indici della matriceflat			
					indexr=URIMatrixIndex.indexOf( e.getSubject().trim()); 
					indexc=URIMatrixIndex.indexOf( e.getObject().trim());

					AWMaux.set(indexr, indexc,1);
				}
			}

		}
		return AWMaux;
	}	





	//
	//The flattening function  F is defined as follows:F(A)i,j = maxr(Ai,j,r) for i,j= 1, . . ., n and r in L
	//here we consider the max function but in the methods the type of function used is parametric
	public Matrix flatMatrixFunction(String funct)
	{
		HashSet<RDFEdge> edges;
		int indexr;
		int indexc;
		double weight;
		//qui dovrei mettere un IF per distinguere i possibili casi 
		//della funzione funct in questo caso considero il MAX
		if (funct.equalsIgnoreCase("MAX"))
		{
			for(String p:AWM.keySet())
			{//RICSUGGESTSTOFIX perché  due if separati con la stessa condizione ?!? Facciamoli diventare uno 
				//p � la propriet�, mi serve per cercare nel vettore dei pesi
				if(Weight.containsKey(p))
				{
					weight= Weight.get(p).doubleValue(); 
					//System.out.println("weight: " +weight+ " p: "+ p+"\n");

				}
				else {
					weight=0.0;
				}

				//
				if(Weight.containsKey(p))
				{
					edges=AWM.get(p);
					for(RDFEdge e: edges)
					{
						if (URIMatrixIndex.contains(e.getSubject().trim()) && URIMatrixIndex.contains(e.getObject().trim()))
						{
							//trasformo gli uri dei subj/obj in indici della matriceflat			
							indexr=URIMatrixIndex.indexOf( e.getSubject().trim()); 
							indexc=URIMatrixIndex.indexOf( e.getObject().trim());

							//faccio il flattening delle varie relazioni
							FAWM.set(indexr, indexc, Math.max(FAWM.get(indexr, indexc), weight));
						}
					}
				}
			}
		}	

		return FAWM;

	}



	public boolean isUriinLinkset(String uri){

		return linkset.contains((String)uri);

	}



	//uri raggiungibili al passo k...ma in totale 
	//senza registrare il nodo da cui parto ...
	//cioè parto sempre e solo dai nodi del linkset 
	//ma non so esattamente da quale parto

	public void insertUriinRNSh(String uri, int k, String p){

		HashSet<RDFEdge> e= new HashSet<RDFEdge>();
		RDFEdge pe=new RDFEdge(p,uri);

		if (k >= RNSh.size())
		{

			e.add(pe);
			RNSh.add(k, e);
		}
		else{
			if(!isURIinRNSh(uri,p))
				RNSh.get(k).add(pe);
		} 

	}




	public int getMinHopOfURIinRNSh(String uri, String p){
		RDFEdge pe=new RDFEdge(p,uri);

		int auxk=Integer.MAX_VALUE; 
		for (int k=0; k <RNSh.size(); k++)
		{
			if (RNSh.get(k).contains(pe))
			{
				if(k<auxk)
				{
					auxk=k;
				}
			}
		}
		return auxk;

	}

	public boolean isURIinRNSh(String uri, String p){
		RDFEdge pe=new RDFEdge(p,uri);

		for (int k=0; k <RNSh.size(); k++)
		{
			if (RNSh.get(k).contains(pe))
			{
				return true;
			}

		}
		return false;

	}


	public boolean isURIinRNSh2(String uri, String p){
		RDFEdge pe=new RDFEdge(p,uri);
		for (HashSet<RDFEdge> peauxlist: RNSh)
		{
			for(RDFEdge peaux: peauxlist)
			{
				if(peaux.equals(pe))
					return true;
			}
		}
		return false;

	}



	public int isEdgeinAWM(RDFEdge e, String p){
		//1 true  0 false -1 p not present in AWM
		if (AWM.containsKey(p)) 
		{

			for (RDFEdge edge: AWM.get(p) )
			{
				if (e.equals(edge))
				{
					return 1;
				}

			}
		}
		else return -1;
		return 0;
	}



	//calculate for each relation/property p 
	//the pair of uri having p

	public void insertRDFEdgeinAWM(RDFEdge e, String p){
		//1 true  0 false -1 p not present in AWM
		int ris=isEdgeinAWM(e,p);
		if (ris==1) 
		{//p present and e present

		}
		else if (ris==0)
			//p present e not present 
		{	
			AWM.get(p).add(e);

		}
		else 
		{//p not present in AWM 
			AWM.put(p, new HashSet() );
			AWM.get(p).add(e);
		}


	}
	
	

	public int isEdgeinAWL(RelationAdjacentConcept e, String l){
		//1 true  0 false -1 p not present in AWM
		if (AWL.containsKey(l)) 
		{

			for (RelationAdjacentConcept edge: AWL.get(l) )
			{
				if (e.equals(edge))
				{
					return 1;
				}

			}
		}
		else return -1;
		return 0;
	}

	
	

	public void insertEdgeinAWL(RelationAdjacentConcept e, String l){
		//1 true  0 false -1 p not present in AWM
		int ris=isEdgeinAWL(e,l);
		if (ris==1) 
		{//p present and e present

		}
		else if (ris==0)
			//p present e not present 
		{	
			AWL.get(l).add(e);

		}
		else 
		{//p not present in AWM 
			AWL.put(l, new HashSet() );
			AWL.get(l).add(e);
		}


	}
	
	
	public void insertURIinURIMatrixIndex(String uri){

		if(!URIMatrixIndex.contains(uri))
		{
			URIMatrixIndex.add(uri);
		}
	}



	public void insertURIinVisitedURI(String uri){

		if(!VisitedURI.contains(uri))
		{
			VisitedURI.add(uri);
		}
	}


	public void setCentralityMeasure(String uri, Double d){

		CM.put(uri, d);

	}


	public Double getCentralityMeasure(String uri){

		return CM.get(uri);

	}



	public ArrayList<Matrix> getPowerM() {
		return powerM;
	}

	public void setPowerM(ArrayList<Matrix> powerM) {
		this.powerM = powerM;
	}

	//create all the power matrix from 1 to k+1 hops
	public void initializePowerMatrix(int k, Matrix m)

	{
		powerM.add(0, null);
		for (int i=1; i<=k; i++)
		{
			powerM.add(i, UtilityFunctionsDegreeAssessment.powerMatrix(m,i));			
		}
	}






	public ArrayList<String> getVisitedURI() {
		return VisitedURI;
	}






	public void setVisitedURI(ArrayList<String> visitedURI) {
		VisitedURI = visitedURI;
	}



	//restricted to relation between object not considering literal value 
	//create a list of (relation, adjacent nodes) ALM instead of the whole AWM matrix
	
	public void extractALRestrict_isURI(String flag, String objnmsp, String subjnmsp, int k)
	{
		String qs=" "; 
		String		qs1=" ";
		Model model=null;
		ArrayList<ResultSetRewindable> alllinkset=new ArrayList<ResultSetRewindable>();
		ResultSetRewindable resP=null;
	
		int offset=0;
		int limit= 1000;
	
		QuerySolution qsol;
		String yuri=" ";	
	
		String nmspc="";
	
		//siccome voglio creare i path nell'obj 
		//allora visito l'obj per creare matrice adiacenza
	
		if(flag.trim().equalsIgnoreCase("o"))
		{
			nmspc=objnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");
			model=modelobj;
		}
		//siccome voglio creare i path nel subj 
		//allora visito il subj per creare matrice adiacenza
	
		if(flag.trim().equalsIgnoreCase("s"))
		{
			nmspc=subjnmsp.toLowerCase().trim().replaceAll("////","\\/\\/");;
			model=modelsubj;
	
		}
	
		if(flag.trim().equalsIgnoreCase("all"))
		{
			/*qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?y ?x "
						+"WHERE  { ?x skos:exactMatch ?y}";*/
			model=modelsubj;
			model.add(modelobj);
	
		}		
		boolean trovato=false;
	
		if(!flag.equalsIgnoreCase("all"))
		{
			for(String l: linkset)
			{
	
				if(l.equalsIgnoreCase("http://yyy/10058"))
				{
					trovato=true;
				}
	
				//TODO this methods compute:  
				//- the multirelational matrix considering all the property reachable 
				//from link l in k hops
				//structure necessary to collect and analyse the results
				String p=" ";
				String y2uri=" ";
	
				//list of query result 
				ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
				//structure necessary at single link level 
				//URI considered during the exploration 
				//of the subgraph for each hop
				//we use to navigate the graph for k hops
				LinkedList<String> URIactual = new LinkedList<String>();
	
				//set of next uris to consider 
				HashSet<String> nu;
				insertURIinURIMatrixIndex(l);
				insertUriinRNSh(l, 0, "");		
				for (int hop=1; hop<=k; hop++)
				{
					LOGGER.info("HOP: "+hop);
					nu= new HashSet();
					//System.out.println("HOP NUMBER: "+ hop+"\n");
					if(hop<=getRNSh().size())
					{
						for(RDFEdge e:getRNSh().get(hop-1))
						{//se non è già visitato e non è nel linkset di partenza
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/9195"))
							{
								trovato=true;
							}
							
							if(e.getSubject().equalsIgnoreCase("http://yyy/12189"))
							{
								trovato=true;
							}
							if( (!getVisitedURI().contains(e.getObject())&& 
									!linkset.contains(e.getObject().trim()))   
									|| e.getObject()==l	) 
								nu.add(e.getObject());
						}
						URIactual.addAll(nu);
						//URIactual.toString();
						//System.out.println("RNSH: ");
						//es.toStringRNSh();
						while(!URIactual.isEmpty())
						{
							l= URIactual.poll();
							insertURIinVisitedURI(l);
							//System.out.println("link considered: "+l +"\n");
							offset=0;
	
	
							do{
								qs="SELECT ?p ?y2 "
										+"\n where {  "
										+" <"+	l +"> ?p ?y2. "
										/* ADDBYRIC, we don't need literals */ 
										+" filter (isURI(?y2))"
										+"}  "
										+"LIMIT "+ limit
										+" OFFSET "+ offset +"\n";
	
								///execute the query
								
								if(l.equalsIgnoreCase("http://yyy/4633"))
								{
									trovato=true;
								}
								
								if(l.equalsIgnoreCase("http://yyy/2441"))
								{
									trovato=true;
								}
	
								SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
								//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
								resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(model, qs, err2, null, false));
	
								//ResultSetFormatter.out(resP);
								resP.reset();
								//in order to consider all the resP 
								//over the iteration considering the offset 
								//we save a list of results
								listresP.add(resP);
								offset=offset+limit;			
	
							}while (resP.size()>0);
	
	
							for(ResultSet resP2:listresP)
							{
								while(resP2.hasNext()) {
	
	
									
									qsol=resP2.next();
	
									//get object and subject
									p=qsol.getResource("?p").getURI();
									//System.out.println("property ?p in qsol: "+ p);
	
									if (qsol.get("?y2").isURIResource())
									{//control if the URI of the triple matches the URI space 
										//of the considered thesaurus
										y2uri=qsol.getResource("?y2").getURI().trim();
										if(y2uri.equalsIgnoreCase("http://yyy/4633"))
										{ 
											trovato=true;
										}	
										if(y2uri.equalsIgnoreCase("http://yyy/2441"))
										{
											trovato=true;
										}										
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 : "+ y2uri.toString() +"\n");
	
											//control if the uri belongs to the correct urispace
											//System.out.println("property: "+ p);
											//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
											//System.out.println("uri EARTH  : "+ y2uri);
											//controls if the uri is already present in the 
											//accessed uri
	
											if(isURIinRNSh(y2uri,p))
											{
												if(y2uri.equalsIgnoreCase("http://yyy/4633"))
												{
													trovato=true;
												}
												
												if(y2uri.equalsIgnoreCase("http://yyy/2441"))
												{
													trovato=true;
												}
												//System.out.println("property: "+ p);
												//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 
	
												//control the hop number this uri has been discovered
												//is greater than the actual hop
												if(hop<getMinHopOfURIinRNSh(y2uri,p))
												{
													//insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
													insertEdgeinAWL(new RelationAdjacentConcept(p, y2uri),l);
													if(y2uri.trim().matches(nmspc+"\\w*"))
													{
														insertUriinRNSh(y2uri, hop,p);
														//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
														//System.out.println("URI: "+ y2uri);
														insertURIinURIMatrixIndex(y2uri);
													}
												}
											}
											else{
												if(y2uri.equalsIgnoreCase("http://yyy/4633"))
												{
													trovato=true;
												}
												
												if(y2uri.equalsIgnoreCase("http://yyy/2441"))
												{
													trovato=true;
												}
												//the uri has been not visited until now
												//System.out.println("property: "+ p);
												//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
												//insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
												insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
												if(y2uri.trim().matches(nmspc+"\\w*"))
												{
													insertUriinRNSh(y2uri, hop,p);
													insertURIinURIMatrixIndex(y2uri);
												}
												//System.out.println("URI contains: "+ URIactual.contains(y2uri));
												//System.out.println("URI: "+ y2uri);
											}
										
									}
									else
										if(qsol.get("?y2").isLiteral())
										{
											//insert LIteral in es
											//System.out.println("property  : "+ p);	
											//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
											//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
											//we insert the pair (l,language) for the property p ) 
											//insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?y2").getLanguage()),p);
											insertEdgeinAWL(new RelationAdjacentConcept(p,y2uri),l);
										}
	
								}
							}
							//System.out.println("URIactual: "+ URIactual.toString());
	
						}
					}//if
				}//hop
			}//linkset
		}
	}




}
