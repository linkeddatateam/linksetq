
set -x #echo on
cd ../../../



#generate testset1 // manual test set for testing the importing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt  -out  Examples/Benchmarking/GEMET/test1/GEMET_it_es_en_XXX_prefLabel100_AltLabel50.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 50 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 100

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test1/GEMET_it_es_en_linkset_exactMatch90.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 90 

cp Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt Examples/Benchmarking/GEMET/test1/GEMET_it_es_en_YYY.nt



#generate testset2
# delete lexical representations of subject dataset 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt  -out  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel10_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 10

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt -out  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 30

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt -out  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 50 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 60

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt -out  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 90 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 100

cp Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt
cp Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt

#generate testset3
# delete lexical representations of object dataset (test set 3)
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 10

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 30

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 50 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 60

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 90 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 100

cp Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt
cp Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt


#generate testset4
# delete  part of the linkset  (test set 3)
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch10.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 10 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch30.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 30 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*   Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch50.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 50
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch90.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 90 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 99 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.9.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 99.9 


cp Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt
cp Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt


#generate testset5
# delete lexical representations of object and subject dataset but we keep the whole linkset 

##subject  
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 50 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 90

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_XXX.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 90 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 100


##object
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 10

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 10 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 30

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 50 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 60

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_YYY.nt -out  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -p "http://www.w3.org/2004/02/skos/core#altLabel"  -percentage 90 -p "http://www.w3.org/2004/02/skos/core#prefLabel" -percentage 100

## we keep the whole  whole linkset
cp Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt


#generate testset 6
# delete lexical representations as in test 5, but here we delete also links from   linkset 


## and we generate reduced linksets
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 10 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 30 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*   Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 50
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 90 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 99 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeletePropertyValues    -in Examples/Benchmarking/GEMET/GEMET_it_es_en_linkset.nt -out  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt -p "http://www.w3.org/2004/02/skos/core#exactMatch"  -percentage 99.9 

##we keep the same subject and object datasets of test 5
cp  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel*_AltLabel*.nt Examples/Benchmarking/GEMET/test6/
cp  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel*_AltLabel*.nt Examples/Benchmarking/GEMET/test6/

cd  Examples/Benchmarking/GEMET/


