
# genererating the summarized results 4 Impact on the testsets for GEMETBenchmarking

set -x #echo on

#let's merge the intestation and the whole file of results
cat   test*/test*.*.csv_core#altLabel_importingAbsolute.csv  >testX.csv_core#altLabel_importingAbsolute.csv 

cat  test*/test*.*.csv_core#altLabel_importingPercentage.csv >testX.csv_core#altLabel_importingPercentage.csv

cat  test*/test*.*.csv_core#prefLabel_importingAbsolute.csv >testX.csv_core#prefLabel_importingAbsolute.csv 
 
cat  test*/test*.*.csv_core#prefLabel_importingPercentage.csv >testX.csv_core#prefLabel_importingPercentage.csv 

#summarizing all the result about preflabel and altlabel in one file



cd ../../../
java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_core#altLabel_importingAbsolute.csv -out  Examples/Benchmarking/GEMET/testX.csv_core#altLabel_importingAbsolute_ALL.csv

java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_core#altLabel_importingPercentage.csv -out  Examples/Benchmarking/GEMET/testX.csv_core#altLabel_importingPercentage_ALL.csv

java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_core#prefLabel_importingAbsolute.csv -out  Examples/Benchmarking/GEMET/testX.csv_core#prefLabel_importingAbsolute_ALL.csv

java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_core#prefLabel_importingPercentage.csv -out  Examples/Benchmarking/GEMET/testX.csv_core#prefLabel_importingPercentage_ALL.csv



cd  Examples/Benchmarking/GEMET/

