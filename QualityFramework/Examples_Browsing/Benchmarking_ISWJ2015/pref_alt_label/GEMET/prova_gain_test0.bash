#creating the complementations 
set -x #echo on
cd ../../../../


##Complementing
java -classpath $CLASSPATH:QualityFramework_paola.jar:./lib/*  Benchmarking.MergeDatasets -in Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Test0GEMET_it_es_en_XXX_fewConcept.nt  -in Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Test0GEMET_it_es_en_YYY_fewConcept.nt -linkset Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Test0GEMET_it_es_en_linkset_fewConcept.nt  -NSConvert Examples_Browsing/Benchmarking/pref_alt_label/GEMET/nsMap.txt  -out Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/prova_test0.0_complementedDataset.rdf
  
## working out the normalized gain  (complemented - subject wrt goldstandard)
java -classpath $CLASSPATH:QualityFramework_paola.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/prova_test0.0_complementedDataset.rdf -subject  Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Test0GEMET_it_es_en_XXX_fewConcept.nt  -NSConvert Examples_Browsing/Benchmarking/pref_alt_label/GEMET/nsMap.txt   -goldStandard Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_GoldStandard_fewConcept.nt -out Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/prova_test0.0_NormalizedGain.csv




cd  Examples_Browsing/Benchmarking/pref_alt_label/GEMET/
