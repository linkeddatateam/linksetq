set -x #echo on
cd ../

##########
########## REACHABILITY IMPORTING
##########


##AGROVOC EARTh
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl  -subjUriSpace http://aims.fao.org/aos/agrovoc/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/  -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/AGROVOC2EARTH_reachability_k4.csv -flagDataset  o  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2EARTH_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2EARTH_importedValues.csv


##AGROVOC - ThIST 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl  -subjUriSpace http://aims.fao.org/aos/agrovoc/  -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/AGROVOC2THIST_reachability_k4.csv -flagDataset  o  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2THIST_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2THIST_importedValues.csv



##AGROVOC - GEMET 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl  -subjUriSpace  http://aims.fao.org/aos/agrovoc/  -objUriSpace http://www.eionet.europa.eu/gemet/concept/  -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/AGROVOC2GEMET_reachability_k4.csv -flagDataset  o -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2GEMET_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2GEMET_importedValues.csv




