set -x #echo on
cd ../

##EARTh - AGROVOC 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/ -objUriSpace http://aims.fao.org/aos/agrovoc/  -linksetPath   Examples/EARThInLusTRE/20160127/EARTh.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EARTH2AGROVOC_reachability_k4.csv -flagDataset  o  

##EARTh - ThIST 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -linksetPath   Examples/EARThInLusTRE/20160127/EARTh.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EARTH2THIST_reachability_k4.csv -flagDataset  o 


##EARTh - GEMET 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/ -objUriSpace http://www.eionet.europa.eu/gemet/concept/  -linksetPath   Examples/EARThInLusTRE/20160127/EARTh.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EARTH2GEMET_reachability_k4.csv -flagDataset  o 




