set -x #echo on


#sum of only the reachability all the thesauri considering K hops
cat *2*_reachability_k4.csv_CWPA4Hop.csv   | sort | uniq > Total_reachab_assessment_CWPAKhops.csv


#sum of all information on linkset - CWPA at K hop 
cat *2*_reachability_k4.csv_DW_browsingLinksetValues.csv   | sort | uniq > Total_reachab_browsingLinksetValues.csv
