package Quality;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.jena.riot.RDFDataMgr;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

/** 
 * 
 * @author riccardoalbertoni
 *
 *this class extends DataSetLinksetState adding 
 *- the possibility to initialize a datasetLinksetState from a void file
 *- managing the dumps specified by a new property http:// ... #localAbsoluteDumpPath
 *
 */

public class DataSetLinksetStateWithVOID extends DataSetLinksetState {

	//public HashMap <String, DatasetInfoWithVOID >mapOfDataset;
	//public ArrayList <LinksetInfoWithVOID> linksetInfoArray;

	/**
	 * Given a file void it initializes the DatasetLinksetStateWithDump
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception 
	 */
	public   DataSetLinksetStateWithVOID(String voidFileName) throws Exception {
		// let's considider all the linkset
		mapOfDataset=new HashMap <String, DatasetInfo> ();
		linksetInfoArray = new ArrayList <LinksetInfo>() ;

		// Read the void file
		Model model = RDFDataMgr.loadModel(voidFileName) ;


		// select the linksets
		ResIterator iter= model.listSubjectsWithProperty(model.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type" ), model.getResource("http://rdfs.org/ns/void#Linkset"));
		DatasetInfoWithVOID subjDataset=null;
		DatasetInfoWithVOID objDataset=null;

		// for each linkset extract subject and object targets 
		while  ( iter.hasNext() )	{
			Resource lsInstance= iter.next();

			// subject dataset
			NodeIterator sbjIter= model.listObjectsOfProperty(lsInstance,model.getProperty("http://rdfs.org/ns/void#subjectsTarget" ));
			Resource subj;
			if (sbjIter.hasNext()) {
				RDFNode s = sbjIter.next();


				if(s.isResource()){
					subj= s.asResource();
					// check if we have the info pertaining to the dataset 
					//  datasets name RDFlabel or dcterm:title  dcterms: <http://purl.org/dc/terms/> .
					Statement st;
					RDFNode no;
					//					 st = model.getProperty(subj,model.getProperty("http://purl.org/dc/terms/#title" ));
					//					RDFNode no= st.getObject();
					//					String dctitle;

					//if (no!=null && no.isLiteral())  dctitle=no.asLiteral().getString().trim().replaceAll(" ", "_");
					//else throw new Exception(" there's no dc:title for "+ subj.getLocalName());
					// do we have already found the  dataset?
					subjDataset= (DatasetInfoWithVOID)  mapOfDataset.get(subj.getLocalName());
					if (subjDataset==null) {
						// first time we meet this dataset
						subjDataset = new DatasetInfoWithVOID();

						// uriPattern
						st = model.getProperty(subj,model.getProperty("http://rdfs.org/ns/void#uriSpace" ));
						no= st.getObject();
						// uriPattern;
						if (no!=null && no.isLiteral()) {
							subjDataset.UriSpace=no.asLiteral().getString();
							subjDataset.consolidatedUriSpace=subjDataset.UriSpace;

						}
						else throw new Exception(" there's no void:uriPattern for "+ subj.getLocalName());


						// void:dataDump <http://purl.oclc.org/net/DumpEarthRDF> ;
						st=model.getProperty(subj, model.getProperty("http://rdfs.org/ns/void#dataDump"));
						if (st!=null){ 
							no= st.getObject();

							if (no!=null && no.isLiteral()) subjDataset.voidDump=no.asLiteral().getString();
							//	else throw new Exception(" there's no dataDump for "+ subj.getLocalName());
						}
						// sparql end point
						st=model.getProperty(subj.asResource(), model.getProperty("http://rdfs.org/ns/void#sparqlEndpoint" ));
						if (st!=null){
							no= st.getObject();

							if (no!=null && no.isResource())  
								subjDataset.sparqlEndPoint=no.toString();
							else throw new Exception(" there's no sparql Endpoint for "+ subj.getLocalName());
							//subjDataset.subject=true;
						}
						mapOfDataset.put(subj.getLocalName(), subjDataset);

					}
					subjDataset.subject=true;
				}else throw new Exception("void:subjectTarget should not be a literal in  VOID file: "+ voidFileName);

				// object dataset
				NodeIterator objIter= model.listObjectsOfProperty(lsInstance,model.getProperty("http://rdfs.org/ns/void#objectsTarget" ));
				Resource obj;
				if (objIter.hasNext()) {
					RDFNode o = objIter.next();


					if(o.isResource()){
						obj= o.asResource();
						// check if we have the info pertaining to the dataset 
						//  datasets name RDFlabel or dcterm:title  dcterms: <http://purl.org/dc/terms/> .
						Statement st;
						RDFNode no;
						//						Statement st = model.getProperty(obj,model.getProperty("http://purl.org/dc/terms/#title" ));
						//						RDFNode no= st.getObject();
						//						String dctitle;
						//
						//						if (no!=null && no.isLiteral())  dctitle=no.asLiteral().getString().trim().replaceAll(" ", "_");
						//						else throw new Exception(" there's no dc:title for "+ obj.getLocalName());
						// do we have already found the  dataset?
						objDataset= (DatasetInfoWithVOID) mapOfDataset.get(obj.getLocalName());
						if (objDataset==null) {
							// first time we meet this dataset
							objDataset = new DatasetInfoWithVOID();

							// uriPattern
							st = model.getProperty(obj,model.getProperty("http://rdfs.org/ns/void#uriSpace" ));
							if (st!=null){
								no= st.getObject();
								// uriPattern;
								if (no!=null && no.isLiteral()) {
									objDataset.UriSpace=no.asLiteral().getString();
									objDataset.consolidatedUriSpace=objDataset.UriSpace;

								}
								else throw new Exception(" there's no void:uriSpace for "+ obj.getLocalName());
							}

							// void:dataDump <http://purl.oclc.org/net/DumpEarthRDF> ;
							st=model.getProperty(obj, model.getProperty("http://rdfs.org/ns/void#dataDump"));
							if (st!=null){
								no= st.getObject();
								if (no!=null /*&& no.isLiteral()*/) objDataset.voidDump=no.toString();
								//	else throw new Exception(" there's no dataDump for "+ subj.getLocalName());
							}
							// sparql end point
							st=model.getProperty(obj.asResource(), model.getProperty("http://rdfs.org/ns/void#sparqlEndpoint" ));
							if (st!=null){
								no= st.getObject();
								if (no!=null /*&& no.isLiteral()*/)  objDataset.sparqlEndPoint=no.toString();
								else throw new Exception(" there's no sparqlEndpoint for "+ obj.getLocalName());
								//subjDataset.subject=true;
							}
							mapOfDataset.put(obj.getLocalName(), objDataset);

						}
						//objDataset.subject=false;
					}else throw new Exception("void:subjectTarget should not be a literal in  VOID file: "+ voidFileName);
					LinksetInfoWithVOID linkset=new LinksetInfoWithVOID();
					// other intersting thing about the linkeset
					// void:linkPredicate

					NodeIterator linksetPredicate= model.listObjectsOfProperty(lsInstance,model.getProperty("http://rdfs.org/ns/void#linkPredicate" ));
					if( linksetPredicate.hasNext()){
						Resource predicate =linksetPredicate.next().asResource();
						linkset.voidlinksetPredicate= predicate.getURI();



						// try to determining the sparql end poing
						// sparql end point
						Statement st=model.getProperty(lsInstance, model.getProperty("http://rdfs.org/ns/void#sparqlEndpoint" ));
						RDFNode no = st.getObject();
						if (no!=null && no.isLiteral())  linkset.sparqlEndPoint=no.asLiteral().getString();
						else {
							// we assume the sparl end point of its subject
							linkset.sparqlEndPoint= subjDataset.sparqlEndPoint;
						}
						//						
						//						Statement st=model.getProperty(lsInstance, model.getProperty("http://rdfs.org/ns/void#sparqlEndpoint" ));
						//						RDFNode no = st.getObject();
						//						if (no!=null && no.isLiteral())  linkset.sparqlEndPoint=no.asLiteral().getString();
						//						else {
						//							// we assume the sparl end point of its subject
						//							linkset.sparqlEndPoint= subjDataset.sparqlEndPoint;
						//						}
						linkset.name=subj.getLocalName()+"_"+obj.getLocalName()+"_"+predicate.getLocalName();
						linkset.subjectLocalName=subj.getLocalName();
						linkset.objectLocalName=obj.getLocalName();
					}
					linksetInfoArray.add(linkset);

				}else throw new Exception("void:linkPredicate is not specified for " + lsInstance.getLocalName());
			}else throw  new Exception("void:subjectTarget missing in the input VOID file: "+ voidFileName);

		}
		// properties to consider 

		//		public String sparqlEndPoint;
		//		
		//		public String UriSpace;
		//		public String consolidatedUriSpace;
		//		
		//		public String lastSparqlAccessProblem="";
		//		public int lastResponseCode; // the http code returned by the last query done on the dataset sparql end point
		//		
		//		public int availabilityResponseCode=0;
		//		public String availabilitySparqlAccessProblem="";
		//		


	}

	
}
