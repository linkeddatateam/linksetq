set -x #echo on
#it is thought to work at the 
cd ../../

# --> mk
##generate mk for k=0
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0
cp Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_XXX.nt Examples_Browsing/Benchmarking/GEMET/mk0/
cp Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_YYY.nt Examples_Browsing/Benchmarking/GEMET/mk0/
cp Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_linkset.nt  Examples_Browsing/Benchmarking/GEMET/mk0/

##generate a testset using the modifier  Mk k=4 in the directory mk4
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CentralityCreatePathWithKhopsMaintainingCompletness2 -inobj Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_YYY.nt -objnmsp http://yyy/   -insubj Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_XXX.nt -subjnmsp http://xxx/  -inlinkset Examples_Browsing/Benchmarking/dump_sources/modified_seed_dump/GEMET/GEMET_it_es_en_linkset.nt  -percentage 90   -outobj Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt -outsubj Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt   -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt  -k  4 -wheretocreatepath o -predlinkset skos:exactMatch

#--> mk+ dc 

# Groupset mk0 + dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dco
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_90.nt -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt  Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt 
 
# groupset mk0 + dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dcs
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_30.nt -predlinkset skos:exactMatch 
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_90.nt  -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt 


# groupset mk4 + dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dco
##generate a testset using the modifier  Mk k=4  and dc in the directory mk4/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_90.nt -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt  Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt  
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt 


# groupset mk4 + dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dcs
##generate a testset using the modifier  Mk k=4  and dc in the directory mk4/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_90.nt -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt  
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt



#--> mk+ dl 

# Groupset mk0 + dl o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dlo
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_30.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_30.nt -predlinkset skos:exactMatch
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  BBenchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_90.nt -predlinkset skos:exactMatch
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_XXX.nt

 
# groupset mk0 + dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dls
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dls
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_30.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_30.nt -predlinkset skos:exactMatch
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_linkset.nt -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_90.nt -predlinkset skos:exactMatch
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/GEMET_it_es_en_YYY.nt Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt


# groupset mk4 + dl o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dlo
##generate a testset using the modifier  Mk k=4  and dl in the directory mk4/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_30.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt -predlinkset skos:exactMatch
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts  -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt -predlinkset skos:exactMatch
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt  


# groupset mk4 + dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dls
##generate a testset using the modifier  Mk k=4  and dl in the directory mk4/dls
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_30.nt -predlinkset skos:exactMatch
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  BBenchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_linkset_mk_4.nt -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_90.nt -predlinkset skos:exactMatch
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/GEMET_it_es_en_YYY_mk_4.nt  Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt  



#--> mk+ dl + dc 

# Groupset mk0 + dl o +dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dlo/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_30.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/GEMET_it_es_en_YYY_dlo_30_dco_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/GEMET_it_es_en_YYY_dlo_90_dco_30.nt -predlinkset skos:exactMatch 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_30.nt  -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_30.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/GEMET_it_es_en_YYY_dlo_30_dco_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY_dlo_90.nt  -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_90.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/GEMET_it_es_en_YYY_dlo_90_dco_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_XXX.nt Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/GEMET_it_es_en_XXX.nt
cp Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dco/.

# Groupset mk0 + dl o +dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dlo/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/GEMET_it_es_en_XXX_dlo_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_30.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/GEMET_it_es_en_XXX_dlo_30_dcs_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_XXX_dlo_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/GEMET_it_es_en_XXX_dlo_90_dcs_30.nt -predlinkset skos:exactMatch 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_XXX_dlo_30.nt  -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_30.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/GEMET_it_es_en_XXX_dlo_30_dcs_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_XXX_dlo_90.nt  -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_90.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/GEMET_it_es_en_XXX_dlo_90_dcs_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_YYY*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/.
cp Examples_Browsing/Benchmarking/GEMET/mk0/dlo/GEMET_it_es_en_linkset_dlo_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dlo/dcs/.
 

# Groupset mk0 + dc o +dl o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dco/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_YYY_dco_30_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_linkset_dco_30_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_YYY_dco_90_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_linkset_dco_90_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_YYY_dco_30_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_linkset_dco_30_dlo_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_YYY_dco_90_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_linkset_dco_90_dlo_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt Examples_Browsing/Benchmarking/GEMET/mk0/dco/dlo/GEMET_it_es_en_XXX.nt

# Groupset mk0 + dc o +dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dco/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_XXX_dco_30_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_linkset_dco_30_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_XXX_dco_90_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_linkset_dco_90_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_XXX_dco_30_dls_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_linkset_dco_30_dls_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_XXX.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_linkset.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_XXX_dco_90_dls_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/GEMET_it_es_en_linkset_dco_90_dls_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dco/GEMET_it_es_en_YYY_dco_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dco/dls/.


# Groupset mk0 + dc s +dl o
# Non sono sicuro che Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_90_dlo_30.nt  sia diverso da Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_30_dlo_30.nt 
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dcs/dlo
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dcs_30_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_30_dlo_30.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dcs_90_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_90_dlo_30.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dcs_30_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_30_dlo_90.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dcs_90_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_90_dlo_90.nt -predlinkset skos:exactMatch 
# Non sono sicuro che Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_90_dlo_30.nt  sia diverso da Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dcs_30_dlo_30.nt quindi riscriverei il sopre come 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_YYY_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/GEMET_it_es_en_linkset_dlo_90.nt -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dlo/.

# Groupset mk0 + dc s +dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls
##generate a testset using the modifier  Mk k=0  and dl in the directory mk0/dcs/dlw
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_XXX_dcs_30_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_linkset_dcs_30_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_XXX_dcs_90_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_linkset_dcs_90_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_XXX_dcs_30_dls_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_linkset_dcs_30_dls_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_XXX_dcs_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_linkset.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_XXX_dcs_90_dls_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/GEMET_it_es_en_linkset_dcs_90_dls_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dcs/GEMET_it_es_en_YYY.nt Examples_Browsing/Benchmarking/GEMET/mk0/dcs/dls/.


# Groupset mk0 + dl s +dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dls/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_30.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/GEMET_it_es_en_YYY_dls_30_dco_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt  -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_30.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/GEMET_it_es_en_YYY_dls_30_dco_90.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/GEMET_it_es_en_YYY_dls_90_dco_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt  -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_90.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/GEMET_it_es_en_YYY_dls_90_dco_90.nt  -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/.
cp Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dls/dco/.



# Groupset mk0 + dl s +dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs
##generate a testset using the modifier  Mk k=0  and dc in the directory mk0/dls/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_30.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/GEMET_it_es_en_XXX_dls_30_dcs_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_30.nt  -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_30.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/GEMET_it_es_en_XXX_dls_30_dcs_90.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_90.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/GEMET_it_es_en_XXX_dls_90_dcs_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_XXX_dls_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_90.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/GEMET_it_es_en_XXX_dls_90_dcs_90.nt  -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_YYY.nt Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/.
cp Examples_Browsing/Benchmarking/GEMET/mk0/dls/GEMET_it_es_en_linkset_dls_*.nt Examples_Browsing/Benchmarking/GEMET/mk0/dls/dcs/.



#MK4  misti  ##################
#--> mk+ dl + dc 

# Groupset mk4 + dl o +dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco
##generate a testset using the modifier  Mk k=0  and dc in the directory mk4/dlo/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/GEMET_it_es_en_YYY_mk_4_dlo_30_dco_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/GEMET_it_es_en_YYY_mk_4_dlo_90_dco_30.nt -predlinkset skos:exactMatch 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_30.nt  -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/GEMET_it_es_en_YYY_mk_4_dlo_30_dco_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_dlo_90.nt  -datasetnmsp  http://yyy/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/GEMET_it_es_en_YYY_mk_4_dlo_90_dco_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/GEMET_it_es_en_XXX_mk_4.nt
cp Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dco/.

# Groupset mk4 + dl o +dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs
##generate a testset using the modifier  Mk k=0  and dc in the directory mk4/dlo/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/GEMET_it_es_en_XXX_mk_4_dlo_30_dcs_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/GEMET_it_es_en_XXX_mk_4_dlo_90_dcs_30.nt -predlinkset skos:exactMatch 

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt  -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/GEMET_it_es_en_XXX_mk_4_dlo_30_dcs_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_XXX_mk_4.nt  -datasetnmsp  http://xxx/  -linkset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/GEMET_it_es_en_XXX_mk_4_dlo_90_dcs_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_YYY_mk_4_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/.
cp Examples_Browsing/Benchmarking/GEMET/mk4/dlo/GEMET_it_es_en_linkset_mk_4_dlo_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dlo/dcs/.
 
# Groupset mk4 + dc o +dl o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo
##generate a testset using the modifier  Mk k=0  and dl in the directory mk4/dco/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_YYY_mk_4_dco_30_dlo_30.nt  -outlinkset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_linkset_mk_4_dco_30_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_YYY_mk_4_dco_90_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_linkset_mk_4_dco_90_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_30.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_YYY_mk_4_dco_30_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_linkset_mk_4_dco_30_dlo_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_90.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_YYY_mk_4_dco_90_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_linkset_mk_4_dco_90_dlo_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dco/dlo/GEMET_it_es_en_XXX_mk_4.nt


# Groupset mk4 + dc o +dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls
##generate a testset using the modifier  Mk k=0  and dl in the directory mk4/dco/dlo
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_XXX_mk_4_dco_30_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_linkset_mk_4_dco_30_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_XXX_mk_4_dco_90_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_linkset_mk_4_dco_90_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_XXX_mk_4_dco_30_dls_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_linkset_mk_4_dco_30_dls_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_XXX_mk_4.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_XXX_mk_4_dco_90_dls_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/GEMET_it_es_en_linkset_mk_4_dco_90_dls_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dco/GEMET_it_es_en_YYY_mk_4_dco_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dco/dls/.

# Groupset mk4 + dc s +dl o
# Non sono sicuro che Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_90_dlo_30.nt  sia diverso da Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_30_dlo_30.nt 
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo
##generate a testset using the modifier  Mk k=0  and dl in the directory mk4/dcs/dlo
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dcs_30_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_30_dlo_30.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dcs_90_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_90_dlo_30.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dcs_30_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_30_dlo_90.nt -predlinkset skos:exactMatch 
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dcs_90_dlo_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_90_dlo_90.nt -predlinkset skos:exactMatch 
# Non sono sicuro che Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_90_dlo_30.nt  sia diverso da Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dcs_30_dlo_30.nt quindi riscriverei il sopre come 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dlo_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dlo_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_YYY_mk_4_dlo_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/GEMET_it_es_en_linkset_mk_4_dlo_90.nt -predlinkset skos:exactMatch 
## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dlo/.

# Groupset mk4 + dc s +dl s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls
##generate a testset using the modifier  Mk k=0  and dl in the directory mk4/dcs/dlw
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_XXX_mk_4_dcs_30_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_linkset_mk_4_dcs_30_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_XXX_mk_4_dcs_90_dls_30.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_linkset_mk_4_dcs_90_dls_30.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_XXX_mk_4_dcs_30_dls_90.nt  -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_linkset_mk_4_dcs_30_dls_90.nt -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteLinksAndTheirConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_XXX_mk_4_dcs_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_linkset_mk_4.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_XXX_mk_4_dcs_90_dls_90.nt -outlinkset Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/GEMET_it_es_en_linkset_mk_4_dcs_90_dls_90.nt -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dcs/GEMET_it_es_en_YYY_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dcs/dls/.

# Groupset mk4 + dl s +dc o
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco
##generate a testset using the modifier  Mk k=0  and dc in the directory mk4/dls/dco
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_30.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/GEMET_it_es_en_YYY_mk_4_dls_30_dco_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt  -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_30.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/GEMET_it_es_en_YYY_mk_4_dls_30_dco_90.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_90.nt  -flag o -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/GEMET_it_es_en_YYY_mk_4_dls_90_dco_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt  -datasetnmsp  http://yyy/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_90.nt  -flag o -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/GEMET_it_es_en_YYY_mk_4_dls_90_dco_90.nt  -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/.
cp Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dls/dco/.


#TTTTT
# Groupset mk4 + dl s +dc s
mkdir -p Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs
##generate a testset using the modifier  Mk k=0  and dc in the directory mk4/dls/dcs
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_30.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_30.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/GEMET_it_es_en_XXX_mk_4_dls_30_dcs_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_30.nt  -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_30.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/GEMET_it_es_en_XXX_mk_4_dls_30_dcs_90.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_90.nt  -flag s -percentage 30 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/GEMET_it_es_en_XXX_mk_4_dls_90_dcs_30.nt  -predlinkset skos:exactMatch 
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.RandomDeleteConcepts -dataset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_XXX_mk_4_dls_90.nt -datasetnmsp  http://xxx/  -linkset Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_90.nt  -flag s -percentage 90 -outdataset  Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/GEMET_it_es_en_XXX_mk_4_dls_90_dcs_90.nt  -predlinkset skos:exactMatch 

## copying subj and linkset 
cp Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_YYY_mk_4.nt Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/.
cp Examples_Browsing/Benchmarking/GEMET/mk4/dls/GEMET_it_es_en_linkset_mk_4_dls_*.nt Examples_Browsing/Benchmarking/GEMET/mk4/dls/dcs/.

