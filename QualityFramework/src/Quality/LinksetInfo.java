package Quality;

import java.io.Serializable;
import java.util.HashMap;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

public class LinksetInfo implements Serializable {
public String name; // obtained combining the two localnames

public String subjectURI;
public String objectURI;

public String subjectLocalName; // local names are the key for  datasetInfo structure 
public String objectLocalName;

public boolean typeIndicatorsRetrieved=false;//

public  HashMap  <String, Integer > subjectTypes=new  HashMap  <String, Integer >(); // contains the types linked in the subject dataset
public  HashMap  <String, Integer > objectTypes=new  HashMap  <String, Integer >();
sparqlEndPointInfo subj;
sparqlEndPointInfo obj;

public String sparqlEndPoint;
public String UriSpace;

// availability for the linkset sparql end point
public int availabilityResponseCode=0;
public String  availabilitySparqlAccessProblem="";

public String subjSparqlAccessProblem="";	
public int subjLastResponseCode;


public String objSparqlAccessProblem="";	
public int objLastResponseCode;

public String voidlinksetPredicate="";


//public HashMap <String >
//public String toString(){
//	
//	 return "name: " + name  +
//	"\n subjectURI: " +subjectURI +
//	"\n objectURI: "+objectURI+
//	"\n subjectLocalName: " +subjectLocalName+
//	"\n typeIndicatorsRetrieved: " +typeIndicatorsRetrieved+
//	"\n subjectTypes: "+ subjectTypes +
//	"\n objectTypes: "+ objectTypes+ "\n \n";
//	
//	}

}
