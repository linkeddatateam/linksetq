package Benchmarking;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;

import Quality.SparqlEndPointErrorMessage;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;

/**
 * 
 * @author riccardoalbertoni
 * it downloads the film from DBPEDIA so that a dump of dbpedia's film can be locally created
 */
public class creatingDBpediaFilm {
	static String DBPEDIASPARQLSERVICE="http://dbpedia.org/sparql";
	static String PREFIX ="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
	"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
	"PREFIX dbpedia: <http://dbpedia.org/resource/>\n" +
	"PREFIX dbpprop: <http://dbpedia.org/property/>\n" +
	"PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" ; 

	public static void main(String[] args) {

		HashSet <String> directors=new HashSet <String>();
		HashSet <String> actors=new HashSet <String>();
		Model modelALL = ModelFactory.createDefaultModel();
		Model model = ModelFactory.createDefaultModel();

		 boolean SimpleType= true; 
		 boolean HitchcockGraceOnly=false;
		
		// directors who have made more than 30 films and at least a film with a actor or actress who have worked with hitchcock
		String queryString =PREFIX +
		"select  ?director \n" +
		"where { \n" +
		" ?film dbpedia-owl:starring ?actor. \n" +
		" ?film dbpedia-owl:director dbpedia:Alfred_Hitchcock. \n" +
		" ?xFilm dbpedia-owl:starring ?actor. \n" +
		"?xFilm dbpedia-owl:director ?director . \n" +
		"?xxFilm  dbpedia-owl:director ?director \n" +
		"}" +
		"group by ?director " +
		"HAVING (count ( distinct (?xxFilm)) >30)";


		// exec the query 
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE, query);
		ResultSet res=null;
		try{
			res= exec.execSelect();
		} catch (QueryExceptionHTTP e){
			System.out.println( DBPEDIASPARQLSERVICE+" queried by \n" + query + "has thrown  \n"+ e.getResponseMessage() +"\n\n\n");
		}


		QuerySolution sol;
		while (res.hasNext()){
			sol=res.next();
			directors.add(sol.get("?director").toString());	
		}
		exec.close();
		if (HitchcockGraceOnly) directors.clear(); // solo hitchcock

		directors.add("http://dbpedia.org/resource/Alfred_Hitchcock");

		
		// for each director
		String templateQuery= PREFIX +
		"Construct {\n" +
		((!SimpleType) ? "dbpedia:Alfred_Hitchcock  rdf:type  ?types; \n" :"dbpedia:Alfred_Hitchcock  rdf:type dbpedia-owl:Director;  rdf:type dbpedia-owl:Person ; \n") +
		"rdfs:label ?directorName; \n" +
		"dbpedia-owl:birthDate ?directorBirthDate;  dbpedia-owl:birthPlace ?directorBirthPlace;\n" +
		"dbpedia-owl:deathDate ?directorDeadDate;dbpedia-owl:deathPlace ?directorDeathPlace .\n" +
		" ?directorDeathPlace rdf:type  dbpedia-owl:Place . \n"+
		" ?directorBirthPlace rdf:type  dbpedia-owl:Place "+
		"}  where {\n" +
		((!SimpleType) ? "dbpedia:Alfred_Hitchcock  rdf:type ?types .\n" :"")+
		"dbpedia:Alfred_Hitchcock  rdfs:label ?directorName. filter langMatches( lang(?directorName), \"en\") \n" +
		"Optional {dbpedia:Alfred_Hitchcock dbpedia-owl:birthDate ?directorBirthDate}\n" +
		"Optional {dbpedia:Alfred_Hitchcock  dbpedia-owl:birthPlace ?directorBirthPlace}\n" +
		"Optional {dbpedia:Alfred_Hitchcock dbpedia-owl:deathDate ?directorDeadDate}\n" +
		"Optional {dbpedia:Alfred_Hitchcock  dbpedia-owl:deathPlace ?directorDeathPlace }\n" +
		"}";

		String templateQuery2=PREFIX+
		"Construct { \n" +
		" ?Film a dbpedia-owl:Film . \n" +
		"?Film dbpedia-owl:releaseDate ?releaseDate .\n" +
		"?Film rdfs:label ?filmtitle .\n" +
		"?Film dbpedia-owl:director dbpedia:Alfred_Hitchcock\n" +
		"} where {\n" +
		"?Film a dbpedia-owl:Film . \n" +
		"Optional{ ?Film dbpedia-owl:releaseDate ?releaseDate \n}" +
		"?Film rdfs:label ?filmtitle .\n" +
		"?Film dbpedia-owl:director dbpedia:Alfred_Hitchcock\n" +
		"}";

		for ( String director : directors){
			String lquery=templateQuery.replaceAll( "dbpedia:Alfred_Hitchcock", "<"+director+">");
			QueryExecution qexec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE,     lquery);
			model=model.add(qexec.execConstruct());
			//qexec.close();
			lquery=templateQuery2.replaceAll("dbpedia:Alfred_Hitchcock", "<"+director+">");
			qexec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE,     lquery); ;
			model=model.add(qexec.execConstruct());
			//qexec.close();
		}


		FileOutputStream out;
		try {
			out = new FileOutputStream("generatedByCreatingDBpediaFilm/directors.n3");
			modelALL=model;
			model.write(out, "N3");
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		String qentities="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n " +
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>" +
				"SELECT distinct ?entities  WHERE {" +
				"  {?entities a <http://dbpedia.org/ontology/Film> } union {?s1   <http://dbpedia.org/ontology/birthPlace> ?entities} union {?s1   <http://dbpedia.org/ontology/deathPlace> ?entities}" +
				"}";

		query = QueryFactory.create(qentities) ;

		exec = QueryExecutionFactory.create(query, model);		
		res= exec.execSelect();

	
		HashSet <String> entitiesIncompleteType =new HashSet <String>(); 
		while (res.hasNext()){
			entitiesIncompleteType.add(res.next().get("?entities").toString());
		}


		// actors who have acted at least in 10 films and have worked at least once with Alfred_Hitchock
		model =ModelFactory.createDefaultModel();
		queryString=PREFIX +
		"select ?actor \n" +
		"where { \n" +
		"?film dbpedia-owl:starring ?actor. \n" +
		"?film dbpedia-owl:director dbpedia:Alfred_Hitchcock \n" +
		"{\n" +
		"select  ?actor \n" +
		"where { ?xFilm dbpedia-owl:starring ?actor.} \n" +
		"group by ?actor \n" +
		"HAVING (count ( distinct (?xFilm)) >10)\n" +
		"}\n" +
		"}";

		// exec the query 
		query = QueryFactory.create(queryString) ;
		exec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE, query);
		res=null;
		try{
			res= exec.execSelect();
		} catch (QueryExceptionHTTP e){
			System.out.println( DBPEDIASPARQLSERVICE+" queried by \n" + query + "has thrown  \n"+ e.getResponseMessage() +"\n\n\n");
		}



		while (res.hasNext()){
			sol=res.next();
			actors.add(sol.get("?actor").toString());	
		}
		exec.close();
		if (HitchcockGraceOnly) actors.clear(); // in case we want only graceKelly 
		actors.add("http://dbpedia.org/resource/Grace_Kelly");
		//		// for each actor
		templateQuery= PREFIX +" Construct { \n" +
		((!SimpleType) ? "dbpedia:Grace_Kelly rdf:type ?types ;" : "dbpedia:Grace_Kelly rdf:type dbpedia-owl:Actor;  rdf:type dbpedia-owl:Person; ") +
		"rdfs:label ?actorName;  \n" +
		"dbpedia-owl:birthDate ?actorBirthDate;  dbpedia-owl:birthPlace ?actorBirthPlace; \n" +
		"dbpedia-owl:deathDate ?actorDeadDate;dbpedia-owl:deathPlace ?actorDeathPlace. \n" +
		" ?actorDeathPlace rdf:type  dbpedia-owl:Place . \n"+
		" ?actorBirthPlace rdf:type  dbpedia-owl:Place \n" +
		"}  where { \n" +
		((!SimpleType) ? "dbpedia:Grace_Kelly rdf:type ?types ." : "" )+
		"dbpedia:Grace_Kelly  rdfs:label ?actorName. filter langMatches( lang(?actorName), \"en\")  \n" +
		"Optional {dbpedia:Grace_Kelly dbpedia-owl:birthDate ?actorBirthDate \n}" +
		"Optional {dbpedia:Grace_Kelly  dbpedia-owl:birthPlace ?actorBirthPlace \n}" +
		"Optional {dbpedia:Grace_Kelly dbpedia-owl:deathDate ?actorDeadDate} \n" +
		"Optional {dbpedia:Grace_Kelly  dbpedia-owl:deathPlace ?actorDeathPlace} \n" +
		"}";


		templateQuery2=PREFIX +"Construct {   \n" +
		"?Film a dbpedia-owl:Film .  \n" +
		"?Film dbpedia-owl:releaseDate ?releaseDate . \n" +
		"?Film rdfs:label ?filmtitle . \n" +
		"?Film dbpedia-owl:starring	 dbpedia:Grace_Kelly \n" +
		"} where { \n" +
		"?Film a dbpedia-owl:Film .  \n" +
		"Optional{ ?Film dbpedia-owl:releaseDate ?releaseDate } \n" +
		"?Film rdfs:label ?filmtitle . \n" +
		"?Film dbpedia-owl:starring dbpedia:Grace_Kelly \n" +
		"}";

		for ( String actor : actors){
			String lquery=templateQuery.replaceAll( "dbpedia:Grace_Kelly", "<"+actor+">");
			QueryExecution qexec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE,     lquery);
			model=model.add(qexec.execConstruct());
			qexec.close();


			lquery=templateQuery2.replaceAll("dbpedia:Grace_Kelly", "<"+actor+">");
			qexec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE,     lquery); ;
			model=model.add(qexec.execConstruct());
			qexec.close();


		}
		try {
			out = new FileOutputStream("generatedByCreatingDBpediaFilm/actors.n3");
			
			model.write(out, "N3");
			out.close();
			
			modelALL.add(model);
			FileOutputStream out1 = new FileOutputStream("generatedByCreatingDBpediaFilm/DirectorsActorsSimpleTypes.n3");
			modelALL.write(out1, "N3");
			out1.close();
			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// let's create a modelWithSimpleTypes Starting from ModellALL in which each instances has only the basic types 
		Model modelWithSimpleType= ModelFactory.createDefaultModel();
		// films are dbpedia-owl:Film
		String constQuery= "";
		// places are dbpedia-owl:Place
		// actors are dbpedia:Actor
		// directors are dbpedia-owl:Director
		// actors and directors are dbpedia-owl:Person  
	
		
		
		
		
		// if in the model there are entities without types we should ask the types to dbpedia
		query = QueryFactory.create(qentities) ;
		exec = QueryExecutionFactory.create(query, model);
		try{
			res= exec.execSelect();
		} catch (QueryExceptionHTTP e){
			System.out.println( DBPEDIASPARQLSERVICE+" queried by \n" + query + "has thrown  \n"+ e.getResponseMessage() +"\n\n\n");
		}

		while (res.hasNext()){
			entitiesIncompleteType.add(res.next().get("?entities").toString());
		}

		String templateQuery3=PREFIX +"Construct {   \n" +
		"ENTITY a ?type  \n"+ 
		"} where { \n" +
		"ENTITY a ?type \n"+ 
		"}";

		model= ModelFactory.createDefaultModel();
		for ( String entity : entitiesIncompleteType){
			String lquery=templateQuery3.replaceAll( "ENTITY", "<"+entity+">");
			QueryExecution qexec = QueryExecutionFactory.sparqlService(DBPEDIASPARQLSERVICE,     lquery);
			model=model.add(qexec.execConstruct());
			qexec.close();
		}
		try {
			out = new FileOutputStream("generatedByCreatingDBpediaFilm/EntityType.n3");
			model.write(out, "N3");
			out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}


}

