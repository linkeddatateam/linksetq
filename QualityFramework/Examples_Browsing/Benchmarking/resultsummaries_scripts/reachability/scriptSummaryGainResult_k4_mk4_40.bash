set -x #echo on
cd ../..

#####################################
# 	EXAMPLE OF CALL 
# ./scriptSummaryGainResult_k4_40.bash ekaw2016 esultsummaries_scripts
# the paramaters are in orders: $1 $2
#####################################


cat "$1"/mk4_40/*/Test*_gain_assessment_k4.csv  "$1"/mk4_40/*/*/Test*_gain_assessment_k4.csv | sort | uniq > "$2"/Total_gain_assessment_k4_mk4_40.csv





