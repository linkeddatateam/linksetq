set -x #echo on
cd ../

##EUROVOC - AGROVOC 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://aims.fao.org/aos/agrovoc/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EUROVOC2AGROVOC_reachability_k4.csv -flagDataset  o  

##AGROVOC - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://aims.fao.org/aos/agrovoc/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/AGROVOC2EUROVOC_reachability_k4.csv -flagDataset  o  


##EUROVOC - ThIST 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EUROVOC2THIST_reachability_k4.csv -flagDataset  o 

##ThIST - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/THIST.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/THIST2EUROVOC_reachability_k4.csv -flagDataset  o  





##EUROVOC - GEMET 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://www.eionet.europa.eu/gemet/concept/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EUROVOC2GEMET_reachability_k4.csv -flagDataset  o  

##GEMET - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://www.eionet.europa.eu/gemet/concept/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/GEMET.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/GEMET2EUROVOC_reachability_k4.csv -flagDataset  o  


##EUROVOC - EARTh 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EUROVOC2EARTH_reachability_k4.csv -flagDataset  o  



##EARTh - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/EARTh.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/EARTh2EUROVOC_reachability_k4.csv -flagDataset  o  


