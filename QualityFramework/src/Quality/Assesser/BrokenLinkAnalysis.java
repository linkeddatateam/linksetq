package Quality.Assesser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Utilities.RDFAccess;

public class BrokenLinkAnalysis {


	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.LinkImp4Centrality.class.getName());
	//info linkset
	static LinksetInfo li=null;
	//info dataset subject
	static DatasetInfo xd=null;
	//info dataset object
	static DatasetInfo yd=null;
	//linkset in input
	static ArrayList<String> objoflinkset=null;
	static int linksetcardinality=-1;	
	static DatasetInfo d=null;


	public static void main(String[] args) {
		// TODO Auto-generated method stub


	}




	public BrokenLinkAnalysis (LinksetInfo li1, DatasetInfo xd1, DatasetInfo yd1)
	{

		//TODO depending on sparql end point setting this query does't work when we work with big datasets, it should be rewritten with offset and limits 
		// get the List of links, 

		li= new LinksetInfo();
		xd=new DatasetInfo();
		yd=new DatasetInfo();
		objoflinkset=new ArrayList<String>();
		String uri;
		//we assign the static fields
		li= li1;
		d=yd1;
		xd=xd1;
		yd=yd1;		
		analyseLinkset();
	}







	private static void analyseLinkset()
	{
		int limit=1000,offset=0;
		ResultSetRewindable resP;
		ArrayList<ResultSetRewindable> listresP=new ArrayList<ResultSetRewindable>();
		//list of query result 
		QuerySolution qsol;
		ArrayList<String> lsy=new ArrayList<String>(), lsx=new ArrayList<String>();
		int totallink=0;
		String uri="";

		String objoflink="select distinct ?y where { ?x <"+li.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}" +
				"order by ?y ";
		String linksetcardin= "select * where { ?x <"+li.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}";

		SparqlEndPointErrorMessage  err;


		String objoflink1="";

		do{ 
			objoflink1=objoflink+" LIMIT "+limit +"\n"+
					" OFFSET  "+offset;

			err=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjrestr \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( RDFAccess.execQuery(li, objoflink1, err, null, true));
			resP.reset();
			listresP.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);


		for(ResultSet resP2:listresP)
		{
			while(resP2.hasNext()) 
			{
				qsol=resP2.next();

				if (qsol.get("?y").isURIResource())
				{
					uri=qsol.getResource("?y").getURI().trim();
					lsy.add(uri);							
				}

			}
		}

		String linksetcardin1="";

		limit=1000;
		offset=0;
		listresP.clear();
		do{ 
			linksetcardin1=linksetcardin+" LIMIT "+limit +"\n"+
					" OFFSET  "+offset;

			err=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjrestr \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( RDFAccess.execQuery(li, linksetcardin1, err, null, true));
			resP.reset();
			listresP.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);


		for(ResultSetRewindable resP2:listresP)
		{
			totallink+=resP2.size();
		}
		linksetcardinality=totallink;
		// List of object's uri 
		objoflinkset=lsy;
	}



	public static int executeBrokenLinkAnalysis(String pathbl) throws IOException
	{
		HashSet <String> brokenlinks= new HashSet <String>(); 
		int brokenlinknumber=0;
		for (String l: objoflinkset)
		{
			try {
				URL myURL = new URL(l);
				HttpURLConnection myURLConnection =(HttpURLConnection) myURL.openConnection();

				int status=myURLConnection.getResponseCode();

				if(status==404)
				{
					brokenlinks.add(myURL.toString());
					brokenlinknumber++;
				}

				//LOGGER.info("status : "+status);
			} 
			catch (MalformedURLException e) { 
				LOGGER.info("message  : "+ e);
			} 
			catch (IOException e) {   
				LOGGER.info("message  : "+ e);
			}
		}
		String[] row1 =new String[1]; 
		File fcwpa=new File(pathbl+"_brokenlinksURI.csv");
		LOGGER.info("file of broken links uri  : "+ pathbl+"_brokenlinksURI.csv");
		fcwpa.getParentFile().mkdirs();
		CSVWriter writer = new CSVWriter(new FileWriter(fcwpa), '\t');	
		for ( String blink: brokenlinks){
			row1[0]=blink;
			writer.writeNext(row1);

		}
		writer.flush();
		writer.close();
		return brokenlinknumber; 

	}		



}
