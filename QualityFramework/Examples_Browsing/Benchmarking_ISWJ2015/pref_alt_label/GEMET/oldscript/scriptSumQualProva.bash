#creating the complementations 
set -x #echo on
cd ../../../


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.7_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.7_DatasetQuality.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf  
## working out the completeness of complemented wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard -in Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf     -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out  Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDatasetQuality.csv
## working out the completeness of XXX wrt goldstandard
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.CompletenessWRTGoldStandard  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.8_DatasetQuality.csv
#cp  Examples/Benchmarking/GEMET/test6.5/test6.5_DatasetQuality.csv Examples/Benchmarking/GEMET/test6.5/test6.8_DatasetQuality.csv





cd  Examples/Benchmarking/GEMET/
