package Benchmarking;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.util.iterator.FilterDropIterator;


import Quality.DataSetLinksetState;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;
/**
 * 
 * @author riccardoalbertoni
 * It works out extensional, intensional and LDS completeness of a dataset with respect to the gold standard.
 * 
 * Extensional and intensional completeness are developed considering the definition in 
 *  Jens Bleiholder and Felix Naumann. 2009. Data fusion. ACM Comput. Surv. 41, 1, Article 1 (January 2009), 41 pages. DOI=10.1145/1456650.1456651 http://doi.acm.org/10.1145/1456650.1456651
 *  
 *  LDS completeness is developed according to  the definition provided in 
 *  Mendes, C.P.N., Bizer, C., Ho, J., Miklos, Z., Calbimonte, J.: PlanetData D2.1 Conceptual model and best practices for high-quality metadata publishing.
 *   
 *  LDSextended is an extension of lDScompleteness which distinguish between completeness on properties when the properties might have more than a value at the time.	
 *
 * The cvs specified as out is in the form
 * 
 *"Dataset"
 *																"Extensional"		"Intensional"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#narrowMatch"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#hiddenLabel"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#inScheme"	ect
 * XXXX	"1.0"			"1.0"			"1.0"																		"1.0"	
 where XXXX=   outWithoutPathbeforeOfUnderscore+"_"+inWithoutPath +"_"+goldStandardWithoutPath;
 *
 */
public class GainWRTGoldStandard {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create Options object
		Options options = new Options();
		options.addOption("complemented", true, "complemented dataset on which we want to workout the normalized Gain");
		options.addOption("subject", true, "subject dataset on which we want to workout the normalized Gain");
		options.addOption("goldStandard", true, "dataset take as goldstandard when working out the completeness");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("out",true, "file csv  containing quality results" );
		
		//options.addOption("out",true, "file containing the results" );

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "GainWRTGoldStandard", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("complemented") ||  !cmd.hasOption("subject") || !cmd.hasOption("goldStandard"))  {
				formatter.printHelp( "GainWRTGoldStandard", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			String subjectPath= cmd.getOptionValue("subject");
			String complementedPath= cmd.getOptionValue("complemented");
			String out=  cmd.hasOption("out")? cmd.getOptionValue("out") : "";
			String goldStandard= cmd.getOptionValue("goldStandard");
			//MergeDatasets.run( in, cmd.getOptionValue("linkset"), cmd.getOptionValue("NSConvert"),cmd.getOptionValue("out"));
			Model complemented = org.apache.jena.riot.RDFDataMgr.loadModel(complementedPath) ; 
			Model subject = org.apache.jena.riot.RDFDataMgr.loadModel(subjectPath) ; 
			Model datasetCopy= ModelFactory.createDefaultModel();
			if (cmd.hasOption("NSConvert")){// let's change NS the in model 
				Map<String,String> ns= MergeDatasets.processNSconversion(cmd.getOptionValue("NSConvert"));
				StmtIterator lstmt = subject.listStatements();
				while (lstmt.hasNext()){
					Statement s = lstmt.next();

					String subjs= s.getSubject().getURI();

					RDFNode objnode=s.getObject();

					String ps = s.getPredicate().getURI();

					String objs=null;
					for (Entry<String, String>  e : ns.entrySet()) {

						if (objnode.isResource()   ) {
							objs=objnode.asResource().getURI();	
							if  (objnode.asResource().getURI().contains(e.getKey())) {
								objs = objs.replace(e.getKey(),e.getValue());
								break;
							}

						}
					}
					for (Entry<String, String>  e : ns.entrySet()) {
						if (subjs.contains(e.getKey())){ 
							subjs= subjs.replace(e.getKey(), e.getValue());
							break;
						}
					}
					for (Entry<String, String>  e : ns.entrySet()) {
						if (ps.contains(e.getKey())){
							ps= ps.replace(e.getKey(),e.getValue());
							break;
						}
					}

					datasetCopy.add(datasetCopy.getResource(subjs), datasetCopy.getProperty(ps) ,objnode.isResource()? datasetCopy.getResource(objs): objnode);


				}
				subject=datasetCopy;
				FileOutputStream file =new FileOutputStream("dumpChangeNS.n3"); 
				datasetCopy.write(file, "N3")	;	

			}



			Model gold = org.apache.jena.riot.RDFDataMgr.loadModel(goldStandard) ; 

			Locale locale  = new Locale("en", "UK");
			String pattern = "############.#######";
			DecimalFormat myFormatter = (DecimalFormat)
			NumberFormat.getNumberInstance(locale);
			myFormatter.applyPattern(pattern);

			HashMap<String, Double[]> CoupleMinMax=new  HashMap<String, Double[]>();
			HashMap<String, Double> EntityGp=new  HashMap<String, Double>();
			
			
			HashMap <String,Double[]> r1= GainWRTGoldStandard.normalizedQualityGain(complemented, 
					subject, gold, CoupleMinMax, EntityGp);

			String[] CSVHeader = new String[1+7*r1.size()];
			String[] CSVVal = new String[1+7*r1.size()];
			String goldStandardWithoutPath;
			String inWithoutPath= subjectPath.substring(subjectPath.lastIndexOf("/")+1);
			String outWithoutPath= out.substring(out.lastIndexOf("/")+1);
			int idxBeforeUnderscore= outWithoutPath.indexOf("_");
			if (idxBeforeUnderscore>-1){
				outWithoutPath=outWithoutPath.substring(0, outWithoutPath.indexOf("_"));
			}
			goldStandardWithoutPath=goldStandard.substring(goldStandard.lastIndexOf("/")+1);
			CSVHeader[0]= "Dataset";

			CSVVal[0]= outWithoutPath+"_"+inWithoutPath +"_"+goldStandardWithoutPath;
			int i=0;

			double Gp, XLp; 

			for ( Entry <String,Double[]> x : r1.entrySet()) {
				
//				CSVHeader[i]="NormalizedGain "+x.getKey(); 
//				CSVVal[i]=myFormatter.format(x.getValue());
//				i++;
				Gp=EntityGp.get(x.getKey()).doubleValue();
				
				Double [] gain=x.getValue();
				CSVHeader[7*i+1]="NormalizedGain_Gp"+x.getKey();
				CSVVal[7*i+1]=myFormatter.format((gain[1]));
				CSVHeader[7*i+2]="NormalizedGain_NORATIO"+x.getKey();
				CSVVal[7*i+2]=myFormatter.format((gain[0]));
				CSVHeader[7*i+3]="Normalized_Ratio_Gp"+x.getKey();
				CSVVal[7*i+3]=myFormatter.format((gain[2]));
				
				Double [] mm=CoupleMinMax.get(x.getKey());
				CSVHeader[7*i+4]="min_"+x.getKey(); 
				CSVVal[7*i+4]=myFormatter.format(mm[0].doubleValue());
				CSVHeader[7*i+5]="max_"+x.getKey(); 
				CSVVal[7*i+5]=myFormatter.format(mm[1].doubleValue());
				CSVHeader[7*i+6]="G|p_"+x.getKey();
				
				CSVVal[7*i+6]=myFormatter.format( Gp);
				CSVHeader[7*i+7]="XL|p_"+x.getKey();				
				CSVVal[7*i+7]=myFormatter.format( 0.0);
				
				i++;
			}

			if (cmd.hasOption("out")){
				CSVWriter writer = new CSVWriter(new FileWriter(cmd.getOptionValue("out")), '\t');

				writer.writeNext(CSVHeader);
				writer.writeNext(CSVVal);
				writer.close();
				System.out.print(" CSV containing the results has been writte into "+ out );
			}

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}


	private static HashMap<String,Double[]> normalizedQualityGain(Model complementeDataset, 
			Model subjectDataset, Model gold, 
			HashMap<String,Double[]> coupleMinMax, 
			HashMap<String,Double>  EntityGp 
			
			) {
		HashSet<String> goldProperties= new HashSet<String>();

		// list of Gold standard's properties 
		String queryString=" select distinct ?p   where { ?s ?p ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();
				goldProperties.add(r.getResource("?p").toString());
			}

		} catch (Exception e){
			System.out.println(e);
		}

		// for each property in the Gold Standard
		Iterator<String> iterp = goldProperties.iterator();
		HashMap <String, Double[]> gain4TestSet = new HashMap <String, Double[]>() ; // entries represent <propriety, normalizedGain>
		HashMap <String, Double> gain4TestSetRestricted = new HashMap <String, Double>() ; // entries represent <propriety, normalizedGain_restrictedtoXl>
		
		
		HashMap<String,Double> quality4LinkPComplemented, //quality of s wrt p in the complemented dataset 
		quality4LinkPSubject,//quality of s wrt p in the subject dataset
		cardinalityValOfEntitiesInComplemented, 
		cardinalityValOfEntitiesInGoldStandard;  // cardinality of the values for property p attached to entities  of complemented
		double Gp=0.0;		
		
		while ( iterp.hasNext()  ) {
			double max=-1.0, min=-1.0, ratio=0.0, ratiogp=0.0;
			String property= iterp.next();

			//valGp=getValue4pInGoldStandard(gold,property);
			Gp+= getNumberOfEntity4pInGoldStandard(gold,property);
			//System.out.println("property "+ property +" G|P :"+(double)Gp);
			EntityGp.put(property,Gp);
			
			cardinalityValOfEntitiesInComplemented=workOutCardinalityValue4p(complementeDataset, property);
			cardinalityValOfEntitiesInGoldStandard= workOutCardinalityValue4p(gold, property);
			//System.out.println("QUALITY XL -G :");			
			quality4LinkPComplemented=QLDExtAVR(complementeDataset, gold, property);
			//System.out.println("QUALITY X -G :");
			quality4LinkPSubject=QLDExtAVR(subjectDataset, gold, property);
			double gain4pwithoutratio=0.0;
			double gain4pGp=0.0;
			double ratiototGp=0.0;
			boolean firstTime=true;
			for (Entry<String,Double> e: quality4LinkPComplemented.entrySet() ){
				double qc,qs; // respectivelly quality of complemented and subject
				String entity= e.getKey();
				qc=e.getValue();
				//System.out.println(entity+ " quality property:"+property+" complemented: "+qc);
				qs= quality4LinkPSubject.get(entity);
				//System.out.println(entity+" quality "+property+" subject: "+qs);
			
				//codice modificato paola
				//modifica introdotta da paola perch� lanciava eccezione null
				if(cardinalityValOfEntitiesInComplemented.get(entity)==null)
				{
					//ratiogp=cardinalityValOfEntitiesInGoldStandard.get(entity);
					
				}
				else{
					ratio=cardinalityValOfEntitiesInGoldStandard.get(entity)/cardinalityValOfEntitiesInComplemented.get(entity);
					ratiogp=cardinalityValOfEntitiesInGoldStandard.get(entity)/cardinalityValOfEntitiesInComplemented.get(entity);
				
				
				if (firstTime ) {
					max=ratio;
					min=ratio;
					firstTime=false;
				}
				max=(max>=ratio)?max: ratio;
				min=(min<=ratio)?min: ratio;
				//System.out.println(" min: "+min);
				//System.out.println(" max: "+max);

				//System.out.println(" ratio XL: "+ratio);
				//System.out.println(" ratio Gp: "+ratiogp);
				gain4pwithoutratio+=(qc-qs);
				//System.out.println(" gain XL: "+(ratio)*(qc-qs));
				gain4pGp+=(ratiogp)*(qc-qs);
				ratiototGp+=ratiogp;
				//System.out.println(" gain Gp: "+(ratiogp)*(qc-qs));
				gain4TestSet.put(property, new Double[]{gain4pwithoutratio,gain4pGp/Gp,ratiototGp/Gp});
				coupleMinMax.put(property, new Double[]{ min, max });
				}
			}

			Gp=0.0;
		
		}
		
		
		
		return gain4TestSet;
	}

	private static double getValue4pInGoldStandard(Model gold, String property) {
		String queryString=" select (count(distinct ?o) as ?card)  where { ?s <"+property+">  ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			if (res.hasNext()) {
				QuerySolution r = res.next();
				return r.getLiteral("?card").getDouble();
			}

		} catch (Exception e){
			System.out.println(e);
		}
		return -1;
	}

	private static double getNumberOfEntity4pInGoldStandard(Model gold, String property) {
		String queryString=" select (count(distinct ?s) as ?card)  where { ?s <"+property+">  ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			if (res.hasNext()) {
				QuerySolution r = res.next();
				return r.getLiteral("?card").getDouble();
			}

		} catch (Exception e){
			System.out.println(e);
		}
		return -1;
	}
	
	private static double getNumberOfEntity4pInDataset(Model dataset, String property) {
		String queryString=" select (count(distinct ?s) as ?card)  where { ?s <"+property+">  ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, dataset);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			if (res.hasNext()) {
				QuerySolution r = res.next();
				return r.getLiteral("?card").getDouble();
			}

		} catch (Exception e){
			System.out.println(e);
		}
		return -1;
	}
	
	
	private static HashMap<String, Double> workOutCardinalityValue4p(
			Model complementeDataset, String property) {

		HashMap<String, Double> cardinalityValuesOfs=new HashMap<String, Double>();
		String queryString=" select distinct ?s   where { ?s ?p ?o}" ;

		queryString=queryString.replace("?p","<"+ property+">" );
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;


		QueryExecution exec = QueryExecutionFactory.create(query, complementeDataset);

		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();

				// get the number of property p that have for s in the gold
				Resource s=r.getResource("?s");
				StmtIterator istm = s.listProperties(complementeDataset.getProperty(property));
				double val=0;
				//Set<Statement> valuess= istm.toSet();
				//cardinalityValuesOfs.put(s.getURI(),(double) valuess.size());
				while (istm.hasNext()) {
					istm.next();
					val++;
				}
				cardinalityValuesOfs.put(s.getURI(),(double) val);
			}
			return cardinalityValuesOfs;

		} catch (Exception e){
			System.out.println(e);
		}
		return null;
	}


	public static HashMap<String, Double> QLDExtAVR(Model dataset, Model gold,
			String property) {
		String queryString;
		ResultSet res;

		int goldResourcesPvalues=0;
		int datasetResourcesPvalues=0;
		int goldResourcesPvaluesTot=0;
		int datasetResourcesPvaluesTot=0;
		//int numberOfEntitiesInGSThatHaveP=0; // that is G|p
		// result set entity s, quality 
		HashMap<String, Double> qualityOfs=new HashMap<String, Double>();
		queryString=" select distinct ?s   where { ?s ?p ?o}" ;
		//String property=iterp.next();
		queryString=queryString.replace("?p","<"+ property+">" );
		com.hp.hpl.jena.query.Query query1 = QueryFactory.create(queryString) ;
		// entities that have that property in the gold standard
		QueryExecution exec1 = QueryExecutionFactory.create(query1, gold);
		try{
			res= exec1.execSelect();
			//	numberOfEntitiesInGSThatHaveP=res.getRowNumber();
			//System.out.println("the goldstandard has "+ numberOfEntitiesInGSThatHaveP+ "for property "+ property );
			while (res.hasNext()) {
				//numberOfEntitiesThatHaveP++;
				QuerySolution r = res.next();
				//goldResourcesWithP.add(r.getResource("?s").toString());

				// get the number of property p that have for s in the gold
				Resource s=r.getResource("?s");
				StmtIterator istm = s.listProperties(gold.getProperty(property));
				while (istm.hasNext()) {
					istm.next();
					goldResourcesPvalues++;
					goldResourcesPvaluesTot++;
				}
				// get the number of property p that have for s in the dataset
				s=dataset.getResource(s.getURI());
				istm = s.listProperties(dataset.getProperty(property));
				while (istm.hasNext()) {

					istm.next();
					datasetResourcesPvalues++;
					datasetResourcesPvaluesTot++;

				}

				// Accumulate the ratio between    (# p in the dataset for s) and (# p in the gold standard for s )
				//System.out.println("datasetResourcesPvalues :" +datasetResourcesPvalues);
				//System.out.println("goldResourcesPvalues :" +goldResourcesPvalues);
				qualityOfs.put(s.toString(), datasetResourcesPvalues/ (double) goldResourcesPvalues);
				goldResourcesPvalues=0;
				datasetResourcesPvalues=0;
			}

		} catch (Exception e){
			System.out.println(e);
		}	
		return qualityOfs;

	}
}


