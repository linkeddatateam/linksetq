set -x #echo on
cd ../

##EUROVOC - AGROVOC 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://aims.fao.org/aos/agrovoc/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2AGROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2AGROVOC_importedValues.csv



##AGROVOC - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://aims.fao.org/aos/agrovoc/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2EUROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/AGROVOC2EUROVOC_importedValues.csv

##EUROVOC - ThIST 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2THIST_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2THIST_importedValues.csv

##ThIST - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/THIST.ttl  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/THIST2EUROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/THIST2EUROVOC_importedValues.csv



##EUROVOC - GEMET 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://www.eionet.europa.eu/gemet/concept/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2GEMET_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2GEMET_importedValues.csv

##GEMET - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://www.eionet.europa.eu/gemet/concept/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/GEMET.ttl  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2EUROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2EUROVOC_importedValues.csv

##EUROVOC - EARTh 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl  -subjUriSpace http://eurovoc.europa.eu/ -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/  -linksetPath   Examples/EARThInLusTRE/20160127/EUROVOC.ttl -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2EARTh_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/EUROVOC2EARTh_importedValues.csv


##EARTh - EUROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EUROVOC.ttl  -subjUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/ -objUriSpace  http://eurovoc.europa.eu/   -linksetPath   Examples/EARThInLusTRE/20160127/EARTh.ttl  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/EARTh2EUROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/EARTh2EUROVOC_importedValues.csv


