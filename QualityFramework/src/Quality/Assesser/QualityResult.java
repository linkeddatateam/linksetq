package Quality.Assesser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.jena.riot.RDFDataMgr;

import Quality.DatasetInfoWithDump;
import Quality.LinksetInfoWithDump;
import Quality.Utilities.RDFAccess;
import au.com.bytecode.opencsv.CSVWriter;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class QualityResult {


	private static Logger LOGGER = LogManager.getLogger(Quality.Assesser.QualityResult.class.getName()); 
	class IntegerW {
		private int val = 0;
		public IntegerW increase(){
			val++;
			return this;
		}
		public int getval(){
			return val;
		}
		public String  toString () {

			return new Integer(val).toString();
		}
	}
	public HashMap<String,LinksetQuality> linksetQualityHashMap;


	public QualityResult(){

		linksetQualityHashMap=new HashMap<String,LinksetQuality>();
	}

	public  void put(String name, LinksetQuality q) {

		linksetQualityHashMap.put(name, q);
	}

	/** 
	 * it writes importing results generating a distinct two  CSV respectively including the impact in terms of absolute number of elements imported and percentage,   and writing two couple for each of the  property  analyzed.
	 * For example, if we are analysing the impact for tree linksets xy1, xy2, xy2, considering skos:prefLabel and skos:altLabel,  it produces four cvs, namely
	 * optionValue_prefLabel_importingAbsolute.cvs
	 * optionValue_prefLabel_importingPercentage.cvs
	 * optionValue_altLabel_importingAbsolute.cvs
	 * optionValue_altLabel_importingPercentage.cvs
	 * 
	 * Each of them will contain tables as it follows
	 * 		total 	en		it	... 
	 * xy1	val		val		val
	 * xy2	val 	val		val
	 * xy4	val 	val		val
	 * 
	 * where the language are all those mentioned in a least a considered property 
	 * 
	 * If the analysis considers Object properties these file are containing only one column
	 * 
	 * @param cmd   set of command line parameters in which the following opetion are specified
	 * impactInCSVFile file path prefix on which a set of CSV file are generated. 
	 * @throws IOException
	 */
	public void saveImpactAsCSV(CommandLine cmd) throws IOException {

		String CVSFilePath=cmd.getOptionValue("impactInCSVFile");
		ArrayList <String> listLanguageInResults =new  ArrayList <String>();
		ArrayList <String> listPropertyInResults =new  ArrayList <String>();
		/// formatter exploited in the writing of percentages
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		//DecimalFormat myFormatter = new DecimalFormat("############.#######");

		for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved
			if(linksetq.getValue().LinkImp4p!=null )
			{
				for ( Entry <String,Double> p : linksetq.getValue().LinkImp4p.entrySet()){
					String propertyAndLang= p.getKey();
					String lang, prop;

					if (propertyAndLang.contains("@") ){ 
						// it is a property with indication of language 
						lang= propertyAndLang.substring(propertyAndLang.indexOf("@")+1);
						prop= propertyAndLang.substring(0, propertyAndLang.indexOf("@"));
						if (!listLanguageInResults.contains(lang)) listLanguageInResults.add(lang);
						if (!listPropertyInResults.contains(prop)) listPropertyInResults.add(prop);
					}

				}
			}
		}
		// now we have the list of property and languages  

		// array of files
		//CSVWriter writer[] = new CSVWriter[listPropertyInResults.size()*2] ;
		//CSVWriter(new FileWriter(optionValue), '\t');
		int numberOfColumns=listLanguageInResults.size()+3; 
		String[] languages= new String[numberOfColumns];
		languages[0]="NameFilesTest";
		languages[2]="total";
		languages[1]="LinksetCardinality(|L|)";

		String outFilename;
		//if (!cmd.hasOption("VOIDFile") ) writeEmptyCVS(cmd, CVSFilePath, languages);

		String[] row =new String[numberOfColumns];
		for (int i=3;i<numberOfColumns; i++) languages[i]=listLanguageInResults.get(i-3);
		for ( String property : listPropertyInResults){
			File f=new File(CVSFilePath+"_"+property.substring(property.lastIndexOf("/")+1)+"_importingPercentage.csv");
			f.getParentFile().mkdirs();

			CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
			//LOGGER.info("preparing "+f);
			writer.writeNext(languages); // we wrote the list of language as first row
			for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
				// for each linkset analyaed we consider which languages are involved
				//Set h = linksetq.getValue().LinkImp4p.entrySet(); 
				row =new String[numberOfColumns];
				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
				row[0]=outFilename+linksetq.getKey();
				HashMap<String, Double> res=linksetq.getValue().LinkImp4p;
				row[2]="0.0";
				row[1]=myFormatter.format((double)linksetq.getValue().linksetCardinality);

				if (res.containsKey(property)) row[2]= myFormatter.format((double)res.get(property));

				for( int i=3;i<numberOfColumns; i++ ){
					row[i]="0.0";
					if (res.containsKey(property+"@"+listLanguageInResults.get(i-3))) 
					{
						row[i]=res.get(property+"@"+listLanguageInResults.get(i-3)).toString();
					}
				}
				writer.writeNext(row); 
			}
			writer.flush();
			writer.close();
		}
		for ( String property : listPropertyInResults){
			String f=CVSFilePath+"_"+property.substring(property.lastIndexOf("/")+1)+"_importingAbsolute.csv";
			CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
			//LOGGER.info("preparing "+f);
			writer.writeNext(languages); // we wrote the list of language as first row
			for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
				// for each linkset analyzed we consider which languages are involved
				//Set h = linksetq.getValue().LinkImp4p.entrySet(); 
				row =new String[numberOfColumns];
				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
				row[0]=outFilename+linksetq.getKey();
				HashMap<String, Integer> res=linksetq.getValue().NumberOfElementImported;
				row[2]="0.0";
				row[1]=myFormatter.format(linksetq.getValue().linksetCardinality);
				if (res.containsKey(property)) row[2]=res.get(property).toString();

				for( int i=3;i<numberOfColumns; i++ ){
					row[i]="0.0";
					if (res.containsKey(property+"@"+listLanguageInResults.get(i-3))) 
					{
						row[i]=res.get(property+"@"+listLanguageInResults.get(i-3)).toString();
					}
				}
				writer.writeNext(row); 
			}
			writer.flush();
			writer.close();

		}

	}





	/** 
	 * it writes centrality results generating a file 
	 * considering the same subj, obj and linkset  
	 * the weights of the considered relations (that are indicated in the name of the result file) 
	 * we vary the number of hops obtaining a file with these columns:  
	 * 
	 * Each of them will contain tables as it follows
	 * K_hops		total_CWPA 
	 * 3				6
	 * 
	 * 
	 * @param cmd   set of command line parameters in which the following opetion are specified
	 * impactInCSVFile file path prefix on which a set of CSV file are generated. 
	 * @throws IOException
	 */

	public void saveBrokenLinkAsCSV(CommandLine cmd) throws IOException {

		String CVSFilePath=cmd.getOptionValue("brokenlinkInCSVFile");
		/// formatter exploited in the writing of percentages
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		String[] row =new String[4];
		String[] CSVHeader =new String[4]; 

		String outFilename;
		//if (!cmd.hasOption("VOIDFile") ) writeEmptyCVSBrowsing(cmd, CVSFilePath);




		for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved
			if(linksetq.getValue().brokenLinkCardinality!=-1)
			{

				File f=new File(CVSFilePath+"_brokenLinks.csv");
				f.getParentFile().mkdirs();
				//LOGGER.info("preparing "+f);
				CSVHeader[0]= "TestName";
				CSVHeader[1]= "|brokenLink|";
				CSVHeader[2]= "|linkset|";
				CSVHeader[3]= "|brokenLink|//|linkset|";
				CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
				writer.writeNext(CSVHeader); 
				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
				row[0]=outFilename;
				row[1]=myFormatter.format((int)linksetq.getValue().brokenLinkCardinality);
				row[2]=myFormatter.format((int)linksetq.getValue().linksetCardinality);
				double bl=(double)((double)(linksetq.getValue().brokenLinkCardinality)/(double)(linksetq.getValue().linksetCardinality));
				row[3]=myFormatter.format((double)bl);
				writer.writeNext(row); 
				writer.flush();
				writer.close();
			}

		}




	}






	public void saveBrowsingAsCSV(CommandLine cmd) throws IOException {

		String CVSFilePath=cmd.getOptionValue("browsingInCSVFile");
		/// formatter exploited in the writing of percentages
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		String[] row =new String[13];
		String[] row1 =new String[1]; 
		String[] CSVHeader =new String[13]; 

		String outFilename;
		//if (!cmd.hasOption("VOIDFile") ) writeEmptyCVSBrowsing(cmd, CVSFilePath);


		String weight;
		if(cmd.hasOption("WeightFilePath"))
			weight=cmd.getOptionValue("WeightFilePath").substring(cmd.getOptionValue("WeightFilePath").lastIndexOf("/")+1); 
		else{

			weight="DW";
		}		


		for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved
			if(linksetq.getValue().CrossWalkingSpreadingPotential!=-1)
			{

				File f=new File(CVSFilePath+"_"+weight+"_browsingLinksetValues.csv");
				f.getParentFile().mkdirs();
				//LOGGER.info("preparing "+f);
				CSVHeader[0]= "TestName";
				CSVHeader[1]= "k";
				CSVHeader[2]= "CWPA";
				CSVHeader[3]= "linkset cardinality";
				CSVHeader[4]= "linkset objects cardinality (|O|)";
				CSVHeader[5]= "TotalConceptsObjectRestrictedNmspAndProp";
				CSVHeader[6]= "TotalConceptsSubjRestrictedNmspAndProp";
				CSVHeader[7]= "TotalConceptsObjectRestrictedNmsp";
				CSVHeader[8]= "TotalConceptsSubjRestrictedNmsp";			
				CSVHeader[9]= "TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept";
				CSVHeader[10]= "TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept";	
				CSVHeader[11]="TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset";
				CSVHeader[12]= "Reachability [ CWPA \\ (TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset)]";
				CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
				writer.writeNext(CSVHeader); 
				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
				row[0]=outFilename;
				row[1]=cmd.getOptionValue("K");
				row[2]=myFormatter.format((int)linksetq.getValue().CrossWalkingSpreadingPotential);
				row[3]=myFormatter.format((int)linksetq.getValue().linksetCardinality);
				row[4]=myFormatter.format((int)linksetq.getValue().linksetObjectCardinality);
				row[5]=myFormatter.format((int)linksetq.getValue().TotalConceptsObjectRestrictedNmspAndProp);
				row[6]=myFormatter.format((int)linksetq.getValue().TotalConceptsSubjRestrictedNmspAndProp);
				row[7]=myFormatter.format((int)linksetq.getValue().TotalConceptsObjectRestrictedNmsp);
				row[8]=myFormatter.format((int)linksetq.getValue().TotalConceptsSubjRestrictedNmsp);
				row[9]=myFormatter.format((int)linksetq.getValue().TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept);
				row[10]=myFormatter.format((int)linksetq.getValue().TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept);
				row[11]=  myFormatter.format((int)linksetq.getValue().TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset);
				double reachab=(double)((linksetq.getValue().CrossWalkingSpreadingPotential)/(linksetq.getValue().TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset));						
				row[12]=myFormatter.format((double)reachab);				
				writer.writeNext(row); 
				writer.flush();
				writer.close();
			}

		}


		for ( Entry <String,LinksetQuality> linksetq: this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved
			if(linksetq.getValue().URIConceptsReachable!=null)
			{
				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
				File f=new File(CVSFilePath+"_URIConceptsReached.csv");
				//f.getParentFile().mkdirs();
				LOGGER.info("preparing URI Reached file"+f);
				CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
				for (String URIreached: linksetq.getValue().URIConceptsReachable)
				{
					row1[0]=URIreached;
					writer.writeNext(row1);

				}
				writer.flush();
				writer.close();
			}

		}




	}

	public void writeEmptyCVSBrowsing(CommandLine cmd, String CVSFilePath) throws IOException {
		// we write the csv empty. In case no importing have been obtained for some specific property.

		String outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
		String[] values= new String[2];
		String weight=""; 
		String sobj; 
		String slinkset; 	
		//		if (!cmd.hasOption("VOIDFile") ){
		// that is in case, the name of object and subjects/datasets are written in the command line
		if(cmd.hasOption("WeightFilePath"))
			weight=cmd.getOptionValue("WeightFilePath").substring(cmd.getOptionValue("WeightFilePath").lastIndexOf("/")+1); 
		else{

			weight="DW";
		}
		File f=new File(CVSFilePath+"_"+weight+"_browsingLinksetValue.csv");
		f.getParentFile().mkdirs();
		values[0]="K";
		values[1]="CWPA";
		// a header
		CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
		LOGGER.info("rewriting empty "+f);
		writer.writeNext(values);
		writer.flush();
		writer.close();
	}





	public void writeEmptyCVS(CommandLine cmd, String CVSFilePath,
			String[] languages) throws IOException {
		// we write the csv empty. In case no importing have been obtained for some specific property.

		String[] cmdSpecifiedImpactProperties= cmd.getOptionValues("impactP");
		String outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
		String[] values= new String[2];
		String ssubj; 
		String sobj; 
		String slinkset; 	
		//		if (!cmd.hasOption("VOIDFile") ){
		// that is in case, the name of object and subjects/datasets are written in the command line
		ssubj=cmd.getOptionValue("subjDatasetPath").substring(cmd.getOptionValue("subjDatasetPath").lastIndexOf("/")+1); 
		sobj=cmd.getOptionValue("objDatasetPath").substring(cmd.getOptionValue("objDatasetPath").lastIndexOf("/")+1); 
		slinkset=cmd.getOptionValue("linksetPath").substring(cmd.getOptionValue("linksetPath").lastIndexOf("/")+1); 	
		//		}else {
		//			
		//			// the name of object and subjects dataset are written in the VOID indicated by command line
		//			 ssubj=cmd.getOptionValue("subjDatasetPath").substring(cmd.getOptionValue("subjDatasetPath").lastIndexOf("/")+1); 
		//			 sobj=cmd.getOptionValue("objDatasetPath").substring(cmd.getOptionValue("objDatasetPath").lastIndexOf("/")+1); 
		//			 slinkset=cmd.getOptionValue("linksetPath").substring(cmd.getOptionValue("linksetPath").lastIndexOf("/")+1); 	
		//			
		//		}

		values[0]=outFilename+ssubj +"_"+sobj+"_"+slinkset+"_"+cmd.getOptionValue("linkingProperty").substring(cmd.getOptionValue("linkingProperty").lastIndexOf("/")+1);
		values[1]="0";

		for (int i=0;i< cmdSpecifiedImpactProperties.length; i++){
			File f=new File(CVSFilePath+"_"+cmdSpecifiedImpactProperties[i].substring(cmdSpecifiedImpactProperties[i].lastIndexOf("/")+1)+"_importingPercentage.csv");
			f.getParentFile().mkdirs();
			// a header
			CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
			LOGGER.info("rewriting empty "+f);
			writer.writeNext(languages); // we wrote the header " " "total"  as first row
			// an empty value row 
			writer.writeNext(values);
			writer.flush();
			writer.close();


			f=new File(CVSFilePath+"_"+cmdSpecifiedImpactProperties[i].substring(cmdSpecifiedImpactProperties[i].lastIndexOf("/")+1)+"_importingAbsolute.csv");
			f.getParentFile().mkdirs();
			// a header
			writer = new CSVWriter(new FileWriter(f), '\t');
			LOGGER.info("rewriting  empty"+f);
			writer.writeNext(languages); // we wrote the header " " "total"  as first row
			// an empty value row 
			writer.writeNext(values);
			writer.flush();
			writer.close();

		}
	}


	//		
	//		CSVWriter writer = new CSVWriter(new FileWriter(optionValue), '\t');
	//				 
	//			     // feed in your array (or convert your data to an array)
	//				 for ( Entry <String,LinksetQuality> e : this.linksetQualityHashMap.entrySet()){
	//					 e.getValue().NumberOfElementImported
	//			     String[] entries = "first#second#third".split("#");
	//			     writer.writeNext(entries);
	//				 }
	//				writer.close();		
	//	}
	public void saveResultInJSON(String optionValue) {

		String jsonS = (new JSONSerializer().include("linksetQualityHashMap")).serialize(this);
		try {
			//optionValue.replaceAll("/", File.separator);
			//optionValue=System.getProperty("user.dir")+optionValue;
			File file = new File(optionValue);
			if (!file.exists()) {
				file.createNewFile();
			}


			FileWriter outFile = new FileWriter(file);
			PrintWriter out = new PrintWriter(outFile);


			// print results of previous phase
			System.out.println("writing results into "+optionValue+"\n"+ jsonS);
			out.print(jsonS);
			out.close();
		}
		catch(IOException ex) {
			System.out.println(ex);
		}
	}

	public static QualityResult loadResultFromJSON(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader( new FileReader (fileName));
		String         line = null;
		StringBuilder  stringBuilder = new StringBuilder();
		String         ls = System.getProperty("line.separator");

		while( ( line = reader.readLine() ) != null ) {
			stringBuilder.append( line );
			stringBuilder.append( ls );
		}

		String jsonS=stringBuilder.toString();
		return new JSONDeserializer<QualityResult>().deserialize( jsonS );	
	}


	/*
	 * 
	 */
	public void generateStatistics(){
		// let's generate stactistics for linkset sparql accessibility
		int numberOfLinksets=linksetQualityHashMap.size();
		HashMap <Double, IntegerW>  availabilityCounter= new HashMap <Double, IntegerW>();
		HashMap <Double, IntegerW>  accessabilityCounter= new HashMap <Double, IntegerW>();
		//HashMap <Double, IntegerW>  accessabilityReasonCounter= new HashMap <Double, IntegerW>();
		HashMap <Double, IntegerW>  indicatorAvailabilityCounter= new HashMap <Double, IntegerW>();


		for ( LinksetQuality x: linksetQualityHashMap.values()){
			double val=	x.LinksetSparqlAccessability;;
			if (accessabilityCounter.containsKey(val)) {
				(accessabilityCounter.get(val)).increase();}
			else{
				accessabilityCounter.put(val, new IntegerW().increase());
			}

			val=	x.LinksetSparqlAvailability;
			if (availabilityCounter.containsKey(val)) {
				(availabilityCounter.get(val)).increase();}
			else{
				availabilityCounter.put(val, new IntegerW().increase());
			}

			//			if (val< 0.34){ //let see the reason why they do not work
			//				
			//				if (accessabilityReasonCounter.containsKey(val)) {
			//					(accessabilityReasonCounter.get(val)).increase();}
			//				else{
			//					accessabilityReasonCounter.put(val, new IntegerW().increase());
			//				}
			//			}

			val=	x.LinksetIndicatorSparqlAvailability;
			if (indicatorAvailabilityCounter.containsKey(val)) {
				(indicatorAvailabilityCounter.get(val)).increase();}
			else{
				indicatorAvailabilityCounter.put(val, new IntegerW().increase());
			}

		}
		System.out.println( "Statistics result for accessability: \n"+ "Number of Linksets "+ numberOfLinksets+"\n  valueCounter" +accessabilityCounter);
		System.out.println( "Statistics result for availability: \n"+ "Number of Linksets "+ numberOfLinksets+"\n  valueCounter" +availabilityCounter);
		System.out.println( "Statistics result for availability: \n"+ "Number of Linksets "+ numberOfLinksets+"\n  valueCounter" +indicatorAvailabilityCounter);
	}

	/** 
	 *  it writes importing results generating results according to the Daq Vocabulary
	 * @param cmd   set of command line parameters in which the following operation are specified
	 * impactInRDFFile file path prefix on which the rdf file is generated. 
	 * @throws IOException
	 */
	@Deprecated
	public void saveImpactAsDAq(CommandLine cmd, Model model) throws IOException {

		String RDFFilePath=cmd.getOptionValue("impactInRDFFile");
		ArrayList <String> listLanguageInResults =new  ArrayList <String>();
		ArrayList <String> listPropertyInResults =new  ArrayList <String>();
		/// formatter exploited in the writing of percentages
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved
			for ( Entry <String,Double> p : linksetq.getValue().LinkImp4p.entrySet()){
				String propertyAndLang= p.getKey();
				String lang, prop;

				if (propertyAndLang.contains("@") ){ 
					// it is a property with indication of language 
					lang= propertyAndLang.substring(propertyAndLang.indexOf("@")+1);
					prop= propertyAndLang.substring(0, propertyAndLang.indexOf("@"));
					if (!listLanguageInResults.contains(lang)) listLanguageInResults.add(lang);
					if (!listPropertyInResults.contains(prop)) listPropertyInResults.add(prop);
				}

			}
		}
		// now we have the list of property and languages  

		// array of files
		//CSVWriter writer[] = new CSVWriter[listPropertyInResults.size()*2] ;
		//CSVWriter(new FileWriter(optionValue), '\t');
		int numberOfColumns=listLanguageInResults.size()+3; 
		String[] languages= new String[numberOfColumns];
		//D languages[0]="NameFilesTest";
		//D languages[2]="total";
		//D languages[1]="LinksetCardinality(|L|)";


		// let 's initialize the daq Model
		//Model model = org.apache.jena.riot.RDFDataMgr.loadModel("Quality/Utilities/importingDAQTemplate.ttl") ;
		model.setNsPrefix("dqv" ,   "http://www.w3.org/ns/dqv#"); 
		model.setNsPrefix("daq" ,   "http://purl.org/eis/vocab/daq#"); 
		model.setNsPrefix("qb", "http://purl.org/linked-data/cube#"); 
		model.setNsPrefix("lsq", "http://linkeddata.ge.imati.cnr.it/LinksetQuality/");
		model.setNsPrefix("exq","http://linkeddata.ge.imati.cnr.it/LinksetQuality/QualityGraph/");
		model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/" );
		model.setNsPrefix("rdfs", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		model.setNsPrefix("sdmx-attribute", "http://purl.org/linked-data/sdmx/2009/attribute#");

		// let's initialize a structure with the properties
		HashMap <String, Property> properties =  new  HashMap<String, Property> ();
		properties.put("rdf:type", model.createProperty(model.getNsPrefixURI("rdf"),"type"));
		properties.put("daq:computedOn", model.createProperty(model.getNsPrefixURI("daq"),"computedOn"));
		properties.put("dc:date", model.createProperty(model.getNsPrefixURI("dc"),"date"));
		properties.put("daq:value", model.createProperty(model.getNsPrefixURI("daq"),"value"));
		properties.put("daq:metric", model.createProperty(model.getNsPrefixURI("daq"),"metric"));
		properties.put("daq:property", model.createProperty(model.getNsPrefixURI("daq"),"property"));
		properties.put("daq:language", model.createProperty(model.getNsPrefixURI("daq"),"language"));
		properties.put("qb:dataSet", model.createProperty(model.getNsPrefixURI("qb"),"dataSet"));
		properties.put("daq:hasObservation", model.createProperty(model.getNsPrefixURI("daq"),"hasObservation"));


		// let's initialize property values that are constant in all the observation
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		Literal dateTypedLiteral =model.createTypedLiteral(dateFormat.format(date), XSDDatatype.XSDdate);
		System.out.println(dateFormat.format(date));
		Resource importing4PropertyPercentual=model.createResource(model.getNsPrefixURI("exq")+"importing4PropertyPercentual" );
		Resource  QualityGraph=model.createResource(model.getNsPrefixURI("exq")+"qualityGraph" );
		String voidFile="";
		if (cmd.hasOption("VOIDFile")) voidFile=cmd.getOptionValue("VOIDFile");

		// how can  we figure out what linkset has been considered? 

		// Map <String, String> namespace = ImmutableMap.of("a", 1, "b", 2, "c", 3);

		String[] row =new String[numberOfColumns];
		for (int i=3;i<numberOfColumns; i++) languages[i]=listLanguageInResults.get(i-3);
		for ( String property : listPropertyInResults){
			//D 	File f=new File(CVSFilePath+"_"+property.substring(property.lastIndexOf("/")+1)+"_importingPercentage.csv");
			//D f.getParentFile().mkdirs();
			// let prepare the name of the Daq file




			//D CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');

			//D 			writer.writeNext(languages); // we wrote the list of language as first row
			for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
				// for each linkset analyzed we consider which languages are involved
				//Set h = linksetq.getValue().LinkImp4p.entrySet(); 
				row =new String[numberOfColumns];
				//D outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);

				//D row[0]=outFilename+linksetq.getKey();
				HashMap<String, Double> res=linksetq.getValue().LinkImp4p;
				//D row[2]="0.0";
				//D row[1]=myFormatter.format((double)linksetq.getValue().linksetCardinality);
				//TODO It seems that the linkset, object and subject 's URI are not available in the object LinksetQuality, we should insert the correct linkset URI  exploited in the VOID instead of    linksetq.getKey();
				// just for now we can retrieve these info form the linksetq.getKey() which is in the form SUBJECT_OBJECT_LINKINGPROPERTIES
				String ObsFirstNamePart=linksetq.getKey();
				String ObsFirstNamePartSplited[]= linksetq.getKey().split("_");
				Resource ComputedOnResource=model.createResource(voidFile+"#"+ObsFirstNamePartSplited[0]+"-"+ObsFirstNamePartSplited[1] );
				if (res.containsKey(property)) row[2]= myFormatter.format((double)res.get(property));

				Resource propertyRes=model.createResource(property );

				for( int i=3;i<numberOfColumns; i++ ){
					row[i]="0.0";
					if (res.containsKey(property+"@"+listLanguageInResults.get(i-3))) 
					{
						row[i]=res.get(property+"@"+listLanguageInResults.get(i-3)).toString();
					}
					String p="";
					if (property.contains("#")) p= property.substring(property.lastIndexOf("#")+1) ;
					else p= property.substring(property.lastIndexOf("/")+1) ;
					Resource obs = model.createResource(model.getNsPrefixURI("exq") + ObsFirstNamePart + p+ listLanguageInResults.get(i-3));

					obs.addProperty(properties.get("daq:computedOn"), ComputedOnResource);
					obs.addLiteral(properties.get("dc:date"),  dateTypedLiteral);
					obs.addLiteral(properties.get("daq:value"), model.createTypedLiteral(row[i], XSDDatatype.XSDdouble));
					obs.addProperty(properties.get("daq:metric"), importing4PropertyPercentual);
					obs.addProperty(properties.get("daq:property"), propertyRes);
					obs.addLiteral(properties.get("daq:language"), listLanguageInResults.get(i-3) );
					obs.addProperty(properties.get("qb:dataSet"), QualityGraph);

					// adding the  hasObservation between instance of metric and  obs
					importing4PropertyPercentual.addProperty(properties.get("daq:hasObservation"),obs);	

				}
			}
		}

		//DD	for ( String property : listPropertyInResults){
		//			String f=CVSFilePath+"_"+property.substring(property.lastIndexOf("/")+1)+"_importingAbsolute.csv";
		//			CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
		//			LOGGER.info("preparing "+f);
		//			writer.writeNext(languages); // we wrote the list of language as first row
		//			for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
		//				// for each linkset analyzed we consider which languages are involved
		//				//Set h = linksetq.getValue().LinkImp4p.entrySet(); 
		//				row =new String[numberOfColumns];
		//				outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
		//				row[0]=outFilename+linksetq.getKey();
		//				HashMap<String, Integer> res=linksetq.getValue().NumberOfElementImported;
		//				row[2]="0.0";
		//				row[1]=myFormatter.format(linksetq.getValue().linksetCardinality);
		//				if (res.containsKey(property)) row[2]=res.get(property).toString();
		//
		//				for( int i=3;i<numberOfColumns; i++ ){
		//					row[i]="0.0";
		//					if (res.containsKey(property+"@"+listLanguageInResults.get(i-3))) 
		//					{
		//						row[i]=res.get(property+"@"+listLanguageInResults.get(i-3)).toString();
		//
		//					}
		//				}
		//				writer.writeNext(row); 
		//			}
		//			writer.flush();
		//			writer.close();
		//
		//		}
		LOGGER.info("Writing results as RDF in "+RDFFilePath);
		RDFAccess.writeModelInTheFileNameSpecifiedEncoding(RDFFilePath, model);
	}
	
	
	
	
	public void saveImpactAsDQV(CommandLine cmd, Model model) throws IOException {

		String RDFFilePath=cmd.getOptionValue("impactInDQVFile");
		ArrayList <String> listLanguageInResults =new  ArrayList <String>();
		ArrayList <String> listPropertyInResults =new  ArrayList <String>();
		/// formatter exploited in the writing of percentages
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
			// for each linkset analysed we consider which languages are involved

			for ( Entry <String,Double> p : linksetq.getValue().LinkImp4p.entrySet()){
				String propertyAndLang= p.getKey();
				String lang, prop;

				if (propertyAndLang.contains("@") ){ 
					// it is a property with indication of language 
					lang= propertyAndLang.substring(propertyAndLang.indexOf("@")+1);
					prop= propertyAndLang.substring(0, propertyAndLang.indexOf("@"));
					if (!listLanguageInResults.contains(lang)) listLanguageInResults.add(lang);
					if (!listPropertyInResults.contains(prop)) listPropertyInResults.add(prop);
				}

			}
		}
		// now we have the list of property and languages  
		int numberOfColumns=listLanguageInResults.size()+3; 
		String[] languages= new String[numberOfColumns];
		//D languages[0]="NameFilesTest";
		//D languages[2]="total";
		//D languages[1]="LinksetCardinality(|L|)";



		//D String outFilename;
		//D if (!cmd.hasOption("VOIDFile") ) writeEmptyCVS(cmd, CVSFilePath, languages);

		//	}
		
		
		
		// let 's initialize the daq Model
	//	Model model = org.apache.jena.riot.RDFDataMgr.loadModel("Quality/Utilities/dqv20160415.ttl") ;

		//let's  define the namespaces
		/*HashMap <String, String> namespace =  new  HashMap<String, String>() {
			{
				put("dqv" ,   "http://www.w3.org/ns/dqv#"); 
				put("daq" ,   "http://purl.org/eis/vocab/daq#"); 
				put("qb", "http://purl.org/linked-data/cube#"); 
				put("lsq", "http://linkeddata.ge.imati.cnr.it/LinksetQuality/");
				put("exq","http://linkeddata.ge.imati.cnr.it/LinksetQuality/QualityGraph/");
				put("dc", "http://purl.org/dc/elements/1.1/" );
				put("rdfs", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
				put("sdmx-attribute", "http://purl.org/linked-data/sdmx/2009/attribute#");
			}
		} ; */
		
		model.setNsPrefix("dqv" ,   "http://www.w3.org/ns/dqv#"); 
		model.setNsPrefix("daq" ,   "http://purl.org/eis/vocab/daq#"); 
		model.setNsPrefix("qb", "http://purl.org/linked-data/cube#"); 
		model.setNsPrefix("lsq", "http://linkeddata.ge.imati.cnr.it/LinksetQuality/");
		model.setNsPrefix("exq","http://linkeddata.ge.imati.cnr.it/LinksetQuality/QualityGraph/");
		model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/" );
		model.setNsPrefix("rdfs", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		model.setNsPrefix("sdmx-attribute", "http://purl.org/linked-data/sdmx/2009/attribute#");
		model.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");

		// let's initialize a structure with the properties
		HashMap <String, Property> properties =  new  HashMap<String, Property> ();
		properties.put("rdf:type", model.createProperty(model.getNsPrefixURI("rdf"),"type"));
		properties.put("dqv:computedOn", model.createProperty(model.getNsPrefixURI("dqv"),"computedOn"));
		properties.put("dc:date", model.createProperty(model.getNsPrefixURI("dc"),"date"));
		properties.put("dqv:value", model.createProperty(model.getNsPrefixURI("dqv"),"value"));
		properties.put("dqv:isMeasurementOf", model.createProperty(model.getNsPrefixURI("dqv"),"isMeasurementOf"));
		properties.put("lsq:onProperty", model.createProperty(model.getNsPrefixURI("lsq"),"onProperty"));
		properties.put("lsq:onLanguage", model.createProperty(model.getNsPrefixURI("lsq"),"onLanguage"));
		properties.put("qb:dataSet", model.createProperty(model.getNsPrefixURI("qb"),"dataSet"));
		properties.put("sdmx-attribute:unitMeasure", model.createProperty(model.getNsPrefixURI("sdmx-attribute"),"unitMeasure"));
	
		//properties.put("daq:hasObservation", model.createProperty(model.getNsPrefixURI("daq"),"hasObservation"));


		// let's initialize property values that are constant in all the observation
		//String ObsFirstNamePart = CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		Literal dateTypedLiteral =model.createTypedLiteral(dateFormat.format(date), XSDDatatype.XSDdate);
		System.out.println(dateFormat.format(date));
		Resource importing4PropertyPercentual=model.createResource(model.getNsPrefixURI("lsq")+"importing4PropertyPercentual" );
		//TODO describing the Metric and Dimension 
		
		 Model metricDimensionModel = RDFDataMgr.loadModel("Quality/Utilities/metricDimensionModel.ttl");
		 model.add(metricDimensionModel);
/*		# Definition of instances for Metric, Dimension and Category. 

		:importingForPropertyPercentage 
		    a dqv:Metric ;
		    skos:definition "Ratio between novel preferred or alternative labels gained via skos:exactMatch links and preferred or alternative labels already in the dataset."@en
		    dqv:expectedDataType xsd:double ;
		    dqv:inDimension  :completeness .

		:completenessGain
		    a  dqv:Dimension ;
		    skos:prefLabel "Completeness Gain"@en ;
		    skos:definition "Degree to which a linkset contributes to obtaining all required information in a particular dataset."@en ;
		    dqv:inCategory :complementationGain 
		    .

		:complementationGain
		    a  dqv:Category ;
		    skos:definition "Category that groups dimensions measuring the data quality gain 
		    obtained by exploiting linksets."@en
		    .
		    
		    */
		Resource  QualityGraph=model.createResource(model.getNsPrefixURI("exq")+"qualityGraph" );
		String voidFile="";
		if (cmd.hasOption("VOIDFile")) voidFile=cmd.getOptionValue("VOIDFile");

		// how can  we figure out what linkset has been considered? 

		// Map <String, String> namespace = ImmutableMap.of("a", 1, "b", 2, "c", 3);

		String[] row =new String[numberOfColumns];
		for (int i=3;i<numberOfColumns; i++) languages[i]=listLanguageInResults.get(i-3);
		for ( String property : listPropertyInResults){
			//D 	File f=new File(CVSFilePath+"_"+property.substring(property.lastIndexOf("/")+1)+"_importingPercentage.csv");
			//D f.getParentFile().mkdirs();
			// let prepare the name of the Daq file




			//D CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');

			//D 			writer.writeNext(languages); // we wrote the list of language as first row
			for ( Entry <String,LinksetQuality> linksetq : this.linksetQualityHashMap.entrySet()){
				// for each linkset analyzed we consider which languages are involved
				//Set h = linksetq.getValue().LinkImp4p.entrySet(); 
				row =new String[numberOfColumns];
				//D outFilename=CVSFilePath.substring(CVSFilePath.lastIndexOf("/")+1);

				//D row[0]=outFilename+linksetq.getKey();
				HashMap<String, Double> res=linksetq.getValue().LinkImp4p;
				//D row[2]="0.0";
				//D row[1]=myFormatter.format((double)linksetq.getValue().linksetCardinality);
				//TODO It seems that the linkset, object and subject 's URI are not available in the object LinksetQuality, we should insert the correct linkset URI  exploited in the VOID instead of    linksetq.getKey();
				// just for now we can retrieve these info form the linksetq.getKey() which is in the form SUBJECT_OBJECT_LINKINGPROPERTIES
				String ObsFirstNamePart=linksetq.getKey();
				String ObsFirstNamePartSplited[]= linksetq.getKey().split("_");
				Resource ComputedOnResource=model.createResource(voidFile+"#"+ObsFirstNamePartSplited[0]+"-"+ObsFirstNamePartSplited[1] );
				if (res.containsKey(property)) row[2]= myFormatter.format((double)res.get(property));

				Resource propertyRes=model.createResource(property );
				//TODO to work out the total value of importing independently from the distinct languages  
				for( int i=3;i<numberOfColumns; i++ ){
					row[i]="0.0";
					if (res.containsKey(property+"@"+listLanguageInResults.get(i-3))) 
					{
						row[i]=res.get(property+"@"+listLanguageInResults.get(i-3)).toString();
					}
					String p="";
					if (property.contains("#")) p= property.substring(property.lastIndexOf("#")+1) ;
					else p= property.substring(property.lastIndexOf("/")+1) ;
					Resource obs = model.createResource(model.getNsPrefixURI("exq") + ObsFirstNamePart + p+ listLanguageInResults.get(i-3));
					obs.addProperty(properties.get("dqv:computedOn"), ComputedOnResource);
					obs.addLiteral(properties.get("dc:date"),  dateTypedLiteral);
					obs.addLiteral(properties.get("dqv:value"), model.createTypedLiteral(row[i], XSDDatatype.XSDdouble));
					obs.addProperty(properties.get("dqv:isMeasurementOf"), importing4PropertyPercentual);
					obs.addProperty(properties.get("lsq:onProperty"), propertyRes);
					obs.addLiteral(properties.get("lsq:onLanguage"), listLanguageInResults.get(i-3) );
					obs.addProperty(properties.get("sdmx-attribute:unitMeasure"), model.createResource("http://www.wurvoc.org/vocabularies/om-1.8/Percentage") );
					obs.addProperty(properties.get("qb:dataSet"), QualityGraph);
				}
			}
			
		}

		LOGGER.info("Writing results as DQV in "+RDFFilePath.toString());
		RDFAccess.writeModelInTheFileNameSpecifiedEncoding(RDFFilePath, model);
	}

	public static void main(String[] args) {
		// create Options object
		QualityResult qr;
		Options options = new Options();

		options.addOption("InputResult", true, "JSON file containing result to be analyzed ");
		//		options.addOption("JSONSerializedRelationFilePath",true, "file containing the  serialization of Equivalence Relation to be adopetd in Quality Completeness" );
		//		options.addOption("JSONResult",true, "JSON serialization of the results" );
		//		options.addOption("accessibility",false, "run accessibility assessment" );
		//		options.addOption("completeness",false, "run completeness assessment" );
		//		
		CommandLineParser parser = new PosixParser();


		try {
			CommandLine cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement

				formatter.printHelp( "QualityAssessment", options );
				System.exit(0);
			}



			// Create Logger

			//			Date date= new Date();
			//			SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			//			String cDate1= dateFormatter1.format(date);
			//			FileHandler fileTxt = new FileHandler("log/QualityStatistics"+cDate1+".txt");
			//			SimpleFormatter formatterTxt = new SimpleFormatter();
			//			fileTxt.setFormatter(formatterTxt);
			//			LOGGER.addHandler(fileTxt);



			if (cmd.hasOption("InputResult")) {
				qr=QualityResult.loadResultFromJSON(cmd.getOptionValue("InputResult"));
				qr.generateStatistics();
			}

		}catch (Exception e){ System.out.println(e.getMessage());}


	}


}
