package Quality.CKANCrawlerIndicatorExtractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONConnectionQuery {
	URL url;

	public JSONConnectionQuery(String surl){
		try {
			url= new URL(surl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		}
	
	public JSONObject getJSONResultFor( ){
	
	URLConnection connection;

	String line=null;
	StringBuilder builder = new StringBuilder();
	BufferedReader reader=null;
	JSONObject json=new JSONObject();
	try {
		connection = url.openConnection();	
		// web call and buffering of results 
		reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		while((line = reader.readLine()) != null) {
			builder.append(line);
		}
		 json = new JSONObject(builder.toString());
	} catch (IOException e) {
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}

return json;
}
}
