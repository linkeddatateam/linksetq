
# genererating the summarized results 4 Impact on the testsets for GEMETBenchmarking

set -x #echo on

grep -v --no-filename "total" test*/test*.*.csv_core#altLabel_importingAbsolute.csv > testX.csv_core#altLabel_importingAbsolute_tobedeleted.csv 

cat  test_intestazione.txt  testX.csv_core#altLabel_importingAbsolute_tobedeleted.csv >testX.csv_core#altLabel_importingAbsolute.csv 

#let's merge the testing file witout intestation assumming all the intestation are the same
grep -v --no-filename "total" test*/test*.*.csv_core#altLabel_importingPercentage.csv > testX.csv_core#altLabel_importingPercentage_tobedeleted.csv 
#put the intestation to the  
cat  test_intestazione.txt  testX.csv_core#altLabel_importingPercentage_tobedeleted.csv >testX.csv_core#altLabel_importingPercentage.csv

grep -v --no-filename "total" test*/test*.*.csv_core#prefLabel_importingAbsolute.csv > testX.csv_core#prefLabel_importingAbsolute_tobedeleted.csv 
#put the intestation to the  
cat  test_intestazione.txt  testX.csv_core#prefLabel_importingAbsolute_tobedeleted.csv >testX.csv_core#prefLabel_importingAbsolute.csv 

#let's merge the testing file witout intestation assumming all the intestation are the same
grep -v --no-filename "total" test*/test*.*.csv_core#prefLabel_importingPercentage.csv > testX.csv_core#prefLabel_importingPercentage_tobedeleted.csv 
#put the intestation to the  
cat  test_intestazione.txt  testX.csv_core#prefLabel_importingPercentage_tobedeleted.csv >testX.csv_core#prefLabel_importingPercentage.csv 

rm -f *_tobedeleted.csv






