# genererating the summarized results 4 Impact on the testsets for GEMETBenchmarking

set -x #echo on


#let's merge the intestation and the whole file of results
cat   test*/test*.*_complementedDatasetQuality.csv  >testX.csv_complementedDatasetQuality.csv 

cat  test*/test*.*_DatasetQuality.csv >testX.csv_DatasetQuality.csv

#summarizing all the result about Quality
cd ../../../
java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_complementedDatasetQuality.csv -out  Examples/Benchmarking/GEMET/testX.csv_complementedDatasetQuality_ALL.csv

java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX.csv_DatasetQuality.csv  -out  Examples/Benchmarking/GEMET/testX.csv_DatasetQuality_ALL.csv

cd  Examples/Benchmarking/GEMET/

