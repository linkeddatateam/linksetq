/**
 * 
 */
package Quality.Assesser.LinksetImpactingStructure;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Paola
 *
 */
public class NotConsideredSkosProperty {

	
	Set<String> nsp; 
	
	public NotConsideredSkosProperty(){
		 nsp = new HashSet<String>();
		
	}
	
	public void insertProperty(String p){
		nsp.add(p);
	} 
	
	
	public boolean isNotConsideredSkosProperty (String p){
		return nsp.contains(p);
		
	}
	
	
	
}
