package Quality.Assesser.setAlgebra;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;

import fr.inrialpes.exmo.align.parser.AlignmentParser;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class ExtensionalRelationFromEquivalenceClasses  implements Relation {

	public 	ArrayList<EquivalenceClass> lec;

	public  ExtensionalRelationFromEquivalenceClasses( ArrayList<EquivalenceClass> listClasses ){
		lec=new ArrayList <EquivalenceClass> ();
		EquivalenceClass  e;
		for (Iterator <EquivalenceClass> i = listClasses.iterator(); i.hasNext();){
			e=i.next();
			// add Equivance class only if the class we add does not share element with the existing 

			//if (!lec.contains(e)) lec.add(e);
			boolean acc=false;

			for (EquivalenceClass  ec: lec) {
				if (ec.equals(e) ) {
					ec.fuse(e); 
					acc= true;
				}
			}
			if (!acc) {
				lec.add(e);
			}

		}
	}

	/**
	 * It generates the relation from  the inhouse Json encoding of collection of classes of equivalence
	 * @param fileName
	 */
	public  ExtensionalRelationFromEquivalenceClasses( String fileName){	
		this(RelationsDeserializer(fileName));			
	}
	/**
	 * It generate the relation reading a set of alignment encoded according to http://alignapi.gforge.inria.fr/format.html
	 * @param FileNames collection of paths for alignment files
	 * @param threshold upper which the alignment are considered  [0,1)
	 */
	public  ExtensionalRelationFromEquivalenceClasses( ArrayList <String> FileNames, double threshold ){	
		this.addRelationsDeserializerFromAlignmentFile(FileNames, threshold);			
	}

	public boolean isRelated(String x, String y) {
		// Stringhey are related id the stay in the same class
		if (x.equals(y)) return true;
		for (EquivalenceClass ec :lec){
			if (ec.hasMember(x) && ec.hasMember(y)) return true;
		}
		return false;
	}


	public ArrayList<EquivalenceClass> getListOfEquivalenceClasses() {

		return lec;
	}


	public HashSet<String>  intersection(Set<String> firstSet, Set<String> secondSet) {
		HashSet<String> intersection= new HashSet <String>();
		for (String x: firstSet ){
			boolean equivalent=false;
			for (String y: secondSet){
				equivalent=equivalent || isRelated(x,y);

			}
			if (equivalent) intersection.add(x); 
		}

		return intersection;
	}


	private static ArrayList <EquivalenceClass> RelationsDeserializer (String fileName ) {

		BufferedReader reader;
		try {
			reader = new BufferedReader( new FileReader (fileName));

			String         line = null;
			StringBuilder  stringBuilder = new StringBuilder();
			String         ls = System.getProperty("line.separator");

			while( ( line = reader.readLine() ) != null ) {
				stringBuilder.append( line );
				stringBuilder.append( ls );
			}

			String jsonS=stringBuilder.toString();
			return new JSONDeserializer<ArrayList <EquivalenceClass>>().deserialize( jsonS );
		} catch (IOException e) {

			e.printStackTrace();
			return new ArrayList <EquivalenceClass>();
		}
	}

	/**
	 * 
	 * @param alignmentToBeConsidered list of the fileName the path to a ontology alignment file (see http://alignapi.gforge.inria.fr/format.html) containing the alignment to be  considered 
	 * @param threshold indicate the threshold of acceptable mapping, it can be set to 0 if no filtering of alignment are required   
	 * @return
	 */
	private void  addRelationsDeserializerFromAlignmentFile (ArrayList <String> alignmentToBeConsidered, double threshold ) {





		//TODO storing the relation on ArrayList <EquivalenceClass> is extremely inefficient when these relations have to be manipulated,  At the same time the structure based EquivalenceClass  is used to generate the the JSON inHouse representation of the mapping, either we can abbandon the inhouse JSON representaion, or store the equivalences in the hashmat and generate the araylist <equivalenceClass> only when required the JSONSerialization  
		HashMap <String,ArrayList <String> > rAccumulator = new HashMap <String, ArrayList <String>> ();

		for (String a :alignmentToBeConsidered){
			AlignmentParser aparser = new AlignmentParser(0);

			try {
				Alignment al = aparser.parse( new File(a).toURI() );
				if (threshold>0) al.cut(threshold);

				for (Iterator<Cell> i = al.iterator(); i.hasNext();){
					Cell x = i.next();
					// 
					org.semanticweb.owl.align.Relation r =x.getRelation();

					//TODO equivalence only ?!?, there are cases in which also "<" and ">"  make sense, we probably have to consider asymmetric relationship of mapability
					if (r.getRelation().equals("=")) {
						// are the resources in the cell  classes? who cares, if they are properties they will not be asked by the quality completeness so it is non so important
						String[] o = new String[2];	
						o[0]=x.getObject1AsURI().toString();
						o[1]= x.getObject2AsURI().toString();

						for (int ii=0; ii<o.length; ii++) {
							int otherEl= (ii==1) ? 0 : 1;
							// if we have already encountered this entity 
							if (rAccumulator.containsKey(o[ii])) {
								ArrayList <String> lstr = rAccumulator.get(o[ii]);
								// considering multiple alignment as input we can have that mapping are repeated
								boolean found=false;

								//TODO probably this checking for existence in not neccessary 
								for (String s : lstr){
									found= s.equals(o[otherEl]) ? true : false; 
								}
								if (!found) lstr.add(o[otherEl]);

							}else {
								ArrayList <String> c= new ArrayList<String> ();
								c.add(o[otherEl]);
								rAccumulator.put(o[ii],c );
							}
						}

					}
				}

			} catch (AlignmentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//TODO let's brutally generate the class of equivalence assuming that the correspondence are only EQUIVALENCE  
		HashSet<String> toBeConsidered = new HashSet <String> (rAccumulator.keySet());
		HashSet <String> alreadyConsidered= new HashSet <String> ();


		lec= new ArrayList <EquivalenceClass>();
		for ( String x : toBeConsidered ) {
			if (!alreadyConsidered.contains(x)){
				ArrayList<String> relx = rAccumulator.get(x);
				relx.add(x);
				EquivalenceClass cl= new EquivalenceClass(relx);
				cl.representativeMember =x;
				lec.add(cl);
				alreadyConsidered.addAll(relx);
			}
		}
		
		

	}






	public  void RelationsSerializer(String fileName){
		// writing an object to a file

		String jsonS = new JSONSerializer().include("elements", "representativeMember").serialize(this.lec);
		try {
			FileWriter outFile = new FileWriter(fileName+".json");
			PrintWriter out = new PrintWriter(outFile);


			// print results of previous phase
			out.print(jsonS);
			out.close();
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

}
