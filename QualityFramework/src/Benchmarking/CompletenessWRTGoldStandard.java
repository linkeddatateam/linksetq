package Benchmarking;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.util.iterator.Filter;
import com.hp.hpl.jena.util.iterator.FilterDropIterator;


import Quality.DataSetLinksetState;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;
/**
 * 
 * @author riccardoalbertoni
 * It works out extensional, intensional and LDS completeness of a dataset with respect to the gold standard.
 * 
 * Extensional and intensional completeness are developed considering the definition in 
 *  Jens Bleiholder and Felix Naumann. 2009. Data fusion. ACM Comput. Surv. 41, 1, Article 1 (January 2009), 41 pages. DOI=10.1145/1456650.1456651 http://doi.acm.org/10.1145/1456650.1456651
 *  
 *  LDS completeness is developed according to  the definition provided in 
 *  Mendes, C.P.N., Bizer, C., Ho, J., Miklos, Z., Calbimonte, J.: PlanetData D2.1 Conceptual model and best practices for high-quality metadata publishing.
 *   
 *  LDSextended is an extension of lDScompleteness which distinguish between completeness on properties when the properties might have more than a value at the time.	
 *
 * The cvs specified as out is in the form
 * 
 *"Dataset"
 *																"Extensional"		"Intensional"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#narrowMatch"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#hiddenLabel"	"LDS Compliteness http://www.w3.org/2004/02/skos/core#inScheme"	ect
 * XXXX	"1.0"			"1.0"			"1.0"																		"1.0"	
 where XXXX=   outWithoutPathbeforeOfUnderscore+"_"+inWithoutPath +"_"+goldStandardWithoutPath;
 *
 */
public class CompletenessWRTGoldStandard {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create Options object
		Options options = new Options();
		options.addOption("in", true, "dataset on which we want to workout the completeness");
		options.addOption("goldStandard", true, "dataset take as goldstandard when working out the completeness");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("out",true, "file csv  containing quality results" );
		//options.addOption("out",true, "file containing the results" );

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "CompletenessWRTGoldStandard", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("in") || !cmd.hasOption("goldStandard"))  {
				formatter.printHelp( "CompletenessWRTGoldStandard", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			String in= cmd.getOptionValue("in");
			String out=  cmd.hasOption("out")? cmd.getOptionValue("out") : "";
			String goldStandard= cmd.getOptionValue("goldStandard");
			//MergeDatasets.run( in, cmd.getOptionValue("linkset"), cmd.getOptionValue("NSConvert"),cmd.getOptionValue("out"));

			Model dataset = org.apache.jena.riot.RDFDataMgr.loadModel(in) ; 
			Model datasetCopy= ModelFactory.createDefaultModel();
			if (cmd.hasOption("NSConvert")){// let's change NS the in model 
				Map<String,String> ns= MergeDatasets.processNSconversion(cmd.getOptionValue("NSConvert"));
				StmtIterator lstmt = dataset.listStatements();
				while (lstmt.hasNext()){
					Statement s = lstmt.next();

					String subjs= s.getSubject().getURI();

					RDFNode objnode=s.getObject();

					String ps = s.getPredicate().getURI();

					String objs=null;
					for (Entry<String, String>  e : ns.entrySet()) {

						if (objnode.isResource()   ) {
							objs=objnode.asResource().getURI();	
							if  (objnode.asResource().getURI().contains(e.getKey())) {
								objs = objs.replace(e.getKey(),e.getValue());
								break;
							}
						}
					}
					for (Entry<String, String>  e : ns.entrySet()) {
						if (subjs.contains(e.getKey())){ 
							subjs= subjs.replace(e.getKey(), e.getValue());
							break;
						}
					}
					for (Entry<String, String>  e : ns.entrySet()) {
						if (ps.contains(e.getKey())){
							ps= ps.replace(e.getKey(),e.getValue());
							break;
						}
					}

					datasetCopy.add(datasetCopy.getResource(subjs), datasetCopy.getProperty(ps) ,objnode.isResource()? datasetCopy.getResource(objs): objnode);


				}
				dataset=datasetCopy;
				FileOutputStream file =new FileOutputStream("dumpChangeNS.n3"); 
				datasetCopy.write(file, "N3")	;	

			}



			Model gold = org.apache.jena.riot.RDFDataMgr.loadModel(goldStandard) ; 

			Locale locale  = new Locale("en", "UK");
			String pattern = "############.#######";
			DecimalFormat myFormatter = (DecimalFormat)
			        NumberFormat.getNumberInstance(locale);
			myFormatter.applyPattern(pattern);
			//DecimalFormat myFormatter = new DecimalFormat("############.#######");

			HashMap <String,Double> r= CompletenessWRTGoldStandard.LDS(dataset, gold);
			HashMap<String, Double > completeness4PropTot = new HashMap<String, Double >();
			HashMap <String,Double> r1= CompletenessWRTGoldStandard.LDSExtended(dataset, gold, completeness4PropTot);
			String[] CSVHeader = new String[1+3+r.size()+r1.size()+completeness4PropTot.size()];
			String[] CSVVal = new String[1+3+r.size()+r1.size()+completeness4PropTot.size()];
			String goldStandardWithoutPath;
			String inWithoutPath= in.substring(in.lastIndexOf("/")+1);
			String outWithoutPath= out.substring(out.lastIndexOf("/")+1);
			int idxBeforeUnderscore= outWithoutPath.indexOf("_");
			if (idxBeforeUnderscore>-1){
			outWithoutPath=outWithoutPath.substring(0, outWithoutPath.indexOf("_"));
			}
			goldStandardWithoutPath=goldStandard.substring(goldStandard.lastIndexOf("/")+1);
			CSVHeader[0]= "Dataset";
			CSVHeader[1]= "Extensional";
			CSVHeader[2]= "Intensional";

			CSVVal[0]= outWithoutPath+"_"+inWithoutPath +"_"+goldStandardWithoutPath;
			CSVVal[1]= myFormatter.format(CompletenessWRTGoldStandard.extensional(dataset,gold));
			CSVVal[2]=myFormatter.format(CompletenessWRTGoldStandard.intensional(dataset,gold));


			System.out.println("Extensional Completeness between \n " + in +"  and  "+ goldStandard+ " \n=" + CSVVal[1]);
			System.out.println("Intensional Completeness between \n " + in +"  and  "+ goldStandard+ " \n=" + CSVVal[2]);

			System.out.println("LDS Completeness   between \n " + in +" and "+ goldStandard+ ": \n ");
			int i=3;
			for ( Entry <String,Double> x : r.entrySet()) {
				System.out.println("Property "+ x.getKey() +": " + x.getValue() );
				CSVHeader[i]="LDS Compliteness "+x.getKey(); 
				CSVVal[i]=myFormatter.format(x.getValue());
				i++;
			}

			System.out.println("LDS Extended   between \n " + in +" and "+ goldStandard+ ": \n ");


			for ( Entry <String,Double> x : r1.entrySet()) {
				System.out.println("Property "+ x.getKey() +": " + x.getValue() );
				CSVHeader[i]="LDS Extended "+x.getKey(); 
				CSVVal[i]=myFormatter.format(x.getValue());
				i++;
			}


			for ( Entry <String,Double> x : completeness4PropTot.entrySet()) {
				System.out.println("Property TOT "+ x.getKey() +": " + x.getValue() );
				CSVHeader[i]="LDS ExtendedTot "+x.getKey(); 
				CSVVal[i]=myFormatter.format(x.getValue());
				i++;
			}

			if (cmd.hasOption("out")){
				CSVWriter writer = new CSVWriter(new FileWriter(cmd.getOptionValue("out")), '\t');

				writer.writeNext(CSVHeader);
				writer.writeNext(CSVVal);
				writer.close();
				System.out.print(" CSV containing the results has been writte into "+ out );
			}

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}

	private static HashMap<String,Double> LDS(Model dataset, Model gold) {
		HashSet<String> goldProperties= new HashSet<String>();

		// list of properties
		String queryString=" select distinct ?p   where { ?s ?p ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();
				goldProperties.add(r.getResource("?p").toString());
			}

		} catch (Exception e){
			System.out.println(e);
		}

		// for each property
		Iterator<String> iterp = goldProperties.iterator();
		HashMap <String, Double> completeness4Prop = new HashMap <String, Double>() ;
		while ( iterp.hasNext()  ) {
			HashSet<String> datasetResourcesWithP= new HashSet<String>();
			HashSet<String> goldResourcesWithP= new HashSet<String>();
			queryString=" select distinct ?s   where { ?s ?p ?o}" ;
			String property=iterp.next();
			queryString=queryString.replace("?p","<"+ property+">" );
			com.hp.hpl.jena.query.Query query1 = QueryFactory.create(queryString) ;
			QueryExecution exec1 = QueryExecutionFactory.create(query1, gold);
			try{
				res= exec1.execSelect();
				while (res.hasNext()) {
					QuerySolution r = res.next();
					goldResourcesWithP.add(r.getResource("?s").toString());
				}

			} catch (Exception e){
				System.out.println(e);
			}

			exec1 = QueryExecutionFactory.create(query1, dataset);
			//ResultSet res=null;
			String s;
			try{
				res= exec1.execSelect();
				while (res.hasNext()) {
					QuerySolution r = res.next();
					s=r.getResource("?s").toString();
					if ( goldResourcesWithP.contains(s)) datasetResourcesWithP.add(s);
				}

			} catch (Exception e){
				System.out.println(e);
			}

			completeness4Prop.put(property, datasetResourcesWithP.size() / ((double) goldResourcesWithP.size()));

		}
		return completeness4Prop;
	}

	private static double intensional(Model dataset, Model gold) {
		// let's extract the list of properties in the gold
		HashSet<String> goldProperties= new HashSet<String>();

		String queryString=" select distinct ?p   where { ?s ?p ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();
				goldProperties.add(r.getResource("?p").toString());
			}

		} catch (Exception e){
			System.out.println(e);
		}

		// let's extract the list of properties in the dataset that are also in the gold
		HashSet<String> datasetProperties= new HashSet<String>();

		//String queryString=" select distinct ?p   where { ?s ?p ?o}" ;
		//com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		exec = QueryExecutionFactory.create(query, dataset);
		//ResultSet res=null;
		String s;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();
				s=r.getResource("?p").toString();
				if ( goldProperties.contains(s)) datasetProperties.add(s);
			}

		} catch (Exception e){
			System.out.println(e);
		}

		return datasetProperties.size() / ((double) goldProperties.size());

	}

	private static double extensional(Model dataset, Model gold) {

		HashSet <String> resourcesInDataset =new HashSet<String> () ;

		HashSet <String> resourcesInGold =new HashSet<String> () ;

		// let's work out the number of entities in the gold
		String queryString=" select distinct ?s  where {{ ?s ?p ?o} union {?o1 ?p1 ?s}  filter (!isBlank(?s))}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();

				RDFNode n = r.get("?s");
				if (n.isResource())
					resourcesInGold.add(n.asResource().toString());
			}

		} catch (Exception e){
			System.out.println(e);
		}

		exec = QueryExecutionFactory.create(query, dataset);
		try{
			String s;
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();

				RDFNode n = r.get("?s");
				if (n.isResource()){
					s=n.asResource().toString();
					if  (resourcesInGold.contains(s)) resourcesInDataset.add(s);
				}
			}

		} catch (Exception e){
			System.out.println(e);
		}

		//		FilterDropIterator <RDFNode> goldNode ;//= gold.listObjects();
		//
		//		goldNode= (com.hp.hpl.jena.util.iterator.FilterDropIterator <RDFNode>) (gold.listObjects()).filterDrop( new Filter<RDFNode> () {
		//			public boolean accept( RDFNode o ) {
		//
		//				return  o.isLiteral() || o.isAnon();
		//			}} );
		//
		//		RDFNode d;
		//
		//		System.out.println(" gold standard Entities :" );
		//
		//		while (goldNode.hasNext()) {
		//			// check if it is present in the goldstandard
		//			d= goldNode.next();
		//			resourcesInGold.add(d.toString()) ;
		//			System.out.println(d.toString());
		//		}
		//
		//
		//		FilterDropIterator <RDFNode> datasetNode;
		//
		//		datasetNode= (FilterDropIterator <RDFNode> ) dataset.listObjects().filterDrop( new Filter<RDFNode> () {
		//			public boolean accept( RDFNode o ) {
		//
		//				return o.isLiteral() || o.isAnon();
		//			}} );
		//
		//		System.out.println(" dataset Entities :" );
		//		while (datasetNode.hasNext()) {
		//			// check if it is present in the goldstandard
		//			d= datasetNode.next();
		//			if (gold.containsResource(d)){
		//				resourcesInDataset.add(d.toString()) ;
		//				System.out.println(d.toString());
		//			}
		//		}


		return resourcesInDataset.size()/ ((double) resourcesInGold.size()); 
	}

	private static HashMap<String,Double> LDSExtended(Model dataset, Model gold, HashMap<String,Double> completeness4PropTot ) {
		HashSet<String> goldProperties= new HashSet<String>();

		// list of properties
		String queryString=" select distinct ?p   where { ?s ?p ?o}" ;
		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, gold);
		ResultSet res=null;
		try{
			res= exec.execSelect();
			while (res.hasNext()) {
				QuerySolution r = res.next();
				goldProperties.add(r.getResource("?p").toString());
			}

		} catch (Exception e){
			System.out.println(e);
		}

		// for each property
		Iterator<String> iterp = goldProperties.iterator();
		HashMap <String, Double> completeness4Prop = new HashMap <String, Double>() ;
		//  completeness4PropTot = new HashMap <String, Double>() ;
		while ( iterp.hasNext()  ) {
			//			HashSet<String> datasetResourcesWithP= new HashSet<String>();
			//			HashSet<String> goldResourcesWithP= new HashSet<String>();
			int goldResourcesPvalues=0;
			int datasetResourcesPvalues=0;
			int goldResourcesPvaluesTot=0;
			int datasetResourcesPvaluesTot=0;
			int numberOfEntities=0;
			queryString=" select distinct ?s   where { ?s ?p ?o}" ;
			String property=iterp.next();
			queryString=queryString.replace("?p","<"+ property+">" );
			com.hp.hpl.jena.query.Query query1 = QueryFactory.create(queryString) ;
			double accRatio = 0.0;
			// entities that have that property in the gold standard
			QueryExecution exec1 = QueryExecutionFactory.create(query1, gold);
			try{
				res= exec1.execSelect();
				while (res.hasNext()) {
					numberOfEntities++;
					QuerySolution r = res.next();
					//goldResourcesWithP.add(r.getResource("?s").toString());

					// get the number of property p that have for s in the gold
					Resource s=r.getResource("?s");
					StmtIterator istm = s.listProperties(gold.getProperty(property));
					while (istm.hasNext()) {
						istm.next();
						goldResourcesPvalues++;
						goldResourcesPvaluesTot++;
					}
					// get the number of property p that have for s in the dataset
					s=dataset.getResource(s.getURI());
					istm = s.listProperties(dataset.getProperty(property));
					while (istm.hasNext()) {
						istm.next();
						datasetResourcesPvalues++;
						datasetResourcesPvaluesTot++;

					}

					// Accumulate the ratio between    (# p in the dataset for s) and (# p in the gold standard for s )   
					accRatio=accRatio + (datasetResourcesPvalues/ (double) goldResourcesPvalues);
					goldResourcesPvalues=0;
					datasetResourcesPvalues=0;
				}

			} catch (Exception e){
				System.out.println(e);
			}	

			completeness4Prop.put(property, accRatio / ((double) numberOfEntities));
			completeness4PropTot.put(property,datasetResourcesPvaluesTot/((double) goldResourcesPvaluesTot) );
		}
		return completeness4Prop;
	}


	// return the ratio between the two

}



