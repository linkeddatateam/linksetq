package Quality.Assesser.LinksetImpactingStructure;

public class RDFEdge {

protected String subject=new String(); 
protected String object=new String();

public String getSubject() {
	return subject;
}
public void setSubject(String subject) {
	this.subject = subject;
}
public String getObject() {
	return object;
}
public void setObject(String object) {
	this.object = object;
}


public String toString()
{
	String str ="(" + subject+ ","+object + ")";
	return str;

}

	
public RDFEdge(String s, String o)
{
	this.subject=s;
	this.object=o;
}
	
public boolean equals(Object e)
{
	return (this.subject.equals(((RDFEdge)e).subject.trim()) &&		
			this.object.equals(((RDFEdge)e).object.trim()));
	
	}

}
