package Quality.Assesser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

import Benchmarking.CentralityCompletenessWRTGoldStandard;
import Benchmarking.CentralityMergeDataset;
import Benchmarking.MergeDatasets;
import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;

public class DatasetAnalysis {



	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.DatasetAnalysis.class.getName()); 




	public static void main(String[] args) {


		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration conf = ctx.getConfiguration();
		conf.getLoggerConfig(Benchmarking.CentralityMergeDataset.class.getName()).setLevel(Level.DEBUG);
		ctx.updateLoggers(conf);


		// create Options object
		Options options = new Options();
		options.addOption("objDatasetPath", true, "object we want to analyse");
		options.addOption("subjDatasetPath", true, "subject we want to analyse");
		options.addOption("linksetPath", true, "linkset we want to analyse");
		options.addOption("linkingProperty", true, "linkingProperty we want to analyse");
		options.addOption("K", true, "considered hops");
		options.addOption("out",true, "file csv  containing dataset analysis results" );
		options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");		
		//-linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch 

		CSVWriter writeroutanalysis=null;
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		Map<String,String> ns=null;		
		String[] CSVHeader = new String[11];
		String[] value= new String[11];

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		HashMap<String, Double> weightForCentrality;
		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "DatasetAnalysis", options );
				System.exit(0);
			}
			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#broader>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#related>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#narrower>", 1.0);
				formatter.printHelp("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default", options);
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));



			//			other check on options that we could want 
			if (!cmd.hasOption("objDatasetPath") ||  !cmd.hasOption("subjDatasetPath")|| 
					!cmd.hasOption("K") ||			!cmd.hasOption("linksetPath") ||	
					!cmd.hasOption("out")|| !cmd.hasOption("linkingProperty") 		
					)  {
				formatter.printHelp( "DataseAnalysis", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}





			String subjectPath= cmd.getOptionValue("subjDatasetPath");
			String objectPath= cmd.getOptionValue("objDatasetPath");
			String outanalysis=cmd.getOptionValue("outanalysis");
			String linksetPath=cmd.getOptionValue("linksetPath");
			String linkingProperty=cmd.getOptionValue("linkingProperty");

			String Wrel= cmd.getOptionValue("WeightFilePath");
			int kinput=Integer.parseInt(cmd.getOptionValue("K"));

			double subj_res=-1.0;
			double obj_res=-1.0;
			double obj_res_noL=-1.0;
			double subj_res_noL=-1.0;			
			double subj_noRes=-1.0;
			double obj_noRes=-1.0;

			if (cmd.hasOption("out")){
				outanalysis=cmd.getOptionValue("out");

				writeroutanalysis = new CSVWriter(new FileWriter(outanalysis), '\t');

				obj_res_noL=DatasetAnalysis.analyseDatasetRestrictedNoLinkset(objectPath, weightForCentrality, linksetPath, linkingProperty);
				subj_res_noL=DatasetAnalysis.analyseDatasetRestrictedNoLinkset(subjectPath,  weightForCentrality, linksetPath, linkingProperty);


				obj_res=DatasetAnalysis.analyseDatasetRestricted(objectPath, weightForCentrality);
				subj_res=DatasetAnalysis.analyseDatasetRestricted(subjectPath,  weightForCentrality);

				obj_noRes=analyseDatasetNotRestricted(objectPath,  weightForCentrality);
				subj_noRes=analyseDatasetNotRestricted(subjectPath, weightForCentrality);	


				CSVHeader[0]= "TestName";
				CSVHeader[1]= "linksetName";
				CSVHeader[2]= "SKOSRelationsConsidered"; 

				CSVHeader[3]= "linkingProperty";
				CSVHeader[4]= "TotalConceptsObjectRestrictedNoLinkset";
				CSVHeader[5]= "TotalConceptsSubjRestrictedNoLinkset";
				CSVHeader[6]= "TotalConceptsObjectRestricted";
				CSVHeader[7]= "TotalConceptsSubjRestricted";				
				CSVHeader[8]= "TotalConceptsObject";
				CSVHeader[9]= "TotalConceptsSubj";				
				CSVHeader[10]= "K";


				StringBuilder sw=new StringBuilder();
				for(String w: weightForCentrality.keySet())
				{
					sw.append(w);
				}


				writeroutanalysis.writeNext(CSVHeader);

				//PrintResults(subj_res, obj_res);


				value[0]=outanalysis.substring(outanalysis.lastIndexOf("/")+1, outanalysis.indexOf("_", outanalysis.lastIndexOf("/")+1));
				value[1]=linksetPath.substring(linksetPath.lastIndexOf("/")+1);
				value[2]=sw.toString();
				value[3]=linkingProperty;
				value[4]=myFormatter.format(obj_res_noL);
				value[5]=myFormatter.format(subj_res_noL);
				value[6]=myFormatter.format(obj_res);
				value[7]=myFormatter.format(subj_res);
				value[8]=myFormatter.format(obj_noRes);
				value[9]=myFormatter.format(subj_noRes);
				value[10]=myFormatter.format(kinput);

				writeroutanalysis.writeNext(value);

				writeroutanalysis.close();

				System.out.print(" CSV containing the results has been writte into "+ outanalysis );

			}

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}




	public static Map<String, String> processNSconversion(String fileNSConvert) {

		HashMap <String,String> result= new HashMap<String, String> ();
		if (fileNSConvert==null) return  result;
		//let's read the file and fill the MAp
		BufferedReader br;
		try {
			if (fileNSConvert!=null){
				br = new BufferedReader(new FileReader(fileNSConvert));

				String line;
				line = br.readLine();

				while (line != null) {
					String[] s= line.split("-->");
					result.put(s[0].trim(), s[1].trim());
					line = br.readLine();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return result;
	}


	public static HashMap<String, Integer> analyseDatasetsConcepts(
			DatasetInfo obj,DatasetInfo subj, String objnsp, String subjnsp, 
			HashMap<String, Double> weightForCentrality, ArrayList<String> objlinkset, 
			String namef) 
					throws IOException {
		/*	 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		 PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>


		 PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
		 select distinct ?x where { 
		  {?x ?p ?y } union { ?y ?p ?x }. 
		   FILTER ( regex(str(?x), "^^http://www.eionet.europa.eu/gemet/concept")). 
		   FILTER ( ?p IN (<http://www.w3.org/2004/02/skos/core#narrower>, <http://www.w3.org/2004/02/skos/core#broader>, 
		       <http://www.w3.org/2004/02/skos/core#related>))
		 } limit 1000 offset 0*/

		HashMap<String, Integer> numberconcepts=new HashMap<String, Integer>();
		int offset= 0, limit=500;
		Query qry;
		QueryExecution qe;
		ResultSetRewindable resP=null;
		SparqlEndPointErrorMessage  err2;
		int totalconcept=0;
		//count concepts of subj and obj restricted on namespace, and considered properties
		String qsubjrestr="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }. "
				+ "FILTER ( regex(str(?x),\"^^"+subjnsp+"\",\"i\") ).";
		String qobjrestr="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "			
				+" {?x ?p ?y2} union { ?y2 ?p ?x }. "
				+ "FILTER ( regex(str(?x),\"^^"+objnsp+"\",\"i\") )."	;


		
		
		//count concepts of subj and obj restricted on namespace		
		String qsubjNONrestrPropRestNmsp="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "				
				+" {?x ?p ?y2} ."
				+ "FILTER ( regex(str(?x),\"^^"+subjnsp+"\",\"i\"))} ";

		String qobjNONrestrPropRestNmsp="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+" {?x ?p ?y2}. \n"
				+ "FILTER ( regex(str(?x),\"^^"+objnsp+"\",\"i\") )}";	


		//count concepts of subj and obj restricted on namespace, and skos type concepts
		String qsubjNONrestrPropRestNmspAndRestSKOSTypeConc="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+" {?x ?p ?y2}."
				+ "FILTER ( regex(str(?x),\"^^"+subjnsp+"\",\"i\"))"
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>}";


		String qobjNONrestrPropRestNmspAndRestSKOSTypeConc="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+" {?x ?p ?y2}. "
				+ "FILTER ( regex(str(?x),\"^^"+objnsp+"\",\"i\"))"	
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>}";




		boolean primo=true;

		for (String rel: weightForCentrality.keySet())
		{
			if(primo)
			{
				qsubjrestr+="filter ( ?p IN (<"+rel+"> ";
				qobjrestr+="filter ( ?p IN ( <"+rel+"> ";
				primo=false;

			}
			else{
				qsubjrestr+=", <"+rel+">";
				qobjrestr+=", <"+rel+">";

			}
		}


		qsubjrestr+="))}";
		qobjrestr+="))}";

		String qsubjrestr1="";
		String qobjrestr1="";

		//analyze subject concepts restricted and non restricted
		//concepts of subj restricted on namespace, and considered properties

		do{ 
			qsubjrestr1=qsubjrestr+" LIMIT "+limit +"\n"+
					" OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjrestr \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(subj, qsubjrestr1, err2, null));
			totalconcept+=resP.size();

			offset=offset+limit;
		}while (resP.size()>0);





		numberconcepts.put("TotalConceptsSubjRestrictedNmspAndProp", Integer.valueOf(totalconcept));
		//numberconcepts.put("TotalConceptsSubjRestrictedNmspAndProp", Integer.valueOf(nodes.size()));


		resP.reset();
		totalconcept=0;
		offset= 0; limit=500;
		//count concepts of subj restricted on namespace			
		do{ 
			qsubjrestr1=qsubjNONrestrPropRestNmsp+" LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjNONrestrPropRestNmspFFFFF \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(subj, qsubjrestr1, err2, null));
			totalconcept+=resP.size();
			offset=offset+limit;
		}while (resP.size()>0);
		numberconcepts.put("TotalConceptsSubjRestrictedNmsp", Integer.valueOf(totalconcept));

		//count concepts of subj  restricted on namespace, and skos type concepts	
		resP.reset();
		totalconcept=0;
		offset= 0; limit=500;
		do{ 
			qsubjrestr1=qsubjNONrestrPropRestNmspAndRestSKOSTypeConc+" LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(subj, qsubjrestr1, err2, null));
			totalconcept+=resP.size();
			offset=offset+limit;
		}while (resP.size()>0);
		numberconcepts.put("TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept", Integer.valueOf(totalconcept));


		//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);

		//analyze object concepts
		//concepts of object restricted on namespace, and considered properties		
		resP.reset();
		totalconcept=0;
		offset= 0; limit=500;
		HashSet<String> nodes=new HashSet<String>();
		ArrayList<ResultSet> lnodes=new ArrayList<ResultSet>();
		do{ 
			qobjrestr1=qobjrestr+" LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(obj, qobjrestr1, err2, null));
			totalconcept+=resP.size();
			lnodes.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);
		//LOGGER.info(" query TotalConceptsObjectRestrictedNmspAndProp \n" +qobjrestr1);
		for(ResultSet resP2:lnodes)
		{
			QuerySolution qsol;
			String y2uri;

			while(resP2.hasNext()) {
				qsol=resP2.next();
				//get object and subject
				y2uri=qsol.getResource("?x").getURI().trim();
				nodes.add(y2uri);
			}
		}		

		numberconcepts.put("TotalConceptsObjectRestrictedNmspAndProp", Integer.valueOf(nodes.size()));
		nodes.removeAll(objlinkset);
		numberconcepts.put("TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset", Integer.valueOf(nodes.size()));
		
		File f=new File(namef+"_URIConceptReached_less_URIObjlinkset.csv");
		String[] row=new String[2];

		CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
		for (String URIreached: nodes)
		{
			row[0]=URIreached;
			writer.writeNext(row);

		}
		writer.flush();
		writer.close();
		
		
		//count concepts of object restricted on namespace
		resP.reset();
		totalconcept=0;
		offset= 0; limit=500;
		do{ 
			qobjrestr1=qobjNONrestrPropRestNmsp+" LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(obj, qobjrestr1, err2, null));
			totalconcept+=resP.size();
			offset=offset+limit;
		}while (resP.size()>0);
		numberconcepts.put("TotalConceptsObjectRestrictedNmsp", Integer.valueOf(totalconcept));


		//count concepts of object  restricted on namespace, and skos type concepts			
		resP.reset();
		totalconcept=0;
		offset= 0; limit=500;
		do{ 
			qobjrestr1=qobjNONrestrPropRestNmspAndRestSKOSTypeConc+"LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;

			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(obj, qobjrestr1, err2, null));
			totalconcept+=resP.size();
			offset=offset+limit;
		}while (resP.size()>0);
		numberconcepts.put("TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept", Integer.valueOf(totalconcept));


		return numberconcepts;
	}













	private static double analyseDatasetRestrictedNoLinkset(String datasetaux,
			HashMap<String, Double> weightForCentrality, String linksetPath, String linkingProperty) 
	{

		int offset= 0, limit=1000;
		//	int r,c;
		String xuri;
		//Stack<String> next= new Stack<String>();
		////String[] row1 =new String[2];

		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		HashSet <String>  conceptsIN=new HashSet<String>();
		Model datasetm = org.apache.jena.riot.RDFDataMgr.loadModel(datasetaux);
		Model datasetl = org.apache.jena.riot.RDFDataMgr.loadModel(linksetPath);

		double dsConceptsNum=-1.0;

		String qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>. "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }. ";

		boolean primo=true;
		for (String rel: weightForCentrality.keySet())
		{
			if(primo)
			{
				qs+="filter ( ?p= "+rel+"";
				primo=false;

			}
			else{
				qs+="|| ?p="+rel+"";
			}
		}
		qs+=")}";
		String qs1="";
		Query qry;
		QueryExecution qe;
		ResultSet rs;
		do{ 
			qs1=qs+"LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;
			qry = QueryFactory.create(qs1);
			qe = QueryExecutionFactory.create(qry,datasetm);
			rs = qe.execSelect();			
			listresP.add(rs);
			offset=offset+limit;
		}while (rs.hasNext());

		QuerySolution qsol;
		for(ResultSet resP2:listresP)
		{
			while(resP2.hasNext()) {

				qsol=resP2.next();
				Property jenap= datasetl.createProperty(linkingProperty);
				if(!(datasetl.contains(null, jenap, qsol.getResource("?x")) 
						|| datasetl.contains(qsol.getResource("?x"), jenap,(RDFNode)null)))
				{	xuri=qsol.getResource("?x").getURI().trim();
				conceptsIN.add(xuri);
				}
			}
		}


		dsConceptsNum=conceptsIN.size();


		return dsConceptsNum;
	}


	private static double analyseDatasetNotRestricted(String datasetaux,
			HashMap<String, Double> weightForCentrality) 
	{

		int offset= 0, limit=1000;
		//	int r,c;
		String xuri;
		//Stack<String> next= new Stack<String>();
		////String[] row1 =new String[2];

		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		HashSet <String>  conceptsIN=new HashSet<String>();
		Model datasetm = org.apache.jena.riot.RDFDataMgr.loadModel(datasetaux);


		double dsConceptsNum=-1.0;

		String qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>. "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }}";
		String qs1="";
		Query qry;
		QueryExecution qe;
		ResultSet rs;
		do{ 
			qs1=qs+"LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;
			qry = QueryFactory.create(qs1);
			qe = QueryExecutionFactory.create(qry,datasetm);
			rs = qe.execSelect();			
			listresP.add(rs);
			offset=offset+limit;
		}while (rs.hasNext());

		QuerySolution qsol;
		for(ResultSet resP2:listresP)
		{
			while(resP2.hasNext()) {

				qsol=resP2.next();
				xuri=qsol.getResource("?x").getURI().trim();
				conceptsIN.add(xuri);
			}
		}


		dsConceptsNum=conceptsIN.size();


		return dsConceptsNum;
	}


	private static double analyseDatasetRestricted(String datasetaux,
			HashMap<String, Double> weightForCentrality) 
	{

		int offset= 0, limit=1000;
		//	int r,c;
		String xuri;
		//Stack<String> next= new Stack<String>();
		////String[] row1 =new String[2];

		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		HashSet <String>  conceptsIN=new HashSet<String>();
		Model datasetm = org.apache.jena.riot.RDFDataMgr.loadModel(datasetaux);


		double dsConceptsNum=-1.0;

		String qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> "
				+ "SELECT distinct ?x WHERE  { "
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>. "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }. ";

		boolean primo=true;
		for (String rel: weightForCentrality.keySet())
		{
			if(primo)
			{
				qs+="filter ( ?p= "+rel+"";
				primo=false;

			}
			else{
				qs+="|| ?p="+rel+"";
			}
		}
		qs+=")}";
		String qs1="";
		Query qry;
		QueryExecution qe;
		ResultSet rs;
		do{ 
			qs1=qs+"LIMIT   "+limit +"\n"+
					"OFFSET  "+offset;
			qry = QueryFactory.create(qs1);
			qe = QueryExecutionFactory.create(qry,datasetm);
			rs = qe.execSelect();			
			listresP.add(rs);
			offset=offset+limit;
		}while (rs.hasNext());

		QuerySolution qsol;
		for(ResultSet resP2:listresP)
		{
			while(resP2.hasNext()) {

				qsol=resP2.next();
				xuri=qsol.getResource("?x").getURI().trim();
				conceptsIN.add(xuri);
			}
		}


		dsConceptsNum=conceptsIN.size();


		return dsConceptsNum;
	}


	private static boolean isInSkosPropertySet(String p) {

		return ( p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#narrower") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#related") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#broader"));
	}

	/**
	 * It reads from a CSV file the relation's weights 
	 * @param filePath
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String,Double> readWeight(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		String [] nextLine;
		HashMap <String, Double> res = new  HashMap <String, Double> ();
		while ((nextLine = reader.readNext()) != null) {
			// nextLine[] is an array of values from the line
			res.put(nextLine[0],  Double.valueOf(nextLine[1]));
		}
		return res;
	}



}
