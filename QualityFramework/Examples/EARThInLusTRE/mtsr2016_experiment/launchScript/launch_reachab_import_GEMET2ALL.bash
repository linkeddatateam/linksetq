set -x #echo on
cd ../

##GEMET EARTh
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath  Examples/EARThInLusTRE/20160127/GEMET.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl  -subjUriSpace http://www.eionet.europa.eu/gemet/concept/  -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/  -linksetPath   Examples/EARThInLusTRE/20160127/GEMET.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/GEMET2EARTH_reachability_k4.csv -flagDataset  o -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2EARTH_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2EARTH_importedValues.csv


##GEMET - ThIST 
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/EARThInLusTRE/20160127/GEMET.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/THIST.ttl  -subjUriSpace  http://www.eionet.europa.eu/gemet/concept/   -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -linksetPath   Examples/EARThInLusTRE/20160127/GEMET.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/GEMET2THIST_reachability_k4.csv -flagDataset  o   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2THIST_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2THIST_importedValues.csv



##GEMET - AGROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath  Examples/EARThInLusTRE/20160127/GEMET.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -subjUriSpace  http://www.eionet.europa.eu/gemet/concept/    -objUriSpace http://aims.fao.org/aos/agrovoc/  -linksetPath   Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/GEMET2AGROVOC_reachability_k4.csv -flagDataset  o  -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2AGROVOC_importing.csv  -importedStatementsPath Examples/EARThInLusTRE/20160127/Importing_evaluation/GEMET2AGROVOC_importedValues.csv

