package Benchmarking;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Quality.SparqlEndPointErrorMessage;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;



/**
 * 
 * @author paolapodestà
 * It works out the centrality quality in terms of number of rachable entities with 
 * respect to those existing in the goldstandard. 
 * IN particular it computes: 
 * (|numberofEntities(dataset)| intersection |numberofEntities(GoldStandard)|) / |numberofEntities(GoldStandard)|  
 * 
 *  
 */
public class CentralityCompletenessWRTGoldStandard {
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName());


	public static void main(String[] args) {

		Options options = new Options();
		options.addOption("complemented", true, "complemented dataset on which we want to workout the normalized Gain");
		options.addOption("goldStandard", true, "dataset take as goldstandard when working out the completeness");
		options.addOption("NSConvert",true, "text file containing the namespace conversions in form of ns1-->ns2 \n ns1-->ns2\n " );
		options.addOption("outcompleteness",true, "file csv  containing completeness results" );
		options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");		
		//options.addOption("out",true, "file containing the results" );

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		CSVWriter writer=null;
		CSVWriter writeroutconceptscomplemented=null;

		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		Map<String,String> ns=null;		
		String[] CSVHeader = new String[8];

		HelpFormatter formatter = new HelpFormatter();
		try {
			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "CentralityCompletenessWRTGoldStandard", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("complemented") 	|| !cmd.hasOption("goldStandard") )  {
				formatter.printHelp( "CentralityCompletenessWRTGoldStandard", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}


			HashMap<String, Double> weightForCentrality;
			String complementedPath= cmd.getOptionValue("complemented");
			String outgain="";
			String outcompleteness="";
			String goldStandard= cmd.getOptionValue("goldStandard");
			String Wrel= cmd.getOptionValue("WeightFilePath");
			String in= cmd.getOptionValue("in");

			//numconcettidatasets numconcetticomplemented numconcetti	conceptsintersection	completeness	gain 


			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#broader>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#related>", 1.0);
				weightForCentrality.put("<http://www.w3.org/2004/02/skos/core#narrower>", 1.0);
				formatter.printHelp("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default", options);
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));

			if (cmd.hasOption("NSConvert")){// let's change NS the in model 
				ns= MergeDatasets.processNSconversion(cmd.getOptionValue("NSConvert"));
			}


			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "CompletenessWRTGoldStandard", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("complemented") || !cmd.hasOption("goldStandard")||!cmd.hasOption("outcompleteness") )  {
				formatter.printHelp( "CompletenessWRTGoldStandard", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			if (cmd.hasOption("NSConvert")){// let's change NS the in model 
				ns= CentralityMergeDataset.processNSconversion(cmd.getOptionValue("NSConvert"));
			}


			if (cmd.hasOption("outcompleteness")){
				writer = new CSVWriter(new FileWriter(cmd.getOptionValue("outcompleteness")), '\t');
				computeCentralityCompletnessRestrictRel(complementedPath, goldStandard,ns,cmd.getOptionValue("outcompleteness"),
						writer,weightForCentrality);
				writer.flush();
				writer.close();
			}
		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}




	/**
	 * It reads from a CSV file the relation's weights 
	 * @param filePath
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String,Double> readWeight(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		String [] nextLine;
		HashMap <String, Double> res = new  HashMap <String, Double> ();
		while ((nextLine = reader.readNext()) != null) {
			// nextLine[] is an array of values from the line
			res.put(nextLine[0],  Double.valueOf(nextLine[1]));
		}


		return res;


	}


	//added december 2015 to explain the error between gain and reachability
	public static HashMap<String,String> computeCentralityCompletnessNoRestrictRel(String inaux, String gsaux, 
			Map<String,String> nsaux, String numbertest, CSVWriter writer, HashMap<String, Double> weightForCentrality) throws IOException
			{

		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		String[] CSVVal = new String[2];
		String[] qsols= new String[1];
		HashMap<String,String> res=new HashMap<String,String>();

		//HashSet <ResultSet> conceptsGS= new HashSet<ResultSet>();
		HashSet <String> conceptsGS= new HashSet<String>();
		ResultSetRewindable intermediateRS=null;
		HashSet <String>  conceptsIN=new HashSet<String>();
		HashSet <String>  intersection=new HashSet<String>();
		Model dataset = org.apache.jena.riot.RDFDataMgr.loadModel(inaux) ; 
		Model goldStandard = org.apache.jena.riot.RDFDataMgr.loadModel(gsaux) ;
		double gsConceptsNum=-1.0;
		double dsConceptsNum=-1.0;

		String qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT distinct ?x "
				+"WHERE  { "
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>. "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }}";


		int offset=0;
		int limit= 1000;

		String qs1=qs+
				"LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;
		LOGGER.info("query to select link in linkset "+qs1);

		res.put("TestNameNoR", numbertest.substring(numbertest.lastIndexOf("/")+1));
		res.put("DatasetsNameNoR", inaux.substring(inaux.lastIndexOf("/")+1)+"_"+gsaux.substring(gsaux.lastIndexOf("/")+1));

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(goldStandard, qs1, err2, null));
		while (intermediateRS.size()>0)
		{
			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			//LOGGER.info("query to select link in linkset "+qs);
			while(intermediateRS.hasNext()) {
				QuerySolution qsol=intermediateRS.next();
				conceptsGS.add(qsol.getResource("?x").getURI());
			}
			err2=new SparqlEndPointErrorMessage();
			intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(goldStandard, qs1, err2, null));
		}

		offset=0;
		limit= 1000;	
		gsConceptsNum=conceptsGS.size();
		res.put("TotalConceptsGSNoR", myFormatter.format(gsConceptsNum));

		qs1=qs+" limit "+ limit+" offset "+ offset;
		err2=new SparqlEndPointErrorMessage();
		intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dataset, qs1, err2, null));
		while (intermediateRS.size()>0)
		{
			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			//LOGGER.info("query to select link in linkset "+qs);
			while(intermediateRS.hasNext()) {
				QuerySolution qsol=intermediateRS.next();
				if(nsaux.size()==1){
					for (String nsToChange: nsaux.keySet()){ 
						Resource qresource= qsol.getResource("?x");
						String qsolnew=qresource.getURI().trim().replaceAll("^"+nsToChange+"*", nsaux.get(nsToChange));
						conceptsIN.add(qsolnew);
						qsols[0]=qsolnew;

						writer.writeNext(qsols);
					}
				}
			}
			err2=new SparqlEndPointErrorMessage();
			intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dataset, qs1, err2, null));
		}
		dsConceptsNum=conceptsIN.size();

		res.put("TotalConceptsDatasetNoR",  String.valueOf(dsConceptsNum));

		conceptsGS.retainAll(conceptsIN);
		res.put("TotalConceptsIntersectionNoR", myFormatter.format(conceptsGS.size()));


		res.put("ValueOfCompletenessNoR", myFormatter.format(conceptsGS.size()/gsConceptsNum));

		return res;
			}


	public static HashMap<String,String> computeCentralityCompletnessRestrictRel(String inaux, String gsaux, 
			Map<String,String> nsaux, String numbertest, CSVWriter writer, HashMap<String, Double> weightForCentrality) throws IOException
			{

		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);

		String[] CSVVal = new String[2];
		String[] qsols= new String[1];
		HashMap<String,String> res=new HashMap<String,String>();

		//HashSet <ResultSet> conceptsGS= new HashSet<ResultSet>();
		HashSet <String> conceptsGS= new HashSet<String>();
		ResultSetRewindable intermediateRS=null;
		HashSet <String>  conceptsIN=new HashSet<String>();
		HashSet <String>  intersection=new HashSet<String>();
		Model dataset = org.apache.jena.riot.RDFDataMgr.loadModel(inaux) ; 
		Model goldStandard = org.apache.jena.riot.RDFDataMgr.loadModel(gsaux) ;
		double gsConceptsNum=-1.0;
		double dsConceptsNum=-1.0;

		String qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT distinct ?x "
				+"WHERE  { "
				+ "?x  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>. "
				+" {?x ?p ?y2} union { ?y2 ?p ?x }. ";
		//+ "FILTER isIRI(?y2).";

		int offset=0;
		int limit= 1000;
		boolean primo=true;
		for (String rel: weightForCentrality.keySet())
		{
			if(primo)
			{
				qs+="filter ( ?p= "+rel+"";
				primo=false;

			}
			else{
				qs+="|| ?p="+rel+"";
			}
		}
		qs+=")}";
		String qs1=qs+
				"LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;
		LOGGER.info("query to select link in linkset "+qs1);

		res.put("TestName", numbertest.substring(numbertest.lastIndexOf("/")+1));
		res.put("DatasetsName", inaux.substring(inaux.lastIndexOf("/")+1)+"_"+gsaux.substring(gsaux.lastIndexOf("/")+1));

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(goldStandard, qs1, err2, null));
		while (intermediateRS.size()>0)
		{
			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			//LOGGER.info("query to select link in linkset "+qs);
			while(intermediateRS.hasNext()) {
				QuerySolution qsol=intermediateRS.next();
				conceptsGS.add(qsol.getResource("?x").getURI());
			}
			err2=new SparqlEndPointErrorMessage();
			intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(goldStandard, qs1, err2, null));
		}

		offset=0;
		limit= 1000;	
		gsConceptsNum=conceptsGS.size();
		res.put("TotalConceptsGS", myFormatter.format(gsConceptsNum));

		qs1=qs+" limit "+ limit+" offset "+ offset;
		err2=new SparqlEndPointErrorMessage();
		intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dataset, qs1, err2, null));
		while (intermediateRS.size()>0)
		{
			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			//LOGGER.info("query to select link in linkset "+qs);
			while(intermediateRS.hasNext()) {
				QuerySolution qsol=intermediateRS.next();
				if(nsaux.size()==1){
					for (String nsToChange: nsaux.keySet()){ 
						Resource qresource= qsol.getResource("?x");
						String qsolnew=qresource.getURI().trim().replaceAll("^"+nsToChange+"*", nsaux.get(nsToChange));
						conceptsIN.add(qsolnew);
						qsols[0]=qsolnew;

						writer.writeNext(qsols);
					}
				}
			}
			err2=new SparqlEndPointErrorMessage();
			intermediateRS=ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dataset, qs1, err2, null));
		}
		dsConceptsNum=conceptsIN.size();

		res.put("TotalConceptsDataset",  String.valueOf(dsConceptsNum));

		conceptsGS.retainAll(conceptsIN);
		res.put("TotalConceptsIntersection", myFormatter.format(conceptsGS.size()));


		res.put("ValueOfCompleteness", myFormatter.format(conceptsGS.size()/gsConceptsNum));

		return res;
			}


}
