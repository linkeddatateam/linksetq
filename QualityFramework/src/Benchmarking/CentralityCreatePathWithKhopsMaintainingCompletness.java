package Benchmarking;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Jama.Matrix;
import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Assesser.LinksetImpactingStructure.RDFEdge;
import Quality.Assesser.LinksetImpactingStructure.UtilityFunctionsDegreeAssessment;
import Quality.Utilities.RDFAccess;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

public class CentralityCreatePathWithKhopsMaintainingCompletness {

	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName());
	static ExecutionStructures es=new ExecutionStructures();

	public CentralityCreatePathWithKhopsMaintainingCompletness(Model modelsubj,Model modelobj,Model modellinks, Model all ){
		//logger = Logger.getLogger(RandomDeletePropertyValues.class);
		//	BasicConfigurator.configure();
		es.setModelsubj(modelsubj);
		es.setModelobj(modelobj);
		es.setModellinks(modellinks);
		es.setAll(all);
	} 


	/**
	 * It reads from a CSV file the relation's weights 
	 * @param filePath
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String,Double> readWeight(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath));
		String [] nextLine;
		HashMap <String, Double> res = new  HashMap <String, Double> ();
		while ((nextLine = reader.readNext()) != null) {
			// nextLine[] is an array of values from the line
			res.put(nextLine[0],  Double.valueOf(nextLine[1]));
		}
		return res;
	}	








	public static void main(String[] args) {

		// create Options object
		Options options = new Options();

		//options.addOption("pathklenghtfile", true, "pathklenghtfile contains the number of paths really created");
		options.addOption("insubj", true, "rdf subject file path from which to delete concept and relation");
		options.addOption("subjnmsp", true, "namespace of subject");
		options.addOption("inobj", true, "rdf subject file path from which to delete concept and relation");
		options.addOption("objnmsp", true, "namespace of object");
		options.addOption("inlinkset", true, "rdf linkset file path from which to delete link");
		options.addOption("predlinkset", true, "type of linkset predicate considered");		
		options.addOption("wheretocreatepath",true, "dataset in which to create paths subj s obj o both all" );
		options.addOption("percentage",true, "percentage expressed as a number between 0 and 100 representing the number of path with length k to create" );
		options.addOption("outsubj",true, "subj output file path" );
		options.addOption("outobj",true, "obj output file path" );
		options.addOption("outlinkset",true, "linkset output file path" );
		options.addOption("k",true, "k number of hops" );
		options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");		
		//options.addOption("logLevel",true, "the level for the logger" );

		//
		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		//String pathklenghtfile="";
		String insubj="";
		String inobj="";
		String subjnmsp="";
		String objnmsp="";
		String inlinkset="";
		String outsubj="";
		String outobj="";
		String outlinkset="";
		String wheretocreatepath="";
		String predlinkset="";
		int kinput, k;
		double percentage;
		HashMap<String, Double> weightForCentrality;


		try {
			cmd = parser.parse( options, args);
			// connecting to the loggerbene
			//	Debugging.loggerConfiguration(logger, cmd.hasOption("logLevel") ? cmd.getOptionValue("logLevel"):null , RandomDeletePropertyValues.class);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "RandomDeleteLinkInLinkset", options );
				System.exit(0);
			}
			//			other check on options that we could want 

			if (!cmd.hasOption("insubj")&& !cmd.hasOption("inobj"))  {
				throw new Exception("insubj and inobj  are missing");
			}

			if (!cmd.hasOption("k"))  {
				throw new Exception("k is missing");
			}else{
				kinput=Integer.parseInt(cmd.getOptionValue("k"));
			}
			if (!cmd.hasOption("wheretocreatepath"))  {
				throw new Exception("wheretocreatepath is missing");
			}else{
				wheretocreatepath=cmd.getOptionValue("wheretocreatepath").toLowerCase().trim();
			}

			if (!cmd.hasOption("predlinkset"))  {
				throw new Exception("predlinkset is missing");
			}else{
				predlinkset=cmd.getOptionValue("predlinkset").trim();
			}			
			if (cmd.hasOption("insubj"))  {
				insubj=cmd.getOptionValue("insubj").trim();
				if (!cmd.hasOption("outsubj"))  {
					throw new Exception("outsubj is missing");
				}
				else{
					if( !cmd.hasOption("subjnmsp"))
					{
						throw new Exception("subjnmsp is missing");
					}
					else{

						outsubj=cmd.getOptionValue("outsubj").trim();
						subjnmsp=cmd.getOptionValue("subjnmsp").trim();
					}
				}
			}

			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#broader", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#related", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#narrower", 1.0);
				LOGGER.warn("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default");
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));


			if (cmd.hasOption("inobj"))  {
				inobj=cmd.getOptionValue("inobj");
				if (!cmd.hasOption("outobj"))  {
					throw new Exception("outobj is missing");
				}
				else{
					if( !cmd.hasOption("objnmsp"))
					{
						throw new Exception("objnmsp is missing");
					}
					else{

						outobj=cmd.getOptionValue("outobj");
						objnmsp=cmd.getOptionValue("objnmsp");
					}
				}
			}

			if (!cmd.hasOption("inlinkset"))  {
				throw new Exception("inlinkset  is missing");
			}
			else{
				inlinkset=cmd.getOptionValue("inlinkset");
			}
			if (!cmd.hasOption("outlinkset"))  {
				throw new Exception("outlinkset  is missing");
			}
			else{
				outlinkset=cmd.getOptionValue("outlinkset");
			}
			if (!cmd.hasOption("percentage")) 
				throw new Exception("percentage of links to cancel is missing");
			else { 
				percentage=Double.parseDouble( cmd.getOptionValue("percentage"));
			}		
			if (!insubj.isEmpty())
			{
				es.setModelsubj(org.apache.jena.riot.RDFDataMgr.loadModel(insubj));
			}
			if (!inobj.isEmpty())
			{
				es.setModelobj(org.apache.jena.riot.RDFDataMgr.loadModel(inobj));
			}			

			//ATTENZIONE! per allineare k input e numero di hop 
			//kinput=khop+1 
			//khop=kinput-1
			//quindi kinput deve essere sempre >=2

			if(kinput>=2)
			{
				k=kinput-1;
				//21/12/2015 forse mettere k=kinput;
				//k=kinput;
				es.setModellinks(org.apache.jena.riot.RDFDataMgr.loadModel(inlinkset));
				//da usare quando avremo l'opzione all
				es.setWeight(weightForCentrality);
				//extract URI of linkset entry points of browsing
				es.extractURILinkofLinkset(wheretocreatepath, objnmsp, subjnmsp, predlinkset);
				//create the multirelational adjacent matrix
				es.extractAM(wheretocreatepath, objnmsp, subjnmsp, k);
				//initialize PWM and VNM 
				es.initializeMatrices(k);
				//caluclate the number of path of length k do be created 
				//basing on the number of path with length at least k 
				//existing in the dataset considered
				es.calculateNumberofPathWithKhopAndTheirLinkOfLinkset2(percentage, k,es.getModelobj(),objnmsp);
				//creation of path of length k
				Model mm=CentralityCreatePathWithKhopsMaintainingCompletness.createPathofLenghtKMaintainginCompleteness(subjnmsp, objnmsp,insubj,inobj,wheretocreatepath);

			
				
				// finally the model must with deletion must been saved as indicated
				if(wheretocreatepath.trim().equalsIgnoreCase("o"))
				{
					RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outsubj, mm);
					RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outobj, es.getModelobj());

				}
				if(wheretocreatepath.trim().equalsIgnoreCase("s"))
				{
					RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outobj,mm);
					RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outsubj, es.getModelsubj());

				} 
				if (es.getModellinks()!=null)
				{
					RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outlinkset,es.getModellinks());

				}
				
				es.getModelsubj().close();
				es.getModellinks().close();
				es.getModelobj().close();				
				if(wheretocreatepath.trim().equalsIgnoreCase("all"))
				{

				}
			}
			else{
				throw new Exception("Parameter k must be >=2");

			}

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}





	//modify the existing linkset in order to create 
	//path of lenght k delenting the node in the correct 
	//graph in order to maintain completeness
	//NEW 29/07/2015: delete duplicats in the file node 
	//deleted 

	private static Model  createPathofLenghtKMaintainginCompleteness(String subjnmsp, String objnmsp, String insubj,String inobj, String flag) 
			throws IOException 
			{//TODO
		int createpath=0;
		ArrayList<Integer> alreadydeleted=new ArrayList<Integer> (); 
		Model model=null; 
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		String modelmodified="";
		int numberofpathwithlengthktocreate=es.getNumberofpathwithlengthktocreate();

		HashMap<Integer, ArrayList<Integer>> Linksoflinksetwithpathk=es.getLinksoflinksetwithpathk();

		//set of pair representing ( matrix index, uri) of 
		//uri entry point of a path of length k created


		Model modellinks=es.getModellinks();
		//voglio creare i path nell'obj e quindi 
		//devo cancellare i corrispettivi nel subj e nel linkset

		//writing:
		//- entry points of path with length k 
		//-  number of path created
		//-  concepts deleted
		//
		File f,fdelete;


		if(flag.trim().equalsIgnoreCase("o"))
		{
			model=es.getModelsubj();
			modelmodified=insubj;

		}
		//voglio creare i path nel subj e quindi 
		//devo cancellare i corrispettivi nel obj e nel linkset

		if(flag.trim().equalsIgnoreCase("s"))
		{
			model=es.getModelobj();
			modelmodified=inobj;
		}

		if(flag.trim().equalsIgnoreCase("all"))
		{

			model=es.getModelsubj();
			model.add(es.getModelobj());
			modelmodified=insubj+"_"+inobj;

		}		

		if(!flag.equalsIgnoreCase("all"))
		{

			//file in which we write the pair 
			//matrix index - URI of entry point of 
			//path with length k
			f=new File(modelmodified+"_indexMatrix_URI_startOfPathLengthK.csv");
			fdelete=new File(modelmodified+"_URI_ofNodeDeleted.csv");
			f.getParentFile().mkdirs();
			//LOGGER.info("preparing "+f);
			String[] row= new String[1];


			CSVWriter writerdelete = new CSVWriter(new FileWriter(fdelete), '\t');
			CSVWriter writerstart = new CSVWriter(new FileWriter(f), '\t');
			String urdelnew="";
			String uridel="";
			String uristart="";
			String uristartnew="";

			//per k=0 significa eliminare solo 
			//i link del linkset
			for(int nn: Linksoflinksetwithpathk.keySet())
			{//per ogni nodo del linkset che ha path di lunghezza =k
				if(!alreadydeleted.contains((Object)nn) && (createpath<numberofpathwithlengthktocreate))
				{
					ArrayList<Integer> nnp=new ArrayList<Integer>();

					//recupero tutti gli altri nodi nnp raggiunti da nn 
					//durante il path e metto a zero il valore della PWM  
					//inoltre cancello nel linkset il link exactMatch 
					//che considera nnp 
					//infine devo cancellare mettere a null anche 
					//la lista dei nodi associati a nnp

					uristart=es.getURIMatrixIndex().get(nn);
					nnp=Linksoflinksetwithpathk.get(nn);
					for(int innp: nnp)
					{
						if(!alreadydeleted.contains((Object)innp) )
						{
							alreadydeleted.add(innp);

							uridel=es.getURIMatrixIndex().get(innp);
							//path with length k
							fdelete.getParentFile().mkdirs();
							//LOGGER.info("preparing "+f);
							row= new String[1];
							//linkset.remove(uridel);
							//modellinks must delete in modellinks 
							//the considered link
							Resource uriro=null;
							//Property pr= modellinks.createProperty("<http://www.w3.org/2004/02/skos/core#exactMatch>");
							//Resource urirs= modellinks.createResource("<http://xxx/2888>");
							StmtIterator linktodelete=null;
							StmtIterator linktodelete2=null;
							urdelnew="";
							uristartnew="";
							if(flag.trim().equalsIgnoreCase("o"))
							{
								//voglio creare path nell'obj quindi 
								//cancello nel subj
								urdelnew=uridel.replaceAll("^"+objnmsp+"*", subjnmsp);
								uristartnew=uristart.replaceAll("^"+objnmsp+"*", subjnmsp);
								uriro= modellinks.createResource(urdelnew);
								linktodelete=modellinks.listStatements(uriro, null,(RDFNode)null );
								linktodelete2=modellinks.listStatements(uriro,null,(RDFNode)null);
							}
							if(flag.trim().equalsIgnoreCase("s"))
							{
								//voglio creare path nel subj quindi 
								//cancello nel obj							
								urdelnew=uridel.replaceAll("^"+subjnmsp+"*", objnmsp);
								uristartnew=uristart.replaceAll("^"+subjnmsp+"*", objnmsp);
								uriro= modellinks.createResource(urdelnew);
								linktodelete=modellinks.listStatements(null,null, (RDFNode) uriro);
								linktodelete2=modellinks.listStatements(null,null, (RDFNode) uriro);							

							}
							row[0]=urdelnew;
							writerdelete.writeNext(row); 
							while(linktodelete2.hasNext())
							{
								Statement s;
								s=linktodelete2.nextStatement();
								//model.remove(model.listStatements(s.getSubject(),null, (RDFNode)null));

								if(flag.trim().equalsIgnoreCase("o"))
								{
									//voglio creare path nell'obj quindi 
									//cancello nel subj
									model.remove(model.listStatements(s.getSubject(),null, (RDFNode)null));
									model.remove(model.listStatements(null,null, (RDFNode)s.getSubject()));

								}
								if(flag.trim().equalsIgnoreCase("s"))
								{
									//voglio creare path nel subj quindi 
									//cancello nel obj							
									model.remove(model.listStatements((Resource)s.getObject(),null,(RDFNode)null));
									model.remove(model.listStatements(null,null,(RDFNode)s.getObject()));
								}							
							}
							modellinks.remove(linktodelete);
						}
					}
					row[0]=uristartnew;		
					writerstart.writeNext(row); 
					createpath++;
				}
			}// consider a random link in the set of the linkset 
			//having at least one path of lenght =k

			writerdelete.flush();
			writerdelete.close();
			writerstart.flush();
			writerstart.close();	
		}
		f=new File(modelmodified+"_NumofPathsWithLengthK.csv");
		f.getParentFile().mkdirs();
		//LOGGER.info("preparing "+f);
		String[] row= new String[2];
		CSVWriter writer = new CSVWriter(new FileWriter(f), '\t');
		row[0]=modelmodified;
		row[1]=myFormatter.format(createpath);		
		writer.writeNext(row); 
		writer.flush();
		writer.close();
		//print csv with index of matrix and uri of starting 
		//point of path of length k created 
		row[0]=modelmodified;
		row[1]=myFormatter.format(createpath);		
		writer.writeNext(row); 
		return model;
			}

	//modify the existing linkset in order to create 
	//path of lenght k delenting the node in the correct 
	//graph in order to maintain completeness






}//fine classe


