package Quality.Assesser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import java.util.Map.Entry;
import java.util.Set;





import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;

import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import com.hp.hpl.jena.rdf.model.StmtIterator;


import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Utilities.Debugging;
import Quality.Utilities.RDFAccess;

import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;
/** 
 * @author riccardoalbertoni
 *
 *Class implementing measures for working out the quality of a linkset according to  the number of values made importable by the linkset
 */
public class LinkImp4p {

	private LinksetIndex linksetIndex;

	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.LinkImp4p.class.getName());
	
	// data structure storing for each property, the links with the respective importing values
	protected HashMap <String, HashMap <String, Double> >LinkImp4pResults= new HashMap <String, HashMap <String, Double>>() ;

	protected HashMap <String, Double> sumOfImpact4Property=new HashMap <String, Double>();

	// It contains the number of link whose subject or object are related with entities/values of p
	private HashMap<String,Integer> numberOfLinks=new HashMap<String,Integer>();

	// a structure which contains the number of imported elements, it is hashed on property and property@lang
	HashMap <String, Integer> numberOfimportedValues4property=new HashMap <String, Integer>();

	//public HashMap <String, Double> sumOfImpact4Property=new HashMap <String, Double>();

	// list of skos properties whose semantics induces a change in the importing algorithm
	static final String[] SKOSLEXREP= new String[] { "http://www.w3.org/2004/02/skos/core#altLabel","http://www.w3.org/2004/02/skos/core#prefLabel", "http://www.w3.org/2004/02/skos/core#hiddenLabel"};
	static final HashSet<String> SKOSlexRepProperty =new HashSet<String>( Arrays.asList(SKOSLEXREP) );

	public static final int LIMIT_in_SPARQLENDPoint = 200;

	// data structure intended to store the encontered skos lexical representation
	protected HashSet <String> encounteredLexicalRep =new HashSet <String> ();

	/**
	 * It returns the percentage of value  imported considering a link and a property   
	 *  @param link the string entity1+entity2 which is expected to identify the link
	 *  @param property the string indicating property on which we want to assess the effect of the interlinking
	 */
	Double getLinkImp4(String link, String property){
		return LinkImp4pResults.get(property).get(LinkImp4pResults);
	}

	/**
	 * It returns the percentage of value  imported listing all the links and a property   
	 *  @param link the string entity1+entity2 which is expected to identify the link
	 *  @param property the string indicating property on which we want to assess the effect of the interlinking
	 */
	Set<Entry<String, Double>> getLinkImp4(String property ){
		return  LinkImp4pResults.get(property).entrySet();

	}

	/**
	 * It returns the average percentage of imported values considering  all the links and a property   
	 *  @param link the string entity1+entity2 which is expected to identify the link
	 *  @param property the string indicating property on which we want to assess the effect of the interlinking
	 */

	double  LinksetImp4(String property ){


		int atIndex=property.indexOf("@");
		if (atIndex == 0) {
			//return sumOfImpact4Property.get(property)/((double) numberOfLinks.get(property));
			return (double)((double)sumOfImpact4Property.get(property)/(double)(this.getTotalNumberOfLink()));
		} else{
			double sum=0.0;
			// int dem=0;
			HashMap<String, Double> h=LinkImp4pResults.get(property);
			for ( Entry <String,Double> e:  h.entrySet()){
				//dem++;
				sum=sum+e.getValue().doubleValue();
				//System.out.println("sum "+property +" language "+e.getKey()+" "+e.getValue().doubleValue());
			}
			//	return sum/ dem;
			return  (double)((double)sum/(double)this.getTotalNumberOfLink());

		}


	}

	/**
	 * It returns the average percentage of imported values considering  all the links and a property   
	 *  @param link the string entity1+entity2 which is expected to identify the link
	 *  @param property the string indicating property on which we want to assess the effect of the interlinking
	 */

	HashMap<String, Double>  LinksetImp4AllProperties( ){

		HashMap<String, Double>  s=new   HashMap <String, Double>();
		for( Entry <String, HashMap<String, Double>> e : LinkImp4pResults.entrySet() ){ 
			String property= e.getKey(); 
			s.put(property,LinksetImp4(property) ); }
		return  s ;

	}
/**
 * 	
 * @return the number of link of the  considered linkset
 */
 int getLinksetCardinality(){ return this.linksetIndex.getNumberOfLink();}
	/**
	 * 
	 * @param modelPath 
	 * @param l: linkset we are considering
	 * @param selectedProperties: list of properties to be considered when working out the importing, 
	 * ( to be develop in future: if this parameter is null or empty then all the possible properties will be considered)
	 * @throws Exception 
	 *  
	 */
	LinkImp4p(LinksetInfo l, DatasetInfo xd, DatasetInfo yd, ArrayList <String> selectedProperties, String modelPath ) throws Exception{

		// let's connect with the logger
		//LOGGER = Logger.getRootLogger();
		
		// let assume that the Linkset and the pertaining  datasets are all uploaded in the same sparql endpoint	
		if (!( l.sparqlEndPoint== xd.sparqlEndPoint &&  l.sparqlEndPoint== yd.sparqlEndPoint)){
			//TODO if dataset and linkset are not all in the same sparql end point we have to query the datasets with a different strategies
			throw new Exception(" datasets and linksets are not in the same sparql endPoint" ); 
		}else{
			// let's create indexing that we use later on for checking whether a link is 
			linksetIndex= new LinksetIndex(l,xd,yd );

			// let's create the model where to store the statements that can be imported
			Model importedStatements= ModelFactory.createDefaultModel();

			// if properties wrt work out the inmporting haven't been  specified, we apply thw importing  on all properties in the dataset
			// let's figure out which are the properties adopted in the datasets, and we store them in selectedProperty 
			if (selectedProperties==null){
				selectedProperties = getProperties4InterlinkedEntities(l, xd, yd);
			}

			//TODO depending on sparql end point setting this query does't work when we work with big datasets, it should be rewritten with offset and limits 
			// get the List of links, 

			int numberCicle4linkset=0, offset;
			boolean thereWereResults;

			ResultSet resSetOfLinks;
			do{
				// asking for a bunch of links
				offset=LIMIT_in_SPARQLENDPoint*numberCicle4linkset;
				thereWereResults=false;
				numberCicle4linkset++;
				String queryString=" select distinct ?x ?y where { ?x <"+l.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}" +
				"order by ?x " +
				"LIMIT   "+LIMIT_in_SPARQLENDPoint +"\n"+
				"OFFSET  "+offset;

				LOGGER.info(" query for the Linkset \n" +queryString);

				// exec the query to get the link in terms of interlinked entities 
				SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
				//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
				resSetOfLinks= RDFAccess.execQuery(l, queryString, err, null, false);
				Query query;

				// for each link's let see what is attached to the entities
				if (resSetOfLinks!=null){
					QuerySolution sol;
					Resource x, y;
					String sx, sy;
					//int numberOfLink=0;
					thereWereResults= resSetOfLinks.hasNext();
					while ( resSetOfLinks.hasNext()){		
						//numberOfLink++;
						sol=resSetOfLinks.next();
						x=sol.get("?x").asResource();
						y=sol.get("?y").asResource();
						sx= "<"+x.getURI()+ ">";
						sy= "<"+y.getURI()+ ">";

						// Let's retrieve the subgraph pertaining to the link and the entities interlinked
						String queryString1=" Construct { "+sx+" <"+l.voidlinksetPredicate+"> "+sy+". "+sx+" ?xp ?xo. "+sy+" ?yp ?yo.} where { "+sx+" <"+l.voidlinksetPredicate+"> "+sy+". Optional { "+sx+" ?xp ?xo}. Optional{ "+sy+" ?yp ?yo	} }";//  +
						//								"FILTER ( regex(str(?sx),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
						//				"FILTER ( regex(str(?sy), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}" ;
						LOGGER.debug(" quering for "+sy+" "+ sx+ "properties and values  \n" + queryString1);
						
						QueryExecution qexec= RDFAccess.createQueryExecution(queryString1, l);
						
						Model resultModel = qexec.execConstruct() ;
						LOGGER.debug(" Query : "+ queryString1+ "\n retuns :\n"  );
						//TODO we should find out which is the logger print stream
						if (LOGGER.isDebugEnabled() ){resultModel.write(  System.out);}

						// Let's analyse what was attached to x and y
						for(String p :selectedProperties) {
							// LinkImp4pResults is a HashMap <String, HashMap <String, Double>>, whose nested hashmap is  result4LiksAssumingP, 
							// the nested hashmap must be  inizialized  before to use it the first time
							HashMap <String, Double> result4LinksAssumingP = this.LinkImp4pResults.get(p);
							if (result4LinksAssumingP==null){
								result4LinksAssumingP =new HashMap <String, Double> ();
								
								this.LinkImp4pResults.put(p, result4LinksAssumingP );
								AddLangToLinkImp4pResults(l, xd, yd, p);
							}
							Property property = resultModel.getProperty(p);
							StmtIterator ix = resultModel.listStatements(resultModel.getResource(x.getURI()), property,  (RDFNode) null);
							StmtIterator iy = resultModel.listStatements(resultModel.getResource(y.getURI()), property,  (RDFNode) null);
							HashSet <String> denominator = new HashSet <String> ();
							//	HashSet <String> vy = new HashSet <String> ();
							int sizeNumerator=0;
							ArrayList <String> importedValues=new ArrayList <String>();

							//we are also counting importing separated for languages, by creating denominatorLang, ImportedValuesLang, numeratorLang, ect
							// same of denominator but organised for languages, so the hashmap is indexed according to lang and brings to the hash map of the values at denominator for that lang 
							HashMap <String,HashSet<String>> denominator4lang= new HashMap <String,HashSet<String>>();
							HashMap <String, ArrayList <String>> importedValues4lang= new HashMap<String, ArrayList <String>>();
							HashMap <String, Integer> sizeNumerator4lang =	new HashMap <String, Integer>(); 

							String toAdd;

							// we should process only the link that have  the property attached to x or y or both 
							// basically numberOfLinks represents L|p
							//TODO not sure that we have to use L|p, probably we should keep all the links independently if they have some value for p attached 
							if (!ix.hasNext()&& !iy.hasNext())  continue; //TODO this instruction does not make sense delete
							int v=0;
							if (numberOfLinks.get(p)!=null)  {
								v=numberOfLinks.get(p); 
							}
							numberOfLinks.put(p, ++v) ;

							// let's first take a look to what is associated to the Subject of the link, 
							while (ix.hasNext()) { 
								toAdd="";
								RDFNode o=ix.next().getObject();
								if (o.isResource()) {
									//check if it is mapped
									Resource resource= resultModel.getResource(o.asResource().getURI());
									// we don't care  how the x's  associate entity are mapped by the linkset, because we want to normalize the entities with respect to the subject dataset  
									toAdd=resource.getURI();
								}
								else toAdd=o.toString();

								if (!denominator.contains(toAdd)){ 
									// if toAdd is not in the denominator where values are collected without considering the lang, it is not even in the denominator4lang
									sizeNumerator++;
									denominator.add(toAdd);
									if (SKOSlexRepProperty.contains(p)) encounteredLexicalRep.add(toAdd);
									// if toadd is a literate with language specified we have to add it in the propers languages bins
									if (toAdd.contains("@")){
										String lang= toAdd.substring(toAdd.indexOf("@"));
										if (denominator4lang.containsKey(lang)) {
											(denominator4lang.get(lang)).add(toAdd);
										} else {
											HashSet<String> temporary= (new HashSet<String>());
											temporary.add(toAdd);
											denominator4lang.put(lang, temporary);
										}
										if (sizeNumerator4lang.containsKey(lang)) sizeNumerator4lang.put(lang,  sizeNumerator4lang.get(lang)+1);
										else  sizeNumerator4lang.put(lang,new Integer(1));
									}
								}
							}

							while(iy.hasNext()) {
								toAdd="";
								RDFNode o=iy.next().getObject();
								if (o.isResource()) {
									// here we have to check if the  entities  associated to the object of the link are mapped into the subject values, because it they are, we are importing duplicates that do not add novel info  
									String rString= o.asResource().getURI();
									toAdd=rString;
									//is that URI a duplicate when normalized with respect to the subject dataset  ?!?
									if (linksetIndex.isInLinksetAsObject(rString) ) {
										// then is mapped into the subject and we shoud add the imported value as the mapped ;
										ArrayList <String> mappedValuesInSubject= linksetIndex.getEntitiesAssociatedToObject(rString);
										// which mapped values should I use  [0] or [1] .. [size-1]? 
										// the first values in the list 
										toAdd= mappedValuesInSubject.get(0);

										// Unless we have a value that we have found associated to the subject of the  link? 
										for ( String m :mappedValuesInSubject){
											if (denominator.contains(m)){ 
												toAdd=m;
												break;}  ;
										}
									} 
								} else {
									toAdd=o.toString();

								}

								if (!denominator.contains(toAdd)){

									if (SKOSlexRepProperty.contains(p)){
										if(!encounteredLexicalRep.contains(toAdd)){
											String lang="";
											if (toAdd.contains("@")){
												lang= toAdd.substring(toAdd.indexOf("@"));
												// we must be sure that we don't import skos:prefLabel where there is already a preflabel for that language in  x
												if (!(p.equals("http://www.w3.org/2004/02/skos/core#prefLabel") && denominator4lang.containsKey(lang)) ){
													if (denominator4lang.containsKey(lang)) {
														(denominator4lang.get(lang)).add(toAdd);
													} else {
														HashSet<String> temporary= (new HashSet<String>());
														temporary.add(toAdd);
														denominator4lang.put(lang, temporary);
													}
													if (importedValues4lang.containsKey(lang)) {
														(importedValues4lang.get(lang)).add(toAdd);
													} else {
														ArrayList<String> temporary= (new ArrayList<String>());
														temporary.add(toAdd);
														importedValues4lang.put(lang, temporary);
													}

													//  if we are considering a skos lexical properties with restrictions, we must ensure that prefLabel/altLabel/hiddenLabel are disjoint
													encounteredLexicalRep.add(toAdd);
													importedValues.add(toAdd);
													// if a model where to write the statements that can be imported via linkset has been specified then  lets write the imported statements
													if (modelPath!=null) importedStatements.add(importedStatements.createResource(x.getURI()), importedStatements.createProperty(p), importedStatements.createLiteral(toAdd));
													denominator.add( toAdd);
												}
											}}
									}else {

										importedValues.add(toAdd);
										// if a model where to write the statements that can be imported via linkset has been specified then  lets write the imported statements
										if (modelPath!=null){

											if (o.isResource()){
												importedStatements.add(importedStatements.createResource(x.getURI()), importedStatements.createProperty(p), importedStatements.createResource(toAdd));
											}else{
												importedStatements.add(importedStatements.createResource(x.getURI()), importedStatements.createProperty(p), importedStatements.createLiteral(toAdd));
											}
										}
										denominator.add( toAdd);
									}


								}

							}
							double val=0.0;

							if (denominator.size()>0){
								// val= 1.0 - ( ((double)sizeNumerator)/ ( denominator.size()));
								val=(double)impactFormula((double)sizeNumerator, (double) denominator.size());
																
							}
							
							// let's add the value for the link //TODO are we sure that we should add val when it is equal to 0, 0+0+0=0
							result4LinksAssumingP.put(sx + sy, (double) val);

							// let update the total sums
							double previousAcc=0.0;
							if (sumOfImpact4Property.containsKey(p)) previousAcc= (double) sumOfImpact4Property.get(p);
							sumOfImpact4Property.put(p,  (double)(previousAcc + val)); 	
							int n= numberOfimportedValues4property.containsKey(p)?  numberOfimportedValues4property.get(p): 0;
							numberOfimportedValues4property.put(p, n+importedValues.size());

							//let's add values for single languages if there no importing independently 
							//from the language does not make sense try to figure out what 
							//is the importing for the specific languages add if (val!=0)  
							for (String lang : denominator4lang.keySet()){	
								val=0.0;
								result4LinksAssumingP = this.LinkImp4pResults.get(p+lang);
								double sizenum= sizeNumerator4lang.get(lang)!=null ? sizeNumerator4lang.get(lang) :0;
								if (denominator4lang.get(lang).size()>0){ 
									//val= 1.0 - ( sizenum / ( denominator4lang.get(lang).size()));
									val=(double)impactFormula((double)sizenum,(double) denominator4lang.get(lang).size());
									//System.out.println("num: "+sizenum + "den: "+denominator4lang.get(lang).size());
									//System.out.println("val per linguaggio "+ lang+" val :"+val);
									
									//result4LinksAssumingP.put(p+lang,val);
									result4LinksAssumingP.put(sx + sy,(double)val);
									
									}


								// let's update the absolute number of imported element
								n=numberOfimportedValues4property.get(p+lang)!=null? numberOfimportedValues4property.get(p+lang): 0;
								int ivl= importedValues4lang.get(lang)!=null? importedValues4lang.get(lang).size():0;
								numberOfimportedValues4property.put(p+lang, n+ ivl);
							}

						}
						qexec.close() ;
					} //while ( resSetOfLinks.hasNext()){
				} //if (resSetOfLinks!=null){
			} while(thereWereResults);
			if (modelPath!=null){
				File f=new File(modelPath);
				f.getParentFile().mkdirs();
				FileWriter fw= new FileWriter(f);
			//	FileOutputStream file= new FileOutputStream(modelPath/*+"_"+l.name+"_ImportedStatements.n3"*/);
				importedStatements.write(fw, "N3" );
				fw.close();
			}
		}
	}
	
	
	private double impactFormula(double numeratorSize, double denominatorSize ){
		double val=1.0;
		//if (denominatorSize>0) val= 1.0 - ( numeratorSize/ denominatorSize);
		//if (denominatorSize>0) val= Math.max(1.0,(Math.max(0, 1-numeratorSize) *denominatorSize)) - (numeratorSize/ denominatorSize);
		// which should be equivalent to
		if (numeratorSize>0) 
			{
			val= (double) 1.0 - ( numeratorSize/ denominatorSize);
				}
		//else val=denominatorSize; // delete the comment if we want to have  the numere of elements imported on a empty subject 
		
		return val;
	}

	/**
	 * Add the languages that are pertinent for the dataset  to the results for property p and returns the array of languages.
	 * @param l
	 * @param xd
	 * @param yd
	 * @param p
	 */
	private String[] AddLangToLinkImp4pResults(LinksetInfo l, DatasetInfo xd,
			DatasetInfo yd, String p) {
		//Query query;
		ArrayList <String> languages= new ArrayList <String>();
		if (SKOSlexRepProperty.contains(p)){
			// we should consider the languagesk
			// which languages are available for p
			//select distinct lang(?l) where {?x skos:prefLabel ?l} LIMIT 100
			String queryString2=" select  distinct (lang(?l) as ?la) where { ?x <"+l.voidlinksetPredicate+"> ?y. {?x <"+p+"> ?l} UNion { ?y <"+p+"> ?l	} "+
			"FILTER (regex(str(?x),\"^"+xd.consolidatedUriSpace+"\", \"i\") )\n "+
			"FILTER (regex(str(?y), \"^"+yd.consolidatedUriSpace+"\", \"i\"))}";
			// Let's retrieve the subgraph pertaining to the link and the entities interlinked
			Query query;
			query = QueryFactory.create(queryString2) ;
			QueryExecution qexec=RDFAccess.createQueryExecution(queryString2, l);
			
			ResultSet r = qexec.execSelect() ;
			if (r!=null)
				while (r.hasNext()){

					String language=r.next().getLiteral("?la").getString();

					HashMap <String, Double> result4LinksAssumingPL =new HashMap <String, Double> ();
					this.LinkImp4pResults.put(p+"@"+language,result4LinksAssumingPL );
				}
			qexec.close();


		}
		String [] la=  languages.toArray(new String [languages.size()]  );
		return la;
	}

	private ArrayList<String> getProperties4InterlinkedEntities(LinksetInfo l, DatasetInfo xd,
			DatasetInfo yd) {
		ArrayList<String> selectedProperties;
		Query query;
		selectedProperties= new ArrayList <String> ();
		// query returning the list of links

		String queryString2=" select  distinct ?p where { ?x <"+l.voidlinksetPredicate+"> ?y. {?x ?p ?xo} UNion { ?y ?p ?yo	} "+
		"FILTER (regex(str(?x),\"^"+xd.consolidatedUriSpace+"\", \"i\") )\n "+
		"FILTER (regex(str(?y), \"^"+yd.consolidatedUriSpace+"\", \"i\"))}";
		// exec the query 
		query = QueryFactory.create(queryString2) ;
		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
		ResultSet resP= RDFAccess.execQuery(l, queryString2, err2, null, false);
		// add results to selectedProperty
		while(resP.hasNext()) {
			String property=resP.next().getResource("?p").getURI();
			selectedProperties.add(property);
		}
		return selectedProperties;
	}


	public int getTotalNumberOfLink(){ return this.linksetIndex.numberOfLinks;}


}
