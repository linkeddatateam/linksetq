package Quality.Assesser;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;

import au.com.bytecode.opencsv.CSVReader;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;






import Quality.DataSetLinksetStateWithDumps;
import Quality.DataSetLinksetStateWithVOID;
import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.DataSetLinksetState;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.setAlgebra.ExtensionalRelationFromEquivalenceClasses;
import Quality.CKANCrawlerIndicatorExtractor.CrawlMetadata;
import Quality.CKANCrawlerIndicatorExtractor.UnretrievableTypesException;
import Quality.Utilities.RDFAccess;
import flexjson.JSONSerializer;
/**
 * The class implementing the quality functions
 * @author riccardoalbertoni
 * 
 *
 */

//TOFIX At the moment it does not work if you specify more than one alignment
public class QualityAssessment {

	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.QualityAssessment.class.getName()); //Logger.getLogger(QualityAssessment.class.getName());

	private static CommandLine cmd;

	private static  HashMap<String, Double> weightForCentrality;

	private static  int K;


	DataSetLinksetState state;
	HashMap <String,LinksetQuality > assessedQuality= new   HashMap <String,LinksetQuality>();



	/**
	 * This constructor reads a serialized version of the data structure ToolState, if toolstate is "LinksetTypeExtracted" work out the quality measures otherwise give an exception
	 * @param serializedState
	 * @throws Exception 
	 */
	public  QualityAssessment (String fileStr, boolean isVOID) {



		// reading an object from a file

		try{ 
			if (isVOID)

				state= new DataSetLinksetStateWithVOID(fileStr);
			else

				state = (new DataSetLinksetState()).loadStatefromJSON(fileStr);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		if (!state.state.equals("LinksetTypeExtracted")){
		//			throw new NoIndicatorsException("the extraction of indicator for that serizalized object is not complete");
		//		} else {
		//this.workOutQuality();

		//}
	}

	/**
	 * Costructor which starts from indicated subj, obj, linkset dumps and  related urispace and linkingProperty
	 * @param subjDump
	 * @param subjUriSpace
	 * @param objDump
	 * @param objUriSpace
	 * @param linksetDump
	 * @param linkingProperty
	 */
	public QualityAssessment (String subjDump, String subjUriSpace, String  objDump, String objUriSpace, String linksetDump, String linkingProperty) {
		try {
			state= new DataSetLinksetStateWithDumps(subjDump,subjUriSpace, objDump,objUriSpace,linksetDump, linkingProperty) ;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private static void getTypesForDataset(DatasetInfo dt) 
			throws UnretrievableTypesException, IOException {
		//	Resource datasetAsResource = modD.getResource(dt.crawledDatasetUri);
		InputStream ip;
		String queryString ;
		//int responseCode=0;
		SparqlEndPointErrorMessage err= new SparqlEndPointErrorMessage();

		ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/D2DatasetTypeAndNumOfOccurenceURISPACE.sparql");
		try {
			queryString = CrawlMetadata.getContents(ip).replace("URISPACE", dt.consolidatedUriSpace);

			LOGGER.debug("quering "+ ((dt.sparqlEndPoint!=null)? dt.sparqlEndPoint: "local model")  +" by " +queryString);
			ResultSet res=null;
			// second attempt to query  
			try {

				res= RDFAccess.execQuery(dt, queryString, err, null, false);
				dt.lastResponseCode=err.responseCode;
				dt.lastSparqlAccessProblem=err.sparqlAccessProblem;
			} catch (com.hp.hpl.jena.sparql.resultset.ResultSetException  e) {
				LOGGER.error("problem quering "+ dt.sparqlEndPoint +" by " +queryString +" "+ e.getMessage());
				dt.lastSparqlAccessProblem+= "tries with group and count: "+e.getMessage()+ "\n";
			}
			if (res!=null) {
				QuerySolution sol;
				RDFNode type; 
				int noccurence; 
				if (!res.hasNext()) new Exception("Empty types retrieving indicators on sparqlendpoint "+ dt.sparqlEndPoint+ " URISPACE "+ dt.consolidatedUriSpace+ " query " + queryString);
				while (res.hasNext()){
					sol=res.next();
					type=sol.get("?type");
					noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
					if (type!=null)
						dt.types.put(type.toString(),noccurence); 
				} 
				dt.typeQueryWhichHasWorked=2;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	private QualityResult workOutQuality() {

		// it is moved with the for 	LinksetQuality q=new LinksetQuality();
		LinksetQuality q;
		QualityResult lsq= new 	QualityResult();

		/* state contains all information extracted from the void file 
		 * and the information about the indicators*/

		for ( LinksetInfo ls : state.linksetInfoArray){
			q=new LinksetQuality();
			//  linkset Impact 4 property
			if (cmd.hasOption("impact")||cmd.hasOption("all")){
				/// what we have to do to work out the impact

				ArrayList <String> properties =	null;
				// are the property specified? 
				/*refer to this class for developing the class for network measurs*/
				if( cmd.hasOption("impactP")){
					properties= new ArrayList <String> (Arrays.asList( cmd.getOptionValues("impactP")));
				}

				LinkImp4p qi;
				try {
					qi = new LinkImp4p(  ls,  state.mapOfDataset.get(ls.subjectLocalName),  state.mapOfDataset.get(ls.objectLocalName), properties, cmd.getOptionValue("importedStatementsPath"));
					q.LinkImp4p=qi.LinksetImp4AllProperties();
					q.NumberOfElementImported=qi.numberOfimportedValues4property;
					q.linksetCardinality=qi.getLinksetCardinality();
					//System.out.print("linksetCardinality  ------->   "+q.linksetCardinality);
				} catch (Exception e) {
					// classcastexception can be thrown 
					e.printStackTrace();
				}
			}
			// linkset centrality 
			if (cmd.hasOption("linksetCentrality")||cmd.hasOption("all")){ 
				//ATTENZIONE! per allineare k input e numero di hop 
				//kinput=khop+1 
				//khop=kinput-1
				//quindi kinput deve essere sempre >=2
				try{
					if(QualityAssessment.K>=2)
					{
						int khop=QualityAssessment.K-1;
						LinkImp4Centrality lic = new LinkImp4Centrality(ls, state.mapOfDataset.get(ls.subjectLocalName),  
								state.mapOfDataset.get(ls.objectLocalName),QualityAssessment.weightForCentrality , khop, cmd.getOptionValue("flagDataset"));
						//q.CrossWalkingSpreadingPotential= LinkImp4Centrality.executeCrossWalkingPotentialityAssessment(cmd.getOptionValue("flagDataset"));
						//q.URIConceptsReachable=LinkImp4Centrality.executeCrossWalkingPotentialityAssessment2(cmd.getOptionValue("flagDataset"));
						q.URIConceptsReachable=LinkImp4Centrality.executeCrossWalkingPotentialityAssessmentRecoursive(cmd.getOptionValue("flagDataset"),
																				cmd.getOptionValue("browsingInCSVFile"));
						HashMap<String, Integer> r1=LinkImp4Centrality.analyseDatasets(cmd.getOptionValue("browsingInCSVFile"));

						//number of concepts of subj and obj restricted on namespace, and considered properties
						q.TotalConceptsObjectRestrictedNmspAndProp=r1.get("TotalConceptsObjectRestrictedNmspAndProp");
						q.TotalConceptsSubjRestrictedNmspAndProp=r1.get("TotalConceptsSubjRestrictedNmspAndProp");
						//number of concepts of subj and obj restricted on namespace	
						q.TotalConceptsObjectRestrictedNmsp=r1.get("TotalConceptsObjectRestrictedNmsp");
						q.TotalConceptsSubjRestrictedNmsp=r1.get("TotalConceptsSubjRestrictedNmsp");			
						//number of concepts of subj and obj restricted on namespace, and skos type concepts
						q.TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept=r1.get("TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept");
						q.TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept=r1.get("TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept");
						q.CrossWalkingSpreadingPotential=q.URIConceptsReachable.size();	
						q.linksetCardinality=r1.get("linksetcardinality");
						q.linksetObjectCardinality=r1.get("linksetobjectscardinality");
						q.TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset=r1.get("TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset");
					}				
					else{
						throw new Exception("Parameter k must be >=2");
					}

				}catch ( Exception e) {
					e.printStackTrace();
				} 
			}



			// analyse broken links in linkset
			
			if (cmd.hasOption("analyseBrokenLinks")||cmd.hasOption("all")){ 
				try{

					BrokenLinkAnalysis bl = new BrokenLinkAnalysis(ls, state.mapOfDataset.get(ls.subjectLocalName),  
							state.mapOfDataset.get(ls.objectLocalName));

					q.linksetCardinality= BrokenLinkAnalysis.linksetcardinality;
					q.brokenLinkCardinality=BrokenLinkAnalysis.executeBrokenLinkAnalysis(cmd.getOptionValue("brokenlinkInCSVFile"));
			
					


			}catch ( Exception e) {
				e.printStackTrace();
			} 
		}

			// linkset SPARQL 
			if (cmd.hasOption("linksetCentralitySPARQL")||cmd.hasOption("all")){ 
				try{
					int khop=QualityAssessment.K;
					q.CrossWalkingSpreadingPotential= LinkImp4Centrality.executeCrossWalkingPotentialityAssessmentsparql(cmd.getOptionValue("flagDataset"));
					HashMap<String, Integer> r1=LinkImp4Centrality.analyseDatasets(cmd.getOptionValue("browsingInCSVFile"));
					q.ObjectCardinality=r1.get("objdataset"); 
					q.SubjectCardinality=r1.get("subjdataset");
			}catch ( Exception e) {
				e.printStackTrace();
			} 
		}

		if (cmd.hasOption("completeness")||cmd.hasOption("all")) {
			// let include the indicator for type in the quality results 
			q.indLinksetSubjectType=ls.subjectTypes;
			q.indLinksetObjectType=ls.objectTypes;
			DatasetInfo dt= state.mapOfDataset.get(ls.objectLocalName);
			q.indObjectType= dt.types;
			dt= state.mapOfDataset.get(ls.subjectLocalName);
			q.indSubjectType= dt.types;

			q.LinksetTypeCoverageForSubject=this.LinksetTypeCoverageForSubject(ls);
			q.LinksetTypeCoverageForObject= this.LinksetTypeCoverageForObject(ls);

			if (cmd.hasOption("JSONSerializedRelationFilePath")) q.LinksetTypeCompleteness= this.LinksetTypeCompleteness(ls, cmd.getOptionValue("JSONSerializedRelationFilePath"));
			else {
				//it must have algn
				ArrayList <String> listOfAlignments = new ArrayList <String> ();
				Double threshold=0d;
				//
				Properties prop=cmd.getOptionProperties("algn");
				Set<Entry<Object, Object>>  t = prop.entrySet();
				for ( Entry <Object,Object> e : t){
					if (e.getKey().toString().equals("filePath") ){
						// add 
						listOfAlignments.add(e.getValue().toString());
					} else LOGGER.error(" no filePath Specified"); 

				}
				if (cmd.hasOption("thr")){
					threshold=Double.valueOf(cmd.getOptionValue("thr"));
				}
				q.LinksetTypeCompleteness= this.LinksetTypeCompleteness(ls,listOfAlignments, threshold );
			}


			q.LinksetEntCov4TypeForObject=this.LinksetEntCov4TypeForObject(ls);
			q.LinksetEntCov4TypeForSubject=this.LinksetEntCov4TypeForSubject(ls);
		}
		// accessibility
		if (cmd.hasOption("accessibility")||(cmd.hasOption("all")&&! cmd.hasOption("subjDatasetPath")) ) {
			this.LsAccSparql(ls, q);
		}

		//availability
		if (cmd.hasOption("availability")||(cmd.hasOption("all")&&! cmd.hasOption("subjDatasetPath"))) {
			this.LsAvailSparql(ls, q);
		}

		// indicatorAvailability
		if (cmd.hasOption("indicatorAvailability")||cmd.hasOption("all")) {
			this.LsAvailIndicatorsSparql(ls, q);
		}

		lsq.put(ls.name, q);
	}
	return lsq;

}

/**
 * it returns -1
 * @param args
 * 
 */
public   double  LinksetTypeCoverageForSubject(LinksetInfo ls){
	// subject datatypes
	DatasetInfo dt= state.mapOfDataset.get(ls.subjectLocalName);
	Set <String> subjectTypes= dt.types.keySet();
	Set <String>linkedTypes=ls.subjectTypes.keySet();
	//TODO rewrite using intersection ??
	//linkedTypes.retainAll(linkedTypes);
	if (linkedTypes.size()>(double) subjectTypes.size()) LOGGER.warn("Problems with the types indicators: the number of  datatypes for the linkset "+ls.name +" are more than the number of datatypes of the subject set"   );
	return (linkedTypes.size()/(double) subjectTypes.size());

}

public  double  LinksetTypeCoverageForObject(LinksetInfo ls){
	// subject datatypes
	DatasetInfo dt= state.mapOfDataset.get(ls.objectLocalName);
	Set <String>objectTypes= dt.types.keySet();
	Set <String>linkedTypes=ls.objectTypes.keySet();
	////TODO rewrite using intersection ?? 
	//linkedTypes.retainAll(linkedTypes);
	if (linkedTypes.size()>(double) objectTypes.size()) LOGGER.debug("Problems with the types indicators: the number of  datatypes for the linkset "+ls.name +" are more than the number of datatypes of the object set"   );
	return (linkedTypes.size()/(double) objectTypes.size());


}

public double LinksetTypeCompleteness(LinksetInfo ls, ArrayList <String> algnFiles,  double threshold){
	return LinksetTypeCompleteness(ls,new ExtensionalRelationFromEquivalenceClasses(algnFiles,   threshold));
}

public double LinksetTypeCompleteness(LinksetInfo ls, String JSONSerializedRelationFilePath ){

	return LinksetTypeCompleteness(ls,new ExtensionalRelationFromEquivalenceClasses(JSONSerializedRelationFilePath));
}

public double LinksetTypeCompleteness(LinksetInfo ls,ExtensionalRelationFromEquivalenceClasses r){
	DatasetInfo objectDt= state.mapOfDataset.get(ls.objectLocalName);
	Set <String> objectTypes =objectDt.types.keySet();
	DatasetInfo subjectDt= state.mapOfDataset.get(ls.subjectLocalName);
	Set <String> subjectTypes =subjectDt.types.keySet();

	Set <String> linkedSBJtype=ls.subjectTypes.keySet();
	//Set <String> linkedOBJTypes= ls.objectTypes.keySet();
	HashSet <String> notLinkedType=new HashSet <String> ();
	HashSet <String> haveEquivalentTypeInObject=new HashSet <String> ();
	// bit linked datatypeIn subject 
	for  (String x: subjectTypes){
		if (!linkedSBJtype.contains(x)) notLinkedType.add(x);	
	}

	haveEquivalentTypeInObject=r.intersection(subjectTypes,objectTypes);

	HashSet <String> intersection =new  HashSet <String>();
	for(String x :notLinkedType ){
		if (haveEquivalentTypeInObject.contains(x)) intersection.add(x); 
	}

	// if the denominator is more than 0 ... otherwise 1
	double res=1;
	if (haveEquivalentTypeInObject.size()>0)  res =  1- (((double)intersection.size())/haveEquivalentTypeInObject.size());
	else LOGGER.warn("There are not equivalente types between subject dataset and object dataset ?!?  I bet you have not configured the equivalence relations ");

	return res;	
}

public HashMap <String, Double> LinksetEntCov4TypeForSubject(LinksetInfo ls){

	DatasetInfo dt= state.mapOfDataset.get(ls.subjectLocalName);
	HashMap <String, Double> res= new HashMap <String, Double>();
	Set <String>linkedTypes=ls.subjectTypes.keySet();
	for(String t : linkedTypes){
		double linksetSubjectTypesNumOccurence=ls.subjectTypes.get(t);
		try {
			double datasetTypesNumOccurence= dt.types.get(t);
			double diff= ((datasetTypesNumOccurence)- linksetSubjectTypesNumOccurence);
			if (diff< 0) LOGGER.warn(" Possible Problems in instance indicators of type "+t+" : in the subject dataset " +ls.subjectLocalName +" the number of subject entities (# "+ datasetTypesNumOccurence +") are less than the number of linkset entities (#" +linksetSubjectTypesNumOccurence +")");
			res.put(t,  linksetSubjectTypesNumOccurence/((double)datasetTypesNumOccurence)); 	
		}catch (Exception e){
			LOGGER.info("LinksetEntCov4TypeForObject linkset: " + ls.name + " " + "type "+ t + " is not a type in  dataset subject types" );
		}
	}

	return res;

}

public HashMap <String, Double> LinksetEntCov4TypeForObject(LinksetInfo ls){

	DatasetInfo dt= state.mapOfDataset.get(ls.objectLocalName);
	HashMap <String, Double> res= new HashMap <String, Double>();
	Set <String>linkedTypes=ls.objectTypes.keySet();
	for(String t : linkedTypes){
		double linksetObjectTypesNumOccurence=ls.objectTypes.get(t);
		double datasetTypesNumOccurence=0;
		try {
			datasetTypesNumOccurence= dt.types.get(t);
			double diff= ((datasetTypesNumOccurence)- linksetObjectTypesNumOccurence);
			if (diff< 0) LOGGER.warn(" Possible Problems in instance indicators "+t+" : in the subject dataset " +ls.objectLocalName +" the number of object entities (# "+ datasetTypesNumOccurence +") are less than the number of linkset entities (#" +linksetObjectTypesNumOccurence +")");
			res.put(t, linksetObjectTypesNumOccurence/ (datasetTypesNumOccurence)); 
		}catch (Exception e){
			LOGGER.info("LinksetEntCov4TypeForObject linkset: " + ls.name + " " + "type "+ t + " is not a type in  object types" );
		}
	}
	return res;

}


public  void LsAccSparql(LinksetInfo ls, LinksetQuality q) {
	DatasetInfo subj= state.mapOfDataset.get(ls.subjectLocalName);
	DatasetInfo obj= state.mapOfDataset.get(ls.objectLocalName);
	if (subj!=null) 
		q.SparqlAccessabilityForSubject= (subj.sparqlEndPoint!=null &&  subj.UriSpace!=null)? 1: 0;
	else LOGGER.info(" "+ls.subjectLocalName+" has not correspondent DatasetInfo");
	// double  subjdumoAccessibility= 
	if (obj!=null)
		q.SparqlAccessabilityForObject= (obj.sparqlEndPoint!=null &&  obj.UriSpace!=null)? 1: 0;
	else LOGGER.info(" "+ls.objectLocalName+" has not correspondent DatasetInfo");
	q.LinksetSparqlAccessability=  (q.SparqlAccessabilityForSubject+ q.SparqlAccessabilityForObject + ((ls.sparqlEndPoint!=null &&  ls.UriSpace!=null) ? 1: 0)) /3.0;	

}

public  void LsAvailSparql(LinksetInfo ls, LinksetQuality q) {
	DatasetInfo subj= state.mapOfDataset.get(ls.subjectLocalName);
	DatasetInfo obj= state.mapOfDataset.get(ls.objectLocalName);
	if (subj!=null) 
		q.SparqlAvailabilityForSubject= (subj.availabilityResponseCode>=200 && obj.availabilityResponseCode<300)? 1: 0;
	else LOGGER.info(" "+ls.subjectLocalName+" has not correspondent DatasetInfo");
	// double  subjdumoAccessibility= 
	if (obj!=null)
		q.SparqlAvailabilityForObject= (obj.availabilityResponseCode>=200 && obj.availabilityResponseCode<300)? 1: 0;
	else LOGGER.info(" "+ls.objectLocalName+" has not correspondent DatasetInfo");
	// FIXIT is the ls end point available ???!
	q.LinksetSparqlAvailability= (q.SparqlAvailabilityForSubject + 	q.SparqlAvailabilityForObject + ((ls.availabilityResponseCode>=200 && ls.availabilityResponseCode<300) ? 1: 0)) /3.0;	

}

public  void LsAvailIndicatorsSparql(LinksetInfo ls, LinksetQuality q) {
	DatasetInfo subj= state.mapOfDataset.get(ls.subjectLocalName);
	DatasetInfo obj= state.mapOfDataset.get(ls.objectLocalName);
	if (subj!=null) 
		q.SparqlIndicatorAvailabilityForSubject= (subj.types.size()>0)? 1: 0;
	else LOGGER.info(" "+ls.subjectLocalName+" has not correspondent DatasetInfo");
	// double  subjdumoAccessibility= 
	if (obj!=null)
		q.SparqlIndicatorAvailabilityForObject= (obj.types.size()>0)? 1: 0;
	else LOGGER.info(" "+ls.objectLocalName+" has not correspondent DatasetInfo");
	// FIXIT is the ls end point available ???!
	q.LinksetIndicatorSparqlAvailability= (q.SparqlIndicatorAvailabilityForSubject + 	q.SparqlIndicatorAvailabilityForObject /*+ ((ls.availabilityResponseCode>=200 && ls.availabilityResponseCode<300) ? 1: 0)*/) /2.0;	

}

public  void saveResultInJSON(String fileName){
	// writing an object to a file

	String jsonS = new JSONSerializer().include( "assessedQuality").serialize(this);
	try {
		FileWriter outFile = new FileWriter(fileName+".json");
		PrintWriter out = new PrintWriter(outFile);
		// print results of previous phase
		out.print(jsonS);
		out.close();
	}
	catch(IOException ex) {
		System.out.println(ex.getMessage());
	}
}


private static void retrieveIndicators(QualityAssessment qa) {
	DataSetLinksetState qaw=   qa.state;
	ArrayList <LinksetInfo> linksetInfoArray=  qaw.linksetInfoArray;
	for (LinksetInfo ll: linksetInfoArray){
		// unsafe casts But if we are here we know these are ok 
		LinksetInfo l=  ll ;
		// for each l which sparqlEndPoint, uriSpace
		DatasetInfo subj=  qaw.mapOfDataset.get(l.subjectLocalName);
		DatasetInfo obj=  qaw.mapOfDataset.get(l.objectLocalName);

		try {
			getTypesForDataset(subj);
			getTypesForDataset(obj);
			getTypesForLinkset( l, subj, obj); 
		} catch (UnretrievableTypesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}





private static void getTypesForLinkset(LinksetInfo ls, DatasetInfo subjectdt, DatasetInfo objectdt) throws UnretrievableTypesException, IOException{



	//
	//		// does the object dataset have a valid uriSpace?
	//		//		if ( objectdt.consolidatedUriSpace ==null) 
	//		//			if(objectdt.UriSpace !=null) objectdt.consolidatedUriSpace=objectdt.UriSpace;
	//		//			else throw new  UnretrievableTypesException("No uriSpace and consolidated uriSpace  so I won't retrieve the object's types for linkset ");
	//		//		if ( !objectdt.typeIndicatorsRetrieved) 
	//		//			throw new  UnretrievableTypesException("Type for Object dataset are not retrived so I won't retrieve the object's types for linkset ");
	//		//		if ( !subjectdt.typeIndicatorsRetrieved) 
	//		//			throw new  UnretrievableTypesException("Type for Suject dataset are not retrived so I won't retrieve the subject's types for linkset ");
	//
	//
	InputStream ip;
	String queryString ;
	//	int responseCode=0;
	//
	//		// query to text which kind of linkset property is supported
	//		queryString=" select distinct *  WHERE"+
	//		"{"+ 
	//		"?y <http://www.w3.org/2002/07/owl#sameAs> ?z. Filter( regex(str(?z), \"^OBJECTURISPACE\"))"+
	//		"}Limit 1";
	//		queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace);
	//		SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
	//
	//		ResultSet res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString,err );
	//		ls.subjLastResponseCode=err.responseCode;
	//		ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
	//
	//		if( !res.hasNext()) { // probably no sameas
	//
	//			// query to text which kind of linkset property is supported
	//			queryString=" select distinct *  WHERE"+
	//			"{"+ 
	//			"?y <http://www.w3.org/2002/07/owl#sameAs> ?z."+
	//			"}Limit 1";
	//
	//			res= CrawlMetadata.execQueryingASparqlEndPoint(subjectdt.sparqlEndPoint, queryString, err);
	//			ls.subjLastResponseCode=err.responseCode;
	//			ls.subjSparqlAccessProblem=err.sparqlAccessProblem;
	//			if( !res.hasNext()) { // its not  a owl:sameAs linkset
	//				throw new  UnretrievableTypesException("NO owl:sameAs links ");
	//			} else throw new  UnretrievableTypesException("UNSUPPORTED filter in sparql queries ");
	//		}	
	// isAnOwlSameAsLinkset(ls); 

	SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
	//

	//we don't even try the construct for linkset type 
	// let start from what was the second attempts  in the case of dataset type: Query with  COUNT, GROUP and FILTER

	ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/LSSUBProp2TypeAndNumOfOccurence.sparql");

	queryString = CrawlMetadata.getContents(ip);

	queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace).replace("PROPERTY", ls.voidlinksetPredicate);

	// second attempt to query  
	LOGGER.debug("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
	ResultSet res= RDFAccess.execQuery(subjectdt, queryString, err, null, false);
	ls.subjLastResponseCode=err.responseCode;
	ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

	if (res!=null) {
		QuerySolution sol;
		RDFNode type; 
		int noccurence; 
		while (res.hasNext()){
			sol=res.next();
			type=sol.get("?type");
			noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
			if(type!=null)
				ls.subjectTypes.put(type.toString(),noccurence); 
		} 
	}


	ip=CrawlMetadata.class.getResourceAsStream("../../TemplateQuery/LSOBJProp2TypeAndNumOfOccurence.sparql");

	queryString = CrawlMetadata.getContents(ip);

	queryString=queryString.replace("OBJECTURISPACE", objectdt.consolidatedUriSpace).replace("SUJBECTURISPACE", subjectdt.consolidatedUriSpace).replace("PROPERTY", ls.voidlinksetPredicate);

	// second attempt to query  
	LOGGER.debug("quering "+ objectdt.sparqlEndPoint +" by " +queryString);
	res= RDFAccess.execQuery(objectdt, queryString, err, null, false);
	ls.subjLastResponseCode=err.responseCode;
	ls.subjSparqlAccessProblem=err.sparqlAccessProblem;

	if (res!=null) {
		QuerySolution sol;
		RDFNode type; 
		int noccurence; 
		while (res.hasNext()){
			sol=res.next();
			type=sol.get("?type");
			noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
			if(type!=null)
				ls.objectTypes.put(type.toString(),noccurence); 
		} 
	}



}


public static void main(String[] args) {

	// create Options object
	Options options = new Options();

	/*Reads the command line option */
	options.addOption("indicators", true, "file  where are stored dataset and their indicators, it is a serialization of a toolState object");
	options.addOption("JSONSerializedRelationFilePath",true, "file containing the  serialization of Equivalence Relation to be adopetd in Quality Completeness" );
	options.addOption("JSONResult",true, "JSON serialization of the results" );
	options.addOption("accessibility",false, "run accessibility assessment" );
	options.addOption("availability",false, "run availability assessment" );
	options.addOption("indicatorAvailability",false, "run availability when quering for indicators assessment" );
	options.addOption("completeness",false, "run completeness assessment" );
	options.addOption("all",false, "run all the quality test");

	// added for impact  linkset, 
	options.addOption("VOIDFile",true, "A VOID file from which to derive  linksets and datasets to be assessed (at the moment,  only linksets whose subject and object target are uplodaded on the same sparql endpoint can be accepted)");

	options.addOption("impact",false, "run impact assessment");
	options.addOption("impactP",true, "Property_URI  indicates the  URI of properties that shoudl be considered working out the linkset impact, if no prop are specified all the properties in the datasets are considered.");
	options.addOption("impactInCSVFile", true, "Path and name  of the CVS files with importing results");
	//options.addOption("impactInRDFFile",true,"Path and name of RDF file with result enconde in the extended DAQ Ontology");
	options.addOption("impactInDQVFile",true,"Path and name of RDF file with result enconde in the extended DQV Ontology");
	options.addOption("browsingInCSVFile", true, "Path and name  of the  CVS files with browsing results ");
	
	
	//		org.apache.commons.cli.Option alignment = OptionBuilder.withArgName("prop=Property_URI")
	//		.hasArgs(10)
	//		.withValueSeparator()
	//		.withDescription( "Property_URI  indicates the  URI of properties that shoudl be considered working out the linkset importing, if no prop are specified all the properties in the datasets are considered. " )
	//		.create( "importing" );


	// added for impact  linkset and centrality
	options.addOption("subjDatasetPath",true, "Path to the subject dataset");
	options.addOption("objDatasetPath",true, "Path to the object dataset");
	options.addOption("subjUriSpace",true, "uriSpace to the subject dataset");
	options.addOption("objUriSpace",true, "uriSpace to the object dataset");
	options.addOption("linksetPath",true, "Path to the linkset");
	options.addOption("flagDataset",true, "flag = o we calculate Centrality of the objdataset if flag= s we calculate Centrality of the subjdataset");


	// added for impact  linkset
	options.addOption("linkingProperty",true, "Property_URI  indicates the  URI of properties that should be considered working out the linkset impact, if no prop are specified all the properties in the datasets are considered.");
	options.addOption("importedStatementsPath",true, "Path and file name prefix to the rdf file containing the statements that can be  imported via the interlinking");

	// added for linkimp4Centrality
	options.addOption("linksetCentrality",false, "run linkset centrality");
	options.addOption("WeightFilePath",true, "Path to the csw file specifying the weight for each relation, one row for each first column <relation url>, second colum  weight in [0, 1] ");
	options.addOption("K",true, "integer specifying the number of hops considered working out the centrality ");

	
	options.addOption("browsingInCSVFile", true, "Path and name  of the  CVS files with browsing results ");
	
	options.addOption("brokenlinkInCSVFile", true, "Path and name  of the  CVS files with brokenlink results ");
	
	options.addOption("analyseBrokenLinks", false, "run analyseBrokenLinks ");
	

	OptionBuilder.withArgName("filePath=File");
	OptionBuilder
			.hasArgs(10);
	OptionBuilder
			.withValueSeparator();
	OptionBuilder
			.withDescription( "filePath indicates the alignments from which Equivalence Relations must be derived as equivalences in the Quality Completeness" );
	//TODO alignement can be provided as source for the equivalence, in that case, the alignment rdf file must be specified and a threshold  upper which the alignment are transformed in equivalence for the type quality assessment  
	//options.addOption("AlignmentFormatFilePath",true, "file containing an alignment from  Equivalence Relation to be adopetd in Quality Completeness" );
	//FIXME At the moment it does not work if you specify more than one alignment
	org.apache.commons.cli.Option alignment = OptionBuilder
			.create( "algn" );

	options.addOption("thr",true, " only alignments whose values are greater than thr, a real value between 0 and 1, are considered  as equivalences in the Quality Completeness" );

	options.addOption(alignment);
	DataSetLinksetState.addOptionForFilteringDatasetOrLinkset(options);

	/*Parse the command line in parser */
	CommandLineParser parser = new PosixParser();


	// we can specify the parameters providing a VOID for the linkset



	try {
		/* Command line Validation*/
		cmd = parser.parse( options, args);
		HelpFormatter formatter = new HelpFormatter();

		if (!cmd.iterator().hasNext()){ // automatically generate the help statement
			formatter.printHelp( "QualityAssessment", options );
			System.exit(0);
		}

		if (cmd.hasOption("completeness") &&!cmd.hasOption("JSONSerializedRelationFilePath") && !cmd.hasOption("algn")) {
			formatter.printHelp( "QualityAssessment", options );
			System.out.println( " you should specify JSONSerializedRelationFilePath or algn ");
			System.exit(0);
		}

		if (!cmd.hasOption("indicators") && !cmd.hasOption("VOIDFile") && !cmd.hasOption("subjDatasetPath")) {
			formatter.printHelp( "QualityAssessment", options );
			System.out.println( " you should specify a file containing Indicator OR VOIDFile OR subj");
			System.exit(0);
		}

		// Create Logger

		//			Date date= new Date();
		//			SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		//			String cDate1= dateFormatter1.format(date);
		//			FileHandler fileTxt = new FileHandler("log/QualityAssessmentLOG"+cDate1+".txt");
		//			SimpleFormatter formatterTxt = new SimpleFormatter();
		//			fileTxt.setFormatter(formatterTxt);
		//			LOGGER.addHandler(fileTxt);
		QualityAssessment qa;
		if (cmd.hasOption("indicators")) {
			qa= new QualityAssessment(cmd.getOptionValue("indicators"), false);
		} else  if (cmd.hasOption("VOIDFile") ){
			/* in this case we have to calculate on the fly the indicators.
			     because we haven't run the crawling  for indicators in the dataHUB*/
			qa= new QualityAssessment(cmd.getOptionValue("VOIDFile"), true);
			retrieveIndicators(qa);
		} else {
			String p;
			if (cmd.hasOption("linkingProperty")) p=cmd.getOptionValue("linkingProperty");
			else p="http://www.w3.org/2002/07/owl#sameAs";
			LOGGER.info("Assuming linking property " +p);

			//	String subjDump, String subjUriSpace, String  objDump, String objUriSpace, String linksetDump, String linkingProperty
			qa= new QualityAssessment(cmd.getOptionValue("subjDatasetPath"), cmd.getOptionValue("subjUriSpace"),cmd.getOptionValue("objDatasetPath"), cmd.getOptionValue("objUriSpace"), cmd.getOptionValue("linksetPath"), p);
			retrieveIndicators(qa);
		}


		if (cmd.hasOption("extractDatasetAndRelated")) {
			ArrayList <String> c=new ArrayList<String> ();
			c.add(cmd.getOptionValue("extractDatasetAndRelated"));
			qa.state=qa.state.saveStateOnlyForMentionedDatasetAndRelatedDatasets(c, cmd.getOptionValue("o") );
		}

		if (cmd.hasOption("extractLinkset")) {
			ArrayList <String> c=new ArrayList<String> ();
			c.add(cmd.getOptionValue("extractLinkset"));
			/*if (cmd.hasOption("o")) */qa.state=qa.state.saveStateOnlyForMentionedLinkset(c, cmd.getOptionValue("o") );	
			/*else 
					qa.state=qa.state.saveStateOnlyForMentionedLinkset(c, null );*/
		}

		//  added for linkimp4Centraity 

		if (cmd.hasOption("all") || cmd.hasOption("linksetCentrality") ){
			// Options that should be specified for centrality 
			if (!cmd.hasOption("K")) { 
				K=3;
				LOGGER.warn("The parameter K isn't specified, It has been set equal to "+ K +"by default");
			}
			else{
				K=Integer.parseInt(cmd.getOptionValue("K"));
			}
			if (!cmd.hasOption("WeightFilePath")) { 
				weightForCentrality=new HashMap<String,Double>();
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#broader", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#related", 1.0);
				weightForCentrality.put("http://www.w3.org/2004/02/skos/core#narrower", 1.0);
				LOGGER.warn("The parameter WeightFilePath isn't specified, It has been set considering  skos:broader skos:related and skos:narrower set equal to 1.0 by default");
			} else weightForCentrality=readWeight(cmd.getOptionValue("WeightFilePath"));

		}

		/* calculate the quality based on indicators and void file*/
		QualityResult 	result=  qa.workOutQuality();
		Model modelResultInDQV= ModelFactory.createDefaultModel();
		// lets create the CSV file for Importing
		if (cmd.hasOption("impactInCSVFile")){
			result.saveImpactAsCSV(cmd);
			LOGGER.info(" the quality results have been written in " + cmd.getOptionValue("impactInCSVFile"));
		}
		if (cmd.hasOption("browsingInCSVFile")){
			result.saveBrowsingAsCSV(cmd);
			LOGGER.info(" the browsing results have been written in " + cmd.getOptionValue("browsingInCSVFile"));
		}
		
		if (cmd.hasOption("brokenlinkInCSVFile")){
			result.saveBrokenLinkAsCSV(cmd);
			LOGGER.info(" the brokenlink results have been written in " + cmd.getOptionValue("brokenlinkInCSVFile"));
		}
		
		if (cmd.hasOption("impactInDQVFile")){
			result.saveImpactAsDQV(cmd, modelResultInDQV);
			LOGGER.info(" the quality results have been written in " + cmd.getOptionValue("impactInFile"));
		}

		if (cmd.hasOption("JSONResult")){
			result.saveResultInJSON(cmd.getOptionValue("JSONResult"));

		}
	}catch ( Exception e) {
		LOGGER.fatal( "Quality assessment failed.  Reason: " +e.getMessage()  );
		e.printStackTrace();
	}




}
/**
 * It reads from a CSV file the relation's weights 
 * @param filePath
 * @return
 * @throws IOException 
 */
private static HashMap<String,Double> readWeight(String filePath) throws IOException {
	CSVReader reader = new CSVReader(new FileReader(filePath));
	String [] nextLine;
	HashMap <String, Double> res = new  HashMap <String, Double> ();
	while ((nextLine = reader.readNext()) != null) {
		// nextLine[] is an array of values from the line
		res.put(nextLine[0],  Double.valueOf(nextLine[1]));
	}


	return res;


}


}



