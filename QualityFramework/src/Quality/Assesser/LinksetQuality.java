package Quality.Assesser;

import java.util.HashMap;
import java.util.HashSet;

public class LinksetQuality {
	
	public double LinksetTypeCoverageForSubject=-1; // -1 means not initializated 
	public double LinksetTypeCoverageForObject=-1;
	
	public double LinksetTypeCompleteness=-1;
	
	public HashMap <String, Double> LinksetEntCov4TypeForSubject=new HashMap <String, Double> ();
	public HashMap <String, Double> LinksetEntCov4TypeForObject=new HashMap <String, Double> ();
	
	public  HashMap  <String, Integer > indSubjectType= new HashMap  <String, Integer > () ;
	public  HashMap  <String, Integer > indObjectType=new HashMap  <String, Integer > () ;
	public  HashMap  <String, Integer > indLinksetObjectType=new HashMap  <String, Integer > () ;
	public  HashMap  <String, Integer > indLinksetSubjectType=new HashMap  <String, Integer > () ;
	
	
	// let us add the accessibility
	public double LinksetDumpAccessability=-1; 
	
	public double SparqlAccessabilityForObject=-1;
	public double SparqlAccessabilityForSubject=-1;
	public double LinksetSparqlAccessability=-1; 
	
	
	public double SparqlAvailabilityForSubject=-1;
	public double SparqlAvailabilityForObject=-1;
	public double LinksetSparqlAvailability=-1;
	
	public double SparqlIndicatorAvailabilityForSubject=-1;
	public double SparqlIndicatorAvailabilityForObject=-1;
	public double LinksetIndicatorSparqlAvailability=-1;
	
	public HashMap <String,Double> LinkImp4p=null;
	public HashMap<String, Integer> NumberOfElementImported=null;
	
	//result of degree centrality for the whole linkset
	public double CrossWalkingSpreadingPotential=-1;
	
	//number of node in the object dataset
	public int  ObjectCardinality=-1;
	
	//number of node in the object dataset
	public int  SubjectCardinality=-1;

	//number of concepts of subj and obj restricted on namespace, and considered properties
	public int TotalConceptsObjectRestrictedNmspAndProp=-1;
	public int TotalConceptsObjectRestrictedNmspAndPropURI_less_URIObjlinkset=-1;
	public int TotalConceptsSubjRestrictedNmspAndProp=-1;
	//number of concepts of subj and obj restricted on namespace	
	public int TotalConceptsObjectRestrictedNmsp=-1;
	public int TotalConceptsSubjRestrictedNmsp=-1;			
	//number of concepts of subj and obj restricted on namespace, and skos type concepts
	public int TotalConceptsObjectRestrictedNmspAndSKOSTypeConcept=-1;
	public int TotalConceptsSubjRestrictedNmspAndSKOSTypeConcept=-1;
	
	//number of path with length k created
	public int numPathKLength=-1;
	
	// number of links in the linkset
	public int linksetCardinality=-1;
	
	public int  linksetObjectCardinality=-1;	
	
	//URI of concepts counted in the Reachability measure
	public HashSet<String> URIConceptsReachable= null;
	
	//Number of Broken link 
	public int  brokenLinkCardinality=-1;	
	
	//URi of Broken Link
	public HashSet<String> URIBrokenLink= null;
	
	
//	public String toString(){
//		String res="LinksetTypeCoverageForSubject \t" +LinksetTypeCoverageForSubject;
//		 res+=" \n LinksetTypeCoverageforObject \t" +LinksetTypeCoverageForObject;
//		 res+=" \n LinksetTypeCompleteness \t" +LinksetTypeCompleteness;
//		 res+=" \n LinksetEntCov4TypeForSubject \n" +LinksetEntCov4TypeForSubject.toString();
//		 res+=" \n LinksetEntCov4TypeForObject \n" +LinksetEntCov4TypeForObject.toString();
//		 
//		 return res;
//		 
//	
//	}
	
	
	
}
