set -x #echo on
cd ../

##ThIST EARTh
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath  Examples/EARThInLusTRE/20160127/THIST.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/EARTh.ttl  -subjUriSpace  http://linkeddata.ge.imati.cnr.it/resource/ThIST/  -objUriSpace http://linkeddata.ge.imati.cnr.it/resource/EARTh/  -linksetPath   Examples/EARThInLusTRE/20160127/THIST.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/THIST2EARTH_reachability_k4.csv -flagDataset  o  


##ThIST GEMET   
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath  Examples/EARThInLusTRE/20160127/THIST.ttl  -objDatasetPath  Examples/EARThInLusTRE/20160127/GEMET.ttl  -subjUriSpace  http://linkeddata.ge.imati.cnr.it/resource/ThIST/   -objUriSpace  http://www.eionet.europa.eu/gemet/concept/  -linksetPath   Examples/EARThInLusTRE/20160127/THIST.ttl -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/THIST2GEMET_reachability_k4.csv -flagDataset  o   


##ThIST - AGROVOC
java -Xms6000m   -Xmx10000m   -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath  Examples/EARThInLusTRE/20160127/THIST.ttl -objDatasetPath Examples/EARThInLusTRE/20160127/AGROVOC.ttl  -subjUriSpace  http://linkeddata.ge.imati.cnr.it/resource/ThIST/    -objUriSpace http://aims.fao.org/aos/agrovoc/  -linksetPath   Examples/EARThInLusTRE/20160127/THIST.ttl  -linksetCentrality   -K 4 -browsingInCSVFile Examples/EARThInLusTRE/20160127/Reachability_evaluation/THIST2AGROVOC_reachability_k4.csv -flagDataset  o  
