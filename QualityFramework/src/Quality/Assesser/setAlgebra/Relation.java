package Quality.Assesser.setAlgebra;

import java.util.ArrayList;

public interface Relation {
	
	public boolean isRelated( String x, String y );
	//public ArrayList<EquivalenceClass> getListOfEquivalenceClasses();

}
