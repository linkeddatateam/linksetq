package Quality;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;

public class LinksetInfoWithDump extends LinksetInfo {

/** model containing only the linkset
 *  	
 */
public Model dumpOfLinkset;
/***
 * Model containing linkset + subj + obj datasets
 */
public Model dumpOfLinksetSbjObj;
}
