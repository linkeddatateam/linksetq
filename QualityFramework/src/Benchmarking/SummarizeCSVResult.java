/**
 * 
 */
package Benchmarking;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import Quality.DataSetLinksetState;

/**
 * @author Paola
 *
 */
public class SummarizeCSVResult {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Options options = new Options();
		options.addOption("in", true, "csv to cleaned");
		options.addOption("out",true, "CSVcleaned" );


		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "SummarizeCSVResult", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (!cmd.hasOption("in"))  {
				formatter.printHelp( "SummarizeCSVResult", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}
			
			if (!cmd.hasOption("out"))  {
				formatter.printHelp( "SummarizeCSVResult", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}			
			
			String in= cmd.getOptionValue("in");
			String out= cmd.getOptionValue("out");
           
			CSVReader reader = new CSVReader(new FileReader(in), '\t');
		    String [] nextLine;
		    String [] nextLine1;
		    ArrayList <String[]> headers= new ArrayList<String[]>  ();
		    ArrayList <String[]> values= new ArrayList<String[]>  ();
		    LinkedHashMap<String, Integer> set= new LinkedHashMap<String, Integer>(); 
		    

		 
		    int i,j=0;
		    //riga senza colonne vuote
		    String[] noemptyline=null; 
		    		
		    while ((nextLine = reader.readNext()) != null) {
		        // nextLine[] is an array of values from the line
		    	try{
		    		//System.out.println("size line from reader:"+nextLine.length);
		    		
		    		if(Double.parseDouble(nextLine[1])>=0.0 ){
		    		// � una riga di valori
		    			values.add(nextLine);		
		    		}
		    	}catch(NumberFormatException  npe)
		    	{
					//�  una riga di instestazione
		    		headers.add(nextLine);
		    		for(i=0; i<nextLine.length; i++)
		    		{
		    			if (!nextLine[i].isEmpty())
		    			set.put(nextLine[i], 0);
		    			//System.out.println("values set order: "+ nextLine[i] );
		    			
		    		}
				} 
		    	
		    }
		    
				    
		    
		    
		    //creo la mia hash di corrispondenza 
		    //tra propriet� e colonna assegnata al valore della propriet�

    	
		    i=0;
		    for (String nl: set.keySet())
    		{
	    		//System.out.println("set values: "+ nl + i);
		    	set.put(nl, i);
	   			i++;
    		}
    		
		    int hmsize=set.size();
		    
    		
    		 CSVWriter writer = new CSVWriter(new FileWriter(out), '\t');
    		 
    		 
    		 //creo intestazione da scrivere nel file  csv 
    		 Set<String> colum;
    		 colum=set.keySet();
    		 
    		 //System.out.println("set intest size: "+ colum.size() );
    		 String[] intest=new String[colum.size()];
    		 
    		 int ii=0;
    		 
    		 
    		 for ( String key : set.keySet() ) {
    			 	 intest[ii]=key;
    			 	//System.out.println("key: "+key);
    			 	ii++;

    				 }
    		 //scrivo header nel file 
    		 
    		 writer.writeNext(intest);
	 
    		 
    		 
    		//creo la riga da scrivere nel file csv 
    		 //a partire dei valori headers e values
		    for(i=0; i<headers.size();i++){
		    	nextLine=headers.get(i); 
		    	nextLine1=values.get(i);
		    	//System.out.println("header size: "+headers.size());
		    	//System.out.println("VALUES size: "+values.size());
		    	
		    	//System.out.println("values size: "+values.get(i).length );
		    	
		    	String[] line=new String[hmsize];
		    	
		    	for (int index=0; index<hmsize;index++)
		    	{//inizializzo tutti i valori a zero 
		    		line[index]="0";
		    	}
		    	
		    	String h; 
		    	String v;

		    	for(j=0; j<Math.min(nextLine.length,nextLine1.length);j++)
		    	{
		    		h=nextLine[j]; 
		    		v=nextLine1[j];
		    		//System.out.println("header: "+h + " "+ j );
		    		//System.out.println("values: "+v +" "+ j );
		    		 if(!h.isEmpty()){
		    			 //questo controllo serve perchè 
		    			 //a volte in fondo alle colonne dei valori e 
		    			 //dell'intestazione viene messa una 
		    			 //colonna vuota, quindi non devo considerarla
				    		line[set.get(h)]=v;
				    		//System.out.println("set size: "+ set.size());		    			 
		    		 }
		    		

			    	
		    	}
		        writer.writeNext(line);
				
		    }
		    
		    writer.close();
		    
		    

			
			
           
		}catch ( Exception e) {
			e.printStackTrace();
		} 
		
		
		
		
	}

}
