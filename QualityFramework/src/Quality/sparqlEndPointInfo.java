package Quality;

public class sparqlEndPointInfo {
	public String sparqlEndPoint;
	
	public String UriSpace;
	public String consolidatedUriSpace;
	
	public String lastSparqlAccessProblem="";
	public int lastResponseCode; // the http code returned by the last query done on the dataset sparql end point
	
	public int availabilityResponseCode=0;
	public String availabilitySparqlAccessProblem="";
	
}
