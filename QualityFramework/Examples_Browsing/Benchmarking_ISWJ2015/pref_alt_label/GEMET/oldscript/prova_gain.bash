#creating the complementations 
set -x #echo on
cd ../../../


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/prova_test2.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test2/test2.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/prova_test2.3_NormalizedGain.csv





cd  Examples/Benchmarking/GEMET/
