

#bach di prova ad uso interno
set -x #echo on
cd ../../../../

#testset1
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_XXX_fewConcept.nt  -objDatasetPath Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_YYY_fewConcept.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_linkset_fewConcept.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test1/test1.0.csv -importedStatementsPath Examples/Benchmarking/GEMET/test1/test1.0_importedValues
#creating the complementations 

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_XXX_fewConcept.nt  -in Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_YYY_fewConcept.nt -linkset Examples/Benchmarking/GEMET/test1/Test0GEMET_it_es_en_linkset_fewConcept.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test0/test1.0_complementedDataset.rdf
  
## working out the normalized gain  (complemented - subject wrt goldstandard)
#java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test1/test1.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_XXX_fewConcept.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/test1/GEMET_it_es_en_GoldStandard_fewConcept.nt -out Examples/Benchmarking/GEMET/test1/test1.1_NormalizedGain.csv

#testset1
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_XXX_fewConcept.nt  -objDatasetPath Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_YYY_fewConcept.nt -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_linkset_fewConcept.nt   -impact   -impactP http://www.w3.org/2004/02/skos/core#prefLabel  -impactP http://www.w3.org/2004/02/skos/core#altLabel  -impactInCSVFile Examples/Benchmarking/GEMET/test1/test1.1.csv -importedStatementsPath Examples/Benchmarking/GEMET/test1/test1.1_importedValues
#creating the complementations 

#
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_XXX_fewConcept.nt  -in Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_YYY_fewConcept.nt -linkset Examples/Benchmarking/GEMET/test1/Test1GEMET_it_es_en_linkset_fewConcept.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test1/test1.1_complementedDataset.rdf
  

