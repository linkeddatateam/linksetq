package Benchmarking;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Assesser.LinksetImpactingStructure.UtilityFunctionsDegreeAssessment;
import Quality.Utilities.RDFAccess;

public class RandomDeleteConcepts {

	static Model dset;
	static Model lset;	
	private static Logger LOGGER= LogManager.getLogger(Benchmarking.RandomDeletePropertyValues.class.getName()); 
	private static ExecutionStructures es=new ExecutionStructures(); 

	public static void main(String[] args) {

		// create Options object
		Options options = new Options();
		options.addOption("dataset", true, "rdf dataset path from which to delete concepts");
		options.addOption("datasetnmsp", true, "namespace of dataset");
		options.addOption("linkset", true, "rdf linkset path from which to delete the link related to the concepts deleted");
		options.addOption("predlinkset", true, "skos predicate used in the linkset");
		options.addOption("flag", true, "flag representing if the dataset is the subjec s or the object o of the linkset");
		options.addOption("percentage",true, "percentage expressed as a number between 0 and 100 of concepts to be deleted" );
		options.addOption("outdataset",true, "output dataset path" );



		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
			// connecting to the logger
			//	Debugging.loggerConfiguration(logger, cmd.hasOption("logLevel") ? cmd.getOptionValue("logLevel"):null , RandomDeletePropertyValues.class);
			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "RandomDeleteConcepts", options );
				System.exit(0);
			}
			//other check on options that we could want 
			if (!cmd.hasOption("dataset")||!cmd.hasOption("linkset") 
					||!cmd.hasOption("flag")||!cmd.hasOption("percentage")
					||!cmd.hasOption("datasetnmsp")  
					||!cmd.hasOption("outdataset")
					||!cmd.hasOption("predlinkset"))  {
				formatter.printHelp( "RandomDeleteConcepts", options );
				throw new Exception("some input value  is missing");
			}
			String dataset= cmd.getOptionValue("dataset").trim();
			String linkset= cmd.getOptionValue("linkset").trim();
			String flag= cmd.getOptionValue("flag").trim();
			String outdataset= cmd.getOptionValue("outdataset").trim();
			String datasetnmsp= cmd.getOptionValue("datasetnmsp").trim();
			String predlinkset= cmd.getOptionValue("predlinkset").trim();
			int perc= Integer.parseInt(cmd.getOptionValue("percentage"));

			dset=org.apache.jena.riot.RDFDataMgr.loadModel(dataset);
			lset=org.apache.jena.riot.RDFDataMgr.loadModel(linkset);
			es.setModellinks(lset);
			es.extractURILinkofLinkset(flag, datasetnmsp, datasetnmsp, predlinkset);
			RandomDeleteConcepts.run2(dataset, linkset,flag,perc,dset,datasetnmsp);

			// finally the model must with deletion must been saved as indicated		
			RDFAccess.writeModelInTheFileNameSpecifiedEncoding(outdataset, dset);

			dset.close();
			lset.close();

		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}


	private static void run2(String datas, String links, String fl, int perc, Model dset,String Mnmsp) throws IOException {
		int numofconcepts=0;
		String qs, qs1;
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	
		String nmspc="";
		int countconcepts=0;
		int deleted=0;
		String uriS;

		File fdelete=new File(datas+"_"+perc+"_URI_ofRandomDeletedConcepts.csv");
		File fdeleteconc=new File(datas+"_"+perc+"_NumberofDeletedConcepts.csv");
		CSVWriter writerdelete = new CSVWriter(new FileWriter(fdelete), '\t');
		CSVWriter writerdelconc = new CSVWriter(new FileWriter(fdeleteconc), '\t');
		fdelete.getParentFile().mkdirs();
		String[] row= new String[1];



		numofconcepts=UtilityFunctionsDegreeAssessment.countNumofConceptinModel(dset, Mnmsp);

		int numElementsToBeCancelled= (int) Math.floor((numofconcepts-es.getLinkset().size())*perc/100);
		//LOGGER.info("total number of concepts in dataset " +numofconcepts + " number of concepts to be deleted " + numElementsToBeCancelled);


		qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?x  "
				+"WHERE  { ?x  a skos:Concept. "
				+"FILTER ( regex(str(?x),\"^"+Mnmsp+"\",\"i\") ) } ORDER BY ?x ";

		//find all the exactMatch in the linkset
		qs1=qs+
				"LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		//
		resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dset, qs1, err2, null));
		while ( deleted<numElementsToBeCancelled)
		{
			while(resP.hasNext() && deleted<numElementsToBeCancelled)
			{
				while(resP.hasNext() && deleted<numElementsToBeCancelled)
				{
					if (Math.random()>0.5) { // random choice
						// we should delete it
						Resource s=resP.next().getResource("x");
						//LOGGER.info("let's delete: "+ s.getURI() );
						row[0]=s.getURI();
						if(!es.isUriinLinkset(row[0]))
						{
							writerdelete.writeNext(row); 
							dset.remove(dset.listStatements(s,null, (RDFNode)null));
							dset.remove(dset.listStatements(null,null, (RDFNode)s));
							deleted++;
						}
					}
				}
				offset=offset+limit;
				qs1=qs+" limit "+ limit+" offset "+ offset;
				err2=new SparqlEndPointErrorMessage();
				resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dset, qs1, err2, null));
			}
			offset=0;
			limit= 1000;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dset, qs1, err2, null));

		}	
		row[0]=String.valueOf(deleted);
		writerdelconc.writeNext(row);


		writerdelconc.flush();
		writerdelconc.close();		

		writerdelete.flush();
		writerdelete.close();

	}


	private static void run(String datas, String links, String fl, int perc, Model dset,String Mnmsp) throws IOException {
		int numofconcepts=0;
		String qs, qs1;
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	
		String nmspc="";
		int countconcepts=0;
		int deleted=0;
		String uriS;

		File fdelete=new File(datas+"_"+perc+"_URI_ofRandomDeletedConcepts.csv");
		CSVWriter writerdelete = new CSVWriter(new FileWriter(fdelete), '\t');
		fdelete.getParentFile().mkdirs();
		String[] row= new String[1];



		numofconcepts=UtilityFunctionsDegreeAssessment.countNumofConceptinModel(dset, Mnmsp);

		int numElementsToBeCancelled= (int) Math.floor(numofconcepts*perc/100);




		//LOGGER.info("total number of concepts in dataset " +numofconcepts + " number of concepts to be deleted " + numElementsToBeCancelled);


		qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?x  "
				+"WHERE  { ?x  a skos:Concept. "
				+"FILTER ( regex(str(?x),\"^"+Mnmsp+"\",\"i\") ) } ";

		//find all the exactMatch in the linkset
		qs1=qs+
				"LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dset, qs1, err2, null));

		while ( deleted<numElementsToBeCancelled && resP.hasNext())
		{

			while(resP.hasNext() )
			{
				if (Math.random()>0.5) { // random choice
					// we should delete it
					Resource s=resP.next().getResource("x");
					//LOGGER.info("let's delete: "+ s.getURI() );
					row[0]=s.getURI();
					if(!es.isUriinLinkset(row[0]))
					{
						writerdelete.writeNext(row); 
						if(fl.trim().equalsIgnoreCase("o"))
						{

							dset.remove(dset.listStatements(s,null, (RDFNode)null));
							dset.remove(dset.listStatements(null,null, (RDFNode)s));							
						}
						if(fl.trim().equalsIgnoreCase("s"))
						{

							dset.remove(dset.listStatements(s,null, (RDFNode)null));
							dset.remove(dset.listStatements(null,null, (RDFNode)s));
						}

						deleted++;
					}
				}
			}

			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(dset, qs1, err2, null));


		}	

		writerdelete.flush();
		writerdelete.close();

	}
}




