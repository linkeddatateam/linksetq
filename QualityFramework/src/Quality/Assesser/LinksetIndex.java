package Quality.Assesser;

import java.util.ArrayList;
import java.util.HashMap;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Utilities.RDFAccess;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;


/**
 * Class maintaining a Linkset copy. It is thought to provide a quick access to associations in the linkset, 
 * It provides some simple methods that can be deployed to verify if a entity is in the linkset and what it is associated with it.
 * 
 * @author riccardoalbertoni
 *
 */

public class LinksetIndex{


	private HashMap <String, ArrayList <String> > linksetSubject = new HashMap  <String, ArrayList <String> >  ();
	private HashMap <String, ArrayList <String> > linksetObject = new HashMap  <String, ArrayList <String> >  ();

	 int numberOfLinks=0;
	/**
	 * It builds the the linkset indexes, which provides some simple methods that can be deployed to verify if a entity is in the linkset and what it is associated with it 
	 * @param l
	 * @param xd
	 * @param yd
	 */
	public  LinksetIndex( LinksetInfo l, DatasetInfo xd, DatasetInfo yd  ){
		// get the List of links
		
		int numberCicle4linkset=0, offset;
		boolean thereWereResults=false;

		do{

			offset=LinkImp4p.LIMIT_in_SPARQLENDPoint*numberCicle4linkset;
			numberCicle4linkset++;

			String queryString=" select distinct ?x ?y where { ?x <"+l.voidlinksetPredicate+"> ?y. " +
					"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
					"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}"+
					"order by ?x " +
					"LIMIT   "+ LinkImp4p.LIMIT_in_SPARQLENDPoint +"\n"+
					"OFFSET  "+offset;

			// exec the query to get the link in terms of interlinked entities 
			SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();

			//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
			ResultSet resSetOfLinks= RDFAccess.execQuery(l, queryString, err, null, false);

			if (resSetOfLinks!=null){
				QuerySolution sol;
				Resource x, y;
				String sx, sy;
			
				thereWereResults= resSetOfLinks.hasNext();
				while ( resSetOfLinks.hasNext()){
					//					NumberOfLink++;
					//					numberOfLink++;
					numberOfLinks++;
					sol=resSetOfLinks.next();
					x=sol.get("?x").asResource();
					y=sol.get("?y").asResource();
					sx= x.getURI();
					sy= y.getURI();
					if (linksetSubject.containsKey(sx)) {
						linksetSubject.get(sx).add(sy);
					} else {	
						ArrayList<String> c= new   ArrayList <String> (); 	
						c.add(sy);
						linksetSubject.put(sx, c ); 
					}

					if (linksetObject.containsKey(sy)) {
						linksetObject.get(sy).add(sx);
					} else {	
						ArrayList<String> c= new   ArrayList <String> (); 	
						c.add(sx);
						linksetObject.put(sy, c ); 
					}


				}

			}
		}while(thereWereResults);

	}
	
	/**
	 * It builds the linkset index assuming that in the linkset file there are only triples of the linkset and for only a Linkproperty  
	 * @param linkset
	 */
	public  LinksetIndex(String linkset ){
		// get the List of links

		Model linksetM = org.apache.jena.riot.RDFDataMgr.loadModel(linkset);
		// exec the query to get the link in terms of interlinked entities 
		SparqlEndPointErrorMessage  err=new SparqlEndPointErrorMessage();
		//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
		String queryString=" select distinct ?x ?y where { ?x ?p ?y }";
		ResultSet resSetOfLinks= RDFAccess.execQuery(linksetM, queryString, err, null,false);

		if (resSetOfLinks!=null){
			QuerySolution sol;
			Resource x, y;
			String sx, sy;
			//SimpleEntry <String> e;


			while ( resSetOfLinks.hasNext()){

				numberOfLinks++;
				sol=resSetOfLinks.next();
				x=sol.get("?x").asResource();
				y=sol.get("?y").asResource();
				sx= x.getURI();
				sy= y.getURI();
				if (linksetSubject.containsKey(sx)) {
					linksetSubject.get(sx).add(sy);
				} else {	
					ArrayList<String> c= new   ArrayList <String> (); 	
					c.add(sy);
					linksetSubject.put(sx, c ); 
				}

				if (linksetObject.containsKey(sy)) {
					linksetObject.get(sy).add(sx);
				} else {	
					ArrayList<String> c= new   ArrayList <String> (); 	
					c.add(sx);
					linksetObject.put(sy, c ); 
				}


			}
		}
	}

	/**
	 * @param x: a Entity
	 * @return True iff the entity is subject in a triple of the linkset 
	 */
	public Boolean isInLinksetAsSubject( String x){
		return this.linksetSubject.containsKey(x);
	}

	/**
	 * @param x: a Entity
	 * @return True iff the entity is Object in a triple of the linkset 
	 */
	public Boolean isInLinksetAsObject( String x){
		return this.linksetObject.containsKey(x);
	}
	/**
	 * @param x: a Entity
	 * @return the list of  Object Entities in the linkset, namely the object appearing in the Linkset's triples having  x as subject
	 */
	public ArrayList<String>  getEntitiesAssociatedToSubject( String x){
		return this.linksetSubject.get(x);
	}
	/**
	 * @param x: a Entity
	 * @return the list of  Subject Entities in the linkset, namely the Subject appearing in the Linkset's triples having  x as object
	 */
	public ArrayList<String> getEntitiesAssociatedToObject( String x){
		return this.linksetObject.get(x);
	}
	
	/**
	 * 
	 * @return the number of distinct  links contained in the linkset.
	 */
	public int getNumberOfLink(){ return numberOfLinks;}

}

