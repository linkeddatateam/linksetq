set -x #echo on
cd ../../../../

# browsing_centrality

#Centrality evaluation
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_XXX_pathK.nt   -objDatasetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_YYY_random_50_del.nt  -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_linkset_fewConcept_pathK_random_50_del.nt   -linksetCentrality -K 1 -browsingInCSVFile Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_XXX_pathK_centrality_assessment_randomdelete.csv  -flagDataset  o

java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Quality.Assesser.QualityAssessment -linkingProperty http://www.w3.org/2004/02/skos/core#exactMatch -subjDatasetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Gemet_merge_random_delete_50.nt   -objDatasetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_YYY_random_50_del.nt  -subjUriSpace http://XXX/  -objUriSpace http://YYY/ -linksetPath Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/GEMET_it_es_en_linkset_fewConcept_pathK_random_50_del.nt   -linksetCentrality -K 1 -browsingInCSVFile Examples_Browsing/Benchmarking/pref_alt_label/GEMET/test1/Gemet_merge_random_delete_50_centrality_assessment.csv  -flagDataset  o

cd  Examples_Browsing/Benchmarking/pref_alt_label/GEMET/
