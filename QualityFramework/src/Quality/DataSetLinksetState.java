package Quality;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import Quality.Assesser.LinksetQuality;
import Quality.Assesser.QualityResult;



import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class DataSetLinksetState  {
	public String state; // it can be one of the following value:  UriSpaceConsolidated; DatasetTypeExtracted; LinksetObjectTypeExtracted, LinksetSubjectTypeExtracted

	public HashMap <String, DatasetInfo> mapOfDataset= new HashMap<String, DatasetInfo> ();
	public ArrayList<LinksetInfo> linksetInfoArray=new ArrayList<LinksetInfo> ();

	//	@Deprecated
	//	public  void  writeStateOnTXT(String filePath){
	//		try {
	//			FileWriter outFile = new FileWriter(filePath);
	//			PrintWriter out = new PrintWriter(outFile);
	//
	//
	//			// print results of previous phase
	//			out.println("---->>> Datasets Number: " +this.mapOfDataset.size() +"\n " +
	//					this.mapOfDataset.toString());
	//			out.println("---->>> Linksets Number: " +this.linksetInfoArray.size() +"\n" + this.linksetInfoArray.toString());
	//			//out.println(assessedQuality.toString());
	//			out.close();
	//		} catch (IOException e){
	//			e.printStackTrace();
	//		}
	//
	//	}

	//	@Deprecated
	//	public  void  writeReults(String filePath){
	//		try {
	//			CSVWriter writer = new CSVWriter(new FileWriter(filePath), '\t');
	//			String[] entries;
	//			String entry;
	//
	//
	//			// print results of previous phase
	//			for (LinksetInfo ls:this.linksetInfoArray){
	//				entry=ls.name +"#";
	//				entry+=ls.subjectLocalName +"#";
	//				entry+=ls.objectLocalName +"#";
	//				DatasetInfo subject=this.mapOfDataset.get(ls.subjectLocalName);
	//				DatasetInfo object=this.mapOfDataset.get(ls.objectLocalName);
	//
	//				if (subject.sparqlEndpoint==null ||subject.sparqlEndpoint.isEmpty() ) entry+="NO subject sparqlEndPoint #";
	//				else if (object.sparqlEndpoint==null|| object.sparqlEndpoint.isEmpty()) entry+="NO object sparqlEndPoint #";
	//				else entry+="OK sparqlEndPoints #";
	//
	//				if (subject.UriSpace==null||subject.UriSpace.isEmpty()) entry+="NO subject UriSpace #";
	//				else if (object.UriSpace==null||object.UriSpace.isEmpty()) entry+="NO object UriSpace #";
	//				else entry+="Ok dataset UriSpace #";
	//
	//				if (subject.consolidatedUriSpace==null||subject.consolidatedUriSpace.isEmpty()) entry+="NO Consolidated subject UriSpace #";
	//				else if (object.consolidatedUriSpace==null||object.consolidatedUriSpace.isEmpty()) entry+="NO Consolidated object UriSpace #";
	//				else entry+="Ok consolidated UriSpace #";
	//
	//				if (subject.types==null||subject.types.isEmpty()) entry+="NO subject types #";
	//				else if (object.types==null||object.types.isEmpty()) entry+="NO object types #";
	//				else entry+="OK dataset types #";
	//
	//				if (ls.subjectTypes==null||ls.subjectTypes.isEmpty()) entry+="NO linkset subject types #";
	//				else if (ls.objectTypes==null||ls.objectTypes.isEmpty()) entry+="NO linkset object types #";
	//				else entry+=" OK Types indicators extracted";
	//
	//				entries=entry.split("#");
	//				writer.writeNext(entries);
	//			}
	//
	//			//out.println(assessedQuality.toString());
	//			writer.close();
	//		} catch (IOException e){
	//			e.printStackTrace();
	//		}
	//
	//	}
	//
	//	@Deprecated
	//	public  void saveState(String fileName){
	//		// writing an object to a file
	//
	//		try {
	//			writeStateOnTXT(fileName+".txt");
	//			ObjectOutputStream outStr = new ObjectOutputStream(new FileOutputStream(fileName));
	//			outStr.writeObject(this);
	//			outStr.flush();
	//			outStr.close();
	//		}
	//		catch(IOException ex) {
	//			System.out.println(ex.getMessage());
	//		}
	//	}

	public  void saveStateInJSON(String fileName){
		// writing an object to a file

		String jsonS = new JSONSerializer().include("mapOfDataset", "linksetInfoArray").serialize(this);
		try {
			FileWriter outFile = new FileWriter(fileName);
			PrintWriter out = new PrintWriter(outFile);

			// print results of previous phase
			out.print(jsonS);
			out.close();
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	public  DataSetLinksetState loadStatefromJSON(String fileName) throws IOException{
		// writing an object to a file

		BufferedReader reader = new BufferedReader( new FileReader (fileName));
		String         line = null;
		StringBuilder  stringBuilder = new StringBuilder();
		String         ls = System.getProperty("line.separator");

		while( ( line = reader.readLine() ) != null ) {
			stringBuilder.append( line );
			stringBuilder.append( ls );
		}

		String jsonS=stringBuilder.toString();
		return new JSONDeserializer<DataSetLinksetState>().deserialize( jsonS );

	}

	



	//	@Deprecated
	//	public  DataSetLinksetState loadState(String fileName){
	//		// reading an object from a file
	//		DataSetLinksetState ts=null;
	//		try {
	//			ObjectInputStream inStr = new ObjectInputStream(new FileInputStream(fileName));
	//			ts = (DataSetLinksetState) inStr.readObject();
	//			inStr.close();
	//		} catch (Exception e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		return ts;
	//	}


	public  DataSetLinksetState saveStateFilteringDatasetAccordingToLinksetClosure(ArrayList<String> nameOfDatasetsToConsiderAsSubjects, String fileName ){

		DataSetLinksetState ts= new DataSetLinksetState();
		ts.state= this.state;

		// let's insert all the datasetinfo that should consider as objects
		for (String nameOfdataset : nameOfDatasetsToConsiderAsSubjects){
			ts.mapOfDataset.put(nameOfdataset, this.mapOfDataset.get(nameOfdataset));
		}

		//let's add all the linksets object dataset and the linksets
		for ( LinksetInfo ls: this.linksetInfoArray){
			if (nameOfDatasetsToConsiderAsSubjects.contains(ls.subjectLocalName) ) {	
				String subjectName= ls.subjectLocalName; 
				DatasetInfo subjectDatasetInfo =mapOfDataset.get(subjectName);

				if (subjectDatasetInfo.subject){
					ts.linksetInfoArray.add(ls);

					if (!nameOfDatasetsToConsiderAsSubjects.contains(ls.objectLocalName)){
						DatasetInfo ds=this.mapOfDataset.get(ls.objectLocalName);
						ds.subject=false; // we don't want it is considered as a subject if it is not in thelist of reduction
						ts.mapOfDataset.put(ls.objectLocalName,ds);
					}

				}

			}

		}
		//ts.saveState(fileName);
		if (fileName!=null) ts.saveStateInJSON(fileName);
		return ts;
	}

	public  DataSetLinksetState saveStateOnlyForMentionedDataset(ArrayList<String> nameOfDatasetsToConsider, String fileName ){

		DataSetLinksetState ts= new DataSetLinksetState();
		ts.state= this.state;

		// let's insert all the datasetinfo that should consider as objects
		for (String nameOfdataset : nameOfDatasetsToConsider){
			ts.mapOfDataset.put(nameOfdataset, this.mapOfDataset.get(nameOfdataset));
		}

		//let's add all the linksets object dataset and the linksets
		for ( LinksetInfo ls: this.linksetInfoArray){
			if (nameOfDatasetsToConsider.contains(ls.subjectLocalName)  && nameOfDatasetsToConsider.contains(ls.objectLocalName)) {	
				ts.linksetInfoArray.add(ls);
			}

		}
		//ts.saveState(fileName);
		if (fileName!=null) ts.saveStateInJSON(fileName);
		return ts;
	}

	public  DataSetLinksetState saveStateOnlyForMentionedDatasetAndRelatedDatasets(ArrayList<String> nameOfDatasetsToConsider, String fileName ){

		DataSetLinksetState ts= new DataSetLinksetState();
		ts.state= this.state;

		// let's insert all the datasetinfo that should consider as objects
		for (String nameOfdataset : nameOfDatasetsToConsider){
			ts.mapOfDataset.put(nameOfdataset, this.mapOfDataset.get(nameOfdataset));
		}

		//let's add all the linksets object dataset and the linksets
		for ( LinksetInfo ls: this.linksetInfoArray){
			if (nameOfDatasetsToConsider.contains(ls.subjectLocalName)  ) {	
				ts.linksetInfoArray.add(ls);
				ts.mapOfDataset.put(ls.objectLocalName, this.mapOfDataset.get(ls.objectLocalName));
			}

		}
		//ts.saveState(fileName);
		if (fileName!=null) ts.saveStateInJSON(fileName);

		return ts;
	}


	public   DataSetLinksetState saveStateOnlyForMentionedLinkset(ArrayList<String> nameOfLinksetToConsider, String fileName ){

		DataSetLinksetState ts= new DataSetLinksetState();
		ts.state= this.state;
		///XXX
		Iterator <LinksetInfo> iter=this.linksetInfoArray.iterator();
		boolean notFound=true;
		LinksetInfo ls=new LinksetInfo() ;
		while ( notFound && iter.hasNext() ){
			ls = iter.next();
			for (String  l :nameOfLinksetToConsider)
				if (ls.name.equalsIgnoreCase(l)){ 
					ts.linksetInfoArray.add(ls) ;
					ts.mapOfDataset.put( ls.subjectLocalName, this.mapOfDataset.get(ls.subjectLocalName));
					ts.mapOfDataset.put( ls.objectLocalName, this.mapOfDataset.get(ls.objectLocalName));
				};
		}

		if (fileName!=null) ts.saveStateInJSON(fileName);
		return ts;
	}



	public    DataSetLinksetState saveStateOnlyForDatasetWithSparqlEndPoint(String SparqlEndPoint, String fileName ){



		DataSetLinksetState ts= new DataSetLinksetState();
		ts.state= this.state;
		String nameDatasetToConsider;
		for ( Entry <String,DatasetInfo>  e: this.mapOfDataset.entrySet()){
			DatasetInfo dt=e.getValue();
			if (SparqlEndPoint.equals(dt.sparqlEndPoint)){
				nameDatasetToConsider = e.getKey();
				ts.mapOfDataset.put(e.getKey(),dt);
				//let's add all the linksets object dataset and the linksets
				for ( LinksetInfo ls: this.linksetInfoArray){
					if (nameDatasetToConsider.equals(ls.subjectLocalName)  ) {	
						ts.linksetInfoArray.add(ls);
						ts.mapOfDataset.put(ls.objectLocalName, this.mapOfDataset.get(ls.objectLocalName));
					}

				}
				break; // go out of the for
			}

		}
		//ts.saveState(fileName);
		if (fileName!=null) ts.saveStateInJSON(fileName);

		return ts;
	}


	/**
	 * It manipolates a list of datasets and their linksets represented as toolState, for example, indicating one dataset it creates a toolstate containing that datasets and its linked datasets
	 *   
	 * @param args
	 */
	public static void main(String[] args){
		// provides a tool to manage and transform the content of a state object

		// create Options object
		Options options = new Options();

		// Options
		options.addOption("i", true, "input serialization of a ToolState");
		//options.addOption("extractState", false, " ");
		//OptionBuilder.withArgName("datasetName").hasArg().withDescription(" it extracts the dataset info  and related datasets/linkset for  <datasetName>").create( "extractDatasetAndRealted" );

		addOptionForFilteringDatasetOrLinkset(options);

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		DataSetLinksetState ts=new DataSetLinksetState();

		try {
			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "DataSetLinksetState", options );
				System.exit(0);
			}
			ts=				ts.loadStatefromJSON(cmd.getOptionValue("i"));

			if (cmd.hasOption("extractDatasetAndRelated")) {
				ArrayList <String> c=new ArrayList<String> ();
				c.add(cmd.getOptionValue("extractDatasetAndRelated"));
				ts.saveStateOnlyForMentionedDatasetAndRelatedDatasets(c, cmd.getOptionValue("o") );
			}
			if (cmd.hasOption("extractLinkset")) {
				ArrayList <String> c=new ArrayList<String> ();
				c.add(cmd.getOptionValue("extractLinkset"));
				ts.saveStateOnlyForMentionedLinkset(c, cmd.getOptionValue("o") );
			}
			if (cmd.hasOption("extractDtLsFromSparqlEndPoint")) {
				ts.saveStateOnlyForDatasetWithSparqlEndPoint(cmd.getOptionValue("extractDtLsFromSparqlEndPoint"),  cmd.getOptionValue("o"));
			}
			if (cmd.hasOption("extractLsIndicatorFromQuality")) {
				String file=cmd.getOptionValue("extractLsIndicatorFromQuality");
				// read the quality file
				QualityResult qr=QualityResult.loadResultFromJSON(file);
				ArrayList <String> linkesetToConsider=new ArrayList <String> ();

				// determining which linkset 

				// we should use reflectionin order to determing which filed in lq to consider but for the moment let's keep the parameter fixed 
				for( Entry<String, LinksetQuality>  e: qr.linksetQualityHashMap.entrySet()){
					LinksetQuality lq = e.getValue();
					if (lq.LinksetIndicatorSparqlAvailability==1) linkesetToConsider.add(e.getKey()); 
				}
				ts.saveStateOnlyForMentionedLinkset(linkesetToConsider, cmd.getOptionValue("o") );

			}


		}catch (Exception e){ System.out.println(e.getMessage());}
	}

	/**
	 * Add the set of options to specify  dataliksetstate filtering with respect to  a linkset or a dataset.  in the case of dataset, we keep also linkset in which the datset is subject dataset 
	 * 
	 * @param options
	 * @return
	 */
	public static void addOptionForFilteringDatasetOrLinkset(Options options){
		org.apache.commons.cli.Option extractDatasetAndRealted =  OptionBuilder.withArgName("datasetName").hasArg().withDescription(" it extracts the dataset info  and related datasets/linkset for  <datasetName>").create( "extractDatasetAndRelated" );
		options.addOption(extractDatasetAndRealted);

		org.apache.commons.cli.Option extractlinkset =  OptionBuilder.withArgName( "linksetName" ).hasArg().withDescription("(FilteringDatasetOrLinkset) it extracts the indicators and info  related linkset from  datasetName>").create( "extractLinkset" );
		org.apache.commons.cli.Option extractDtLsFromSparqlEndPoint =  OptionBuilder.withArgName( "SparqlEndPoint" ).hasArg().withDescription(" (FilteringDatasetOrLinkset) it extracts the info  related linkset and dataset with <SparqlEndPoint>").create( "extractDtLsFromSparqlEndPoint" );
		org.apache.commons.cli.Option extractFromQuality =OptionBuilder.withArgName("qualityFile").hasArg().withDescription("(FilteringDatasetOrLinkset) it extracts linkset with indicators quality from the specified <qualityFile>").create("extractLsIndicatorFromQuality" );

		// OptionBuilder.withArgName( "kindOfQuality:qualityFile" ).hasArgs(2).withValueSeparator(':').withDescription(" it extracts the info related to linkset having <kindOfquality> ( fixed to indicatorAvailability for the moment) maximun in the qualityFile indicated ").create( "extractLsWithQuality" );



		options.addOption(extractlinkset);
		options.addOption(extractDtLsFromSparqlEndPoint);
		options.addOption(extractFromQuality);

		options.addOption("o", true, "(FilteringDatasetOrLinkset) the name of the output  JSON file  where to write filtered indicators");
	}

}
