/**
 * Class providing  method for querying Linkset and Dataset according to metadata modelling done in the QualityFramework  
 */
package Quality.Utilities;

import com.hp.hpl.jena.rdf.model.Model;

/**
 * 
 * @author riccardoalbertoni
 * Type of RDF encoding in which a model can be serialized  by {@link Quality.Utilities.RDFAccess#writeModelInTheFileNameSpecifiedEncoding(String, Model)}
 */
public enum RDFEncoding {
	  rdf,  ttl ,  nt,  n3
	}
