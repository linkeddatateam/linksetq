package Quality.Assesser.setAlgebra;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class EquivalenceClass  {

	public ArrayList <String> elements; // list of equivalent elements // no repetition should be present
	//HashMap <DatasetInfo,String> representativePerDataset; // representative elements for a given dataset 
    public String representativeMember;
	
    public EquivalenceClass( Collection<String> e) {
		elements=new ArrayList <String>();
		
		Iterator<String> i=  e.iterator();
		// the first element is selected as representative
		if (i.hasNext()) {
			representativeMember=i.next();
			elements.add(representativeMember);
		}
		
		while (i.hasNext()) { 
			String x=i.next();
			if (!elements.contains(x)) elements.add(x); 
		}
	}

	public EquivalenceClass(String e) {
		elements=new ArrayList <String> ();
		elements.add(e);
		representativeMember=e;
	}

	public EquivalenceClass() {
		elements=new ArrayList <String> ();
		representativeMember="";
	}

	public boolean add(String e) {
		if (!elements.contains(e)) return elements.add(e);
		return false;
	}
	
//	public Iterator <String> getIteratorOnElements(){
//		return elements.iterator();
//	}
	/**
	 * Insert the elements of ec in this object if they are not yet in
	 * @param ec
	 */
	public void fuse( EquivalenceClass ec){
		for (String i : ec.elements ){
			String x= i;
			if (!elements.contains(x)) elements.add(x); 
		}
		// we don't change the representative 
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
//		if (getClass() != obj.getClass()  ) {
//			return false;
//		}
		final EquivalenceClass other = (EquivalenceClass) obj;
		boolean acc=false;
		for (String  i :other.elements ){  
			boolean res= elements.contains(i) ;
			acc=acc||res;
		}
		return acc;
	}

	@Override
	public int hashCode() {
		return this.representativeMember.hashCode();
	}



	//	public boolean equals(EquivalenceClass <String> obj) {
		//		if (obj == null) {
			//			return false;
	//		}
	//		boolean acc=false;
	//		if (( getClass() == obj.getClass() )  ) {
	//			final EquivalenceClass <String> other = (EquivalenceClass <String>) obj;      
	//			//			return elements.contains(obj) ;
	//			//
	//			//		}
	//
	//           // two  
	//			for (Iterator<String>  i=other.getIteratorOnElements(); i.hasNext(); ){  
	//				boolean res= elements.contains(i.next()) ;
	//				acc=acc||res;
	//			}
	//
	//
	//		}
	//		return acc;
	//	}
	//	
	//	
	//	public boolean equals(String e){
	//	 return elements.contains(e);
	//	}

	public boolean hasMember( String obj) {
		return elements.contains(obj);
	}
}