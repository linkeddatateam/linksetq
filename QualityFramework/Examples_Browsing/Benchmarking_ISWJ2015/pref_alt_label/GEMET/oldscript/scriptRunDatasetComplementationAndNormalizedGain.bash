#creating the complementations 
set -x #echo on
cd ../../../

#testset2
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel10_AltLabel10.nt  -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test2/test2.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel10_AltLabel10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.1_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test2/test2.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel30_AltLabel10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test2/test2.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel60_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.3_NormalizedGain.csv






##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_YYY.nt -linkset Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_linkset.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test2/test2.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test2/test2.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test2/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test2/test2.4_NormalizedGain.csv



#testset3
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test3/test3.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.1_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test3/test3.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt  -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test3/test3.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -in Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt  -linkset Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test3/test3.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test3/test3.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test3/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test3/test3.4_NormalizedGain.csv




#testset4
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch10.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.1_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.1_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch30.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.3_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.4_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.4_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.5_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.5_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt -in Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_YYY.nt  -linkset Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_linkset_exactMatch99.9.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test4/test4.6_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test4/test4.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test4/GEMET_it_es_en_XXX.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test4/test4.6_NormalizedGain.csv



#testset5
##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.1_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.2_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.3_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.3_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.4_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.4_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.5_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.6_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.6_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.7_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.7_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_linkset.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test5/test5.8_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test5/test5.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test5/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test5/test5.8_NormalizedGain.csv




#testset6
#DIRECTORY test6.1: consiDering GEMET_it_es_en_linkset_exactMatch10.nt
#XXX_prefLabel90_AltLabel50

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.1_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.1_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.2_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.2_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.3_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.3_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.4_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.4_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.5_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.6_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.7_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch10.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.1/test6.8_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.1/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.1/test6.8_NormalizedGain.csv




#test 6.2: considering GEMET_it_es_en_linkset_exactMatch30.nt

#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.1_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.3_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.4_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.4_NormalizedGain.csv



#XXX_prefLabel100_AltLabel90

##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.6_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.7_NormalizedGain.csv


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch30.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.2/test6.8_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.2/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.2/test6.8_NormalizedGain.csv



#test3: considering GEMET_it_es_en_linkset_exactMatch50.nt


#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.1_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.2_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.4_NormalizedGain.csv



#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.6_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.7_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.3/test6.8_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.3/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.3/test6.8_NormalizedGain.csv





#test6.4: considering GEMET_it_es_en_linkset_exactMatch90.nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.1_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.2_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.4_NormalizedGain.csv



#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.6_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.7_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.4/test6.8_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.4/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.4/test6.8_NormalizedGain.csv







#test 6.5:considering GEMET_it_es_en_linkset_exactMatch99.nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.1_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.2_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.2_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.4_NormalizedGain.csv



#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.6_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.7_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.5/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.5/test6.8_NormalizedGain.csv







#test 6.6:considering GEMET_it_es_en_linkset_exactMatch99.9nt



#XXX_prefLabel90_AltLabel50


##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.1_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.1_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.1_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.2_complementedDataset.rdf  
 
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.2_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.2_NormalizedGain.csv




##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.3_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.3_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.3_NormalizedGain.csv





##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.4_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.4_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel90_AltLabel50.nt   -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.4_NormalizedGain.csv




#XXX_prefLabel100_AltLabel90



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel10_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.5_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.5_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.5_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel30_AltLabel10.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.6_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.6_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.6_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel60_AltLabel50.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.7_complementedDataset.rdf  

## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.7_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.7_NormalizedGain.csv



##Complementing
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.MergeDatasets -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt -in Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_YYY_prefLabel100_AltLabel90.nt -linkset Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_linkset_exactMatch99.9.nt  -NSConvert Examples/Benchmarking/GEMET/nsMap.txt  -out Examples/Benchmarking/GEMET/test6.6/test6.8_complementedDataset.rdf  
## working out the normalized gain  (SUM (coeff_norm*(complemented quality wrt goldstandard - subject quality wrt goldstandard)))
java -classpath $CLASSPATH:QualityFramework.jar:./lib/*  Benchmarking.GainWRTGoldStandard -complemented Examples/Benchmarking/GEMET/test6.6/test6.8_complementedDataset.rdf -subject  Examples/Benchmarking/GEMET/test6/GEMET_it_es_en_XXX_prefLabel100_AltLabel90.nt    -NSConvert Examples/Benchmarking/GEMET/nsMap.txt   -goldStandard Examples/Benchmarking/GEMET/GEMET_it_es_en.rdf -out Examples/Benchmarking/GEMET/test6.6/test6.8_NormalizedGain.csv



cd  Examples/Benchmarking/GEMET/
