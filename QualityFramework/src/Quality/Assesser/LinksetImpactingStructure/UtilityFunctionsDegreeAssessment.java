/**
 * 
 */
package Quality.Assesser.LinksetImpactingStructure;

import java.util.ArrayList;
import java.util.HashSet;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;

import Jama.Matrix;
import Quality.SparqlEndPointErrorMessage;

/**
 * @author Paola
 *
 *The class contains all the function 
 *necessary to compute the degree function
 *
 *
 */
public final class UtilityFunctionsDegreeAssessment {
	
	

	//the power of matrix A 
	public static Matrix powerMatrix(Matrix A, int k)
	{
	
		Matrix aux= new Matrix(0,0);
		aux= A.copy();
		//System.out.println("A:dentropowerMatr: ");
		//A.print(0, 0);
		
		for(int i=2; i<=k; i++)
		{
			A=A.times(aux);
			//System.out.println("A:for: "+i);
			//A.print(0, 0);
		}
		
		return A;		
		
	}


	//matrix containing as elements number of paths existing between two node in  k hops
	public static Matrix VNMMatrix(Matrix AFlat,int k ) 
	{
		//TODO
		Matrix AFlataux=AFlat; 
		for(int i=1; i<=k; i++)
		{
			AFlataux=AFlataux.plus(powerMatrix(AFlat,i));

		}
		

		return AFlataux;
	}
	

	
	
	
	//	OPTIMIZED matrix containing as elements number of paths existing between two node in  k hops
	public static Matrix VNMMatrix2(ExecutionStructures es,int k ) 
	{
		//TODO
		Matrix AFlataux=es.getPowerM().get(1); 
		for(int i=2; i<=k; i++)
		{
			AFlataux=AFlataux.plus(es.getPowerM().get(i));

		}
		

		return AFlataux;
	}
	
	
	
	
	//the set of nodes reachable in  k hops
	public static HashSet<Integer> RNSSet(Matrix AFlat, int indexuri, int k) 
	{
		//TODO
		Matrix AFlataux=AFlat; 
		HashSet<Integer> HS=new HashSet<Integer>();
		
		if (k>0)
		{
			AFlataux=powerMatrix(AFlat,k);

				for(int j=0;j<AFlataux.getColumnDimension();j++ )
				{
					if(AFlataux.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}
		}
		return HS;
	}	
	
	

	public static HashSet<Integer> RNSSet2(Matrix AFlat, int indexuri, int k) 
	{
		//TODO
		//Matrix AFlataux=AFlat; 
		HashSet<Integer> HS=new HashSet<Integer>();
		
		if (k>0)
		{
			//AFlataux=powerMatrix(AFlat,k);

				for(int j=0;j<AFlat.getColumnDimension();j++ )
				{
					if(AFlat.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}
		}
		return HS;
	}	


	//the set of all the nodes visited in  k hops
	public static HashSet<Integer> VNSSet( Matrix AFlat,int indexuri,int k) 
	{
		//TODO
		Matrix VNM;
		HashSet<Integer> HS=new HashSet<Integer>();
		HS.add(indexuri);
		
		if(k>0)
		{
			VNM=VNMMatrix(AFlat,k);
			for(int j=0;j<VNM.getColumnDimension();j++ )
				{
					if(VNM.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}			
		}
		return HS;
	}	
	
	
	
	
	//FURTHER OPTIMIZED the set of all the nodes visited in  k hops
	public static HashSet<Integer> VNSSet3( Matrix VNM, ExecutionStructures es,int indexuri,int k) 
	{
		//TODO
		
		HashSet<Integer> HS=new HashSet<Integer>();
		HS.add(indexuri);
		
		if(k>0)
		{
			//VNM=VNMMatrix2(es,k);
			for(int j=0;j<VNM.getColumnDimension();j++ )
				{
					if(VNM.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}			
		}
		return HS;
	}	
	
	
	
	
	
	//FURTHER OPTIMIZED the set of all the nodes visited in  k hops
	public static HashSet<Integer> VNSSet3( ExecutionStructures es,int indexuri,int k) 
	{
		//TODO
		Matrix VNM;
		HashSet<Integer> HS=new HashSet<Integer>();
		HS.add(indexuri);
		
		if(k>0)
		{
			VNM=es.getVNMlist().get(k);
			for(int j=0;j<VNM.getColumnDimension();j++ )
				{
					if(VNM.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}			
		}
		return HS;
	}	
	
	
	//OPTIMIZED the set of all the nodes visited in  k hops
	public static HashSet<Integer> VNSSet2( ExecutionStructures es,int indexuri,int k) 
	{
		//TODO
		Matrix VNM;
		HashSet<Integer> HS=new HashSet<Integer>();
		HS.add(indexuri);
		
		if(k>0)
		{
			VNM=VNMMatrix2(es,k);
			for(int j=0;j<VNM.getColumnDimension();j++ )
				{
					if(VNM.get(indexuri,j)>0)
					{
						HS.add(j);
					}
					
				}			
		}
		return HS;
	}		
	
	
	
	public static void  toStringVNSRNSSet(ArrayList<String> umi, HashSet<Integer> VNS, int k){
		//output per righe 

		for(int i: VNS)
		{
			System.out.print(" "+umi.get(i) +" ");
		}
		System.out.print("\n");
	}

	
	

	//degree gen function
	public static double degreeGenExtfunction(Matrix AFlat, ArrayList<String> umi,String l,int k ) 
	{
		//TODO
		double deg=0.0;
		int indexl=-1;
		
		HashSet<Integer> VNS= new HashSet<Integer>();
		HashSet<Integer> VNS_1= new HashSet<Integer>();
		HashSet<Integer> RNS= new HashSet<Integer>();
		HashSet<Integer> RNS1= new HashSet<Integer>();
		HashSet<Integer> TVN= new HashSet<Integer>();
		
		
		
		RNS=RNSSet(AFlat,umi.indexOf(l),k);
		
		
		/*
		System.out.print("\n");
		System.out.print("RNS :");
		System.out.print("\n");	
		//
		//toStringVNSRNSSet(umi, RNS, k);
		 * 
		 */
		/*
		System.out.print("\n");
		System.out.print("VNS :");
		System.out.print("\n");
		
		*/
		
		//toStringVNSRNSSet(umi, VNS, k);
		VNS=VNSSet(AFlat,umi.indexOf(l),k);
		indexl=umi.indexOf(l);
		
		RNS1=RNSSet(AFlat,indexl,1);
		TVN.addAll(RNS1);
		
		/*System.out.print("\n");
		System.out.print("RNS 1 di :"+l);
		System.out.print("\n");	
		toStringVNSRNSSet(umi, RNS1, k);*/
		deg=RNS1.size();
		//System.out.print("deg 1 hop :"+deg);
		for(int h=1; h<=k;h++)
		{
			

			RNS=RNSSet(AFlat,indexl,h);
			/*System.out.print("\n");
			System.out.print("RNS hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, RNS, k);*/

			VNS_1=VNSSet(AFlat,indexl,h-1);
			/*System.out.print("\n");
			System.out.print("VNS_1 hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, VNS_1, k);*/
			
			VNS=VNSSet(AFlat,indexl,h);
			
			
			RNS.removeAll(VNS_1);
			TVN.addAll(RNS);
			/*System.out.print("\n");
			System.out.print("RNS meno VNS_1 hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, RNS, k);*/

			
			if(RNS.size()>0)
			{
				for(int z2: RNS)
				{
					RNS1=RNSSet(AFlat,z2,1);
					/*System.out.print("\n");
					System.out.print("RNS 1 di :"+umi.get(z2));
					System.out.print("\n");	
					toStringVNSRNSSet(umi, RNS1, k);*/

					
					if(RNS1.size()>0)
					{
						RNS1.removeAll(VNS); 
						TVN.addAll(RNS1);
						/*System.out.print("\n");
						System.out.print("RNS1 meno VNS");
						System.out.print("\n");	
						toStringVNSRNSSet(umi, RNS1, k);*/
						/*
						if(RNS1.size()>0)
						{
							for(int y: RNS1)
							{
								System.out.print(" matrice aflat\n");
								System.out.print(" "+z2 + " "+y+ ": "+AFlat.get(z2, y)+"\n");	
								deg=deg+AFlat.get(z2, y);
								
								
							}
							
						}*/
					}
				}
				
			}
			
			
		}
		
		//System.out.print(" TVN size: "+TVN.size() +"\n");
		
		//toStringVNSRNSSet(umi, TVN, k);
		
		
		return deg;
		
	}
	
	
	//optimized versions
	public static HashSet<String> degreeSetSingleLinkfunction2( ExecutionStructures es,String l,int k ) 
	{
		//TODO
		
		int indexl=-1;
		
		HashSet<Integer> VNS= new HashSet<Integer>();
		HashSet<Integer> VNS_1= new HashSet<Integer>();
		HashSet<Integer> RNS= new HashSet<Integer>();
		HashSet<Integer> RNS1= new HashSet<Integer>();
		HashSet<Integer> TVN= new HashSet<Integer>();
		HashSet<String> UriTVN= new HashSet<String>();
		
		Matrix AFlat=es.getFAWM(); 
		ArrayList<String> umi= es.getURIMatrixIndex();
		
		
		
		//Matrix AFlataux=powerMatrix(AFlat,k);
		/*
		System.out.print("\n");
		System.out.print("RNS :");
		System.out.print("\n");	
		
		//toStringVNSRNSSet(umi, RNS, k);
		 * 
		 */
		RNS=RNSSet2(es.getPowerM().get(k),umi.indexOf(l),k);
		/*
		System.out.print("\n");
		System.out.print("VNS :");
		System.out.print("\n");
		
		*/
		VNS=VNSSet3(es,umi.indexOf(l),k);
		//toStringVNSRNSSet(umi, VNS, k);
		indexl=umi.indexOf(l);
		
		RNS1=RNSSet2(es.getPowerM().get(1),indexl,1);
		TVN.addAll(RNS1);
		
		//System.out.print("\n");
		//System.out.print("RNS 1 di :"+l);
		//System.out.print("\n");	
		//toStringVNSRNSSet(umi, RNS1, k);
		for(int h=1; h<=k;h++)
		{
			

			RNS=RNSSet2(es.getPowerM().get(h),indexl,h);
			/*
			System.out.print("\n");
			System.out.print("RNS hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, RNS, k);
             */
			VNS_1=VNSSet3(es,indexl,h-1);
			/*System.out.print("\n");
			System.out.print("VNS_1 hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, VNS_1, k);
			*/
			VNS=VNSSet3(es,indexl,h);
			
			
			RNS.removeAll(VNS_1);
			TVN.addAll(RNS);
			/*System.out.print("\n");
			System.out.print("RNS meno VNS_1 hop :"+h);
			System.out.print("\n");	
			toStringVNSRNSSet(umi, RNS, k);
			*/

			
			if(RNS.size()>0)
			{
				for(int z2: RNS)
				{
					RNS1=RNSSet2(es.getPowerM().get(1),z2,1);
					/*System.out.print("\n");
					System.out.print("RNS 1 di :"+umi.get(z2));
					System.out.print("\n");	
					toStringVNSRNSSet(umi, RNS1, k);
					*/

					
					if(RNS1.size()>0)
					{
						RNS1.removeAll(VNS); 
						TVN.addAll(RNS1);
						/*System.out.print("\n");
						System.out.print("RNS1 meno VNS");
						System.out.print("\n");	
						toStringVNSRNSSet(umi, RNS1, k);*/
						/*
						if(RNS1.size()>0)
						{
							for(int y: RNS1)
							{
								System.out.print(" matrice aflat\n");
								System.out.print(" "+z2 + " "+y+ ": "+AFlat.get(z2, y)+"\n");	
								
								
							}
							
						}*/
					}
				}
				
			}
			
			
		}
		
		//System.out.print(" TVN size: "+TVN.size() +"\n");
		
		
		for(int i: TVN)
		{

			UriTVN.add(umi.get(i));
		}

		
		//toStringVNSRNSSet(umi, TVN, k);
		
		
		return UriTVN;
		
	}

	
	
	
	
	
	
	/*Compute the number of skos concepts in a model with a specific namespace 
	 * 
	 * 
	 * */


	public static int countNumofConceptinModel(Model m, String Mnmsp) {

		String qs=" "; 
		String		qs1=" ";
		ResultSetRewindable resP=null;

		int offset=0;
		int limit= 1000;

		QuerySolution qsol;
		String yuri=" ";	
		String nmspc="";
		int countconcepts=0;
		
		qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT (count (?x) as ?numconcepts) "
				+"WHERE  { ?x  a skos:Concept. "
				+"FILTER ( regex(str(?x),\"^"+Mnmsp+"\",\"i\") ) } ";
		
		
		//find all the exactMatch in the linkset
		qs1=qs+
				"LIMIT   "+limit +"\n"+
				"OFFSET  "+offset;

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(m, qs1, err2, null));
		
		while (resP.size()>0)
		{
			countconcepts=countconcepts+resP.next().getLiteral("numconcepts").getInt();
			offset=offset+limit;
			qs1=qs+" limit "+ limit+" offset "+ offset;
			
			err2=new SparqlEndPointErrorMessage();
			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(m, qs1, err2, null));
		}
		return countconcepts;
	}

	
	
	
	
	
	
	//return for link l the set of nodes reachable in k hops from l
		public static HashSet<String> degreeSetSingleLinkfunction(Matrix AFlat, ArrayList<String> umi,String l,int k ) 
		{
			//TODO
			
			int indexl=-1;
			
			HashSet<Integer> VNS= new HashSet<Integer>();
			HashSet<Integer> VNS_1= new HashSet<Integer>();
			HashSet<Integer> RNS= new HashSet<Integer>();
			HashSet<Integer> RNS1= new HashSet<Integer>();
			HashSet<Integer> TVN= new HashSet<Integer>();
			HashSet<String> UriTVN= new HashSet<String>();
			
			
			/*
			System.out.print("\n");
			System.out.print("RNS :");
			System.out.print("\n");	
			
			//toStringVNSRNSSet(umi, RNS, k);
			 * 
			 */
			RNS=RNSSet(AFlat,umi.indexOf(l),k);
			/*
			System.out.print("\n");
			System.out.print("VNS :");
			System.out.print("\n");
			
			*/
			VNS=VNSSet(AFlat,umi.indexOf(l),k);
			//toStringVNSRNSSet(umi, VNS, k);
			indexl=umi.indexOf(l);
			
			RNS1=RNSSet(AFlat,indexl,1);
			TVN.addAll(RNS1);
			
			//System.out.print("\n");
			//System.out.print("RNS 1 di :"+l);
			//System.out.print("\n");	
			//toStringVNSRNSSet(umi, RNS1, k);
			for(int h=1; h<=k;h++)
			{
				

				RNS=RNSSet(AFlat,indexl,h);
				/*
				System.out.print("\n");
				System.out.print("RNS hop :"+h);
				System.out.print("\n");	
				toStringVNSRNSSet(umi, RNS, k);
                 */
				VNS_1=VNSSet(AFlat,indexl,h-1);
				/*System.out.print("\n");
				System.out.print("VNS_1 hop :"+h);
				System.out.print("\n");	
				toStringVNSRNSSet(umi, VNS_1, k);
				*/
				VNS=VNSSet(AFlat,indexl,h);
				
				
				RNS.removeAll(VNS_1);
				TVN.addAll(RNS);
				/*System.out.print("\n");
				System.out.print("RNS meno VNS_1 hop :"+h);
				System.out.print("\n");	
				toStringVNSRNSSet(umi, RNS, k);
				*/

				
				if(RNS.size()>0)
				{
					for(int z2: RNS)
					{
						RNS1=RNSSet(AFlat,z2,1);
						/*System.out.print("\n");
						System.out.print("RNS 1 di :"+umi.get(z2));
						System.out.print("\n");	
						toStringVNSRNSSet(umi, RNS1, k);
						*/

						
						if(RNS1.size()>0)
						{
							RNS1.removeAll(VNS); 
							TVN.addAll(RNS1);
							/*System.out.print("\n");
							System.out.print("RNS1 meno VNS");
							System.out.print("\n");	
							toStringVNSRNSSet(umi, RNS1, k);*/
							/*
							if(RNS1.size()>0)
							{
								for(int y: RNS1)
								{
									System.out.print(" matrice aflat\n");
									System.out.print(" "+z2 + " "+y+ ": "+AFlat.get(z2, y)+"\n");	
									
									
								}
								
							}*/
						}
					}
					
				}
				
				
			}
			
			//System.out.print(" TVN size: "+TVN.size() +"\n");
			
			
			for(int i: TVN)
			{

				UriTVN.add(umi.get(i));
			}

			
			//toStringVNSRNSSet(umi, TVN, k);
			
			
			return UriTVN;
			
		}
	

}
