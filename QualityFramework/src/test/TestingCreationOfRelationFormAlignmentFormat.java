package test;

import java.util.ArrayList;
import java.util.Arrays;

import Quality.Assesser.setAlgebra.ExtensionalRelationFromEquivalenceClasses;

public class TestingCreationOfRelationFormAlignmentFormat {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList <String> paths= new ArrayList<String>(Arrays.asList( "/Users/riccardoalbertoni/Documents/workspace/QualityFramework/bin/test/equivalenceAF.rdf")); 
		
		ExtensionalRelationFromEquivalenceClasses rel= new ExtensionalRelationFromEquivalenceClasses(paths, 0.9d);
		rel.RelationsSerializer(" ExtensionalRelationFromalignmentFormat.json");

	}

}
