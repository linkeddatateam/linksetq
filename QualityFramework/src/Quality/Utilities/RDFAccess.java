/**
 * it provides some static method to access to RDF stored in dumps, model and spqrql endpoints assuming the access information are properly described into a Jena Model or  DatasetInfo, or DatasetInfoWithDump or DatasetInfoWithVOID or LinksetInfoWithDump or LinksetInfoWithVOID   
 */
package Quality.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;


import Quality.Utilities.RDFEncoding;
import Quality.DatasetInfo;
import Quality.DatasetInfoWithDump;
import Quality.DatasetInfoWithVOID;
import Quality.LinksetInfo;
import Quality.LinksetInfoWithDump;
import Quality.LinksetInfoWithVOID;
import Quality.SparqlEndPointErrorMessage;
import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;

/**
 * @author riccardoalbertoni
 *
 */
public class RDFAccess {
	//private  Logger logger;
	private static Logger LOGGER= LogManager.getLogger(Quality.Utilities.RDFAccess.class.getName()); 
	
/***
 * It returns the JENA query execution class prepared to work on a remote SPARQL endpoint or in a local model  depending on the dynamic type of l 
 * It can be handy when using "construct" instead of "select" in the sparql query or  if  qexec  need to be close.    
 * @param sparqlSelectQueryString
 * @param d
 * @return a QueryExecution object
 */
	public static QueryExecution createQueryExecution( String sparqlSelectQueryString, Object d ){
		
		
		Query query = QueryFactory.create(sparqlSelectQueryString) ;
		QueryExecution qexec=null;
		LOGGER.info("quering by" +sparqlSelectQueryString);
		if (d instanceof Model){
			//LOGGER.info("quering  local model  by " +sparqlSelectQueryString);
			 qexec = QueryExecutionFactory.create( query, (Model) d	);
		}else if (d instanceof DatasetInfoWithDump ){
			 qexec = QueryExecutionFactory.create( query, (( DatasetInfoWithDump) d).dump	);
		}else if (d instanceof LinksetInfoWithDump  ){ 
			qexec = QueryExecutionFactory.create( query, ((LinksetInfoWithDump) d).dumpOfLinksetSbjObj);
		}else if (d instanceof DatasetInfo || d instanceof DatasetInfoWithVOID ) { 
			DatasetInfo dt= (DatasetInfo) d;
			LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +sparqlSelectQueryString);
			qexec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);
		}else if (d instanceof LinksetInfoWithVOID){
		 qexec = QueryExecutionFactory.sparqlService( ((LinksetInfo) d).sparqlEndPoint, query);
		}else {
			LOGGER.error(" type of model or rdfstore or metadata structure for querying not yet managed");
		}
		return qexec;
	}

	
	
	
/**
 * added for querying a local model or a sparql endpoint depending on the dynamic type of d 
 * @param d a Jena Model or  DatasetInfo, or DatasetInfoWithDump or DatasetInfoWithVOID or LinksetInfoWithDump or LinksetInfoWithVOID
 * @param queryString a 
 * @param err info about error that have been returned by the sparql end point
 * @param blackListOfSparqlEndPoint  put null if there are no  a sparql endpoint blacklist
 * @return query results in a JENA ResultSet
 */
public static ResultSet execQuery(Object d, String queryString, SparqlEndPointErrorMessage err, HashSet <String> blackListOfSparqlEndPoint, boolean linksetOnly  ) {
	
	ResultSet res=null;
	Query query = QueryFactory.create(queryString);
	
	if (d instanceof Model){
		QueryExecution exec = QueryExecutionFactory.create( query, (Model) d);
		LOGGER.info("quering  local model  by " +queryString);
		res= exec.execSelect();

	}else if (d instanceof DatasetInfoWithDump ){
		QueryExecution exec = QueryExecutionFactory.create( query, (( DatasetInfoWithDump) d).dump	);
		try{
			res= exec.execSelect();
		
		} catch (Exception e){
			LOGGER.error("problems quering local model"+ e.getMessage());
		}
		
	} else if (d instanceof LinksetInfoWithDump)  {
		QueryExecution exec;
		if (linksetOnly) 	exec = QueryExecutionFactory.create( query, (( LinksetInfoWithDump) d).dumpOfLinkset);
		else exec = QueryExecutionFactory.create( query, (( LinksetInfoWithDump) d).dumpOfLinksetSbjObj);
		try{
			res= exec.execSelect();
		} catch (Exception e){
			LOGGER.error("problems quering local model for the linkset "+   ((LinksetInfoWithDump) d).name +" "+ e.getMessage());	
		}
		
	}else if (d instanceof DatasetInfo || d instanceof DatasetInfoWithVOID ) { 
		DatasetInfo dt = (DatasetInfo) d;
		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);
		//Context c=exec.getContext();
		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {
			try{
				res= exec.execSelect();
				//defaultModel.add(answer) ;
				err.responseCode=200;
			} catch (QueryExceptionHTTP e){
				err.responseCode= e.getResponseCode();
				err.sparqlAccessProblem+= "\n"+e.getMessage();
				LOGGER.error("problems quering "+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());
			}
		} else {
			err.responseCode= -1;
			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
		}
		
	} else if ( d instanceof LinksetInfoWithVOID ) {
		LinksetInfo dt = (LinksetInfo) d;
		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);

		//Context c=exec.getContext();
		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {

			try{
				res= exec.execSelect();
				//defaultModel.add(answer) ;
				err.responseCode=200;
			} catch (QueryExceptionHTTP e){
				err.responseCode= e.getResponseCode();
				err.sparqlAccessProblem+= "\n"+e.getMessage();
				LOGGER.error("problems quering"+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());

			}
		} else {
			err.responseCode= -1;
			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
		}
	}else {
		LOGGER.error(" type of model or rdfstore or metadata structure for querying not yet managed");
	}
	return  res;


}



public static ResultSet execQueryObj(Object d, String queryString, SparqlEndPointErrorMessage err, HashSet <String> blackListOfSparqlEndPoint) {
	
	ResultSet res=null;
	Query query = QueryFactory.create(queryString);
	
	if (d instanceof Model){
		QueryExecution exec = QueryExecutionFactory.create( query, (Model) d);
		LOGGER.info("quering  local model  by " +queryString);
		res= exec.execSelect();

	}else if (d instanceof DatasetInfoWithDump ){
		QueryExecution exec = QueryExecutionFactory.create( query, (( DatasetInfoWithDump) d).objdump	);
		try{
			res= exec.execSelect();
		
		} catch (Exception e){
			LOGGER.error("problems quering local model"+ e.getMessage());
		}
		
	} else if (d instanceof LinksetInfoWithDump)  {
		QueryExecution exec = QueryExecutionFactory.create( query, (( LinksetInfoWithDump) d).dumpOfLinksetSbjObj);
		try{
			res= exec.execSelect();
		} catch (Exception e){
			LOGGER.error("problems quering local model for the linkset "+   ((LinksetInfoWithDump) d).name +" "+ e.getMessage());	
		}
		
	}else if (d instanceof DatasetInfo || d instanceof DatasetInfoWithVOID ) { 
		DatasetInfo dt = (DatasetInfo) d;
		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);
		//Context c=exec.getContext();
		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {
			try{
				res= exec.execSelect();
				//defaultModel.add(answer) ;
				err.responseCode=200;
			} catch (QueryExceptionHTTP e){
				err.responseCode= e.getResponseCode();
				err.sparqlAccessProblem+= "\n"+e.getMessage();
				LOGGER.error("problems quering "+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());
			}
		} else {
			err.responseCode= -1;
			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
		}
		
	} else if ( d instanceof LinksetInfoWithVOID ) {
		LinksetInfo dt = (LinksetInfo) d;
		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);

		//Context c=exec.getContext();
		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {

			try{
				res= exec.execSelect();
				//defaultModel.add(answer) ;
				err.responseCode=200;
			} catch (QueryExceptionHTTP e){
				err.responseCode= e.getResponseCode();
				err.sparqlAccessProblem+= "\n"+e.getMessage();
				LOGGER.error("problems quering"+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());

			}
		} else {
			err.responseCode= -1;
			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
		}
	}else {
		LOGGER.error(" type of model or rdfstore or metadata structure for querying not yet managed");
	}
	return  res;


}








/**
 * It writes the model in a file serializing According to the file extension see {@link Quality.Utilities.RDFEncoding} for supported RDFencodings.
 * @param file including its path 
 * @param model jena model to be serialized
 * @throws IOException
 */

public static void writeModelInTheFileNameSpecifiedEncoding(String file, Model model)
		throws IOException {
	
	/// connecting to the logger
	
	
	File f=new File(file);
	f.getParentFile().mkdirs();
	FileOutputStream fw= new  FileOutputStream(f);
	//FileWriter fw= new FileWriter(f);
	//org.apache.jena.riot.RDFDataMgr.
	// depending on the extension we write in a different format
	String fExtension= file.substring(file.lastIndexOf(".")+1).toLowerCase();
	
	switch( RDFEncoding.valueOf(fExtension)){
	case rdf : 			model.write(fw, "RDF/XML"); break;
	case ttl :           model.write(fw, "TURTLE"); break;
	case nt :           model.write(fw, "N-TRIPLE"); break;
	case n3 :           model.write(fw, "N3"); break;
	default: model.write(fw, "RDF/XML"); break;
	}
	
	LOGGER.info(" rdf has been written at: " + file);
	fw.close();
}


///**
// * added for querying a local model or a sparql endpoint depending on the dynamic type of d 
// * @param d a Jena Model or  DatasetInfo, or DatasetInfoWithDump or DatasetInfoWithVOID or LinksetInfoWithDump or LinksetInfoWithVOID
// * @param queryString a 
// * @param err
// * @param blackListOfSparqlEndPoint  put null if there are no  a sparql endpoint blacklist
// * @return
// */
//public static ResultSet execQuery1(Object d, String queryString, SparqlEndPointErrorMessage err, HashSet <String> blackListOfSparqlEndPoint){
//	Logger LOGGER = Logger.getLogger(RDFAccess.class);
//	BasicConfigurator.configure();
//	
//	ResultSet res=null;
//	QueryExecution exec=createQueryExecution(queryString, d);
//	
//	try{
//		res= exec.execSelect();
//	
//	} catch (QueryExceptionHTTP e){
//	
//	}
//
//	
//	}else if (d instanceof DatasetInfo || d instanceof DatasetInfoWithVOID ) { 
//		DatasetInfo dt = (DatasetInfo) d;
//		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
//		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);
//
//		//Context c=exec.getContext();
//		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {
//
//			try{
//				res= exec.execSelect();
//				//defaultModel.add(answer) ;
//				err.responseCode=200;
//			} catch (QueryExceptionHTTP e){
//				err.responseCode= e.getResponseCode();
//				err.sparqlAccessProblem+= "\n"+e.getMessage();
//				LOGGER.error("problems quering"+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());
//
//			}
//		} else {
//			err.responseCode= -1;
//			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
//		}
//	} else if ( d instanceof LinksetInfoWithVOID ) {
//		LinksetInfo dt = (LinksetInfo) d;
//		LOGGER.info("quering "+ dt.sparqlEndPoint +" by " +queryString);
//		QueryExecution exec = QueryExecutionFactory.sparqlService(dt.sparqlEndPoint, query);
//
//		//Context c=exec.getContext();
//		if (blackListOfSparqlEndPoint==null || !blackListOfSparqlEndPoint.contains(dt.sparqlEndPoint)) {
//
//			try{
//				res= exec.execSelect();
//				//defaultModel.add(answer) ;
//				err.responseCode=200;
//			} catch (QueryExceptionHTTP e){
//				err.responseCode= e.getResponseCode();
//				err.sparqlAccessProblem+= "\n"+e.getMessage();
//				LOGGER.error("problems quering"+  dt.sparqlEndPoint+ " by SELECT: "+ err.responseCode +" "+ e.getMessage());
//
//			}
//		} else {
//			err.responseCode= -1;
//			err.sparqlAccessProblem+= "sparqlEndPoint in BlackList";
//		}
//	}
//	return  res;
//
//
//}
//	public static ResultSet execQueryingASparqlEndPoint(sparqlEndPointInfo s, String queryString){
//
//		LOGGER.fine("quering "+ s.sparqlEndPoint +" by " +queryString);
//		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
//		QueryExecution exec = QueryExecutionFactory.sparqlService(s.sparqlEndPoint, query);
//		//		QueryEngineHTTP exec = (QueryEngineHTTP) QueryExecutionFactory.sparqlService(endpoint_service, query);
//		//		if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);
//
//		ResultSet res=null;
//		Context c=exec.getContext();
//		try{
//			res= exec.execSelect();
//			//defaultModel.add(answer) ;
//			s.lastResponseCode=200;
//		} catch (QueryExceptionHTTP e){
//			s.lastResponseCode= e.getResponseCode();
//			s.lastSparqlAccessProblem+= "\n"+e.getMessage();
//			LOGGER.info("problems quering"+  s.sparqlEndPoint+ " by SELECT: "+ s.lastResponseCode +" "+ e.getMessage());
//
//		}
//		return  res;
//
//
//	}
//}
}


