package Quality.CKANCrawlerIndicatorExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.DataSetLinksetState;
import Quality.SparqlEndPointErrorMessage;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.tdb.TDBFactory;
@Deprecated
public  class SparqlEndPointTypeDumps {

	//Dataset dataset;
	static Logger logger;
	private static CommandLine cmd; 

	public SparqlEndPointTypeDumps(String jenaStore,  Logger llogger){
		//dataset = TDBFactory.createDataset(jenaStore) 

		// in the default model we write all the crawling entities and pertaining metadata
		// long cDate= System.currentTimeMillis();
		logger=llogger;
		logger.fine("Created the TDB store");
	}

	/**
	 * Given a JENA model serialize that model as Ntriple file, naming it differently depending on if the dataset is a subject or a object-
	 * 
	 * @param UriFromWhichDeriveTheFileName
	 * @param m Jena model 
	 * @param subject boolean true if it is a subject dataset
	 * @throws IOException
	 */
	static public void writeDumpsOnFile( String UriFromWhichDeriveTheFileName, Model m, Boolean subject) throws IOException{
		UriFromWhichDeriveTheFileName=UriFromWhichDeriveTheFileName.substring(UriFromWhichDeriveTheFileName.lastIndexOf("/")+1);
		java.io.OutputStream out;
		if (subject){
			out= new FileOutputStream("TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
			logger.info("Start to write "+"TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
			m.write(out,"N-TRIPLE", null);
			logger.info("Finish to write "+"TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");


		}else{
			out= new FileOutputStream("TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
			logger.info("Start to write "+"TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
			m.write(out,"N-TRIPLE", null);
			logger.info("Finish to write "+"TypeDumps/Object"+UriFromWhichDeriveTheFileName+".nt");
		}
		out.close();

	}
 /*
  * Check if a dataset dump of a dataset is already in the dump directory
  */
	static public boolean isTheDumpAlreadyPresent(String UriFromWhichDeriveTheFileName,Boolean subject){
		UriFromWhichDeriveTheFileName=UriFromWhichDeriveTheFileName.substring(UriFromWhichDeriveTheFileName.lastIndexOf("/")+1);
		java.io.InputStream in;
		try{
			if (subject){
				in= new FileInputStream("TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
			} else {
				in= new FileInputStream("TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
			}
		}
		catch (IOException e) {
			return false;
		}
		try {
			in.close();
		} catch (IOException e) {

		}
		return true;
	}

	static public boolean isTheTDBDumpAlreadyPresent(String UriFromWhichDeriveTheFileName){
		UriFromWhichDeriveTheFileName=UriFromWhichDeriveTheFileName.substring(UriFromWhichDeriveTheFileName.lastIndexOf("/")+1);
		java.io.File in;
		in= new File("TypeDumpsTDB/"+UriFromWhichDeriveTheFileName+"/");
		return in.exists();
	}


	private ResultSet queryOnTDBDump(String UriFromWhichDeriveTheFileName, String queryString) {

		UriFromWhichDeriveTheFileName=UriFromWhichDeriveTheFileName.substring(UriFromWhichDeriveTheFileName.lastIndexOf("/")+1);
		Dataset dataset = TDBFactory.createDataset("TypeDumpsTDB/"+UriFromWhichDeriveTheFileName+"/") ;
		Model m= dataset.getDefaultModel();


		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		return res;
	}

	static public Model loadDumpsFromFile( String UriFromWhichDeriveTheFileName, Model m, boolean subject) throws IOException{

		InputStream in = null; 
		UriFromWhichDeriveTheFileName=UriFromWhichDeriveTheFileName.substring(UriFromWhichDeriveTheFileName.lastIndexOf("/")+1);
		if (subject){
			in=new FileInputStream("TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
			logger.info("Start to read "+"TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
			m=m.read(in,null,"N-TRIPLE" );
			logger.info("Finish to read "+"TypeDumps/DumpSubject"+UriFromWhichDeriveTheFileName+".nt");
		}else{
			in=new FileInputStream("TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
			logger.info("Start to read "+"TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
			m=m.read(in,null, "N-TRIPLE" );
			logger.info("Finish to read "+"TypeDumps/DumpObject"+UriFromWhichDeriveTheFileName+".nt");
		}
		in.close();
		return m;
	}

	/**
	 * this method dumps all the triples like {?y rdf:type ?type. ?y owl:sameAs ?z. }  from the subject's sparql Endpoint
	 *  the Dump is written as N-Triple on disk TypeDumps/DumpSubject+datasetcrawlname.nt 
	 *  No filter on type and dataset are performed in order to have less problematic intereaction on Sparql End Point
	 * @param subjectds  specifies metadata of the subject dataset 
	 * @return 
	 * @return 
	 * @throws Exception is raised when the dump is not performed
	 */
	public  void constructOnSubjectLinksetResultSet (DatasetInfo subjectds) throws Exception{

		if ( !isTheDumpAlreadyPresent(subjectds.crawledDatasetUri,true)) {
			InputStream ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LinksetSubjectDump.sparql");
			String queryString = CrawlMetadata.getContents(ip);

			Boolean sameAs=false;
			Boolean exactMatch=false;
			
			
			ResultSet res;
			if (!isTheTDBDumpAlreadyPresent(subjectds.crawledDatasetUri)){
				logger.info(" start quering  "+ subjectds.sparqlEndPoint +" by " +queryString);
				SparqlEndPointErrorMessage err = new SparqlEndPointErrorMessage();
				res = CrawlMetadata.execQueryingASparqlEndPoint(subjectds.sparqlEndPoint, queryString, err);

				logger.info(" result received  "+ subjectds.sparqlEndPoint );
			}else {
				// exec query on the local tdb dump 
				res = queryOnTDBDump(subjectds.sparqlEndPoint, queryString);

			}
			
			Model m= ModelFactory.createDefaultModel();
			try{
				if (res!=null && res.hasNext()) { 
					//m=dataset.getNamedModel(subjectds.crawledDatasetUri);
					Property typeP= m.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
					Property sameAsP=m.createProperty("http://www.w3.org/2002/07/owl#sameAs");
					QuerySolution sol;
					Resource y, type,  z;
					if (res.hasNext()){
						System.out.println("I am about to download a result sets of "+ res.getRowNumber());	
						sameAs=true;
					}
					logger.info(" started to add result to model  " );

					while(res.hasNext()){ // store result in the Store
						sol = res.next();
						y = sol.getResource("?y");
						type = sol.getResource("?type");
						z = sol.getResource("?z");
						m.add(y, typeP, type );
						m.add(y, sameAsP, z );
						System.out.println(" added in the dumpStore graph "+subjectds.crawledDatasetUri +" "+ y+" rdf:type " + type +"; owl:sameAs " +z);
					}
					logger.info(" finished to add result to model  "+ subjectds.sparqlEndPoint );

				}
			} catch (Exception e) { 
				System.out.println(e.getMessage());
			}


			ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LinksetSubjectDumpExactMatch.sparql");
			queryString = CrawlMetadata.getContents(ip);

			logger.fine("quering "+ subjectds.sparqlEndPoint +" by " +queryString);

			if (!isTheTDBDumpAlreadyPresent(subjectds.crawledDatasetUri)){
				logger.info(" start quering  "+ subjectds.sparqlEndPoint +" by " +queryString);
				SparqlEndPointErrorMessage err = new SparqlEndPointErrorMessage();
				res = CrawlMetadata.execQueryingASparqlEndPoint(subjectds.sparqlEndPoint, queryString, err);

				logger.info(" result received  "+ subjectds.sparqlEndPoint );
			}else {
				// exec query on the local tdb dump 
				res = queryOnTDBDump(subjectds.sparqlEndPoint, queryString);

			}

			if (res!=null ) { 
				//m=dataset.getNamedModel(subjectds.crawledDatasetUri);
				Property typeP= m.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
				Property ExactMatch=m.createProperty("http://www.w3.org/2004/02/skos/core#exactMatch");
				QuerySolution sol;
				Resource y, type,  z;
				if (res.hasNext())
					exactMatch=true;
				while(res.hasNext()){ // store result in the Store
					sol = res.next();
					y = sol.getResource("?y");
					type = sol.getResource("?type");
					z = sol.getResource("?z");
					m.add(y, typeP, type );
					m.add(y, ExactMatch, z );
				}

			} 
			if (!sameAs && !exactMatch)  throw new Exception( "DUMP  of SubjectLinksetResultSet FAILED, no owl:sameAs or skos:exactMatch  for  "+ subjectds.crawledDatasetUri );
			else writeDumpsOnFile( subjectds.crawledDatasetUri, m, true);
			m.close(); 
		}


	}



	/**
	 * it  dumps all the triples like {?y rdf:type ?type } for linkset having   @subject and object respectively as 
	 * linkset target and object, this method downloads  from the object sparql endpoint 
	 * @param objectds specifies metadata of the object dataset 
	 * @throws Exception  is raised when the dump is not performed
	 */
	public void constructOnObjectLinksetResultSet ( DatasetInfo objectds) throws Exception{
		if (!isTheDumpAlreadyPresent(objectds.crawledDatasetUri,false)){

			InputStream ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LinksetObjectDump.sparql");
			String queryString = CrawlMetadata.getContents(ip);
			ResultSet res;

			logger.fine("quering "+ objectds.sparqlEndPoint +" by " +queryString);
			if (!isTheTDBDumpAlreadyPresent(objectds.crawledDatasetUri)){
				logger.info(" start quering  "+ objectds.sparqlEndPoint +" by " +queryString);
				SparqlEndPointErrorMessage err = new SparqlEndPointErrorMessage();
				res = CrawlMetadata.execQueryingASparqlEndPoint(objectds.sparqlEndPoint, queryString,err);

				logger.info(" result received  "+ objectds.sparqlEndPoint );
			}else {
				// exec query on the local tdb dump 
				res = queryOnTDBDump(objectds.sparqlEndPoint, queryString);

			}

			Model  m = ModelFactory.createDefaultModel();
			if (res!=null) { 
				//m=dataset.getNamedModel(subjectds.crawledDatasetUri);
				Property typeP= m.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
				QuerySolution sol;
				Resource y, type;
				while(res.hasNext()){ // store result in the Store
					sol = res.next();
					y = sol.getResource("?y");
					type = sol.getResource("?type");
					m.add(y, typeP, type );	
					System.out.println(" added in the dumpStore "+objectds.crawledDatasetUri+ "  graph "+ y+" rdf:type " + type );
				}
			} else throw new Exception( "DUMP  of ObjectLinksetResultSet FAILED for  "+ objectds.crawledDatasetUri );


			writeDumpsOnFile( objectds.crawledDatasetUri, m, false); 
			m.close();
		}


	}

	/**
	 * It works out the object linkset types for the linkset having subject and object respectively as linkset's subject and object 
	 * @param linksetInfo
	 * @param subjectInfo
	 * @param objectInfo
	 * @param InputModel  if it is null the subject linkset triples are uploaded into the returned model otherwise they are supposed to be already uploaded
	 * @return 
	 * @throws Exception
	 */
	public Model  workOutObjectLinksetType (LinksetInfo linksetInfo, DatasetInfo subjectInfo, DatasetInfo objectInfo, Model inputModel) throws Exception{


		InputStream ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LinksetObjectTypeOnDump.sparql");
		String queryString = CrawlMetadata.getContents(ip);


		queryString= queryString.replace("SUJBECTURISPACE", subjectInfo.consolidatedUriSpace).replace("OBJECTURISPACE", objectInfo.consolidatedUriSpace);

		//Model m=dataset.getNamedModel(subjectInfo.crawledDatasetUri);
		Model m =  inputModel==null ? ModelFactory.createDefaultModel(): inputModel;
		if (inputModel==null)
			loadDumpsFromFile( subjectInfo.crawledDatasetUri,  m, true) ;

		loadDumpsFromFile( objectInfo.crawledDatasetUri,  m, false) ;

		logger.fine("quering "+ objectInfo.sparqlEndPoint +" by " +queryString);


		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 
			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					linksetInfo.objectTypes.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subjectInfo.crawledDatasetUri +" and " +  objectInfo.crawledDatasetUri );

			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ objectInfo.crawledDatasetUri );
		return m;






	}

	public Model  workOutSubjectLinksetType (LinksetInfo ls, DatasetInfo subject, DatasetInfo object, Model inputModel) throws Exception{
		InputStream ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/LinksetSubjectTypeOnDump.sparql");
		String queryString = CrawlMetadata.getContents(ip);

		queryString= queryString.replace("SUJBECTURISPACE", subject.consolidatedUriSpace).replace("OBJECTURISPACE", object.consolidatedUriSpace);

		//Model m=dataset.getNamedModel(subject.crawledDatasetUri);
		logger.fine("quering "+ object.sparqlEndPoint +" by " +queryString);
		Model m = inputModel==null? ModelFactory.createDefaultModel(): inputModel;
		loadDumpsFromFile( subject.crawledDatasetUri,  m, true) ;


		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 
			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					ls.objectTypes.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subject.crawledDatasetUri +" and " +  object.crawledDatasetUri );
			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ object.crawledDatasetUri );
		return m;

	}


	public Model  workOutDatasetType (DatasetInfo subject, Model inputModel) throws Exception{
		InputStream ip = CrawlMetadata.class.getResourceAsStream("../TemplateQuery/DatasetTypeOnDump.sparql");
		String queryString = CrawlMetadata.getContents(ip);


		//Model m=dataset.getNamedModel(subject.crawledDatasetUri);
		logger.fine("quering "+ subject.sparqlEndPoint +" by " +queryString);
		Model m = inputModel==null? ModelFactory.createDefaultModel(): inputModel;
		loadDumpsFromFile( subject.crawledDatasetUri,  m, true) ;


		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
		QueryExecution exec = QueryExecutionFactory.create(query, m);

		ResultSet res=null;
		res= exec.execSelect();

		if (res!=null) {
			QuerySolution sol;
			RDFNode type; 
			int noccurence; 
			while (res.hasNext()){
				sol=res.next();
				type=sol.get("?type");
				noccurence=sol.get("?numOfOccurrence").asLiteral().getInt();
				if (type!=null){
					subject.types.put(type.toString(),noccurence); 
				}else throw new Exception( "query to ObjectLinksetResultSet FAILED probably there are no sameAs between " + subject.crawledDatasetUri +" and " +  subject.crawledDatasetUri );
			} 
		} else throw new Exception( "query to ObjectLinksetResultSet FAILED for  "+ subject.crawledDatasetUri );
		return m;


	}


	public void workOutTypes(DataSetLinksetState ts) {



		for (LinksetInfo ls: ts.linksetInfoArray){
			DatasetInfo subjectInfo=ts.mapOfDataset.get(ls.subjectLocalName);
			DatasetInfo objectInfo=ts.mapOfDataset.get(ls.objectLocalName);
			Model m=null;
			// have been indicator extracted?
			try {
				this.constructOnSubjectLinksetResultSet(ts.mapOfDataset.get(ls.subjectLocalName));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				this.constructOnObjectLinksetResultSet(ts.mapOfDataset.get(ls.objectLocalName));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ls.objectTypes=new  HashMap<String, Integer > ();
			ls.subjectTypes=new  HashMap<String, Integer > (); 

			if (subjectInfo.consolidatedUriSpace!=null && objectInfo.consolidatedUriSpace!=null){
				try {
					//TODO it does not make sense to load subject info into the model  both on workOutSubjectDatasetType and workOutObjectLinksetType
					//TODO if a dataset is target of some linkset, we know we have to download it for determing the linkset types on the  targets side, so adaptation shoudn't be considered for that kidn of dataset 
					m=workOutSubjectLinksetType (ls, subjectInfo, objectInfo, null);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try{
					m=workOutObjectLinksetType (ls, subjectInfo, objectInfo, m);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (objectInfo.types.isEmpty()){
					//TODO then we can use the dump made  for determining the objetc dataset type
					try {
						m=workOutDatasetType(objectInfo, m);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.warning(e.getMessage());
					}
				}
				if (subjectInfo.types.isEmpty()){
					//TODO then we check if we have already dumped it, otherwise we will dump it, and eventually determine them
					try{	
						if (!isTheTDBDumpAlreadyPresent(subjectInfo.crawledDatasetUri)&&!isTheDumpAlreadyPresent(subjectInfo.crawledDatasetUri, false) ) constructOnObjectLinksetResultSet(subjectInfo);
						m=workOutDatasetType(subjectInfo, m);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.warning(e.getMessage());
					}
				}
			}else  logger.info( "query to  ObjectLinksetResultSet FAILED NO CONSOLIDATED uriSpace for one of the target datasets  "+ objectInfo.crawledDatasetUri +" or "+ objectInfo.crawledDatasetUri );

		}

	}


	//	/**
	//	 * It queries an sparql end point with a Construct and return a model containing the  construct result 
	//	 * @param endpoint_service
	//	 * @param queryString
	//	 * @return 
	//	 */
	//	public static Model execConstructQueryingASparqlEndPoint(String endpoint_service, String queryString, Logger logger){
	//		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
	//		QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint_service, query);
	//		//		QueryEngineHTTP exec = (QueryEngineHTTP) QueryExecutionFactory.sparqlService(endpoint_service, query);
	//		//		if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);
	//		//	
	//		Model answer=null;
	//		try{
	//			answer = exec.execConstruct();
	//			//defaultModel.add(answer) ;
	//		} catch (Exception e){
	//			logger.info("problems quering "+  endpoint_service+ " by contruct: " + e.getMessage());
	//
	//		}finally{
	//			exec.close();
	//		}
	//		return  answer;
	//
	//
	//	}
	//	
	//	public static ResultSet execQueryingASparqlEndPoint(String endpoint_service, String queryString, Logger logger ){
	//		com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString) ;
	//		QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint_service, query);
	//		//		QueryEngineHTTP exec = (QueryEngineHTTP) QueryExecutionFactory.sparqlService(endpoint_service, query);
	//		//		if (Integer.getInteger(sparqlEndPointTimeOut)>0)	exec.addParam("timeout",sparqlEndPointTimeOut);
	//
	//		ResultSet res=null;
	//		try{
	//			res= exec.execSelect();
	//			//defaultModel.add(answer) ;
	//		} catch (QueryExceptionHTTP e){
	//			logger.info("problems quering"+  endpoint_service+ " by SELECT: " + e.getMessage());
	//		}
	//		return  res;
	//
	//
	//	}

	public static void main(String[] args)  {

		Logger LOGGER =
			Logger.getLogger(SparqlEndPointTypeDumps.class.getName());

		// commands 

		// create Options object
		Options options = new Options();

		// Options
		options.addOption("listOflinkset", true , " a JSON file serializing a DataSetLinksetState Object ");


		CommandLineParser parser = new PosixParser();

		try {
			cmd = parser.parse( options, args);
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "DumpManager", options );
				System.exit(0);
			}

			// Create Logger
			LOGGER = Logger.getLogger("");
			LOGGER.setLevel(Level.ALL);
			Date date= new Date();
			SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			String cDate1= dateFormatter1.format(date);
			FileHandler fileTxt = new FileHandler("DumpManagerLOG"+cDate1+".txt");
			SimpleFormatter formatterTxt = new SimpleFormatter();
			fileTxt.setFormatter(formatterTxt);
			LOGGER.addHandler(fileTxt);


			//??
			SparqlEndPointTypeDumps septd= new SparqlEndPointTypeDumps("typeDumps",LOGGER );


			DataSetLinksetState ts=new DataSetLinksetState();
			// load the list of datasets/linkset
			ts=ts.loadStatefromJSON(cmd.getOptionValue("listOflinkset"));

			// if dataset urispace are not consolidated we consolidate it by default
			for ( DatasetInfo ds : ts.mapOfDataset.values()) {
				if (ds.consolidatedUriSpace==null||ds.consolidatedUriSpace.isEmpty()) {ds.consolidatedUriSpace=ds.UriSpace;}

			}

			//ts.saveStateInJSON("DatasetTypeExtractedResultForcedConsolidation");

			//
			//				ArrayList <String> objectsName= new ArrayList <String> ();
			//				objectsName.add("rkb-explorer-acmcr2012-06-21");
			//				objectsName.add("rkb-explorer-ieeecr2012-06-21");
			//
			//				//ts.saveStateFilteringDatasetAccordingToLinksetClosure(objectsName, "filteredbrkb-explorer-acmIEEEcr2012-06-21");
			//				ts.saveStateOnlyForMentionedDataset(objectsName, "filteredbrkb-explorer-acmIEEEcr2012-06-21");
			//
			//				try {
			//					ts=ts.loadStatefromJSON("filteredbrkb-explorer-acmIEEEcr2012-06-21");
			//				} catch (IOException e) {
			//					// TODO Auto-generated catch block
			//					e.printStackTrace();
			//				}


			// we should keep somewheer the list of dataset and the position of its dump
		

			septd.workOutTypes(ts);
			//ts.saveStateInJSON("DatasetTypeExtractedfromDumpAfterForcedConsolidation");

			ts.saveStateInJSON("FinalTypeIndicator");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

