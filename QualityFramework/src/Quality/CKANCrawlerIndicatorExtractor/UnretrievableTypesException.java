package Quality.CKANCrawlerIndicatorExtractor;

public class UnretrievableTypesException extends Exception {
 private String problem;
 public UnretrievableTypesException( String s) { problem=s;}
 public String toString(){
     return problem;
 }
 
}
