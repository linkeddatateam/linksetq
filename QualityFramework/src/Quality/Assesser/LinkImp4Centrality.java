/**
 * 
 */
package Quality.Assesser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;


import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;






















import java.util.Locale;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

import Quality.DatasetInfo;
import Quality.LinksetInfo;
import Quality.SparqlEndPointErrorMessage;
import Quality.Assesser.LinksetImpactingStructure.ExecutionStructures;
import Quality.Assesser.LinksetImpactingStructure.RDFEdge;
import Quality.Assesser.LinksetImpactingStructure.UtilityFunctionsDegreeAssessment;
import Quality.Utilities.Debugging;
import Quality.Utilities.RDFAccess;


import Benchmarking.CentralityMergeDataset;
import Jama.*; 

import org.apache.logging.log4j.Logger; 
import org.apache.logging.log4j.LogManager;

/**
 * @author Paola
 * 
 * 
 * The class execute the degree assessment of a 
 * input Linkset
 * It counts (one time) all the concepts (discarding literal values) reachable 
 * from the concepts belonging to the linkset l in k hops.   
 * 
 * 
 * 
 *ATTENTION: we suppose to consider a number k of hops at least (GREAT or EQUAL) >= 1
 *we do not consider K=0
 *Because we start to calculate the number of hops from the nodes 
 *object of the linkset, belonging to the object dataset. 
 *And the hop K=1 consider the nodes in object dataset reachable from 
 *those belonging to the linkset
 * 
 * 
 *
 */
public class LinkImp4Centrality {


	private static Logger LOGGER= LogManager.getLogger(Quality.Assesser.LinkImp4Centrality.class.getName()); 
	//information in input
	//probabily we will read from 
	//the command line...see the class 
	//http://commons.apache.org/proper/commons-cli/javadocs/api-release/index.html

	//info linkset
	static LinksetInfo li=null;
	//info dataset subject
	static DatasetInfo xd=null;
	//info dataset object
	static DatasetInfo yd=null;
	//linkset in input
	static ArrayList<String> objoflinkset=null;
	//static ArrayList<String> subjoflinkset=null;

	static int[] CWPA4Hop=null;

	//we suppose to consider only one link 
	//linkset.add("http://linkeddata.ge.imati.cnr.it:2020/resource/EARTh/100000");
	static int linksetcardinality=-1;

	//array of weight
	static HashMap<String, Double> w= null;
	//number of hops
	static int k=1;
	//execution structures	
	static ExecutionStructures es=null;
	//dataset to consider in the centrality
	static DatasetInfo d=null;
	static int info=0;
	static   HashSet<String> visitedn=new HashSet<String>();
	static  HashMap<String,Integer> hopnode=new HashMap<String,Integer>(); 

	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//	/*START OLD INITIALIZATION
		//initialization of info to be passed in input
		//linkset.add("http://linkeddata.ge.imati.cnr.it/resource/EARTh/10010");
		//linkset.add("http://linkeddata.ge.imati.cnr.it:2020/resource/EARTh/11850");
		//information about sparql end point
		//yd.sparqlEndPoint="http://sirio.private.imati.net:8890/sparql";
		//yd.consolidatedUriSpace="http://linkeddata.ge.imati.cnr.it:2020/resource/EARTh/";
		//w.put("http://www.w3.org/2004/02/skos/core#broader", 1.0);
		//w.put("http://www.w3.org/2004/02/skos/core#narrower", 1.0);
		//complete path of the result file 
		//String resultfile=new String("");
		//resultset offset 
		//	END OLD INITIALIZATION*/
		//all the structure necessary at linkset level
		//ExecutionStructures contains all the structure 
		//necessary for the execution at linkeset level 			
	}




	public LinkImp4Centrality( LinksetInfo li1, DatasetInfo xd1, DatasetInfo yd1, HashMap<String, Double> w1, int k1, String flag)
	{

		//TODO depending on sparql end point setting this query does't work when we work with big datasets, it should be rewritten with offset and limits 
		// get the List of links, 

		li= new LinksetInfo();
		xd=new DatasetInfo();
		yd=new DatasetInfo();
		objoflinkset=new ArrayList<String>();
		//subjoflinkset=new ArrayList<String>();
		//we suppose to consider only one link 
		//linkset.add("http://linkeddata.ge.imati.cnr.it:2020/resource/EARTh/100000");

		//array of weight
		w= new HashMap<String, Double>();

		//execution structures	
		es=new ExecutionStructures();		

		String uri;

		//we assign the static fields
		li= li1;
		xd=xd1;
		yd=yd1;
		w= w1;		
		k=k1;

		if(flag.equalsIgnoreCase("o"))
			d=yd;
		else {
			if(flag.equalsIgnoreCase("s"))
				d=xd;
		}
		analyseLinkset();
	}


	private static void analyseLinkset()
	{
		int limit=1000,offset=0;
		ResultSetRewindable resP;
		ArrayList<ResultSetRewindable> listresP=new ArrayList<ResultSetRewindable>();
		//list of query result 
		QuerySolution qsol;
		ArrayList<String> lsy=new ArrayList<String>(), lsx=new ArrayList<String>();
		int totallink=0;
		String uri="";
		//calculating linkset to analyse
		//we compute the linkset considering the link like:  s li.voidlinksetPredicate o
		//so we save all the o belonging to a particular urispace 
		//in a set so we do not consider duplicate link 
		//this is the linkset we want to analyse considering the centrality 
		/*
		String objoflink="select distinct ?x ?y where { ?x <"+li.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}" +
				"order by ?x ?y ";
		 */	
		String objoflink="select distinct ?y where { ?x <"+li.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}" +
				"order by ?y ";
		String linksetcardin= "select * where { ?x <"+li.voidlinksetPredicate+"> ?y. " +
				"FILTER ( regex(str(?x),\"^"+xd.consolidatedUriSpace+"\",\"i\") )\n "+
				"FILTER ( regex(str(?y), \"^"+yd.consolidatedUriSpace+"\",\"i\") )}";
		//LOGGER.info("query object "+ objoflink);
		//LOGGER.info("query link card "+ linksetcardin);
		// exec the query to get the link in terms of interlinked entities 
		SparqlEndPointErrorMessage  err;


		String objoflink1="";

		do{ 
			objoflink1=objoflink+" LIMIT "+limit +"\n"+
					" OFFSET  "+offset;

			err=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjrestr \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( RDFAccess.execQuery(li, objoflink1, err, null, true));
			resP.reset();
			listresP.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);


		for(ResultSet resP2:listresP)
		{
			while(resP2.hasNext()) 
			{
				qsol=resP2.next();

				if (qsol.get("?y").isURIResource())
				{
					uri=qsol.getResource("?y").getURI().trim();
					lsy.add(uri);							
				}
				/*
				if (qsol.get("?x").isURIResource())
				{
					uri=qsol.getResource("?x").getURI().trim();
					lsx.add(uri);							
				}*/				

			}
		}

		String linksetcardin1="";

		limit=1000;
		offset=0;
		listresP.clear();
		do{ 
			linksetcardin1=linksetcardin+" LIMIT "+limit +"\n"+
					" OFFSET  "+offset;

			err=new SparqlEndPointErrorMessage();
			//LOGGER.info(" query qsubjrestr \n" +qsubjrestr1);
			resP=  ResultSetFactory.makeRewindable( RDFAccess.execQuery(li, linksetcardin1, err, null, true));
			resP.reset();
			listresP.add(resP);
			offset=offset+limit;
		}while (resP.size()>0);


		for(ResultSetRewindable resP2:listresP)
		{
			totallink+=resP2.size();
		}
		linksetcardinality=totallink;
		// List of object's uri 
		objoflinkset=lsy;
		//subjoflinkset=lsx;
	}




	private static boolean isInSkosPropertySet(String p) {

		return ( p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#narrower") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#related") || p.equalsIgnoreCase("http://www.w3.org/2004/02/skos/core#broader"));
	}


	public static HashMap<String, Integer> analyseDatasets(String namefile) throws IOException {

		int offset=0;
		int limit= 500;
		int totalconcept=0;
		Query query; 
		HashMap<String, Integer> reachresults=new HashMap<String, Integer>();
		String [] CSVHeader= new String[11];

		ResultSetRewindable resP=null;
		reachresults=DatasetAnalysis.analyseDatasetsConcepts(d, xd, yd.consolidatedUriSpace, 
				xd.consolidatedUriSpace, w, objoflinkset, namefile);
		reachresults.put("linksetcardinality", Integer.valueOf(linksetcardinality));
		reachresults.put("linksetobjectscardinality", Integer.valueOf(objoflinkset.size()));
		return reachresults;
	}



	public static int analyseDataset()
	{
		int offset=0;
		int limit= 1000;
		int totalconcept=0;
		Query query; 

		ResultSetRewindable resP=null;
		String qs="prefix skos: <http://www.w3.org/2004/02/skos/core#> select distinct ?x where { ?x a skos:Concept} limit "+ limit+" offset "+ offset;
		///execute the query

		SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
		query = QueryFactory.create(qs);
		//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
		resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(yd, qs, err2, null));

		totalconcept+=resP.size();
		offset=offset+limit;

		while (resP.size()>0)
		{
			qs="PREFIX skos: <http://www.w3.org/2004/02/skos/core#> select distinct ?x where { ?x a skos:Concept} limit "+ limit+" offset "+ offset;
			//qs="select count (distinct (?x) as ?countall) where { ?x a skos:Concept} limit "+ limit+" offset "+ offset;
			//LOGGER.debug("query : "+qs);
			err2=new SparqlEndPointErrorMessage();

			resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(yd, qs, err2,null));



			totalconcept+=resP.size();
			offset=offset+limit;
		}

		return totalconcept;
	}


	public static double executeCrossWalkingPotentialityAssessment(String flag)
	{
		int limit=500;

		double CWPA=0.0;

		//all the structure necessary at linkset level
		//ExecutionStructures contains all the structure 
		//necessary for the execution at linkeset level 	
		settingWeightkosProperty(es,w);

		LOGGER.debug("Building the multirelational matrix for Linkset object");

		analyseLinkset2(limit,flag);

		LOGGER.debug("Working out centrality ");

		//CWPA=computeLinksetMisures(k+1,linkset,es);
		CWPA=computeLinksetMisures2();
		//writeLinksetMisures(k,linkset,es, resultfile);
		return CWPA;

	}


	public static double executeCrossWalkingPotentialityAssessmentsparql(String flag)
	{
		int limit=500;

		double CWPA=0.0;

		//all the structure necessary at linkset level
		//ExecutionStructures contains all the structure 
		//necessary for the execution at linkeset level 	
		settingWeightkosProperty(es,w);

		LOGGER.debug("Building the multirelational matrix for Linkset object");

		analyseLinkset2(limit,flag);

		LOGGER.debug("Working out centrality ");

		//CWPA=computeLinksetMisures(k+1,linkset,es);
		CWPA=computeLinksetMisures2();
		//writeLinksetMisures(k,linkset,es, resultfile);
		return CWPA;

	}



	public static HashSet<String> executeCrossWalkingPotentialityAssessment2(String flag)
	{
		int limit=500;

		HashSet<String> CWPA;

		//all the structure necessary at linkset level
		//ExecutionStructures contains all the structure 
		//necessary for the execution at linkeset level 	




		settingWeightkosProperty(es,w);

		LOGGER.debug("Building the multirelational matrix for Linkset object");


		analyseLinkset2(limit,flag);

		LOGGER.debug("Working out centrality ");

		//CWPA=computeLinksetMisures(k+1,linkset,es);
		CWPA=computeLinksetMisures3();
		//writeLinksetMisures(k,linkset,es, resultfile);
		return CWPA;

	}


	public static 	HashSet<String> executeCrossWalkingPotentialityAssessmentRecoursive(String f, 
			String pathcwpa)
			{
		//settingWeightkosProperty(es,w);
		CWPA4Hop=new int[k+2];
		for(int i=1; i<=k+1; i++)
		{
			CWPA4Hop[i]=0;
		}


		analyseLinksetRecoursive(f);
		String outFilename=pathcwpa.substring(pathcwpa.lastIndexOf("/")+1);
		Locale locale  = new Locale("en", "UK");
		String pattern = "############.#######";
		DecimalFormat myFormatter = (DecimalFormat)
				NumberFormat.getNumberInstance(locale);
		myFormatter.applyPattern(pattern);
		String[] CSVHeader =new String[k+2];
		CSVHeader[0]="Hop";

		for(int i=1; i<=k+1; i++)
		{
			CSVHeader[i]=String.valueOf(i);
		}
		try{
			File fcwpa=new File(pathcwpa+"_CWPA4Hop.csv");
			fcwpa.getParentFile().mkdirs();
			CSVWriter writer = new CSVWriter(new FileWriter(fcwpa), '\t');		
			writer.writeNext(CSVHeader);
			int total=0;
			CSVHeader[0]=outFilename;
			
			for(int i=1; i<=k+1; i++)
			{
				total=total+(int)CWPA4Hop[i];
				CSVHeader[i]=myFormatter.format((int)total);

			}
			writer.writeNext(CSVHeader);
			writer.flush();
			writer.close();
			
		}catch ( Exception e) {
			e.printStackTrace();
		} 

		LOGGER.debug("Total elements in the list hopnode: "+hopnode.size());
		//LOGGER.debug("Elements in the list hopnode: "+hopnode.toString());
		LOGGER.debug("Total elements in the list visitedn: "+visitedn.size());
		LOGGER.debug("Total object elements in the linkset: "+objoflinkset.size());
		visitedn.removeAll(objoflinkset);
		LOGGER.debug("Total elements in the list without object linkset: "+visitedn.size());

		return visitedn;
			}





	private static void analyseLinksetRecoursive(String fl)
	{
		for (String l: objoflinkset)
		{
			//LOGGER.debug("Reachability for : "+l);
			visitAndAssessProgressiveReachability( l,0,k+1, fl);
		}
	}		







	//new version in which we calculate at the same time the reachability from 0 to k
	private static void visitAndAssessProgressiveReachability(String l, int hop, int k, String flag)
	{

		//LOGGER.debug("Reachability for" + l ); 
		int offset= 0, limit=1000;
		ResultSetRewindable resP;
		String p;
		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		String qs, qs1;

		try{
			if(hop<=k )
			{
				if(hopnode.get(l.toLowerCase().trim())==null || hop<=hopnode.get(l.toLowerCase().trim())) 
				{

					if (hopnode.get(l.toLowerCase().trim())!=null)
					{
						if(hop<=hopnode.get(l.toLowerCase().trim()))
						{
							CWPA4Hop[hopnode.get(l.toLowerCase().trim())]--;
						}
					}
					CWPA4Hop[hop]++;
					visitedn.add(l.toLowerCase().trim());
					hopnode.put(l.toLowerCase().trim(), hop);

					if(flag.equalsIgnoreCase("s"))
					{
						qs="SELECT distinct ?y2 "
								+"\n where {  "
								+"{?y2  ?p <"+	l +"> }. "
								/* ADDBYRIC, we don't need literals */ 
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") ).";	
					}
					if (flag.equalsIgnoreCase("o"))
					{
						qs="SELECT distinct ?y2"
								+"\n where {  "
								+"{ <"+	l +"> ?p ?y2 }."
								/* ADDBYRIC, we don't need literals */
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") ).";
					}else{
						qs="";
						new Exception("Dataset to calculate centrality not specified ");
					}
					//LOGGER.info("query : "+qs);

					boolean primo=true;

					for (String rel: w.keySet())
					{
						if(primo)
						{
							qs+="filter ( ?p IN (<"+rel+"> ";
							primo=false;

						}
						else{
							qs+=", <"+rel+">";
						}
					}
					qs+="))}";
					qs1="";
					LOGGER.info("query new : "+qs);
					do{ 


						qs1=qs	+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";

						SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
						resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(d, qs1, err2, null));
						resP.reset();
						listresP.add(resP);
						offset=offset+limit;
					}while (resP.size()>0);

					//Let's identify the adjacent nodes
					for(ResultSet resP2:listresP)
					{
						QuerySolution qsol;
						String y2uri;

						while(resP2.hasNext()) {
							qsol=resP2.next();
							//get object and subject
							y2uri=qsol.getResource("?y2").getURI().trim();
							visitAndAssessProgressiveReachability(y2uri, hop+1, k,flag);
						}
					}
				}
			}

		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//sb.append(l);
			//sb.append("\n");
			LOGGER.info("NULL POINTER EXCEPTION l : "+l);
			LOGGER.info("NULL POINTER EXCEPTION hop : "+hop);
			LOGGER.info("NULL POINTER EXCEPTION e msm : "+ e.getMessage());
		}
	}













	//new version in which the control of ?p and ?y2 are made via sparql query
	private static void visitAndAssessReachability(String l, int hop, int k, String flag)
	{

		//LOGGER.debug("Reachability for" + l ); 
		int offset= 0, limit=1000;
		ResultSetRewindable resP;
		String p;
		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		String qs, qs1;
		try{
			if(hop<=k )
			{
				if(hopnode.get(l)==null || hop<=hopnode.get(l)) 
				{

					visitedn.add(l.toLowerCase().trim());
					hopnode.put(l.toLowerCase().trim(), hop);

					if(flag.equalsIgnoreCase("s"))
					{
						qs="SELECT distinct ?y2 "
								+"\n where {  "
								+"{?y2  ?p <"+	l +"> }. "
								/* ADDBYRIC, we don't need literals */ 
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") ).";	


					}
					if (flag.equalsIgnoreCase("o"))
					{
						qs="SELECT distinct ?y2"
								+"\n where {  "
								+"{ <"+	l +"> ?p ?y2 }."
								/* ADDBYRIC, we don't need literals */
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") ).";
					}else{
						qs="";
						new Exception("Dataset to calculate centrality not specified ");
					}
					//LOGGER.info("query : "+qs);

					boolean primo=true;

					for (String rel: w.keySet())
					{
						if(primo)
						{
							qs+="filter ( ?p IN (<"+rel+"> ";
							primo=false;

						}
						else{
							qs+=", <"+rel+">";
						}
					}
					qs+="))}";
					qs1="";
					//LOGGER.info("query new : "+qs);
					do{ 


						qs1=qs	+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";

						SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
						resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQueryObj(d, qs1, err2, null));
						resP.reset();
						listresP.add(resP);
						offset=offset+limit;
					}while (resP.size()>0);

					//Let's identify the adjacent nodes
					for(ResultSet resP2:listresP)
					{
						QuerySolution qsol;
						String y2uri;

						while(resP2.hasNext()) {
							qsol=resP2.next();
							//get object and subject
							y2uri=qsol.getResource("?y2").getURI().trim();
							visitAndAssessReachability(y2uri, hop+1, k,flag);
						}
					}
				}
			}

		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//sb.append(l);
			//sb.append("\n");
			LOGGER.info("NULL POINTER EXCEPTION l : "+l);
			LOGGER.info("NULL POINTER EXCEPTION hop : "+hop);
			LOGGER.info("NULL POINTER EXCEPTION e msm : "+ e.getMessage());
		}
	}


	//old version in which the control of ?p and ?y2 are made via code 
	private static void visitAndAssessReachability_old(String l, int hop, int k, String flag)
	{

		//LOGGER.debug("Reachability for" + l ); 
		int offset= 0, limit=1000;
		ResultSetRewindable resP;
		String p;
		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		String qs, qs1;
		try{
			if(hop<=k )
			{
				if(hopnode.get(l)==null || hop<=hopnode.get(l)) 
				{

					if (!visitedn.contains(l))
					{
						visitedn.add(l);
						//LOGGER.info("add in node : "+l);

					}
					else {

						//LOGGER.info("already existing in node : "+l);
					}


					hopnode.put(l, hop);
					if(flag.equalsIgnoreCase("s"))
					{
						qs="SELECT distinct ?y2 ?p "
								+"\n where {  "
								+"{?y2  ?p <"+	l +"> }. "
								/* ADDBYRIC, we don't need literals */ 
								+" FILTER isURI(?y2) "
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") )"	
								+"}  ";

					}
					if (flag.equalsIgnoreCase("o"))
					{
						qs="SELECT distinct ?p ?y2"
								+"\n where {  "
								+"{ <"+	l +"> ?p ?y2 }."
								/* ADDBYRIC, we don't need literals */
								+" FILTER isURI(?y2) "
								+ "FILTER ( regex(str(?y2),\"^^"+d.consolidatedUriSpace+"\",\"i\") )"								
								+"}  ";
					}else{
						qs="";
						new Exception("Dataset to calculate centrality not specified ");
					}
					LOGGER.info("query : "+qs);
					do{ 


						qs1=qs	+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";

						SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
						resP = ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(d, qs1, err2, null, false));
						resP.reset();
						listresP.add(resP);
						offset=offset+limit;
					}while (resP.size()>0);

					//Let's identify the adjacent nodes
					for(ResultSet resP2:listresP)
					{
						QuerySolution qsol;
						String y2uri;

						while(resP2.hasNext()) {
							qsol=resP2.next();

							//get object and subject
							p=qsol.getResource("?p").getURI();
							//System.out.println("property ?p in qsol: "+ p);

							// check if is it in the skos relation set
							if (isInSkosPropertySet(p) ){
								//control if the URI of the triple matches the URI space 
								//of the considered thesaurus
								y2uri=qsol.getResource("?y2").getURI().trim();
								if(y2uri.toLowerCase().matches( ((d.consolidatedUriSpace.toLowerCase().trim())+".*")))
								{

									visitAndAssessReachability_old(y2uri, hop+1, k,flag);
								}
							}
						}
					}
				}
			}

		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//sb.append(l);
			//sb.append("\n");
			LOGGER.info("NULL POINTER EXCEPTION l : "+l);
			LOGGER.info("NULL POINTER EXCEPTION hop : "+hop);
			LOGGER.info("NULL POINTER EXCEPTION e msm : "+ e.getMessage());
		}




	}



	private static void settingWeightkosProperty(ExecutionStructures es, HashMap<String, Double> w)
	{ //setting the skos:propertyes we must discard 
		//in the resultset of the query 


		es.setWeight(w);


	}






	//costruisce le strutture per il calcolo della centralità
	//sulla base dei datasetinfo presi in input e al linkset
	private static void analyseLinkset2(int limit, String flag)
	{
		//insieme di uri subject e object 
		//	LinkedHashSet <String> URISet= new   LinkedHashSet <String>();
		//	String y1= new String();
		//	Resource y1r=null;
		//	String y2= new String();
		//	Resource  y2r=null;
		//	String p=new String();

		//definisco oggetto edge
		//	RDFEdge edge=null;
		//definisco la hasmap che contiene le 
		//info della matrice multidimensionale

		//AWMInfo= new HashMap<String, List<RDFEdge>>();		


		//query to execute
		//	Query query;
		//	QuerySolution qs;
		//list of query result 
		//	ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();

		//loading all the results using OFFSET and LIMIT in order 
		//to divide the result in several pages


		//for each link in the linkset 
		//a query is created, executed and the rsult is saved 
		//LOGGER.info("Building the multirelational matrix for Linkset object");
		for (String l: objoflinkset)
		{
			//compute the subgraph reachable in k hops from ll
			//fill the structure RNSG and Listedge 
			//with the subgraph information 
			LOGGER.debug("I am analysing the object attached to : "+l);

			//analyseSingleLink(k, l,es, xd, yd, li,  limit);
			analyseSingleLink2( l,limit, flag);

			// /* code to control output
			//System.out.println("RNSH: \n");
			//es.toStringRNSh();
			//System.out.println("\n");
			//*/
			//System.out.println("AWM: \n");
			//es.toStringAWM();
			//System.out.println("URIMatrixIndex: \n");
			//es.toStringURIMatrixIndex();
		}

		// /* code to control output
		//System.out.println("RNSH: \n");
		//es.toStringRNSh();
		//System.out.println("\n");
		//System.out.println("AWM: \n");
		//es.toStringAWM();
		//System.out.println("URIMatrixIndex: \n");
		//es.toStringURIMatrixIndex();


	}	

	private static HashSet<String> computeLinksetMisures3()
	{//TODO computation of our centrality measure considering the whole linkset and possible 
		//other statistics concerning the whole linkset 

		//global set of all the nodes visited considering all links 
		//in the linkset
		HashSet<String> GVN= new HashSet<String>(); 
		//Matrix power;
		//LOGGER.debug("power matrix for k "+k);
		//degree value 
		double d=0.0;

		ArrayList<String> lsaux=es.getVisitedURI();
		lsaux.removeAll(objoflinkset);

		if(!lsaux.isEmpty())
		{

			es.initilizeMatrix();
			es.flatMatrixFunction("MAX");
			es.initializePowerMatrix(k+1, es.getFAWM()); 
			es.initializeVNM(k+1,es.getPowerM());


			//power=UtilityFunctionsDegreeAssessment.powerMatrix(es.getFAWM(),k);

			for (String l: objoflinkset)
			{
				//assess our centrality measure 
				//for the link ll
				LOGGER.debug("assessSingleLinkDegreeCentrality2(k, es, l) : k "+ (k+1) +" l  "+l );
				GVN.addAll(assessSingleLinkDegreeCentrality2( l));
				//write the result of degree centrality 
				//computation in a file
				//commento perch� forse non ha senso questo metodo 
				//a livello di singolo link
				//writeSingleLinkResult();

			}
			//System.out.println("\n prima global linkset : \n"+GVN.toString());
			GVN=removeLinksetsFromGlobalNodeSet(objoflinkset, GVN);
			//LOGGER.info("global linkset degree value: "+GVN.size());
			//LOGGER.info("\ndopo global linkset : \n"+GVN.toString());


		}
		return GVN;

	}

	private static double computeLinksetMisures2()
	{//TODO computation of our centrality measure considering the whole linkset and possible 
		//other statistics concerning the whole linkset 

		//global set of all the nodes visited considering all links 
		//in the linkset
		HashSet<String> GVN= new HashSet<String>(); 
		//Matrix power;
		//LOGGER.debug("power matrix for k "+k);
		//degree value 
		double d=0.0;

		ArrayList<String> lsaux=es.getVisitedURI();
		lsaux.removeAll(objoflinkset);

		if(!lsaux.isEmpty())
		{

			es.initilizeMatrix();
			es.flatMatrixFunction("MAX");
			es.initializePowerMatrix(k+1, es.getFAWM()); 
			es.initializeVNM(k+1,es.getPowerM());


			//power=UtilityFunctionsDegreeAssessment.powerMatrix(es.getFAWM(),k);

			for (String l: objoflinkset)
			{
				//assess our centrality measure 
				//for the link ll
				LOGGER.debug("assessSingleLinkDegreeCentrality2(k, es, l) : k "+ (k+1) +" l  "+l );
				GVN.addAll(assessSingleLinkDegreeCentrality2( l));
				//write the result of degree centrality 
				//computation in a file
				//commento perch� forse non ha senso questo metodo 
				//a livello di singolo link
				//writeSingleLinkResult();

			}
			//System.out.println("\n prima global linkset : \n"+GVN.toString());
			GVN=removeLinksetsFromGlobalNodeSet(objoflinkset, GVN);
			//LOGGER.info("global linkset degree value: "+GVN.size());
			//LOGGER.info("\ndopo global linkset : \n"+GVN.toString());


		}
		return new Double(GVN.size());

	}






	//if some link of initial linkset is inside the set of nodes reached in k hops 
	//it is necessary to eliminate them before calculating the degree
	private static HashSet<String> removeLinksetsFromGlobalNodeSet(ArrayList<String> ls, HashSet<String> GVN)
	{
		HashSet<String> GVNaux=new HashSet<String>();

		for(String i: GVN)
		{
			if(!ls.contains(i))
			{
				System.out.print("\n link:"+i);
				GVNaux.add(i);
			}

		}

		return GVNaux;



	}










	private static void writeLinksetMisures(int k, ArrayList<String> ls, ExecutionStructures es, 
			String resultfile)
	{//TODO writing all the computed statistics in a result file 

	}



	//methods concerning the analysis of a single link 
	//belonging to the linkset

	//
	private static void analyseSingleLinkRestrictRel( String l,int limit, String flag)
	{
		//TODO this methods compute:  
		//- the multirelational matrix considering all the property reachable from link l in k hops

		//structure necessary to collect and analyse the results

		QuerySolution qsol;
		String p=" ";
		String y2uri=" ";

		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		ResultSetRewindable resP=null;

		//structure necessary at single link level 
		//URI considered during the exploration 
		//of the subgraph for each hop
		//we use to navigate the graph for k hops
		LinkedList<String> URIactual = new LinkedList<String>();



		//set of next uris to consider 

		HashSet<String> nu;
		String qs;
		es.insertURIinURIMatrixIndex(l);
		es.insertUriinRNSh(l, 0, "");		

		for (int hop=1; hop<=k+1; hop++)
		{
			nu= new HashSet();

			//if(es.getRNSh().contains(hop-1))
			//{
			//System.out.println("HOP NUMBER: "+ hop+"\n");
			if(es.getRNSh().size()>=hop)
			{
				for(RDFEdge e:es.getRNSh().get(hop-1))
				{
					if( (!es.getVisitedURI().contains(e.getObject())&& 
							!objoflinkset.contains(e.getObject().trim()))   
							|| e.getObject()==l	) 
						nu.add(e.getObject());
				}
				URIactual.addAll(nu);
				//URIactual.toString();
				//System.out.println("RNSH: ");
				//es.toStringRNSh();
				while(!URIactual.isEmpty())
				{
					l= URIactual.poll();
					es.insertURIinVisitedURI(l);
					//System.out.println("link considered: "+l +"\n");
					int offset=0;

					if(flag.equalsIgnoreCase("s"))
					{
						qs="SELECT ?c ?p "
								+"\n where {  "
								+"?c  ?p <"+	l +"> . "
								/* ADDBYRIC, we don't need literals */ 
								+" filter (isURI(?c))"
								+"}  "
								+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";
					}
					if (flag.equalsIgnoreCase("o"))
					{
						qs="SELECT ?p ?c"
								+"\n where {  "
								+" <"+	l +"> ?p ?c ."
								/* ADDBYRIC, we don't need literals */ 
								+" filter (isURI(?c))"
								+"}  "			
								+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";
					}else{
						qs="";
						new Exception("Dataset to calculate centrality not specified ");
					}

					//System.out.println("query \n"+qs);

					///execute the query
					SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
					//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
					resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(d, qs, err2, null,false));

					//ResultSetFormatter.out(resP);
					resP.reset();
					//in order to consider all the resP 
					//over the iteration considering the offset 
					//we save a list of results
					listresP.add(resP);
					offset=offset+limit;			


					//collecting all the portion of the final result (using offset and limit) 
					//from the dataset : the result is made by pair (?property ?object) 
					//?property is a skos property attached to link l 
					//?object is the value of the skos property: a literal or a uri
					//we consider now all the skos:prop attached to l 
					while (resP.size()>0)
					{
						//repeat the query untill the result it is empty 
						//in order to keep all the result
						//create the query considering offset and limit
						if(flag.equalsIgnoreCase("s"))
						{
							qs="SELECT ?c ?p "
									+"\n where {  "
									+"?c  ?p <"+	l +"> . "
									/* ADDBYRIC, we don't need literals */ 
									+" filter (isURI(?c))"
									+"}  "
									+"LIMIT "+ limit
									+" OFFSET "+ offset +"\n";
						}
						if (flag.equalsIgnoreCase("o"))
						{
							qs="SELECT ?p ?c"
									+"\n where {  "
									+" <"+	l +"> ?p ?c ."
									/* ADDBYRIC, we don't need literals */ 
									+" filter (isURI(?c))"
									+"}  "			
									+"LIMIT "+ limit
									+" OFFSET "+ offset +"\n";
						}else{
							qs="";
							new Exception("Dataset to calculate centrality not specified ");
						}

						//System.out.println(" query :\n"+qs);
						///execute the query

						err2=new SparqlEndPointErrorMessage();

						resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(d, qs, err2,null,false));

						//ResultSetFormatter.out(resP);
						resP.reset();
						//in order to consider all the resP 
						//over the iteration considering the offset 
						//we save a list of results
						listresP.add(resP);
						offset=offset+limit;
					}


					//now we have the entire result 
					//for the number "hop" of step 
					//we analyse the result in order 
					//to fill the structure necessary for the execution

					//procedure to load the result in the adjacence matrix
					//and the new links to follow in the next hop

					for(ResultSet resP2:listresP)
					{
						while(resP2.hasNext()) {
							qsol=resP2.next();
							//get object and subject
							p=qsol.getResource("?p").getURI();
							//System.out.println("property ?p in qsol: "+ p);


							if (qsol.get("?c").isURIResource() 
									//ADDBYRIC
									&& w.containsKey(p))
							{//control if the URI of the triple matches the URI space 
								//of the considered thesaurus
								y2uri=qsol.getResource("?c").getURI().trim();
								//System.out.println("property: "+ p);
								//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

								//control if the uri belongs to the correct urispace
								if ( y2uri.toLowerCase().matches( ((d.consolidatedUriSpace.toLowerCase().trim())+".*")) ) 
								{
									//System.out.println("property: "+ p);
									//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
									//System.out.println("uri EARTH  : "+ y2uri);
									//controls if the uri is already present in the 
									//accessed uri
									if(es.isURIinRNSh(y2uri,p))
									{
										//System.out.println("property: "+ p);
										//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

										//control the hop number this uri has been discovered
										//is greater than the actual hop
										if(hop<es.getMinHopOfURIinRNSh(y2uri,p))
										{
											es.insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
											es.insertUriinRNSh(y2uri, hop,p);
											//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
											//System.out.println("URI: "+ y2uri);

											es.insertURIinURIMatrixIndex(y2uri);
										}
									}
									else{
										//the uri has been not visited until now
										//System.out.println("property: "+ p);
										//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
										es.insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
										es.insertUriinRNSh(y2uri, hop,p);
										es.insertURIinURIMatrixIndex(y2uri);
										//System.out.println("URI contains: "+ URIactual.contains(y2uri));
										//System.out.println("URI: "+ y2uri);


									}
								}
							}
							else
								if(qsol.get("?c").isLiteral())
								{
									//insert LIteral in es
									//System.out.println("property  : "+ p);	
									//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
									//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
									//we insert the pair (l,language) for the property p ) 
									es.insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?c").getLanguage()),p);
								}
						}

					}
					//System.out.println("URIactual: "+ URIactual.toString());

				}
			}
		}



	}




	private static boolean  analyseUriactual(LinkedList<String> URIactual, String uri)
	{

		for(String s:URIactual)
		{
			if(s.equalsIgnoreCase(uri))
			{
				return true;
			}
		}
		return false;
	}



	//OLD method
	private static HashSet<String>  assessSingleLinkDegreeCentrality(int k,ExecutionStructures es, String l)
	{
		//TODO this methods compute:  
		//- the flat matrix 
		//-our centrality measure 
		Matrix power;

		HashSet<String> TVN= new HashSet<String>(); 

		es.initilizeMatrix();
		es.flatMatrixFunction("MAX");
		//es.toStringFAWM();
		LOGGER.debug("power matrix for k "+k);
		power=UtilityFunctionsDegreeAssessment.powerMatrix(es.getFAWM(),k);
		/*
		for(int j=0;j<power.getRowDimension();j++)
		{


			for(int i=0; i<power.getColumnDimension();i++)
			{

					//System.out.print( "    "+power.get(j, i));

			}
			//System.out.print("\n");

		}*/
		LOGGER.debug("degreeSetSingleLinkfunction");
		TVN=UtilityFunctionsDegreeAssessment.degreeSetSingleLinkfunction(es.getFAWM(),es.getURIMatrixIndex(),l,k-1);

		return TVN;



	}


	private static HashSet<String>  assessSingleLinkDegreeCentrality2( String l)
	{
		//TODO this methods compute:  
		//- the flat matrix 
		//-our centrality measure 


		HashSet<String> TVN= new HashSet<String>(); 

		LOGGER.debug("degreeSetSingleLinkfunction");
		TVN=UtilityFunctionsDegreeAssessment.degreeSetSingleLinkfunction2(es,l,k);

		return TVN;



	}




	private static void  writeSingleLinkResult(String f)
	{
		//TODO this methods: 
		//-write the result respect to a single link
		//NON SO SE HA SENSO A LIVELLO DI SINGOLO LINK


	}




	public static HashSet<String> executeCrossWalkingPotentialityAssessmentRestricRel(String flag)
	{
		int limit=500;

		HashSet<String> CWPA;

		//all the structure necessary at linkset level
		//ExecutionStructures contains all the structure 
		//necessary for the execution at linkeset level 	




		settingWeightkosProperty(es,w);

		LOGGER.debug("Building the multirelational matrix for Linkset object");


		analyseLinksetRestrictRel(limit,flag);

		LOGGER.debug("Working out centrality ");

		//CWPA=computeLinksetMisures(k+1,linkset,es);
		CWPA=computeLinksetMisures3();
		//writeLinksetMisures(k,linkset,es, resultfile);
		return CWPA;

	}




	//costruisce le strutture per il calcolo della centralità
	//sulla base dei datasetinfo presi in input e al linkset
	private static void analyseLinksetRestrictRel(int limit, String flag)
	{
		//insieme di uri subject e object 
		//	LinkedHashSet <String> URISet= new   LinkedHashSet <String>();
		//	String y1= new String();
		//	Resource y1r=null;
		//	String y2= new String();
		//	Resource  y2r=null;
		//	String p=new String();

		//definisco oggetto edge
		//	RDFEdge edge=null;
		//definisco la hasmap che contiene le 
		//info della matrice multidimensionale

		//AWMInfo= new HashMap<String, List<RDFEdge>>();		


		//query to execute
		//	Query query;
		//	QuerySolution qs;
		//list of query result 
		//	ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();

		//loading all the results using OFFSET and LIMIT in order 
		//to divide the result in several pages


		//for each link in the linkset 
		//a query is created, executed and the rsult is saved 
		//LOGGER.info("Building the multirelational matrix for Linkset object");
		for (String l: objoflinkset)
		{
			//compute the subgraph reachable in k hops from ll
			//fill the structure RNSG and Listedge 
			//with the subgraph information 
			LOGGER.debug("I am analysing the object attached to : "+l);

			//analyseSingleLink(k, l,es, xd, yd, li,  limit);
			analyseSingleLinkRestrictRel( l,limit, flag);

			// /* code to control output
			//System.out.println("RNSH: \n");
			//es.toStringRNSh();
			//System.out.println("\n");
			//*/
			//System.out.println("AWM: \n");
			//es.toStringAWM();
			//System.out.println("URIMatrixIndex: \n");
			//es.toStringURIMatrixIndex();
		}

		// /* code to control output
		//System.out.println("RNSH: \n");
		//es.toStringRNSh();
		//System.out.println("\n");
		//System.out.println("AWM: \n");
		//es.toStringAWM();
		//System.out.println("URIMatrixIndex: \n");
		//es.toStringURIMatrixIndex();


	}




	//methods concerning the analysis of a single link 
	//belonging to the linkset

	//
	private static void analyseSingleLink2( String l,int limit, String flag)
	{
		//TODO this methods compute:  
		//- the multirelational matrix considering all the property reachable from link l in k hops
		//structure necessary to collect and analyse the results

		QuerySolution qsol;
		String p=" ";
		String y2uri=" ";

		//list of query result 
		ArrayList<ResultSet> listresP=new ArrayList<ResultSet>();
		ResultSetRewindable resP=null;

		//structure necessary at single link level 
		//URI considered during the exploration 
		//of the subgraph for each hop
		//we use to navigate the graph for k hops
		LinkedList<String> URIactual = new LinkedList<String>();



		//set of next uris to consider 

		HashSet<String> nu;
		String qs;
		es.insertURIinURIMatrixIndex(l);
		es.insertUriinRNSh(l, 0, "");		

		for (int hop=1; hop<=k+1; hop++)
		{
			nu= new HashSet();

			//if(es.getRNSh().contains(hop-1))
			//{
			//System.out.println("HOP NUMBER: "+ hop+"\n");
			if(es.getRNSh().size()>=hop)
			{
				for(RDFEdge e:es.getRNSh().get(hop-1))
				{
					if( (!es.getVisitedURI().contains(e.getObject())&& !objoflinkset.contains(e.getObject().trim()))   
							|| e.getObject()==l	) 
						nu.add(e.getObject());
				}
				URIactual.addAll(nu);
				//URIactual.toString();
				//System.out.println("RNSH: ");
				//es.toStringRNSh();
				while(!URIactual.isEmpty())
				{
					l= URIactual.poll();
					es.insertURIinVisitedURI(l);
					//System.out.println("link considered: "+l +"\n");
					int offset=0;

					if(flag.equalsIgnoreCase("s"))
					{
						qs="SELECT ?c ?p "
								+"\n where {  "
								+"?c  ?p <"+	l +"> . "
								/* ADDBYRIC, we don't need literals */ 
								+" filter (isURI(?c))"
								+"}  "
								+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";
					}
					if (flag.equalsIgnoreCase("o"))
					{
						qs="SELECT ?p ?c"
								+"\n where {  "
								+" <"+	l +"> ?p ?c ."
								/* ADDBYRIC, we don't need literals */ 
								+" filter (isURI(?c))"
								+"}  "			
								+"LIMIT "+ limit
								+" OFFSET "+ offset +"\n";
					}else{
						qs="";
						new Exception("Dataset to calculate centrality not specified ");
					}

					//System.out.println("query \n"+qs);

					///execute the query
					SparqlEndPointErrorMessage  err2=new SparqlEndPointErrorMessage();
					//LOGGER.fine("quering "+ subjectdt.sparqlEndPoint +" by " +queryString);
					resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(d, qs, err2, null,false));

					//ResultSetFormatter.out(resP);
					resP.reset();
					//in order to consider all the resP 
					//over the iteration considering the offset 
					//we save a list of results
					listresP.add(resP);
					offset=offset+limit;			


					//collecting all the portion of the final result (using offset and limit) 
					//from the dataset : the result is made by pair (?property ?object) 
					//?property is a skos property attached to link l 
					//?object is the value of the skos property: a literal or a uri
					//we consider now all the skos:prop attached to l 
					while (resP.size()>0)
					{
						//repeat the query untill the result it is empty 
						//in order to keep all the result
						//create the query considering offset and limit
						if(flag.equalsIgnoreCase("s"))
						{
							qs="SELECT ?c ?p "
									+"\n where {  "
									+"?c  ?p <"+	l +"> . "
									/* ADDBYRIC, we don't need literals */ 
									+" filter (isURI(?c))"
									+"}  "
									+"LIMIT "+ limit
									+" OFFSET "+ offset +"\n";
						}
						if (flag.equalsIgnoreCase("o"))
						{
							qs="SELECT ?p ?c"
									+"\n where {  "
									+" <"+	l +"> ?p ?c ."
									/* ADDBYRIC, we don't need literals */ 
									+" filter (isURI(?c))"
									+"}  "			
									+"LIMIT "+ limit
									+" OFFSET "+ offset +"\n";
						}else{
							qs="";
							new Exception("Dataset to calculate centrality not specified ");
						}

						//System.out.println(" query :\n"+qs);
						///execute the query

						err2=new SparqlEndPointErrorMessage();

						resP=  ResultSetFactory.makeRewindable( Quality.Utilities.RDFAccess.execQuery(d, qs, err2,null,false));

						//ResultSetFormatter.out(resP);
						resP.reset();
						//in order to consider all the resP 
						//over the iteration considering the offset 
						//we save a list of results
						listresP.add(resP);
						offset=offset+limit;
					}


					//now we have the entire result 
					//for the number "hop" of step 
					//we analyse the result in order 
					//to fill the structure necessary for the execution

					//procedure to load the result in the adjacence matrix
					//and the new links to follow in the next hop

					for(ResultSet resP2:listresP)
					{
						while(resP2.hasNext()) {
							qsol=resP2.next();
							//get object and subject
							p=qsol.getResource("?p").getURI();
							//System.out.println("property ?p in qsol: "+ p);


							if (qsol.get("?c").isURIResource() 
									//ADDBYRIC
									&&w.containsKey(p))
							{//control if the URI of the triple matches the URI space 
								//of the considered thesaurus
								y2uri=qsol.getResource("?c").getURI().trim();
								//System.out.println("property: "+ p);
								//System.out.println(" Y2 : "+ y2uri.toString() +"\n");

								//control if the uri belongs to the correct urispace
								if ( y2uri.toLowerCase().matches( ((d.consolidatedUriSpace.toLowerCase().trim())+".*")) ) 
								{
									//System.out.println("property: "+ p);
									//System.out.println(" Y2 after: "+ y2uri.toString() +"\n");
									//System.out.println("uri EARTH  : "+ y2uri);
									//controls if the uri is already present in the 
									//accessed uri
									if(es.isURIinRNSh(y2uri,p))
									{
										//System.out.println("property: "+ p);
										//System.out.println("ALREADY visited Y2URI: "+ y2uri+"\n"); 

										//control the hop number this uri has been discovered
										//is greater than the actual hop
										if(hop<es.getMinHopOfURIinRNSh(y2uri,p))
										{
											es.insertRDFEdgeinAWM(new RDFEdge(l,y2uri),  p);
											es.insertUriinRNSh(y2uri, hop,p);
											//System.out.println("URI contains pippo: "+ URIactual.contains(y2uri));
											//System.out.println("URI: "+ y2uri);

											es.insertURIinURIMatrixIndex(y2uri);
										}
									}
									else{
										//the uri has been not visited until now
										//System.out.println("property: "+ p);
										//System.out.println("NOT YET visited Y2URI: "+ y2uri+"\n");
										es.insertRDFEdgeinAWM(new RDFEdge(l,y2uri), p);
										es.insertUriinRNSh(y2uri, hop,p);
										es.insertURIinURIMatrixIndex(y2uri);
										//System.out.println("URI contains: "+ URIactual.contains(y2uri));
										//System.out.println("URI: "+ y2uri);


									}
								}
							}
							else
								if(qsol.get("?c").isLiteral())
								{
									//insert LIteral in es
									//System.out.println("property  : "+ p);	
									//System.out.println("Y2   literal : "+ qsol.getLiteral("?y2").getLexicalForm());	
									//System.out.println("Y2   literal language: "+ qsol.getLiteral("?y2").getLanguage());
									//we insert the pair (l,language) for the property p ) 
									es.insertRDFEdgeinAWM(new RDFEdge(l, qsol.getLiteral("?c").getLanguage()),p);
								}
						}

					}
					//System.out.println("URIactual: "+ URIactual.toString());

				}
			}
		}



	}}
