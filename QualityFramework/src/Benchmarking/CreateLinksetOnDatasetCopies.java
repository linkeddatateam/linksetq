/**
 * @author riccardoalbertoni
 * it creates two copies of the dataset, and a linkset including all the entities in the two copies
 */
package Benchmarking;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.OutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;


public class CreateLinksetOnDatasetCopies {


/**
 *  it creates two copies of the dataset, and a linkset including all the entities in the two copies, the namespace in the copies will be replaced by http://xxx/ and http://yyy/ respectively
 * @param inp file path for dataset that have to be copied, the dataset  considered as Gold Standard in the benchmarking
 * @param out1 file path for first dataset copy to be created encoded as implicitly indicated in the file extension
 * @param out2 file path for second dataset copy to be created encoded as implicitly indicated in the file extension
 * @param l    file path for linkset to be created encoded as implicitly indicated in the file extension
 * @param authNS authoritative namespace in the Gold standard that must be changed in XXX and YYY
 * @param linksetProperty
 * 
 * options.addOption("in", true, "");
		options.addOption("out1",true, "file path for first dataset copy to be created encoded as implicitly indicated in the file extension" );
		options.addOption("out2",true, "file path for second dataset copy to be created encoded as implicitly indicated in the file extension" );
		options.addOption("l",true, "file path for linkset to be created encoded as implicitly indicated in the file extension" );
		options.addOption("authNS",true, "authoritative Name space that must be changed" );
		options.addOption("linksetProperty", true, "to force the generated linkset to use the indicate linkset property");
 */
	public static void run(String inp, String out1, String out2, String l, String authNS, String linksetProperty){

		// create an empty model
		Model model = ModelFactory.createDefaultModel();
		Model modelX = ModelFactory.createDefaultModel();
		Model modelY = ModelFactory.createDefaultModel();
		Model linkXY = ModelFactory.createDefaultModel();
		OutputStream outp1, outp2, outl;
		// use the FileManager to find the input file
		//InputStream in = FileManager.get().open( inp );
		try {
		
			//			if (in == null) {
			//				throw new IllegalArgumentException(
			//						"File: " + inp + " not found");
			//			}

			// read the RDF file
			model=org.apache.jena.riot.RDFDataMgr.loadModel(inp);


			// for each statement I check if Object or Subject of the kind that have to be linked, 
			// if they are I change the namespace 
			// add a triple to the linked linkset
			//otherwise a copy in both..
			StmtIterator stm = model.listStatements();
			while (stm.hasNext()){
				Statement s = stm.next();
				Resource subject = s.getSubject();
				Property predicate = s.getPredicate();
				RDFNode n = s.getObject();
				Resource object=null;
				Resource sbj1, sbj2, obj1, obj2;
				String ns=  subject.getNameSpace();
				if (ns.contains(authNS)){
					sbj1=modelX.createResource(subject.getURI().replace(authNS,"http://xxx/"));
					sbj2=modelY.createResource(subject.getURI().replace(authNS,"http://yyy/"));
				}else{
					sbj1=modelX.createResource(subject.getURI());
					sbj2=modelY.createResource(subject.getURI());
				}

				if (n.isResource())  { 
					object= n.asResource();
					ns=  object.getNameSpace();
					if (ns.contains(authNS)){
						obj1=modelX.createResource(object.getURI().replace(authNS,"http://xxx/"));
						obj2=modelY.createResource(object.getURI().replace(authNS,"http://yyy/"));
						// if linkset property is indicated then use it for generate the interlinking
						if (linksetProperty!=null){
							linkXY.add(obj1, linkXY.getProperty(linksetProperty), obj2);
						}else if (model.contains(object, model.getProperty("http://www.w3.org/2000/01/rdf-schema#type"), model.getResource("http://www.w3.org/2004/02/skos/core#Concept")) )
							linkXY.add(obj1, linkXY.getProperty("http://www.w3.org/2004/02/skos/core#exactMatch"), obj2);
						else 
							linkXY.add(obj1, linkXY.getProperty("http://www.w3.org/2002/07/owl#sameAs"), obj2);
					}else{
						obj1=modelX.createResource(object.getURI());
						obj2=modelY.createResource(object.getURI());
					}

					modelX.add(sbj1, predicate, obj1);
					modelY.add(sbj2, predicate, obj2);

				}else{
					modelX.add(sbj1, predicate, n );
					modelY.add(sbj2, predicate,n );
				}

				if (linksetProperty!=null){
					linkXY.add(sbj1, linkXY.getProperty(linksetProperty), sbj2);
				}else if (model.contains(subject, model.getProperty("http://www.w3.org/2000/01/rdf-schema#type"), model.getResource("http://www.w3.org/2004/02/skos/core#Concept")) )
						linkXY.add(sbj1, linkXY.getProperty("http://www.w3.org/2004/02/skos/core#exactMatch"), sbj2);
					else 
						linkXY.add(sbj1, linkXY.getProperty("http://www.w3.org/2002/07/owl#sameAs"), sbj2);


			}
//			Let's use the writeModelInTheFileNameSpecifiedEncoding instead of the instruction below
//			outp1 =new FileOutputStream(out1);
//			outp2 =new FileOutputStream(out2);
//			outl= new FileOutputStream(l);
//			modelX.write(outp1, "N3");
//			modelY.write(outp2, "N3");
//			linkXY.write(outl, "N3");

			Quality.Utilities.RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out1, modelX);
			Quality.Utilities.RDFAccess.writeModelInTheFileNameSpecifiedEncoding(out2, modelY);
			Quality.Utilities.RDFAccess.writeModelInTheFileNameSpecifiedEncoding(l, linkXY);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	/**
	 * 
	 * Run it to  see the option available
	 * 
	 */
	public static void main(String[] args) {
		
		// let's connect with the logger
		
		// create Options object
		Options options = new Options();
		options.addOption("in", true, "file path for dataset that we consider as Gold Standard");
		options.addOption("out1",true, "file path for first dataset copy to be created encoded as implictly indicated in the file extension" );
		options.addOption("out2",true, "file path for second dataset copy to be created encoded as implictly indicated in the file extension" );
		options.addOption("l",true, "file path for linkset to be created encoded as implictly indicated in the file extension" );
		options.addOption("authNS",true, "authoritative Name space that must be changed" );
		options.addOption("linksetProperty", true, "to force the generated linkset to use the indicate linkset property");

		//options.addOption("c4owlsameAs",false, "rdfs/owl classes to be  considered linked with owl:sameAs" );
		//options.addOption("c4skosexactMatch",false, "rdfs/owl classes to be considered in the linkset as linked with skos:exactMatch"); 


		//DataSetLinksetState.addOptionForFilteringDatasetOrLinkset(options);

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;

		try {
			cmd = parser.parse( options, args);
// connecting to the logger
			//Debugging.loggerConfiguration(logger, null,LinkImp4p.class);

			HelpFormatter formatter = new HelpFormatter();
			if (!cmd.iterator().hasNext()){ // automatically generate the help statement
				formatter.printHelp( "CreateLinksetOnDatasetCopies", options );
				System.exit(0);
			}
			//			other check on options that we could want 
			if (cmd.hasOption("in") &&!cmd.hasOption("out1") && !cmd.hasOption("out2")) {
				formatter.printHelp( "CreateLinksetOnDatasetCopies", options );
				//formatter.printHelp( " you should specify out1 or out2");
				System.exit(0);
			}

			CreateLinksetOnDatasetCopies.run( cmd.getOptionValue("in"), cmd.getOptionValue("out1"), cmd.getOptionValue("out2"), cmd.getOptionValue("l"),cmd.getOptionValue("authNS"), cmd.hasOption("linksetProperty") ? cmd.getOptionValue("linksetProperty"):null);
		}catch ( Exception e) {
			e.printStackTrace();
		} 
	}

}
