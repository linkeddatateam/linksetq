# genererating the summarized results 4 Impact on the testsets for GEMETBenchmarking

set -x #echo on


#NOT MORE USED, SINCE NOW WE OUTPUT GAIN DIRECTLY - let's merge the intestation and the whole file of results
#cat   test*/test*.*_complementedDataset.csv  >testX.csv_complementedDataset.csv 

cat  test*/test*.*_NormalizedGain.csv >testX_NormalizedGain.csv

#summarizing all the result about Quality
cd ../../../

java -classpath $CLASSPATH:QualityFramework.jar:./lib/* Benchmarking.SummarizeCSVResult -in Examples/Benchmarking/GEMET/testX_NormalizedGain.csv  -out  Examples/Benchmarking/GEMET/testX_NormalizedGain_ALL.csv

cd  Examples/Benchmarking/GEMET/

